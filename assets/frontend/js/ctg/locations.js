
Location = function(_containerSelector) {
    var _object = {
        _counter: 0,
        _map: null,
        _container: null,
        /*
         * Methods
         */
        getLocations: function() {
            var _that = this;
            var locs = _that._container.find(".node-location");
            var locarray = [];

            for (var i = 0; i < locs.length; i++)
            {
                var latlng = _that.getLoationLatLong(locs[i]);
                locarray[i] = [i + 1, locs[i].id, latlng[0], latlng[1], 0, latlng[2]];

                if (locs[i].classList.contains('location-highlight')) {
                    locarray[i][4] = 1;
                }
            }
            //console.log('locs ' + locarray);

            return locarray;
        },
        getLoationLatLong: function(location) {
             var type = $(location).find('.field-type-geofield').find('.field-item2')[0].innerHTML;
            var latlng = $(location).find('.field-type-geofield').find('.field-item')[0].innerHTML.split(",");
            var locarray = [latlng[0], latlng[1],type];

            return locarray;
        },
        initialize: function() {

            var _that = this;

            //alert('aaa');
            var mapOptions = {
                center: new google.maps.LatLng(33.941213, -84.213531),
                zoom: 9,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false
            };

            _that._map = new google.maps.Map(_that._container.find(".map-canvas")[0], mapOptions);

            //var markers = [];
            var locations = _that.getLocations();

            var markerLocation = null;

            for (var i = 0; i < locations.length; i++) {
                // create a new span element for the plot marker id
                var plotspan = document.createElement('span');
                var plotid = document.createTextNode(locations[i][0].toString());
                //console.log('locations =' + locations[i][1]);
                var htag = _that._container.find('#' + locations[i][1]).find('strong')[0];


                if (_that._container.find('#' + locations[i][1]).find('.loc-plot').length == 0) {
                    plotspan.appendChild(plotid);
                    plotspan.setAttribute('class', 'loc-plot badge badge-success');
                    htag.parentNode.insertBefore(plotspan, htag);
                }

                if(locations[i][5]=='Retail'){
                    color_code='003f22';
                }else if(locations[i][5]=='Cooler'){
                    color_code='E0272A';
                }

                // attach new marker
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][2], locations[i][3]),
                    map: _that._map,
                    icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + (i + 1) + '|'+color_code+'|ffffff'
                });

                if (!markerLocation) {
                    if (locations[i][2]) {
                        markerLocation = new google.maps.LatLng(locations[i][2], locations[i][3]);
                    }
                }

                // check if this location is active.
                if (locations[i][4]) {
                    var latlng = new google.maps.LatLng(locations[i][2], locations[i][3]);
                    _that._map.setZoom(14);
                    _that._map.setCenter(latlng);
                    markerLocation = latlng;
                }

                // set the onclick event handler to the node and plots
                _that.zoomPlot(marker, locations[i][1]);

            }

            if (markerLocation) {
                _that._map.setCenter(markerLocation);
            }



            // get the height of the window and set map
            _that.setMapSize();
        },
        zoomPlot: function(marker, locid) {
            var _that = this;

            _that._counter++;

            var loc = _that._container.find('#' + locid)[0];
            var ll = _that.getLoationLatLong(loc);
            var latlng = new google.maps.LatLng(ll[0], ll[1]);



            google.maps.event.addDomListener(loc, 'click', function() {
                //alert(document.URL+' ## zoomlot'+ii);
                var locs = _that.getLocations();
                for (var i = 0; i < locs.length; i++) {
                    if (locs[i][4]) {
                        var el = _that._container.find('#' + locs[i][1])[0];
                        el.classList.remove('location-highlight');
                    }
                }
                loc.classList.add('location-highlight');
                _that._map.setZoom(14);
                _that._map.setCenter(latlng);

                /*
                 * Animate Down the map
                 */
                //_that._container.find(".map-canvas").animate({'margin-top': $(loc).position().top}, 1000);
                //console.log($(loc).position().top);
            });

            google.maps.event.addListener(marker, 'click', function() {
                // get current active location
                var locs = _that.getLocations();
                for (var i = 0; i < locs.length; i++) {
                    if (locs[i][4]) {
                        var el = _that._container.find('#' + locs[i][1])[0];
                        el.classList.remove('location-highlight');
                    }
                }

                loc.classList.add('location-highlight');
                _that._map.setZoom(14);
                _that._map.setCenter(latlng);

            });

        },
        setMapSize: function() {
            var _that = this;
            var map = _that._container.find(".map-canvas")[0];
            var locblock = _that._container.find(".view-location")[0];
            var mapheight = locblock.clientHeight;
            //map.style.height = ((mapheight - 5) + 'px'); commnet by joe
        },
        markLocation: function() {
            var _that = this;
            var locations = _that.getLocations();
            for (var i = 0; i < locations.length; i++) {
                if (locations[i][2]) {
                    var latlng = new google.maps.LatLng(locations[i][2], locations[i][3]);
                    _that._map.setCenter(latlng);
                    return;
                }
            }
        },
        init: function() {
            var _that = this;

            _that._container = $(_containerSelector);



            if (_that._container.length > 0) {


                if (_that._container.find('#location-list').length > 0) {
                    _that.initialize();
                    google.maps.event.addDomListener(window, 'load', function() {
                        _that.initialize();
                    });
                    google.maps.event.addDomListener(window, 'resize', function() {
                        _that.setMapSize();
                    });



                    /*
                     * Resize Map on when tab activates.
                     */
                    $('a[href="' + _containerSelector + '"]').on('shown.bs.tab', function(e) {
                        _that._map.setZoom(9);
                        google.maps.event.trigger(_that._map, "resize");
                        _that.markLocation();
                        //_that._container.find(".map-canvas").animate({'margin-top': 0}, 1000);
                    });

                    /*
                     * Activate first location.
                     */
                }

            }
        }

    };
    return _object;
};

