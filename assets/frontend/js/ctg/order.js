
OrderDeliveryType = function() {
    return {
        selected: true,
        isPickup: true, //Default should be false, when we start asking user for location type.
        pickupDate: null,
        init: function() {
        },
        selectType: function() {
        },
        selectPickupDate: function(_enabledDates) {
        },
    };

};

OrderLocation = function() {
    return {
        selected: false,
        id: null,
        isCooler: false,
        init: function() {
        },
        selectLocation: function() {
        },
        getNextPickupAvailability: function() {
        },
    };
};

MenuItem = function() {
    return {
        id: null,
        title: null,
        size: null,
        cost: null,
        weight: null,
    };
};

Order = function() {
    var _object = {
        location: OrderLocation(),
        delivery: OrderDeliveryType(),
        items: [],        
        totalAmount: 0,
        totalItems: 0,
        canTakeOrder: function() { // Logic to make sure that delivery & location are selected.
            if (this.location.selected && this.delivery.selected) {
                return true;
            } else {
                return false;
            }
        },
        init: function() {
            this.delivery.init();
            this.location.init();
            this.initClickHandlers();
        },
        initClickHandlers: function() {
            var that = this;
            $('.menu-item').on('click', function(e) {
                e.preventDefault();
                that.processMenuItemClick($(this));
            });
        },
        askDeliverAndLocation: function() {
            if (!this.delivery.selected) {
                this.delivery.selectType();
            }

            if (!this.location.selected) {
                this.location.selectLocation();
            }
        },
        processMenuItemClick: function() {
            if (!this.canTakeOrder()) {
                this.askDeliverAndLocation();
            } else {
                /*
                 * Process Menu Item Click Here.
                 */
            }

        },
        checkoutOrder: function() {
            if (this.validateOrderAmount()) {
                /*
                 * Process Order Here.
                 */
            } else {
                /*
                 * Display notice here.
                 */
            }
        },
        validateOrderAmount: function() {
            if (this.totalAmount >= 500) {
                return false;
            } else {
                return true;
            }
        }
    };
    return _object;
};

//Usage

CustomerOrder = Order();
CustomerOrder.init();

//To initiate order Checkout. 
CustomerOrder.checkoutOrder(); //This can also be done using click handlers withing order object. 