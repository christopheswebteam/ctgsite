
/*
 * Routines to help with image paning to zoom in the center.
 */

ImagePan = function(_imageContainerSelector) {
    var _object = {
        imageContainerSelector: _imageContainerSelector,
        scaleImage: function(sourceWidth, sourceHeight, targetWidth, targetHeight, adjustInBox) {

            var result = {width: 0, height: 0, isSacaledToWidth: true};

            if ((sourceWidth <= 0) || (sourceHeight <= 0) || (targetWidth <= 0) || (targetHeight <= 0)) {
                return result;
            }

            // scale to the target width
            var scaleX1 = targetWidth;
            var scaleY1 = (sourceHeight * targetWidth) / sourceWidth;

            // scale to the target height
            var scaleX2 = (sourceWidth * targetHeight) / sourceHeight;
            var scaleY2 = targetHeight;

            // now figure out which one we should use
            if (scaleX2 > targetWidth) { // Scale on width
                result.isSacaledToWidth = adjustInBox;
            }
            else {
                result.isSacaledToWidth = !adjustInBox;
            }

            if (result.isSacaledToWidth) {
                result.width = Math.floor(scaleX1);
                result.height = Math.floor(scaleY1);

            }
            else {
                result.width = Math.floor(scaleX2);
                result.height = Math.floor(scaleY2);


            }
            result.targetLeft = Math.floor((targetWidth - result.width) / 2);
            result.targetTop = Math.floor((targetHeight - result.height) / 2);

            return result;
        },
        rememberOriginalSize: function(image) {

            if (!image.originalSize) {
                image.originalSize = {width: image.width, height: image.height};
            }

        },
        centerImage: function(adjustInBox, container, image) {

            this.rememberOriginalSize(image);

            var targetWidth = $(container).width();
            var targetHeight = $(container).height();
            var sourceWidth = image.originalSize.width;
            var sourceHeight = image.originalSize.height;

            var result = this.scaleImage(sourceWidth, sourceHeight, targetWidth, targetHeight, adjustInBox);

            $(image).css("width", result.width);
            $(image).css("height", result.height);
            $(image).css("left", result.targetLeft);
            $(image).css("top", result.targetTop);
        },
        stretchImage: function(container, image) {

            this.rememberOriginalSize(image);

            var targetWidth = $(container).width();
            var targetHeight = $(container).height();

            image.width = targetWidth;
            image.height = targetHeight;
            $(image).css("left", 0);
            $(image).css("top", 0);
        },
        fixImages: function(adjustInBox) {
            if (this.imageContainerSelector) {
                var that = this;
                $(this.imageContainerSelector).each(function(index, container) {
                    var image = $(container).find("img").get(0);
                    that.centerImage(adjustInBox, container, image);
                });
            }
        }
    };
    return _object;

};

globalImagePanUtility = new ImagePan(null);

function onImageLoad(e) {

    var image = e.currentTarget;
    if (image.complete) {
        globalImagePanUtility.centerImage(false, $(image).parent(), image);
    }

}