var EcommerceProductsEdit = function () {

    var handleImages = function() {

        // see http://www.plupload.com/
        
        var uploader = new plupload.Uploader({
		
            runtimes : 'html5,flash,silverlight,html4',            
            browse_button : document.getElementById('tab_images_uploader_pickfiles'), // you can pass in id...
            container: document.getElementById('tab_images_uploader_container'), // ... or DOM Element itself
            url : BASEURL + "backend/products/ajax_photo_upload/",
            filters : {
                max_file_size : '10mb',
                mime_types: [
                    {title : "Image files", extensions : "jpg,gif,png"},
                    {title : "Zip files", extensions : "zip"}
                ]
            },
            settings:{file_data_name:'userfile'},
            flash_swf_url : 'assets/plugins/plupload/js/Moxie.swf',
            silverlight_xap_url : 'assets/plugins/plupload/js/Moxie.xap',
            init: {
                PostInit: function() {
                    $('#tab_images_uploader_filelist').html("");
                    $('#tab_images_uploader_uploadfiles').click(function() {
                        uploader.start();
                        return false;
                    });
                    $('#tab_images_uploader_filelist').on('click', '.added-files .remove', function(){
                        uploader.removeFile($(this).parent('.added-files').attr("id"));    
                        $(this).parent('.added-files').remove();                     
                    });
                },
                FilesAdded: function(up, files) {
                    plupload.each(files, function(file) {
                        $('#tab_images_uploader_filelist').append('<div class="alert alert-warning added-files" id="uploaded_file_' + file.id + '">' + file.name + '(' + plupload.formatSize(file.size) + ') <span class="status label label-info"></span>&nbsp;</div>');
                    });
                },
                UploadProgress: function(up, file) {
                    $('#uploaded_file_' + file.id + ' > .status').html(file.percent + '%');
                },
                FileUploaded: function(up, file, response) {
                    var response = $.parseJSON(response.response);
                    if (response.result && response.result == 'OK') {
                        var id = response.id; // uploaded file's unique name. Here you can collect uploaded file names and submit an jax request to your server side script to process the uploaded files and update the images tabke                       
						
						ajax_new_product_photo_line();
                        
						$('#uploaded_file_' + file.id + ' > .status').removeClass("label-info").addClass("label-success").html('<i class="fa fa-check"></i> Done'); // set successfull upload
                    } else {
                        $('#uploaded_file_' + file.id + ' > .status').removeClass("label-info").addClass("label-danger").html('<i class="fa fa-warning"></i> Failed'); // set failed upload
                        Metronic.alert({type: 'danger', message: 'One of uploads failed. Please retry.', closeInSeconds: 10, icon: 'warning'});
                    }

                },
                Error: function(up, err) {
                    Metronic.alert({type: 'danger', message: err.message, closeInSeconds: 10, icon: 'warning'});
                }
            }
        });
		
        uploader.init();
		
    }
	
    var handleReviews = function () {
	
        var grid = new Datatable();

        grid.init({

            src: $("#datatable_reviews"),

            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                "aLengthMenu": [

                    [20, 50, 100, 150, -1],

                    [20, 50, 100, 150, "All"] // change per page values here

                ],

                "iDisplayLength": 20,

                "bServerSide": true,

                "sAjaxSource": "demo/ecommerce_product_reviews.php",

                "aoColumnDefs" : [{  // define columns sorting options(by default all columns are sortable extept the first checkbox column)

                    'bSortable' : true

                }],

                "aaSorting": [[ 0, "asc" ]] // set first column as a default sort by asc

            }

        });

    }



    var handleHistory = function () {



        var grid = new Datatable();

        grid.init({

            src: $("#datatable_history"),

            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                "aLengthMenu": [

                    [20, 50, 100, 150, -1],

                    [20, 50, 100, 150, "All"] // change per page values here

                ],

                "iDisplayLength": 20,

                "bServerSide": true,

                "sAjaxSource": "demo/ecommerce_product_history.php",

                "aoColumnDefs" : [{  // define columns sorting options(by default all columns are sortable extept the first checkbox column)

                    'bSortable' : true

                }],

                "aaSorting": [[ 0, "asc" ]] // set first column as a default sort by asc

            }

        });

    } 

    var initComponents = function () {

        //init datepickers

        $('.date-picker').datepicker({

            rtl: Metronic.isRTL(),

            autoclose: true

        });

        //init datetimepickers

        $(".datetime-picker").datetimepicker({
            isRTL: Metronic.isRTL(),
            autoclose: true,
            todayBtn: true,
            pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left"),
            minuteStep: 10
        });

        //init maxlength handler

        $('.maxlength-handler').maxlength({

            limitReachedClass: "label label-danger",

            alwaysShow: true,

            threshold: 5

        });

    }
	
    return {
	
        //main function to initiate the module

        init: function () {
            initComponents();
            handleImages();
            handleReviews();
            handleHistory();
        }

    };

}();

function ajax_new_product_photo_line()
{
    $.post(BASEURL + 'backend/products/ajax_new_product_photo_line/',{ 'param1':'file' }, function(data) {
	
       if (data != '') {
	   
            //$('#tablebody').html('');
            $('#tablebody').append(data);
			
       } else {

			return false;

       }
	   
    });
}



function ajax_remove_product_photo(products_id, products_photos_id)
{
	if (confirm('Are you sure that you want to delete this photo?')) {
	
		$.post( BASEURL + 'backend/products/ajax_remove_product_photo/',{ 'products_id':products_id, 'products_photos_id':products_photos_id } , function(data) {
	  
			if (data != "") {

				$('#tablebody').html('');
				$('#tablebody').append(data);

			} else {

				return false;

			}
		  
		});
		
	}
}


