<?php

class Ctg_api
{

	public $api_url = "http://api.christophestogo.com";
	public $api_version = "v1";
	public $api_format = "serialized";
	public $api_key = "841c87f2b73813be3e01a05be65b4f94f5a27161";
	public $debug = FALSE;

	public function __construct()
	{

	}

	public function post($method, $params = FALSE)
	{
		if (isset($params['format'])) {

			$this->api_format = $params['format'];

		}

		$params['CTG-API-KEY'] = $this->api_key;

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $this->api_url.'/'.$this->api_version.'/'.$method.'.'.$this->api_format);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		$response = curl_exec($ch);

		curl_close($ch);

		return $this->format_response($response);
	}

	public function get($method, $params = FALSE)
	{
		if (!isset($params['format'])) {

			$this->api_format = $params['format'];

		}

		$params['CTG-API-KEY'] = $this->api_key;

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $this->api_url.'/'.$this->api_version.'/'.$method.'.'.$this->api_format.'?'.http_build_query($params));
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		$response = curl_exec($ch);

		curl_close($ch);

		return $this->format_response($response);
	}

	public function format_response($response)
	{
		if ($this->debug) {

			return $response;

		} else {

			if ($this->api_format == "serialized") {

				return unserialize($response);

			} else {

				return $response;

			}

		}
	}

}

?>