<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-file"></i>Edit Product Tag
        </div>
        <div class="tools">
            <a href="" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body">
            <form class="form-horizontal" method="post" action="<?php echo current_url() ?>">

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Product Tag</label>
                    <div class="col-md-10">
                        <input type="text" placeholder="Product Tag" class="form-control" name="product_tag" value="<?php if (!empty($product_tags->product_tag)) echo $product_tags->product_tag; ?>"><?php echo form_error('product_tag'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Description</label>
                    <div class="col-md-10">
                        <textarea class="tinymce_editor form-control" cols="100" rows="12" name="description"><?php if (!empty($product_tags->description)) echo $product_tags->description; ?></textarea>
                        <?php echo form_error('description'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Status</label>
                    <div class="col-md-10">
                        <select name="status" class="form-control">
                            <option value="1" <?php if ($product_tags->status == 1) echo 'selected="selected"'; ?> >Active</option>
                            <option value="0" <?php if ($product_tags->status == 0) echo 'selected="selected"'; ?> >Deactive</option>
                        </select>
                    </div>
                </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn blue">Submit</button>
            <a href="<?php echo base_url() . 'superadmin/product_tags'; ?>" ><button class="btn btn-danger" type="button">Cancel</button></a></div>
        </form>
    </div>
</div>