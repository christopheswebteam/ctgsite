<div class="row">
   <div class="col-md-12">
   <h3 class="page-title">
   Add Product 
   <small>create & edit product</small>
   </h3>
   </div>
</div>
<br>  <br>  
 <div class="row">
  <?php if(!empty($nutritional_info)): ?>
 <?php $info=array(); ?> 
      <?php foreach ($nutritional_info as $value) {
         $info[$value->size]= array(
                                    'size' =>$value->size , 
                                    'nutritional_info' =>$value->nutritional_info ,
                                    'price' =>$value->price ,
                                    'quantity'=>$value->quantity
                                   );

      } 

?>

  <?php endif; ?>

  <?php if(form_error('single_nutritional')!='' || form_error('single_price')!=''): ?>
    <div class="alert alert-danger alert-dismissable">
      <button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>
       <?php if(form_error('single_nutritional')!=''){ ?>
       <span><?php echo form_error('single_nutritional'); ?></span>
       <?php } if(form_error('single_price')!=''){ ?>
       <span><?php echo form_error('single_price'); ?></span>
       <?php } ?>
     </div>
  <?php endif; ?> 

  <?php if(form_error('additional_info')!='' || form_error('ingredients')!='' || form_error('instructions')!=''): ?>
    <div class="alert alert-danger alert-dismissable">
      <button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>
       <?php if(form_error('additional_info')!=''){ ?>
       <span><?php echo form_error('additional_info'); ?></span>
       <?php } if(form_error('ingredients')!=''){ ?>
       <span><?php echo form_error('ingredients'); ?></span>
       <?php } if(form_error('instructions')!=''){ ?>
       <span><?php echo form_error('instructions'); ?></span>
       <?php } ?>
     </div>
  <?php endif; ?>


  <?php if(form_error('medium_nutritional')!='' || form_error('medium_price')!='' || form_error('medium_quantity')!='' ): ?>
    <div class="alert alert-danger alert-dismissable">
      <button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>
      <?php if(form_error('medium_nutritional')!=''){ ?>
        <span><?php echo form_error('medium_nutritional'); ?></span>
      <?php } if(form_error('medium_price')!=''){ ?>
        <span><?php echo form_error('medium_price'); ?></span>
      <?php } ?>
      <?php if(form_error('medium_quantity')!=''){ ?>
        <span><?php echo form_error('medium_quantity'); ?></span>
      <?php } ?>
      
     </div>
  <?php endif; ?>
  <?php if(form_error('family_nutritional')!='' || form_error('family_price')!='' || form_error('family_quantity')!='' ): ?>
    <div class="alert alert-danger alert-dismissable">
      <button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>
      <?php if(form_error('family_nutritional')!=''){ ?>      
      <span><?php echo form_error('family_nutritional'); ?></span>
      <?php } ?>
      <?php if(form_error('family_price')!=''){ ?>      
      <span><?php echo form_error('family_price');?></span>
      <?php } ?>
      <?php if(form_error('family_quantity')!=''){ ?>            
      <span><?php echo form_error('family_quantity');?></span>
      <?php } ?>
    </div>
  <?php endif; ?>

  <?php if(form_error('large_nutritional')!='' || form_error('large_price')!='' || form_error('large_quantity')!='' ): ?>
    <div class="alert alert-danger alert-dismissable">
      <button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button> 
    <?php if(form_error('large_nutritional')!=''){ ?>      
      <span><?php echo form_error('large_nutritional'); ?></span>
      <?php } ?>
      <?php if(form_error('large_price')!=''){ ?>      
      <span><?php echo form_error('large_price');?></span>
      <?php } ?>
      <?php if(form_error('large_quantity')!=''){ ?>            
      <span><?php echo form_error('large_quantity');?></span>
      <?php } ?>
    </div>
  <?php endif; ?>

  <?php if(form_error('small_nutritional')!='' || form_error('small_price')!='' || form_error('small_quantity')!='' ): ?>
    <div class="alert alert-danger alert-dismissable">
       <button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>      

     <?php if(form_error('small_nutritional')!=''){ ?>      
      <span><?php echo form_error('small_nutritional'); ?></span>
      <?php } ?>
      <?php if(form_error('small_price')!=''){ ?>      
      <span><?php echo form_error('small_price');?></span>
      <?php } ?>
      <?php if(form_error('small_quantity')!=''){ ?>            
      <span><?php echo form_error('small_quantity');?></span>
      <?php } ?>
    </div>
  <?php endif; ?>

     
      
<?php echo form_open(current_url()); ?>
            <div class="col-md-12">
               <div class="tabbable tabbable-custom boxless">
                  <ul class="nav nav-tabs">
                     <li class="active"><a href="#tab_0" data-toggle="tab"> General </a></li>
                     <li><a href="#tab_1" data-toggle="tab">Additional Info & Ingridients </a></li>
                     <li><a  href="#tab_2" data-toggle="tab">Nutritional Info & Pirce</a></li>
                     
                  </ul>
                  <div class="tab-content">
                     <div class="tab-pane active" id="tab_0">
                        <div class="portlet-body form">
                           <!-- BEGIN FORM-->
                     <div  class="form-horizontal">
                        <div class="form-body">

                        <h4 class="form-section">Product General Info</h2>                                    
                        <div class="form-group">
                           <label  class="col-md-3 control-label">Product Name</label>
                           <div class="col-md-6">
                              <input type="text" class="form-control" name="product_name" value="<?php if(!empty($products->product_name)) echo $products->product_name; else echo set_value('product_name');?>" placeholder="Product Name">
                              <span class="help-block"><?php echo form_error('product_name');?></span>
                           </div>
                        </div>
                           
                        <div class="form-group">
                           <label class="col-md-3 control-label">
                              Description:
                              <span class="required"> * </span>
                           </label>
                           <div class="col-md-6">
                              <textarea class="form-control" name="product_description"><?php if(!empty($products->product_description)) echo $products->product_description; else echo set_value('product_description');?></textarea>
                                <span class="help-block"><?php echo form_error('product_description');?></span>
                           </div>
                        </div>


                        <div class="form-group">
                           <label class="control-label col-md-3">Start Date</label>
                           <div class="col-md-6">
                              <input class="form-control form-control-inline input-medium date-picker" placeholder="Start Date" type="text" value="<?php if(!empty($products->start_date)) echo  date('m/d/Y',strtotime($products->start_date)); ?>" name="start_date" size="16">
                              <span class="help-block"><?php echo form_error('start_date');?></span>
                           </div>
                        </div> 

                        <div class="form-group">
                           <label class="control-label col-md-3">End Date</label>
                           <div class="col-md-6">
                              <input class="form-control form-control-inline input-medium date-picker1" placeholder="End Date" type="text" value="<?php if(!empty($products->end_date)) echo date('m/d/Y',strtotime($products->end_date)); ?>" name="end_date" size="16">
                              <span class="help-block"><?php echo form_error('end_date');?></span>
                           </div>
                        </div>
                              <br>

                        <div class="form-group">
                          <label class="col-sm-2 col-sm-2 control-label">Product Type</label>
                          <div class="col-md-10">
                            <select class="form-control" name="item_type">
                              <option value=""> Select product Type </option>
                              <?php if(!empty($item_types)){ foreach ($item_types as $row){ ?>
                                  <option value="<?php echo $row->id; ?>" <?php if(!empty($products->item_type_id)){  if($products->item_type_id==$row->id) echo 'selected="selected"'; } ?> ><?php echo $row->type_name; ?></option>              
                              <?php } }  ?>
                            </select>
                          <?php echo form_error('product_type'); ?>
                         </div>
                        </div>
                      </br>
                
                         <div class="form-group">
                          <label class="col-sm-2 col-sm-2 control-label">Tags</label>
                          <div class="col-md-10">
                            <div class="row-fluid">
                              <?php if(!empty($tags)){ foreach ($tags as $row){ ?>
                                  <div class="col-md-3">       
                                    <input type="checkbox" value="<?php echo $row->id; ?>" name="tags[]"  <?php if(!empty($tags_association)){   
                                    foreach ($tags_association as $row1) {
                                    if($row1->tags_id==$row->id) echo 'checked="checked"'; 
                                    }
                                  }?> ><?php echo $row->tag; ?>
                                  </div>
                              <?php } }  ?>
                            </div>
                          </br>
                          <?php echo form_error('tags[]'); ?>
                         </div>
                        </div>
                        <br>
                                 
                         <div class="form-group">
                            <label class="col-md-3 control-label">Status</label>
                            <div class="col-md-6">
                              <select name="status" class="form-control">
                              <option value="1" <?php if($products->status==1) echo 'selected="selected"';?>>Active</option>
                              <option value="0" <?php if($products->status==0) echo 'selected="selected"';?>>Deactive</option>
                              </select>
                              <?php echo form_error('status'); ?>
                            </div>
                          </div>  
                                <br>         
                                    
                           </div>
                          
                        </div>
                        <!-- END FORM--> 
                     </div>                        
                     </div>

                     <div class="tab-pane" id="tab_1">
                                                   
                      <div class="portlet-body form">
                     <!-- BEGIN FORM-->
                        <div action="#" class="form-horizontal">
                           <div class="form-body">

                              <h4 class="form-section">Product Meta Info</h4>
                               <div class="form-group">
                               <label class="col-sm-2 col-sm-2 control-label">Additional Information</label>
                               <div class="col-md-10">
                               <textarea class="tinymce_editor form-control" cols="200" rows="15" name="additional_info"><?php if(!empty($products->additional_info)) echo $products->additional_info; ?></textarea>
                               <?php echo form_error('additional_info'); ?>
                              </div>
                             </div> 


                             <div class="form-group">
                               <label class="col-sm-2 col-sm-2 control-label">Ingredients</label>
                               <div class="col-md-10">
                               <textarea class="tinymce_editor form-control" cols="200" rows="15" name="ingredients"><?php if(!empty($products->ingredients)) echo $products->ingredients; ?></textarea>
                               <?php echo form_error('ingredients'); ?>
                              </div>
                             </div> 


                             <div class="form-group">
                               <label class="col-sm-2 col-sm-2 control-label">Instructions</label>
                               <div class="col-md-10">
                               <textarea class="tinymce_editor form-control" cols="200" rows="15" name="instructions">
                               <?php if(!empty($products->instructions)) echo $products->instructions; ?></textarea>
                               <?php echo form_error('instructions'); ?>
                              </div>
                             </div>
                            
                        </div>
                     </div>
                     <!-- END FORM--> 
                  </div>                      
                        
                     </div>
                     <div class="tab-pane " id="tab_2">
                       <div class="portlet-body form">
                           <!-- BEGIN FORM-->
                     <div action="#" class="form-horizontal">
                        <div class="form-body">
                           <h4 class="form-section">Product Nutritional Information</h4>
                           <div class="form-group">
                            <label class="col-md-3 control-label">Size</label>
                            <div class="col-md-8">
                             <div calss="row-fluid" id="size">
                                <div class="col-md-2">
                                   <input type="checkbox" onchange="test();" name="size[]" id="single"  <?php  if(!empty( $info['single'])){ if($info['single']['size']=='single') echo 'checked="checked"'; } ?>  value="single"><span>Single</span>
                                </div> 

                                <div class="col-md-2" >
                                   <input type="checkbox" onchange="test();" name="size[]" <?php  if(!empty( $info['small'])){ if($info['small']['size']=='small') echo 'checked="checked"'; } ?> class="group" value="small"><span>Small</span>
                                </div> 

                                <div class="col-md-2" >
                                   <input type="checkbox" onchange="test();" name="size[]" <?php  if(!empty( $info['medium'])){ if($info['medium']['size']=='medium') echo 'checked="checked"'; } ?> class="group" value="medium"><span>Medium</span>
                                </div> 

                                <div class="col-md-2" >
                                  <input type="checkbox" onchange="test();" name="size[]" <?php  if(!empty( $info['large'])){ if($info['large']['size']=='large') echo 'checked="checked"'; } ?> class="group" value="large"><span>Large</span>  
                                </div> 

                                <div class="col-md-2" >
                                  <input type="checkbox" onchange="test();" name="size[]" <?php  if(!empty( $info['family'])){ if($info['family']['size']=='family') echo 'checked="checked"'; } ?> class="group" value="family"><span>Family</span>
                                </div>

                             </div> <br><br>
                            <?php echo form_error('size[]'); ?>
                            </div>
                           </div> 
                           <br>
                           <div class="single_group" <?php if(!empty($info['single'])){ echo 'style="display:block;"'; } else{echo 'style="display:none;"';} ?>>
                              <div class="form-group">
                                 <label  class="col-md-3 control-label">For Single Nutritional Info</label>
                                 <div class="col-md-6">
                                    <textarea class="tinymce_editor form-control" cols="200" rows="15" name="single_nutritional"><?php if(!empty($info['single']['nutritional_info'])) echo $info['single']['nutritional_info']; ?></textarea>
                                    <?php echo form_error('single_nutritional'); ?>
                                 </div>
                              </div> 

                              <div class="form-group">
                                 <label  class="col-md-3 control-label">Single Price</label>
                                 <div class="col-md-6">
                                    <input type="text"  class="form-control" name="single_price" value="<?php if(!empty($info['single']['price'])) echo $info['single']['price']; ?>">
                                    <?php echo form_error('single_nutritional'); ?>
                                 </div>
                              </div>
                
                           </div> 

                           <div class="medium_group" <?php if(!empty( $info['medium'])){ echo 'style="display:block;"'; }else{ echo'style="display:none;"'; } ?> >
                              <div class="form-group">
                                 <label  class="col-md-3 control-label">For Medium Nutritional Info</label>
                                 <div class="col-md-6">
                                    <textarea class="tinymce_editor form-control" cols="200" rows="15" name="medium_nutritional"><?php if(!empty($info['medium']['nutritional_info'])) echo $info['medium']['nutritional_info']; ?></textarea>
                                    <?php echo form_error('medium_nutritional'); ?>
                                 </div>
                              </div>

                              <div class="form-group">
                                 <label  class="col-md-3 control-label">Medium Price</label>
                                 <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Medium Price" name="medium_price" value="<?php if(!empty($info['medium']['price'])) echo $info['medium']['price']; ?>">
                                    <?php echo form_error('medium_price'); ?>
                                 </div>
                              </div>

                              <div class="form-group">
                                 <label  class="col-md-3 control-label">Medium Quantity</label>
                                 <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Medium Quantity" name="medium_quantity" value="<?php if(!empty($info['medium']['quantity'])) echo $info['medium']['quantity']; ?>">
                                    <?php echo form_error('medium_quantity'); ?>
                                 </div>
                              </div>

                          </div>


                          <div class="small_group" <?php if(!empty( $info['small'])){ echo 'style="display:block;"'; }else{ echo'style="display:none;"'; } ?>>
                              <div class="form-group">
                                 <label  class="col-md-3 control-label">For Small Nutritional Info</label>
                                 <div class="col-md-6">
                                    <textarea class="tinymce_editor form-control" cols="200" rows="15" name="small_nutritional"><?php if(!empty($info['small']['nutritional_info'])) echo $info['small']['nutritional_info']; ?></textarea>
                                    <?php echo form_error('small_nutritional'); ?>
                                 </div>
                              </div>

                              <div class="form-group">
                                 <label  class="col-md-3 control-label">Small Price</label>
                                 <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Small Price" name="small_price" 
                                    value="<?php if(!empty($info['small']['price'])) echo $info['small']['price']; ?>">
                                    <?php echo form_error('small_price'); ?>
                                 </div>
                              </div>

                              <div class="form-group">
                                 <label  class="col-md-3 control-label">Small Quantity</label>
                                 <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Small Quantity" name="small_quantity" value="<?php if(!empty($info['small']['quantity'])) echo $info['small']['quantity']; ?>">
                                    <?php echo form_error('small_quantity'); ?>
                                 </div>
                              </div>

                           </div>



                          <div class="large_group" <?php if(!empty( $info['large'])){ echo 'style="display:block;"'; }else{ echo'style="display:none;"'; } ?>> 
                            <div class="form-group">
                                <label  class="col-md-3 control-label">For Large Nutritional Info</label>
                                <div class="col-md-6">
                                  <textarea class="tinymce_editor form-control" cols="200" rows="15" name="large_nutritional"><?php if(!empty($info['large']['nutritional_info'])) echo $info['large']['nutritional_info']; ?></textarea>
                                   <?php echo form_error('large_nutritional'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label  class="col-md-3 control-label">Large Price</label>
                                <div class="col-md-6">
                                <input type="text"  class="form-control" placeholder="Large Price" name="large_price" value="<?php if(!empty($info['large']['price'])) echo $info['large']['price']; ?>">
                                  <?php echo form_error('large_price'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                 <label  class="col-md-3 control-label">Large Quantity</label>
                                 <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Large Quantity" name="large_quantity" value="<?php if(!empty($info['large']['quantity'])) echo $info['large']['quantity']; ?>">
                                    <?php echo form_error('large_quantity'); ?>
                                 </div>
                            </div>
                          </div>

                          <div class="family_group" <?php if(!empty( $info['family'])){ echo 'style="display:block;"'; }else{ echo'style="display:none;"'; } ?>> 
                            
                            <div class="form-group">
                                <label  class="col-md-3 control-label">For Family Nutritional Info</label>
                                <div class="col-md-6">
                                  <textarea class="tinymce_editor form-control" cols="200" rows="15" name="family_nutritional"><?php if(!empty($info['family']['nutritional_info'])) echo $info['family']['nutritional_info']; ?></textarea>
                                   <?php echo form_error('family_nutritional'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                 <label  class="col-md-3 control-label">Family Price</label>
                                 <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Family Price" name="family_price" value="<?php if(!empty($info['family']['price'])) echo $info['family']['price']; ?>">
                                    <?php echo form_error('family_price'); ?>
                                 </div>
                            </div>

                            <div class="form-group">
                                 <label  class="col-md-3 control-label">Family Quantity</label>
                                 <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Family Quantity" name="family_quantity" value="<?php if(!empty($info['family']['quantity'])) echo $info['family']['quantity']; ?>">
                                    <?php echo form_error('family_quantity'); ?>
                                 </div>
                            </div>
                          </div>

                              </div> 
                           </div> 
                        </div> 
                     </div> 

                     


                     <hr>
                     <div class="form-actions fluid">
                        <div class="col-md-offset-3 col-md-9">
                           <button type="submit" class="btn blue">Save & Submit</button>
                           <button type="button" class="btn default">Cancel</button>                              
                        </div>
                     </div>   
                  <?php echo form_close(); ?>

                  <hr>
                  <br>
                  <div>
                    <div class="portlet-body form">
                      <div action="#" class="form-horizontal">
                        <div class="form-body">
                          <div class="form-group">
                            <label style="margin-top:150px;"  class="col-md-3 control-label">Product Images</label>
                            <div class="col-md-6">
                              <form action="<?php echo base_url()?>backend/products/upload" class="dropzone"></form>
                              <span class="help-block"></span>
                            </div>
                          </div>
                        </div> 
                      </div> 
                    </div> 
                  </div>                              
               </div>
            </div>
         </div>
      </div>