<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>My Profile
        </div>
        <div class="tools">
            <a href="" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post" action="<?php echo current_url() ?>">

            <div class="form-body">

                <div class="form-group">
                    <label class="control-label">Name</label>
                    <input type="text" placeholder="Name" class="form-control" name="name" value="<?php if (!empty($user->name)) echo $user->name; ?>"><?php echo form_error('name'); ?>
                </div>

                <div class="form-group">
                    <label class="control-label">Email Address</label>
                    <input type="email" placeholder="Email Address" class="form-control" name="email" value="<?php if (!empty($user->email)) echo $user->email; ?>"><?php echo form_error('email'); ?>

                </div>


            </div>

            <div class="form-actions">
                <button type="submit" class="btn blue">Submit</button>
                <a href="<?php echo base_url() ?>backend/dashboard" class="btn btn-danger">Cancel </a>
            </div>
        </form>
    </div>
</div>


