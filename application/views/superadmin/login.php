<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo SITE_NAME ?>| Admin login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="<?php echo BACKEND_THEME_URL ?>global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo BACKEND_THEME_URL ?>global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo BACKEND_THEME_URL ?>global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo BACKEND_THEME_URL ?>global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?php echo BACKEND_THEME_URL ?>global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo BACKEND_THEME_URL ?>global/plugins/select2/select2-metronic.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo BACKEND_THEME_URL ?>admin/pages/css/login-soft.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?php echo BACKEND_THEME_URL ?>global/css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo BACKEND_THEME_URL ?>global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo BACKEND_THEME_URL ?>admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link id="style_color" href="<?php echo BACKEND_THEME_URL ?>admin/layout/css/themes/default.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo BACKEND_THEME_URL ?>admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>

    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="<?php echo base_url() ?>">
                <img src="<?php echo base_url() ?>assets/logo-mini.png" alt=""/>
            </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="menu-toggler sidebar-toggler">
        </div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <?php echo form_open(current_url(), array('class' => 'login-form')); ?>
            <h3 class="form-title">Login to your account</h3>

            <?php if ($this->session->flashdata('msg_error')): ?>
                <div class="form-group">
                    <div class="text-center">

                        <p class="help-block label label-danger">
                            <?php echo $this->session->flashdata('msg_error'); ?></p>

                    </div>
                </div>
            <?php endif; ?>

            <!--   <div class="alert alert-danger display-hide">
                  <button class="close" data-close="alert"></button>
                  <span>
                  Enter any username and password. </span>
              </div> -->
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">E-mail</label>
                <div class="input-icon">
                    <i class="fa fa-user"></i>
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="E-mail" name="email"/>
                    <?php echo form_error('email') ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
                <div class="input-icon">
                    <i class="fa fa-lock"></i>
                    <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
                    <?php echo form_error('password') ?>
                </div>
            </div>
            <div class="form-actions">
                <!--  <label class="checkbox">
                 <input type="checkbox" name="remember" value="1"/> Remember me </label> -->
                <button type="submit" class="btn blue pull-right">
                    Login <i class="m-icon-swapright m-icon-white"></i>
                </button>
            </div>

        </form>

        <!-- END LOGIN FORM -->




    </div>
    <!-- END LOGIN -->
    <!-- BEGIN COPYRIGHT -->
    <div class="copyright">
        2014 &copy; <?php echo SITE_NAME ?>
    </div>
    <!-- END COPYRIGHT -->
    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- BEGIN CORE PLUGINS -->
    <!--[if lt IE 9]>
        <script src="<?php echo BACKEND_THEME_URL ?>global/plugins/respond.min.js"></script>
        <script src="<?php echo BACKEND_THEME_URL ?>global/plugins/excanvas.min.js"></script>
        <![endif]-->
    <script src="<?php echo BACKEND_THEME_URL ?>global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="<?php echo BACKEND_THEME_URL ?>global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <script src="<?php echo BACKEND_THEME_URL ?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>


    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->

    <script src="<?php echo BACKEND_THEME_URL ?>global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>

    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->

    <script src="<?php echo BACKEND_THEME_URL ?>admin/pages/scripts/login-soft.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        jQuery(document).ready(function() {
            // Metronic.init(); // init metronic core components
            //Layout.init(); // init current layout
            // Login.init();

            $.backstretch([
                "<?php echo BACKEND_THEME_URL ?>admin/pages/media/bg/1.jpg",
                "<?php echo BACKEND_THEME_URL ?>admin/pages/media/bg/2.jpg",
                "<?php echo BACKEND_THEME_URL ?>admin/pages/media/bg/3.jpg",
                    //"<?php echo BACKEND_THEME_URL ?>admin/pages/media/bg/4.jpg"
            ], {
                fade: 1000,
                duration: 8000
            });

        });
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>