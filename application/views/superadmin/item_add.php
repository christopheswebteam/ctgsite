<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-file"></i>Add Item
        </div>
        <div class="tools">
            <a href="" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body">
            <form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo current_url() ?>">

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Item Name</label>
                    <div class="col-md-10">
                        <input type="text" placeholder="Item Type" class="form-control" name="item_name" value="<?php echo set_value('item_name'); ?>"><?php echo form_error('item_name'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Item Description</label>
                    <div class="col-md-10">
                        <textarea class="tinymce_editor form-control" cols="100" rows="12" name="item_description"><?php echo set_value('item_description'); ?></textarea>
                        <?php echo form_error('item_description'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Item Image</label>
                    <div class="col-md-10">
                        <input type="file" name="item_image">         
                        <?php echo form_error('item_image'); ?>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Additional Information</label>
                    <div class="col-md-10">
                        <textarea class="tinymce_editor form-control" cols="100" rows="12" name="additional_info"><?php echo set_value('additional_info'); ?></textarea>
                        <?php echo form_error('additional_info'); ?>
                    </div>
                </div> 


                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Ingredients</label>
                    <div class="col-md-10">
                        <textarea class="tinymce_editor form-control" cols="100" rows="12" name="ingredients"><?php echo set_value('ingredients'); ?></textarea>
                        <?php echo form_error('ingredients'); ?>
                    </div>
                </div> 


                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Instructions</label>
                    <div class="col-md-10">
                        <textarea class="tinymce_editor form-control" cols="100" rows="12" name="instructions"><?php echo set_value('instructions'); ?></textarea>
                        <?php echo form_error('instructions'); ?>
                    </div>
                </div>



                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Item Type</label>
                    <div class="col-md-10">
                        <select class="form-control" name="item_type">
                            <option value=""> Select Item Type </option>
                            <?php if (!empty($item_types)) { foreach ($item_types as $row) { ?>
                                    <option value="<?php echo $row->id; ?>"><?php echo $row->type_name; ?></option>              
                                <?php } } ?>
                        </select>
                        <?php echo form_error('item_type'); ?>
                    </div>
                </div>
                </br>

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Product Tags</label>
                    <div class="col-md-10">
                        <div class="row-fluid">
                            <?php if (!empty($product_tags)) { foreach ($product_tags as $row) { ?>
                                    <div class="col-md-3">       
                                        <input type="checkbox" value="<?php echo $row->id; ?>" name="tags[]" ><?php echo $row->product_tag; ?>
                                    </div>
                                <?php } } ?>
                        </div>
                        </br>
                        <?php echo form_error('tags[]'); ?>
                    </div>
                </div>
                </br>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Status</label>
                    <div class="col-md-10">
                        <select name="status" class="form-control">
                            <option value="1">Active</option>
                            <option value="0">Deactive</option>
                        </select>
                        <?php echo form_error('status'); ?>
                    </div>
                </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn blue">Submit</button>
            <a href="<?php echo base_url() . 'superadmin/item_types'; ?>"><button class="btn btn-danger" type="button">Cancel</button></a></div>
        </form>
    </div>
</div>