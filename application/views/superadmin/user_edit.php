<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Profile Edit
        </div>
        <div class="tools">
            <a href="" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post" action="<?php echo current_url() ?>">
            <?php echo validation_errors(); ?>
            <div class="row">
                <div class="form-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control " name="name" placeholder="Name" value="<?php if (!empty($user->name)) echo $user->name; ?>">
                            <?php echo form_error('name'); ?>`
                        </div>

                        <div class="form-group">
                            <label>Email Address</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </span>
                                <input type="text" class="form-control" name="email" placeholder="Email" value="<?php if (!empty($user->email)) echo $user->email; ?>">
                            </div>
                            <?php echo form_error('email'); ?>
                        </div>

                    </div>
                    <div class="col-md-6">


                        <div class="form-group">
                            <label class="control-label">Address</label>

                            <input type="text" placeholder="Address" class="form-control" name="address" value="<?php

                            if (!empty($user->address)) echo $user->address;
                            else echo set_value('address')

                                ?>">
                                   <?php echo form_error('address'); ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Town</label>

                            <input type="text" placeholder="Town" class="form-control" name="city" value="<?php if (!empty($user->town)) echo $user->town; ?>">
                            <?php echo form_error('city'); ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label">County</label>

                            <input type="text" name="county" class="form-control" value="<?php echo $user->county ?>" ><?php echo form_error('county') ?>

                        </div>

                    </div>
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn blue">Submit</button>
                <a href="<?php echo base_url() ?>superadmin/users" class="btn btn-danger">Cancel </a>
            </div>
        </form>
    </div>
</div>