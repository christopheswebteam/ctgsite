<?php $segment_location =$this->uri->segment(3) ?>
<div class="dynamic-content menu-wrapper" id="main-content">
    <div  class="menu padding-wrapper">
        <div class="container animate-in animate-in-fade">
            <div class="row">

                <header>
                <div class="col-xs-12 col-sm-12 col-md-4 ">
                  <div class="title">
                    <h2>locations</h2>

                  </div>
                </div>
                </header>


              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                     <div class="col-md-6">
                         <div id="map-canvas" class="view-location" style="height: 600px; margin-bottom:20px;border: 10px solid #ffffff;">
                         Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                         </div>

                     </div>
                     <div id="location-list" class="col-md-6">
                        <div id="node-location-0" class="location-box-white node-location <?php if($segment_location=='brookhaven') echo 'location-highlight'; ?>">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="location-text">
                                        <div class="header">

                                            <span class="store_title"><strong> Brookhaven</strong></span>
                                        </div>
                                        <div class="detail">

                                            <p> (678) 686-0580</p>
                                            <p>4060 Peachtree Road<br> Brookhaven, GA 30319</p>
                                            <p><a href="#">Order From Brookhaven</a></p>

                                            <div class="field field-name-field-location-geofield field-type-geofield field-label-hidden">
                                                <div class="field-items">
                                                    <div class="field-item even">33.859595700000, -84.340287300000</div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                     <div class="location-image">
                                         <img class="img-rounded img-responsive" src="<?php echo FRONTEND_THEME_URL ?>img/locations/brookhaven.jpg" alt="" >
                                         <p> Open Daily, 10:00 am to 8:00 pm</p>
                                     </div>
                                </div>
                            </div>
                         </div> <!--end location-box-white -->


                       <div id="node-location-1" class="location-box-white node-location <?php if($segment_location=='sandy-springs') echo 'location-highlight'; ?>">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="location-text">
                                        <div class="header">

                                            <span class="store_title"><strong> Sandy Springs</strong></span>
                                        </div>
                                        <div class="detail">

                                                <p>(678) 686-0581</p>
                                                <p>6309 Roswell Road<br>Sandy Springs, GA 30328</p>
                                                <p><a href="#">Order From Sandy Springs</a></p>

                                            <div class="field field-name-field-location-geofield field-type-geofield field-label-hidden">
                                                <div class="field-items">
                                                    <div class="field-item even">33.926487200000, -84.379086100000</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="location-image">
                                        <img class="img-rounded img-responsive" src="<?php echo FRONTEND_THEME_URL ?>img/locations/sandy-springs.jpg" alt="" >
                                        <p>Open Daily, 10:00 am to 8:00 pm</p>
                                    </div>
                                </div>
                            </div>
                         </div> <!--end location-box-white -->
                         <div id="node-location-2" class="location-box-white node-location <?php if($segment_location=='johns-creek') echo 'location-highlight'; ?>">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="location-text">
                                        <div class="header">

                                            <span class="store_title"><strong> Johns Creek</strong></span>
                                        </div>
                                        <div class="detail">

                                                <p> (678) 686-0599</p>
                                                <p>9775 Medlock Bridge Road Suite P<br>Johns Creek, GA 30097  </p>
                                                <p><a href="#">Order From Johns Creek</a></p>

                                            <div class="field field-name-field-location-geofield field-type-geofield field-label-hidden">
                                                <div class="field-items">
                                                    <div class="field-item even">34.016122300000, -84.190444500000</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                     <div class="location-image">
                                        <img class="img-rounded img-responsive" src="<?php echo FRONTEND_THEME_URL ?>img/locations/johns-creek.jpg" alt="" >
                                        <p>Open Daily, 10:00 am to 8:00 pm</p>
                                    </div>
                                </div>
                            </div>
                         </div> <!--end location-box-white -->


                     </div>

                </div>


                </div>

            </div>
        </div>
    </div>
</div>

