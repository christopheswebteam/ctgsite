<div class="dynamic-content contact-wrapper" id="main-content">
    <section id="contact" class="padding-wrapper">
        <div class="container">

            <div id="map" class="map">
                <iframe src="https://www.google.com/maps/?output=embed&q=<?php $add_str="Christophe's To Go - Corporate Office 123 Fake St. Atlanta, GA 54321"; echo urlencode($add_str)?>" width="100%" height="300" frameborder="0" style="border:0"></iframe>
            </div>
        </div>
        <div class="container  white-bg">
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="row">
                        <div class="col-sm-6 col-md-12">
                            <div class="info">
                                <h1>About The Company</h1>
                                <p>Christophe's To Go is a collaboration between Christophe and two local Atlanta entrepreneurs. The owners discovered Christophe a few years back and recognized his enormous talent for making delicious, healthy food. All of us at Christophe's To Go eat our own food and we feed it to our families regularly.</p>
                            </div>
                        </div>
                        <div class="col-sm-offset-1 col-sm-5 col-md-12 col-md-offset-0">
                            <div class="social-media">
                                <h1>Find us on</h1>
                                <ul class="social-icon">
                                    <li>
                                        <a href="https://www.facebook.com/christophes.kitchen"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="https://plus.google.com/local/s/Christophe's%20To%20Go"><i class="fa fa-google-plus"></i></a>
                                    </li>
                                    <li>
                                        <a href="https://twitter.com/christophestogo"><i class="fa fa-twitter"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" col-sm-6 col-md-4 col-md-offset-1">
                 <div class="vcard">
                        <h1>Contact Us </h1>
                        <p class="adr">
                            Visit <?php echo anchor('locations', 'Locations'); ?> to see our stores, hours, directions, and phone numbers. You may also email us.
                        </p>
                        <p>
                            <span class="email">Catering questions : <br> <i class="fa fa-envelope"></i>  <a class="email" href="mailto:catering@christophestogo.com" title="">catering@christophestogo.com</a></span>
                        </p>
                        <p>
                            <span class="email">Feedback about the food: <br> <i class="fa fa-envelope"></i>  <a class="email" href="mailto:feedback@christophestogo.com" title="">feedback@christophestogo.com</a></span>
                        </p>
                        <p>
                            <span class="email">Everything else: <br><i class="fa fa-envelope"></i>  <a class="email" href="mailto:contact@christophestogo.com" title="">contact@christophestogo.com</a></span>
                        </p>
                    </div>

                </div>
                <div class="col-sm-offset-1 col-sm-5 col-md-3 col-md-offset-0 form">
                    <form id="contact-form" name="contact-form" action="?" method="post">
                        <h1>Contact Form</h1>
                        <div class="input-wrapper">
                            <input class="contact-form" type="text" placeholder="name" name="form-name" id="form-name">
                        </div>
                        <div class="input-wrapper">
                            <input class="contact-form" type="text" placeholder="e-mail" name="form-email" id="form-email">
                        </div>
                        <div class="input-wrapper">
                            <input class="contact-form" type="text" placeholder="subject" name="form-subject" id="form-subject">
                        </div>
                        <div class="message">
                            <div class="input-wrapper">
                                <textarea class="contact-form" placeholder="message" name="form-message" id="form-message"></textarea>
                            </div>
                        </div>
                        <div class="message">
                            <div class="input-wrapper">
                               <!--  <div class="captcha-container">
                                   <div>
                                       <input class="contact-form" type="text" placeholder="captcha" name="form-captcha" id="form-captcha">
                                       <img id="captcha" src="<?php //echo base_url() ?>contact_us/securimage_show" alt="captcha">
                                       <button class="refresh-captcha"><i class="fa fa-refresh"></i></button>
                                   </div>
                               </div> -->
                            </div>
                        </div>
                        <div class="alert alert-success hidden">Your message was sent. Thank you!</div>
                        <div>
                            <input type="submit" value="Send" class="btn btn-info">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
