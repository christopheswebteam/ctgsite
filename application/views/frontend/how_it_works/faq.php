<div class="dynamic-content menu-wrapper" id="main-content">

  <div id="faq" class="menu padding-wrapper">

    <div class="container animate-in animate-in-fade">

      <div class="row">

        <header>
          <div class="col-md-4">
            <div class="title">
              <h2>FAQ</h2>
            </div>
          </div>
        </header>

        <div class="col-xs-12 col-sm-12 col-md-12">

          <div class="row">

            <div class="col-md-12">

              <div class="information-page">

                <div class="row">

                  <div class="col-md-5">

                    <div id="topics" class="topics">

                      <h3>Topics</h3>

    <ul class="nav">
      <li> <a  href="#t1"><span class="glyphicon glyphicon-plus"></span> Menu & Ingredients</a></li>
      <li> <a  href="#t2"><span class="glyphicon glyphicon-plus"></span> Catering, Delivery, and Large Orders</a></li>
      <li> <a  href="#t3"><span class="glyphicon glyphicon-plus"></span> Food Preparation</a></li>
     <!--  <li> <a  href="#t4"><span class="glyphicon glyphicon-plus"></span> Website and Online Ordering</a></li> -->
    </ul>

                    </div>  <!-- topics -->


                  </div>

                  <div id="q_a" class="col-md-7">

<div class="topic_heading" id="t1">
  <h3>Menu & Ingredients</h3>

  <div class="panel-group">

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
            <span class="glyphicon glyphicon-plus"></span>
            Does Christophe use preservatives?
          </a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse">
        <div class="panel-body">
          <p>No. We do not use any preservatives in our food. Our food is made fresh daily. No preservatives means a shorter shelf-life, but we would rather have a little extra waste than sell our customers food that was made a week ago and artificially made to keep with chemicals like sulfur dioxide.</p>
        </div>
      </div>
    </div>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
            <span class="glyphicon glyphicon-plus"></span>
            Is the Food Organic?
          </a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">
          <p>Some of our meals are organic. To truly qualify as organic, every single item in the dish must be certified USDA organic. We cannot get every vegetable as organic so only some of our meals are 100% organic. Those meals are labeled organic on our in-store menus and in the online menu. Also our fish is always wild.</p>
        </div>
      </div>
    </div>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
            <span class="glyphicon glyphicon-plus"></span>
            Is it diet food?
          </a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        <div class="panel-body">
          <p>We make our food healthy. Not fried, not frozen. Additionally many of our items fit within the Paleo Diet, are gluten free, and have controlled sodium and carbohydrate levels. We are not a &ldquo;diet&rdquo; food company, however. Our primary focus is to make fresh, delicious food that is also healthful. Some of our food, like the Chocolate Croissant, can&rsquo;t count as diet food but it sure tastes good!</p>
        </div>
      </div>
    </div>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
            <span class="glyphicon glyphicon-plus"></span>
            Do you do meal plans or custom dishes?
          </a>
        </h4>
      </div>
      <div id="collapse4" class="panel-collapse collapse">
        <div class="panel-body">
          <p>We do not customize our meals as it is too difficult to do so when making hundreds of meals daily. Also we do not prescribe a diet plan although you can certainly create your own using our detailed ingredient and nutrition information, which is available for every item on the Menu. If you would like to suggest a recipe, or comment on something you&rsquo;ve tried, please send us comments at feedback@christophestogo.com</p>
        </div>
      </div>
    </div>



    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
            <span class="glyphicon glyphicon-plus"></span>
            What is Gluten Free?
          </a>
        </h4>
      </div>
      <div id="collapse5" class="panel-collapse collapse">
        <div class="panel-body">
          <p>For us gluten free means the dish has no ingredients that contain gluten. See our answer to allergies below for additional information about gluten free.</p>
        </div>
      </div>
    </div>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
            <span class="glyphicon glyphicon-plus"></span>
            What is paleo?
          </a>
        </h4>
      </div>
      <div id="collapse6" class="panel-collapse collapse">
        <div class="panel-body">
          <p>We did not invent the term &ldquo;paleo.&rdquo; It is simply a type of diet that attempts to mimic the presumed diet of ancient humans. We consider an item to be &ldquo;paleo&rdquo; if it excludes grains, legumes, dairy, potatoes, refined salt, refined sugar, and processed oils. Many of our meals are cooked consistently with the paleo diet requirements. An online search will reveal a wealth of additional information about the diet.</p>
        </div>
      </div>
    </div>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
            <span class="glyphicon glyphicon-plus"></span>
            What if I have allergies to specific foods?
          </a>
        </h4>
      </div>
      <div id="collapse7" class="panel-collapse collapse">
        <div class="panel-body">
          <p>Our food is made in a single kitchen. For example, although an item may be gluten free since it has no ingredients with gluten, it is possible the food came into contact with a cooking utensil or pan that also previously touched something with gluten in it. Anyone with severe allergies to soy, nuts, dairy, or gluten should avoid our food. If you have a specific question about a dish or other items please email us at feedback@christophestogo.com.</p>
        </div>
      </div>
    </div>

  </div><!--  panel-group -->

</div>


<div class="topic_heading" id="t2">
  <h3>Catering, Delivery, and Large Orders</h3>

  <div class="panel-group">

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapset1">
            <span class="glyphicon glyphicon-plus"></span>
            Do you deliver?
          </a>
        </h4>
      </div>
      <div id="collapset1" class="panel-collapse collapse">
        <div class="panel-body">
          <p>We do not offer delivery at this time. The exception is for catered orders.</p>

        </div>
      </div>
    </div>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapset2">
            <span class="glyphicon glyphicon-plus"></span>
            Do you cater?
          </a>
        </h4>
      </div>
      <div id="collapset2" class="panel-collapse collapse">
        <div class="panel-body">
          <p>Yes! Our catering menu is much larger than our standard menu in the stores. If you need full service catering we can introduce you to event planners who can coordinate wait staff, plates, etc. Please email catering@christophestogo.com to inquire further.</p>

        </div>
      </div>
    </div>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapset3">
            <span class="glyphicon glyphicon-plus"></span>
            How do I order meals for a large organization or business?
          </a>
        </h4>
      </div>
      <div id="collapset3" class="panel-collapse collapse">
        <div class="panel-body">
          <p>Please email catering@christophestogo.com to inquire. We are happy to discuss providing meals to organizations.</p>

        </div>
      </div>
    </div>

  </div><!--  panel-group -->

</div>


<div class="topic_heading" id="t3">
  <h3>Food Preparation</h3>

  <div class="panel-group">

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapset31">
            <span class="glyphicon glyphicon-plus"></span>
            Where is the food made?
          </a>
        </h4>
      </div>
      <div id="collapset31" class="panel-collapse collapse">
        <div class="panel-body">
          <p>Christophe and his team of chefs make all the food daily in our kitchen located in Johns Creek. The food is delivered via refrigerated truck to each store before opening each day.</p>
        </div>
      </div>
    </div>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapset32">
            <span class="glyphicon glyphicon-plus"></span>
            What are the containers made of? Are they Oven Safe?
          </a>
        </h4>
      </div>
      <div id="collapset32" class="panel-collapse collapse">
        <div class="panel-body">
          <p>They are made from plant fiber and they are oven safe up to 350 degrees. For more information about the containers see our page about the containers.</p>
        </div>
      </div>
    </div>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapset33">
            <span class="glyphicon glyphicon-plus"></span>
            Does Chef Christophe actually make the food himself?
          </a>
        </h4>
      </div>
      <div id="collapset33" class="panel-collapse collapse">
        <div class="panel-body">
          <p>Christophe is in the kitchen chopping, saut&eacute;ing, and baking daily. He works with a highly accomplished team of chefs to hand make every meal.</p>
        </div>
      </div>
    </div>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapset34">
            <span class="glyphicon glyphicon-plus"></span>
            Is the food frozen?
          </a>
        </h4>
      </div>
      <div id="collapset34" class="panel-collapse collapse">
        <div class="panel-body">
          <p>Never. We may offer desserts that need to remain frozen, and those are frozen. Also we may buy certain raw goods in frozen form, but once they are prepared our meals are never frozen.</p>
        </div>
      </div>
    </div>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapset35">
            <span class="glyphicon glyphicon-plus"></span>
            What is the shelf life?
          </a>
        </h4>
      </div>
      <div id="collapset35" class="panel-collapse collapse">
        <div class="panel-body">
          <p>The shelf life for our meals will vary depending on the ingredients. Check the label to see when we recommend you consume the item.</p>
        </div>
      </div>
    </div>


  </div><!--  panel-group -->

</div>


<!-- <div class="topic_heading" id="t4">
   <h3>Website and Online Ordering</h3>

</div> -->

                  </div> <!-- q_a -->

                </div>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</div>