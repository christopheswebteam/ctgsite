<div class="dynamic-content menu-wrapper" id="main-content">



    <div  class="menu padding-wrapper">



        <div class="container animate-in animate-in-fade">



            <div class="row">



                <header>

                  <div class="col-md-5">

                    <div class="title">

                      <h2>Chef Christophes Team</h2>

                    </div>

                  </div>

                </header>



                <div class="col-xs-12 col-sm-12 col-md-12">



                  <div class="row">



                      <div class="col-md-12">



                          <div class="information-page">



                              <div class="row">



                                <div class="col-md-12">



                                  <div class="row">

                                    <div class="col-md-8">

                                      <p><strong style=" font-size: 28px;">Tiera Aguillon</strong> </p>

                                    <p style="text-align: justify; font-size: 16px;">

                                      Tierra Aguillon developed an interest in international cuisine at an early age traveling around the world with her father, a pilot. Her early exposure to a variety of cultures and cuisines established a deep-seeded appreciation for food and inspired Tiera to attend culinary school. Tiera graduated from Le Cordon Bleu in 2005 and began working as a pastry chef at Sea Island Resort in St. Simons, GA. She then joined the Ritz Carlton Hotel Group where she advanced quickly. In 2009, Tiera took a break from the culinary arts to study economics at The University of Memphis. In her spare time, Tiera enjoys eating, writing her food blog, and volunteering at The Humane Society.

                                    </p>

                                    </div>

                                    <div class="col-md-4">

                                            <div>

                                              <p><img class="img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/ChristophesTeam/Tiera-Aguillon.jpg" alt=""></p>

                                            </div>

                                    </div>







                                  </div>



                                  <div class="row">

                                    <div class="col-md-8">

                                      <p><strong style=" font-size: 28px;">Chad Morris</strong> </p>

                                    <p style="text-align: justify; font-size: 16px;">

                                      Chad Morris has always had an appreciation for healthy living and proper nutrition. He began his career as a personal trainer before attending culinary school. In school he pursued his interest in healthy, clean food and his passion for the culinary arts. After graduating in 2005, Chad applied his training as he gained experience in the restaurant industry. He has worked at almost every level of the food service industry from small-counter sandwich shops to fine dining to large-scale production. Chad enjoys cooking clean, simple meals using fresh ingredients with an emphasis on balanced nutrients.

                                    </p>

                                    </div>

                                    <div class="col-md-4">

                                          <div>

                                            <p><img class="img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/ChristophesTeam/Chad-Morris-resize.jpg" alt=""></p>

                                          </div>

                                    </div>



                                  </div>





                                  <div class="row">

                                      <div class="col-md-8">

                                          <p><strong style=" font-size: 28px;">Kevin Vance</strong> </p>

                                        <p style="text-align: justify; font-size: 16px;">

                                          Kevin Vance discovered his passion for cooking at a very young age. Born in Atlantic City, NJ, Kevin grew up learning from his father, who was a chef at Caesars Casino. Kevin worked in a variety of businesses within the food service industry before acquiring an Associate in Culinary Arts degree at Atlantic Cape Community College.

                                        </p>

                                      </div>

                                      <div class="col-md-4">

                                            <div> <p><img class="img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/ChristophesTeam/Kevin-Vance.jpg" alt=""></p> </div>

                                      </div>



                                  </div>



                                </div>



                              </div>



                            </div>



                          </div>



                        </div>



                      </div>



                    </div>



                  </div>



                </div>



              </div>



            </div>



          </div>



        </div>



      </div>



    </div>



  </div>



</div>