<div class="dynamic-content menu-wrapper" id="main-content">



    <div  class="menu padding-wrapper">



        <div class="container animate-in animate-in-fade">



            <div class="row">



                <header>

                  <div class="col-md-4">

                    <div class="title">

                      <h2>Our Containers</h2>

                    </div>

                  </div>

                </header>



                <div class="col-xs-12 col-sm-12 col-md-12">



                  <div class="row">



                      <div class="col-md-12">



                          <div class="information-page">



                              <div class="row">



                                <div class="col-md-12">



                                  <div class="features-image">



                                    <div class="col-xs-12 col-sm-12 col-md-6">

                                      <img class="img-rounded img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/OurContainers/christophe-to-go-environmentally-friendly-bag.jpg" alt="" />

                                    </div>



                                  </div>



                                  <div>







                                <h3>At Christophe’s To Go believe we strive to reduce our impact on the environment.</h3>



<p>Before we opened our stores, we spent countless hours researching, analyzing, and testing dozens of products to find the very best reusable and eco-friendly materials to minimize waste and provide ideal, responsible packaging for our meals.

 </p>

<h3>Our containers are fully compostable or recyclable. </h3>



<p>Most of our freshly prepared meals are packaged in innovative, modernfood containers made from annually renewable,100% natural plant fibers. These durable containers can be stored in the freezer, and are safe to use when heating meals directly in a microwave or traditional oven. By using these renewable products, we reduce the dependence on petroleum-based materialsinplastic, Styrofoam, and other non-biodegradable packaging.</p>



<h3>Our plant-based containers do not contain additives found in other packaging materials. </h3>



<p>Traditional food container materialsoften containchemicals and other potentially harmful substances such as chlorine, various plastics, and bleach. These plant-based containers are ideal for the majority of our meals, but in a few instances containers that are made of non-plant-based materials are more practical. Fortunately these containers are recyclable, and are made with high-quality food-grade plastic that does not contain Bisphenol-A, also known as BPA. </p>

 <div class="features-image">



                                    <div class="col-xs-12 col-sm-12 col-md-6">

                                      <img class="img-rounded img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/OurContainers/sustainable-packaging-Johns-Creek-to-go.jpg" alt="" />

                                    </div>



                                  </div>

<h3>Our Paper Bags are made from 95% recycled content.</h3>



<p>Our paper bags are also fully recyclable. As an even more environmentally friendly alternative, we also offer stylish 100% cotton reusable bags to further reduce the use of plastic and paper bags.</p>



<h3>Our delivery packages are recyclable and biodegradable.</h3>



<p>When we deliver meals to your door the meals arrive in a container with mineral fiber insulation, which is made of natural stone and a minimum of 40% recycled content. When disposed of, it will break down back into the dirt. No fiberglass!  The insulation is encased in a blue biodegradable plastic that will break down when exposed to bacteria.The outside box is a standard recyclable corrugated box and is accepted at most recycling centers.  The frozen gel packs are reusable, saving water.</p>



<h3>Our Gift Card Holders are plantable!</h3>



<p>We offer gift cards that can be conveniently reloaded in our stores for reuse. Each gift cards is packaged in novel, plantable seed papermade with post-consumer materials, meaning no trees are harmed in the production process. This uniqueeco-paper is also embedded with real plant seeds! When the gift card holder is planted in a pot of soil, the biodegradable paper composts away and the seeds germinate to become beautiful flowers.</p>



<h3>We use tasting spoons made from renewable Forest Stewardship Council (FSC)-approved paperboard.</h3>



<p>When we offer samples in our stores and at community events we use these renewable spoons. FSCcertification is considered the gold standard for sustainable forest management. These convenient spoons are compostable and break down in as few as four weeks. They can also be recycled and are made in the USA, meaning they have a significantly smaller carbon footprint than traditional plastic utensils made overseas.</p>





<h3>Stay healthy, well-fed, and green at Christophe’s To Go.</h3>



<p>At Christophe’s To Go, you’ll enjoy fresh, convenient meals that are free of preservatives and artificial colors and flavors. Our commitment to environmental responsibility means you’ll also enjoy peace of mind knowing your meal is packaged in safe, non-toxic containers that are renewable, recyclable, or both.</p>









                                  </div>



                                </div>



                              </div>



                            </div>



                          </div>



                        </div>



                      </div>



                    </div>



                  </div>



                </div>



              </div>



            </div>



          </div>



        </div>



      </div>



    </div>



  </div>



</div>