<div class="dynamic-content menu-wrapper" id="main-content">



    <div  class="menu padding-wrapper">



        <div class="container animate-in animate-in-fade">



            <div class="row">



                <header>

                  <div class="col-md-4">

                    <div class="title">

                      <h2>Chef Christophe</h2>

                    </div>

                  </div>

                </header>



                <div class="col-xs-12 col-sm-12 col-md-12">



                  <div class="row">



                      <div class="col-md-12">



                          <div class="information-page">



                              <div class="row">



                                <div class="col-md-12">



                                  <div class="features-image">



                                    <div class="col-xs-12 col-sm-12 col-md-6">

                                      <img class="img-rounded img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/ChefChristophe/Christophe-Le-Metayer.jpg" alt="" />

                                    </div>



                                  </div>



                                  <div>



                                    <p>French-born Chef Christophe Le Metayer has been pursuing his passion for good food and elegant cuisine for nearly thirty years.</p>

                                    <p>Prior to opening Christophe’s To Go in Johns Creek, Chef Le Metayer spent several years honing his culinary style at The Ritz-Carlton Buckhead, one of Atlanta’s busiest restaurants, first as a sous-chef and then as Chef de Cuisine of The Café beginning in 2003. His responsibilities at The Café included overseeing menus and seasonal offerings at Buckhead’s favorite locale for power breakfasts and business lunches.</p>

                                    <p>Before immigrating to the United States, Chef Le Metayer operated his own restaurant, Saint Martin, just outside Paris for nearly a decade. The restaurant achieved critical acclaim and earned listings in several distinguished French guidebooks including The Michelin Guide (Bib Gourmand), The Pudlowski Guide, and Bottin Gourmand.</p>

                                    <p>Chef Le Metayer previously worked at Hotel d’Angleterre and Sollerod Kro in Copenhagen, and also held positions at a three-star Michelin restaurant operated by Alain Ducasse in Hotel de Paris, Monte Carlo, as well as at a one-star Michelin restaurant, Hotel Sofitel, in Paris, before opening Saint Martin.</p>

                                    <p>Christophe was born in Meulan, France, 25 miles northwest of Paris.</p>

                                    <p>Trained at Lycee dEat deSt Quentin on Yvelines, a formal french culinary school, though he enjoy offering a truly international menu.</p>



                                  </div>



                                </div>



                              </div>



                            </div>



                          </div>



                        </div>



                      </div>



                    </div>



                  </div>



                </div>



              </div>



            </div>



          </div>



        </div>



      </div>



    </div>



  </div>



</div>