<div class="dynamic-content menu-wrapper" id="main-content">

    <div  class="menu padding-wrapper">

        <div class="container animate-in animate-in-fade">

            <div class="row">

                <header>
                  <div class="col-md-4">
                    <div class="title">
                      <h2>Catering</h2>
                    </div>
                  </div>
                </header>

                <div class="col-xs-12 col-sm-12 col-md-12">

                  <div class="row">

                      <div class="col-md-12">

                          <div class="information-page">

                              <div class="row">

                                <div class="col-md-12">

                        <br>
                                    <div class="row">

                                      <div class="col-md-6">

                                        <div>


<p class="text-justify">Have Christophe cater your next event!  We cater dinner parties, weddings, bar mitzvahs, office events, and much more.</p>
<br>
<p class="text-justify">If you would like to order our chef prepared food from our store menu in larger quantities, we can accommodate your request with notification at least 48 hours prior to your event. </p>
<br>
<p class="text-justify">Christophe’s To Go offers many other options, which are significantly more varied than our daily in-store offerings.  A sample of what we offer <a href="<?php echo base_url(); ?>downloads/catering_menu" target="_blank" data-djax-exclude="true"><span style="border-bottom:1px solid;font-weight:bold;">can be found here</span></a>.  And if you would like a more unique and lavish catering arrangement, we would be happy to provide a customized catering solution to precisely satisfy your culinary desires.   If you can imagine it then Christophe can make it.</p>
<br>
<p class="text-justify">Our catering services include delivery with serving trays and serving utensils. If you prefer full service catering including servers, dishwashers, place settings, and more, we would be happy to refer you to event planning companies that will provide the additional services you need. </p>
<br>
<p class="text-justify">All catering orders must meet certain minimum quantities and prices.  The minimums vary depending on the type of order. Generally the minimum catering order is for 20 people. If you are unsure, please email us and we will do our best to accommodate you. </p>
<br>
<p class="text-justify">All catering orders require a minimum 50% deposit at the time of the order.</p>
<p class="text-justify">We deliver within the metro Atlanta area.  Delivery fees may apply depending on the location, time, and size of order.</p>
<br>
<p class="text-justify">If you still have questions regarding catering, <a href="mailto:catering@christophestogo.com">email</a> us or call us at 678-686-0599 and we’ll be happy to help.
</p>
                                        </div>
                                      </div>
                                      <div class="col-md-6">
<img class="img-rounded img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/Catering/Christophe-food.jpg" alt="" />
<br>
<img class="img-rounded img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/Catering/DSC-0013.jpg" alt="" />


                                      </div>

                                     </div>








                                </div>

                              </div>

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</div>