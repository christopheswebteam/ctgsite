<div class="dynamic-content menu-wrapper" id="main-content">



  <div  class="menu padding-wrapper">



    <div class="container animate-in animate-in-fade">



      <div class="row">



        <header>

          <div class="col-md-4">

            <div class="title">

              <h2>The Food</h2>

            </div>

          </div>

        </header>



        <div class="col-xs-12 col-sm-12 col-md-12">



          <div class="row">



            <div class="col-md-12">



              <div class="information-page">



                <div class="row">



                  <div class="col-md-6">



 <p>

                                      <strong style=" font-size: 28px;">“Keep food simple and beautiful and it will stir all of the senses.”</strong> <span>-- Chef Christophe</span>

                                    </p>



                                    <p>Christophe’s To Go is committed to clean eating, the belief that food should be enjoyed straight from nature with little or no processing. Christophe’s To Go never uses artificial colors or flavors, or preservatives of any kind. The food labels at Christophe’s To Go have simple lists of ingredients like bell peppers, sweet potatoes, and grass-fed beef.We believe that if you can’t pronounce the name of an ingredient, you probably shouldn’t put it in your body. We use all-natural ingredients because we know they are essential for making great meals. Unless you start your own farm, it’s pretty hard to find food fresher and better than this</p>



                                    <p>We search extensively for the freshest seasonal produce, incorporating organic fruits and vegetables whenever possible. We also go to great lengths to offer premium meat, poultry, and seafood from the best sources in the industry. We choose to work with <a target="_balnk" href="http://www.joyce-farms.com/">Joyce Farms</a>, <a target="_balnk" href="http://www.nimanranch.com/Index.aspx">Niman Ranch</a>, and <a target="_balnk" href="http://www.lummiislandwild.com/">Lummi Island Fisheries</a> not only because they are responsible growers committed to sustainability, but because they offer the freshest, most flavorful beef, bison, chicken, pork, turkey, and fish available. We make these extra efforts to find the finest ingredients because Chef Christophe and his team know that freshness is the most important factor for creating excellent meals.</p>





                                    <p>When our raw ingredients arrive at our kitchen, our chefsskillfully incorporate them into wholesome, satisfying meals. Instead of using butter and cream, we use only healthy oils like olive oil, rich with antioxidants. We never deep-fry any of our food and our chefs rarely add salt. We prefer the natural flavors of our premium ingredients. Unlike other food providers, our meals are never frozen. Freezing detracts from the taste, texture, and overall quality of food. With these and other cooking techniques, our kitchen is committed to providing elegant, convenient meals that are part of a healthy lifestyle.</p>



                                    <p>Chef Christophe and his team of experienced chefs combine classical French techniques with many international styles and flavors including Asian, Latin American, Caribbean, andMiddle Eastern culinary traditions. Our chefs take advantage of all the natural flavors of simple raw ingredients to create thoroughly enjoyable entrées. They do not try to improve on nature by adding sugar or additives. Christophe’s To Go also offers an extensive selection of paleo, gluten-free, vegetarian, and vegan entrées so it’s easy for everyone to find rich, flavorful food to enjoy. We offer delicious meals that remind you that good food should be savored.</p>



                                    <p>At Christophe’s To Go, you can taste the difference our passion for outstanding food and premiumquality make in each enjoyable bite. Every day our goal is to create truly memorable dining experiences with pure food artfully prepared in elegant simplicity.</p>







                  </div>



                   <div class="col-md-6">





                                      <img class="img-rounded img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/TheFood/Vegetarian-Vegan-Gluten-Free-Burger-Atlanta.jpg" alt="" />



<br>









                                      <img class="img-rounded img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/TheFood/Gluten-free-Cod-Quinoa-Atlanta-Gourmet-Healthy-Lifestyle.jpg" alt="" />

<br>

 <img class="img-rounded img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/TheFood/Healthy-Carry-Out-Fresh-Vegetables-Prepared.jpg" alt="" />

 


















                  </div>  <!-- col-md-6 -->





                </div>



              </div>



            </div>



          </div>



        </div>



      </div>



    </div>



  </div>



</div>



  </div>



    </div>



  </div>



</div>