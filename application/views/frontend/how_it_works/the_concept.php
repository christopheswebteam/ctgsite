<div class="dynamic-content menu-wrapper" id="main-content">

    <div  class="menu padding-wrapper">

        <div class="container animate-in animate-in-fade">

            <div class="row">

                <header>
                    <div class="col-md-4">
                        <div class="title">
                            <h2>The Concept</h2>
                        </div>
                    </div>
                </header>

                <div class="col-xs-12 col-sm-12 col-md-12">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="information-page the_concept">

                                <div class="row">


                                    <div class="col-md-7">

                                        <div>
                                            <p>Christophe’s To Go is a new concept for people who want to eat well in their own home, but do not have the time or skills to prepare good food.  Christophe’s To Go makes it easy to eat well.</p>
                                            <br>
                                            <ul class="nav nav-text">
                                                <li><span class="glyphicon glyphicon-ok"></span> Choose your meals</li>
                                                <li><span class="glyphicon glyphicon-ok"></span> Heat the meals in the oven or microwave</li>
                                                <li><span class="glyphicon glyphicon-ok"></span> Enjoy a delicious, healthful meal</li>
                                            </ul>
                                            <br>
                                            <p>Christophe’s To Go gives you all the options you want</p>
                                            <br>

                                            <table class="table table-bordered">
                                                <tr>
                                                    <th width="33.333333333333%"><a href="<?php echo base_url(); ?>locations#tab_retail">Retail Stores</a></th>
                                                    <th width="33.333333333333%"><a href="<?php echo base_url(); ?>locations#tab_cooler">Coolers</a></th>
                                                    <th width="33.333333333333%"><a href="<?php echo base_url(); ?>locations#tab_delivery">Delivery</a></th>
                                                </tr>
                                                <tr>
                                                    <td>

                                                        <ul class="nav">
                                                            <li><span class="glyphicon glyphicon-minus"></span> Walk In to any of our four retail stores and pick from over 50 meals right off the shelf.</li>
                                                            <li> <center><strong>OR</strong></center></li>
                                                            <li><span class="glyphicon glyphicon-minus"></span> Order online in advance and have the food ready for pickup.  You can be in and out in 60 seconds.</li>
                                                        </ul>

                                                    </td>
                                                    <td>

                                                        <ul class="nav">
                                                            <li><span class="glyphicon glyphicon-minus"></span> Order online the day before our scheduled delivery days and we’ll drop your order in one of our coolers, which are conveniently located throughout the city.  No minimum order and no additional fees.</li>
                                                        </ul>
                                                    </td>
                                                    <td>
                                                        <ul class="nav">
                                                        <!--  <li><span class="glyphicon glyphicon-minus"></span> Delivery is available to most of the Metro-Atlanta region.</li>
                                                        <li><span class="glyphicon glyphicon-minus"></span> Your food is specially packed in thermally insulated container with a temperature-tracking sensor.</li>
                                                        <li><span class="glyphicon glyphicon-minus"></span> Food is delivered the same day via courier.</li>
                                                        <li><span class="glyphicon glyphicon-minus"></span> Receive email updates when your package is out for delivery and when it is delivered.</li> -->
                                                            <li>COMING SOON!</li>
                                                        </ul>

                                                    </td>
                                                </tr>
                                            </table>


                                            <br>

                                            <p>Christophe’s To Go also makes it easy for people with specific dietary needs to eat what they want.</p>
                                            <br>
                                            <ul  class="nav nav-text">
                                                <li><span class="glyphicon glyphicon-ok"></span> Paleo | Gluten Free | Vegetarian | Vegan</li>
                                                <li><span class="glyphicon glyphicon-ok"></span> No Preservatives</li>
                                                <li><span class="glyphicon glyphicon-ok"></span> Never Frozen </li>
                                                <li><span class="glyphicon glyphicon-ok"></span> Labeled with Nutrition Facts and Ingredients</li>
                                            </ul>
                                            <br>
                                            <p>Christophe’s To Go can provide you with the ultimate flexibility.  Do you want chicken, but your spouse wants pork?  No problem.  Kids asking for pasta?  Christophe has it covered.  Now you have the flexibility of serving each family member his or her own meal without the time, expense, and hassle of shopping and cooking.</p>

                                        </div>

                                    </div>

                                    <div class="col-md-5">


                                        <div class="features-image">




                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                <br>
                                                <p>  <img class="img-rounded img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/TheConcept/Christophe-To-Go-Paleo-Vegetarian-Gluten-Free.jpg" alt="" /></p>
                                                <br>
                                                <p>   <img class="img-rounded img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/TheConcept/Christophes-To-Go-Healthy-Gourmet-Fresh-Concept.jpg" alt="" /></p>
                                                <br>

                                                <p>   <img class="img-rounded img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/TheConcept/Christophes-To-Go-Takeout-Concept.jpg" alt="" /></p>
                                                <br>



                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>