<div class="dynamic-content menu-wrapper" id="main-content">

    <div  class="menu padding-wrapper">

        <div class="container animate-in animate-in-fade">

            <div class="row">

                <header>
                  <div class="col-md-4">
                    <div class="title">
                      <h2>The Company</h2>
                    </div>
                  </div>
                </header>

                <div class="col-xs-12 col-sm-12 col-md-12">

                  <div class="row">

                      <div class="col-md-12">

                          <div class="information-page">

                              <div class="row">

                                <div class="col-md-12">

                                  <div class="features-image">

                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                      <img class="img-rounded img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/TheCompany/Gourmet-To-Go-Carry-Out-Company-Brookhaven.jpg" alt="" />
                                    </div>

                                  </div>

                                  <div>

                                    <p style="font-size: 16px">Christophe’s To Go is a collaboration between Christophe and two local Atlanta entrepreneurs, who discovered Christophe in 2012.  They quickly recognized his enormous talent for making delicious, healthy food.  Everyone at Christophe’s To Go enjoys Christophe’s cooking, and we feed it to our families regularly.  We are steadfastly committed to bring the freshest meals to our customers.  Because we eat our own food and feed it to our families you can be confident that you are getting the best quality and top ingredients.  We think Christophe is a true prodigy with incredible talents.  When you combine the best ingredients, the best chef, and the convenience of meals at home without shopping, cooking, or cleaning then you have a winning combination.  We hope you enjoy the food as much as we do.</p>
                                    <p><a href="<?php echo base_url('contact_us') ?>">Contact Us</a></p>

                                  </div>

                                </div>

                              </div>

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</div>