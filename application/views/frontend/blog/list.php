<div class="dynamic-content container " id="main-content">

    <div id="blog" class="blog padding-wrapper">
	
        <div class="row">
		
            <div class="col-md-8 col-md-offset-2 white-bg">
			
				<?php if ($posts): ?>
				
					<?php foreach ($posts as $post): ?>
					
						<section class="blog-post">
						
							<a href="<?php echo base_url('blog/post/'.$post['slug'].'/'.$post['id']) ?>" class="img-blog hover-post">
								<figure>
									<img src="<?php echo base_url($post['image']) ?>" alt="" />
								</figure>
							</a>
							
							<header>
								<h1><?php echo character_limiter($post['title'], 55) ?></h1>
								<div class="data-post">
									<span class="date-post"><?php echo date('m/d/Y', strtotime($post['created'])) ?></span>
									<?php if ($post['author_name_first']): ?><span class="author-post"><?php echo trim($post['author_name_first']. " ".$post['author_name_last']); ?></span><?php endif; ?>
								</div>
							</header>
						
							<?php if ($post['tags']): ?>
								<p>
									<?php $post_tags = explode(',', $post['tags']) ?>
									<?php foreach ($post_tags as $tag): ?>
										<a href="#"><span class="label label-green"><?php echo trim(strtolower($tag)) ?></span></a>
									<?php endforeach ?>
								</p>
							<?php endif ?>
					   
							<article class="text-post">
								<p><?php echo character_limiter($post['content'], 200) ?></p>
							</article>
						
						</section>
					
					<?php endforeach ?>
					
				<?php else: ?>
				
					 <header>
						<p><center>No Post Found.</center></p>
					</header>
					
				<?php endif ?>

                <div class="text-center">
                    <?php echo $pagination ?>
                </div>

            </div>

        </div>
		
    </div>
	
</div>
