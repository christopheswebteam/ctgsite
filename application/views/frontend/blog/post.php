<div class="dynamic-content container " id="main-content">

    <div id="post" class="blog padding-wrapper">
	
        <div class="row">
		
            <div class="col-md-8 col-md-offset-2 white-bg">
			
                <figure align="center">
                    <img class="img-rounded img-responsive" src="<?php echo base_url($post['image']) ?>" alt=""/>
                </figure>
				
                <header>
                    <h1><?php echo $post['title'] ?></h1>
                    <div class="data-post">
                        <span class="date-post"><?php echo date('m/d/Y',strtotime($post['created'])) ?></span>
                        <?php if ($post['author_name_first']): ?><span class="author-post"><?php echo trim($post['author_name_first']. " ".$post['author_name_last']); ?></span><?php endif; ?>
                    </div>
                </header>
				
                <?php if ($post['tags']): ?>
					<p>
						<?php $post_tags = explode(',', $post['tags']) ?>
						<?php foreach ($post_tags as $tag): ?>
							<a href="#"><span class="label label-green"><?php echo trim(strtolower($tag)) ?></span></a>
						<?php endforeach ?>
					</p>
				<?php endif ?>
				
                <section class="text-post">
                   <?php echo ($post['content']) ?>
                </section>
				
            </div>
			
            <div class="col-md-offset-1 col-md-3"></div>
			
        </div>
		
    </div>
	
</div>
