<div class="dynamic-content menu-wrapper" id="main-content">



	<div  class="menu padding-wrapper">



		<div class="container animate-in animate-in-fade">



			<div class="row">



				<header>

					<div class="col-md-2">

						<div class="title">

							<h2>Cart</h2>

						</div>

					</div>

				</header>



				<div  class="col-xs-12 col-sm-12 col-md-12">



					<div class="row">



						<div class="col-md-12">



							<div id="cart" class="information-page">



								<div class="row">



									<div class="col-md-12">

										<?php if($this->cart->contents()): ?>



											<?php echo form_open(base_url('cart/update')); ?>



											<table class="table table-bordered" >



												<tr>

													<th width="60%">Item Description</th>

													<th width="10%">QTY</th>

													<th width="10%" style="text-align:right">Item Price</th>

													<th width="10%" style="text-align:right">Sub-Total</th>

													<th width="2%"></th>

												</tr>



												<?php $i = 1; ?>



												<?php foreach ($this->cart->contents() as $items): ?>



													<?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>



													<tr>

														<td>

															<?php echo $items['name']; ?>



															<?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>



																<p>

																	<?php $j=0; foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): $j++;?>

																	<?php if ($j>2): ?>

																		<strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br/>

																	<?php endif ?>

																<?php endforeach; ?>

															</p>



														<?php endif; ?>



													</td>

													<td><?php echo form_input(array('name' => $i.'[qty]', 'value' => $items['qty'], 'maxlength' => '3', 'size' => '3','class'=>'qty','autocomplete'=>'off')); ?></td>

													<td style="text-align:right">$<?php echo $this->cart->format_number($items['price']); ?></td>

													<td style="text-align:right"><strong>$<?php echo $this->cart->format_number($items['subtotal']); ?></strong></td>



													<td style="text-align:right"> <a href="<?php echo base_url('cart/delete/'.$items['rowid']) ?>" class="label label-danger">X</a></td>

												</tr>



												<?php $i++; ?>



											<?php endforeach; ?>



											<tr>

												<td></td>

												<td> <?php echo form_submit(array('class'=>'btn btn-warning btn-sm'), 'Update your Cart'); ?></td>

												<td class="text-right"><strong>Total : </strong></td>

												<td class="text-right"><strong>$<?php echo $this->cart->format_number($this->cart->total()); ?></strong></td>

											</tr>



											<?php   if ($locations): ?>

												<tr>

													<td colspan="3" class="text-right"><strong>Tax : </strong></td>

													<td class="text-right"><span>+</span> <strong>$<?php

														echo  $tax= number_format($this->cart->total()*($locations->tax_rate/100),2);

														?></strong></td>

													</tr>



													<tr>

														<td colspan="3" class="text-right"><strong>Estimated Total : </strong></td>

														<td class="text-right"><strong>$<?php echo $estimated_total=($this->cart->total()+$tax); ?></strong></td>

													</tr>

													<?php ;



													$this->session->set_userdata('locations_tax',array('location_id'=>$locations->id,'tax_rate'=>$tax,'estimated_total'=>$estimated_total));

													?>



												<?php endif ?>





											</table>



											<div class="row">

												<div class="col-md-6"><p><a href="<?php echo base_url('menu') ?>" class="btn btn-success ">< CONTINUE SHOPPING</a></p></div>

												<div class="col-md-6"><p>
													<a class="btn btn-success btn-lg pull-right" href="<?php echo base_url('cart/checkout') ?>" >CHECKOUT</a>

													<!-- <button type="button" class="btn btn-success btn-lg pull-right" data-toggle="modal" data-target="#myModal" data-backdrop="static">CHECKOUT</button> -->



												</p>

											</div>

										</div>



										<?php echo form_close(); ?>



									<?php else: ?>

										<center><h2>Your cart is empty</h2>

											<h3><a href="<?php echo base_url('menu') ?>">Browse the Menu</a> to find something good to eat.</h3></center>



										<?php endif ?>


									</div>


								</div>
							</div>
						</div>







					</div> <!-- col-md-12 -->



				</div>



			</div>



		</div>



	</div>



</div>



</div>



</div>



</div>



</div>



</div>



</div>



</div>



</div>