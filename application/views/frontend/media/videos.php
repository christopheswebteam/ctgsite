<div class="dynamic-content menu-wrapper" id="main-content">

    <div  class="menu padding-wrapper">

        <div class="container animate-in animate-in-fade">

            <div class="row">

                <header>
                  <div class="col-md-3">
                    <div class="title">
                      <h2>Videos</h2>
                    </div>
                  </div>
                </header>

                <div id="video" class="col-xs-12 col-sm-12 col-md-12">

                  <div class="row">

                      <div class="col-md-12">

                          <div class="white-bg0">

                              <div class="row">

                                <div class="col-md-12">
                                  <article class="row">
                                    <div class="col-md-4">
                                       <div class="video_box">
                                          <div> <a class="fancybox-media" href="http://www.youtube.com/watch?v=AcVQ1ohcJ0c"><?php echo get_youtube_thumbnail('AcVQ1ohcJ0c') ?></a></div>
                                          <div>
                                            <h4>How To Cook An Omelette</h4>
                                          </div>
                                       </div> <!-- video_box -->
                                    </div>
                                     <div class="col-md-4">
                                         <div class="video_box">
                                          <div> <a class="fancybox-media" href="http://www.youtube.com/watch?v=0bTp3vvVI9A"><?php echo get_youtube_thumbnail('0bTp3vvVI9A') ?></a></div>
                                           <div>
                                            <h4>How To Saute A Steak</h4>
                                          </div>

                                       </div> <!-- video_box -->
                                    </div>
                                     <div class="col-md-4">
                                        <div class="video_box">
                                          <div> <a class="fancybox-media" href="http://www.youtube.com/watch?v=SxvtVxwKwXQ"><?php echo get_youtube_thumbnail('SxvtVxwKwXQ') ?></a></div>
                                          <div>
                                            <h4>Heritage Sandy Springs Farmers Market</h4>
                                          </div>
                                        </div> <!-- video_box -->
                                    </div>
                                  </article>

                                  <article class="row">
                                    <div class="col-md-4">
                                       <div class="video_box">
                                          <div> <a class="fancybox-media" href="http://www.youtube.com/watch?v=qOSeCM2Q_GU"><?php echo get_youtube_thumbnail('qOSeCM2Q_GU') ?></a></div>
                                          <div>
                                            <h4>How To Sharpen Your Knives</h4>
                                          </div>
                                       </div> <!-- video_box -->
                                    </div>

                                  </article>
                                </div> <!-- col-md-12 -->

                              </div>

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

                </div>

                <br><br>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</div>