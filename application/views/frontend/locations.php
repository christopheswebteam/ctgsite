<div class="dynamic-content menu-wrapper" id="main-content">
    <div id="location_page" class="menu padding-wrapper">
        <div class="container animate-in animate-in-fade">
            <div class="row">

                <header>
                    <div class="col-xs-12 col-sm-12 col-md-3 ">
                        <div class="title">
                            <h2>Locations</h2>
                        </div>
                    </div>
                </header>


                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs tab-location" id="myTab" role="tablist">
                                <li class="active"><a href="#tab_portion" role="tab" data-index="1" data-toggle="tab">All Locations</a></li>
                                <li><a href="#tab_retail" role="tab" data-index="2" data-toggle="tab">Retail</a></li>
                                <li><a href="#tab_cooler" role="tab" data-index="3" data-toggle="tab">Cooler</a></li>
                                <li><a href="#tab_delivery" role="tab" data-index="4" data-toggle="tab">Delivery Area</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
<style>
    .location-lists{
    height: 630px;
    overflow-y: scroll;}
</style>

                        <div class="tab-content">

                            <div class="tab-pane fade in active" id="tab_portion">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div id="map3" class="map-canvas view-location" style="height: 630px; min-height: 300px; margin-bottom:20px;border: 10px solid #ffffff;">MAP</div>

                                    </div>
                                    <div id="location-list" class="col-md-6 location-lists">

                                        <?php if (!empty($front_locations)): ?>
                                            <?php $i = 0; foreach ($front_locations as $row): $i++;?>

                                                <!--   <div class="col-md-6 l1"> -->
                                                <div id="node-location1-<?php echo $row->id ?>" class="location-box-white node-location <?php if ($location_slug == $row->location_slug) echo 'location-highlight1'; ?>">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="location-text">
                                                                <div class="header">

                                                                    <span class="store_title"><strong>  <?php echo $row->name ?></strong></span>
                                                                </div>
                                                                <div class="detail">

                                                                    <p> <?php echo $row->phone_number ?></p>
                                                                    <p><?php echo $row->street_address_1 ?><br> <?php echo $row->city ?>, <?php echo $row->state ?> <?php echo $row->zip_code ?></p>


                                                                    <div class="field field-name-field-location-geofield field-type-geofield field-label-hidden">
                                                                        <div class="field-items">
                                                                            <div class="field-item even"><?php echo $row->latitude ?>, <?php echo $row->longitude ?></div>
                                                                            <div class="field-item2"><?php echo $row->type ?></div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="location-image">

                                                                <?php $image = get_location_images($row->id); if(!empty($image)){   ?>

                                                                <img class="img-rounded img-responsive" width="220px" height="146px" src="<?php echo base_url($image->thumbnail_path); ?>" alt="" >
                                                                <?php }else{ ?>

                                                                <img class="img-rounded img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/locations/brookhaven.jpg" alt="" >
                                                                <?php }  ?>
                                                                <?php if ($row->type=='Retail'): ?>
                                                                <p> Open Daily <br>  <strong><?php echo $row->opening_time ?> to <?php echo $row->closing_time ?></strong></p>
                                                                <?php endif ?>
                                                                 <?php if ($row->type=='Cooler'): ?>
                                                                <p> Pickup available <br> <strong>Mondays, Wednesdays, Fridays</strong> </p>
                                                                <?php endif ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> <!--end location-box-white -->
                                                <!-- </div> -->
                                            <?php endforeach ?>
                                        <?php endif ?>


                                    </div>
                                </div> <!-- location-list1 -->
                            </div>
                            <div class="tab-pane fade" id="tab_retail">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div id="map2" class="map-canvas view-location" style="height: 630px; min-height: 300px; margin-bottom:20px;border: 10px solid #ffffff;">MAP</div>

                                    </div>
                                    <div id="location-list" class="col-md-6 location-lists">
                                        <?php if (!empty($retail)): ?>
                                            <?php $i = 0; foreach ($retail as $row): $i++; ?>
                                                <div class="">
                                                    <div id="node-location2-<?php echo $row->id ?>" class="location-box-white node-location <?php if ($location_slug == $row->location_slug) echo 'location-highlight2'; ?>">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="location-text">
                                                                    <div class="header">
                                                                        <span class="store_title"><strong>  <?php echo $row->name ?></strong></span>
                                                                    </div>
                                                                    <div class="detail">

                                                                        <p> <?php echo $row->phone_number ?></p>
                                                                        <p><?php echo $row->street_address_1 ?><br> <?php echo $row->city ?>, <?php echo $row->state ?> <?php echo $row->zip_code ?></p>


                                                                        <div class="field field-name-field-location-geofield field-type-geofield field-label-hidden">
                                                                            <div class="field-items">
                                                                                <div class="field-item even"><?php echo $row->latitude ?>, <?php echo $row->longitude ?></div>
                                                                                <div class="field-item2"><?php echo $row->type ?></div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="location-image">
                                                                    <?php $image = get_location_images($row->id); if(!empty($image)){   ?>

                                                                <img class="img-rounded img-responsive" width="220px" height="146px" src="<?php echo base_url($image->thumbnail_path); ?>" alt="" >
                                                                <?php }else{ ?>

                                                                <img class="img-rounded img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/locations/brookhaven.jpg" alt="" >
                                                                <?php }  ?>
                                                                    <p> Open Daily <br>  <strong><?php echo $row->opening_time ?> to <?php echo $row->closing_time ?></strong></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> <!--end location-box-white -->
                                                </div>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </div>
                                </div> <!-- location-list2 -->
                            </div>

                            <div class="tab-pane fade" id="tab_cooler">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div id="map3" class="map-canvas view-location" style="height: 630px; min-height: 300px; margin-bottom:20px;border: 10px solid #ffffff;">MAP</div>

                                    </div>
                                    <div id="location-list" class="col-md-6 location-lists">
                                        <?php if (!empty($cooler)): ?>
                                            <?php $i = 0; foreach ($cooler as $row): $i++; ?>

                                                <div class="">
                                                    <div id="node-location3-<?php echo $row->id ?>" class="location-box-white node-location <?php if ($location_slug == $row->location_slug) echo 'location-highlight3'; ?>">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="location-text">
                                                                    <div class="header">

                                                                        <span class="store_title"><strong>  <?php echo $row->name ?></strong></span>
                                                                    </div>
                                                                    <div class="detail">
                                                                        <p> <?php echo $row->phone_number ?></p>
                                                                        <p><?php echo $row->street_address_1 ?><br> <?php echo $row->city ?>, <?php echo $row->state ?> <?php echo $row->zip_code ?></p>


                                                                        <div class="field field-name-field-location-geofield field-type-geofield field-label-hidden">
                                                                            <div class="field-items">
                                                                                <div class="field-item even"><?php echo $row->latitude ?>, <?php echo $row->longitude ?></div>
                                                                                <div class="field-item2"><?php echo $row->type ?></div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="location-image">
                                                                   <?php $image = get_location_images($row->id); if(!empty($image)){   ?>

                                                                <img class="img-rounded img-responsive" width="220px" height="146px" src="<?php echo base_url($image->thumbnail_path); ?>" alt="" >
                                                                <?php }else{ ?>

                                                                <img class="img-rounded img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/locations/brookhaven.jpg" alt="" >
                                                                <?php }  ?>
                                                                    <p> Pickup available<br> <strong>Mondays, Wednesdays, Fridays</strong> </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> <!--end location-box-white -->
                                                </div>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </div>
                                </div> <!-- location-list3 -->
                            </div>

                            <div class="tab-pane fade" id="tab_delivery">
                                <div class="row">
                                    <div id="location-list4" class="col-md-12">

                                        <div class="col-md-12">
                                            <div class="location-box-white ">
                                                <br>
                                                <br>
                                                <center> <h2>Coming Soon</h2></center>
                                            </div> <!--end location-box-white -->
                                        </div>

                                    </div>
                                </div> <!-- location-list4 -->
                            </div>
                        </div>
                        <br>
                        <br>  <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
