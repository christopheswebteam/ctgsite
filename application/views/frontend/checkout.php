<div class="dynamic-content menu-wrapper" id="main-content">

    <div  class="menu padding-wrapper">

        <div class="container animate-in animate-in-fade">

            <div class="row">

                <header>
                    <div class="col-md-2">
                        <div class="title">
                            <h2>Cart</h2>
                        </div>
                    </div>
                </header>

                <div  class="col-xs-12 col-sm-12 col-md-12">

                    <div class="row">

                        <div class="col-md-12">

                            <div id="cart" class="information-page">
                                <?php msg_alert_front() ?>

                                <?php if (!empty($order_error)): ?>

                                    <div class="alert alert-danger">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <?php echo $order_error; ?>

                                    </div>
                                <?php endif ?>

                                <?php if (validation_errors()): ?>

                                    <div class="alert alert-danger">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <p>Please correct the errors in the form below.</p>

                                    </div>
                                <?php endif ?>


                                <form role="form" method="POST" action="<?php echo current_url() ?>" id="checkout-form">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <h3>Customer Information</h3>

                                            <div class="form-group">
                                                <label for="first_name">First Name </label>
                                                <input type="text" class="form-control" name="first_name" placeholder="First Name" value="<?php echo set_value('first_name'); ?>">
                                                <?php echo form_error('first_name'); ?>
                                            </div>
                                            <div class="form-group">
                                                <label for="last_name">Last Name</label>
                                                <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="<?php echo set_value('last_name'); ?>">
                                                <?php echo form_error('last_name'); ?>
                                            </div>

                                            <div class="form-group">
                                                <label for="email_address">E-Mail Address</label>
                                                <input type="email" class="form-control" name="email_address" placeholder="E-Mail Address" value="<?php echo set_value('email_address'); ?>">
                                                <?php echo form_error('email_address'); ?>
                                            </div>

                                            <div class="form-group">
                                                <label for="phone_number">Phone number</label>
                                                <input type="text" class="form-control" name="phone_number" placeholder="Phone number" value="<?php echo set_value('phone_number'); ?>">
                                                <?php echo form_error('phone_number'); ?>
                                            </div>


                                            <div class="form-group">
                                                <label for="street_address_1">Street Address 1</label>
                                                <input type="text" class="form-control" name="street_address_1" placeholder="Street Address 1" value="<?php echo set_value('street_address_1'); ?>">
                                                <?php echo form_error('street_address_1'); ?>
                                            </div>

                                            <div class="form-group">
                                                <label for="street_address_2">Street Address 2</label>
                                                <input type="text" class="form-control" name="street_address_2" placeholder="Street Address 1" value="<?php echo set_value('street_address_2'); ?>">
                                                <?php echo form_error('street_address_2'); ?>
                                            </div>

                                            <div class="form-group">
                                                <label for="city">City</label>
                                                <input type="text" class="form-control" name="city" placeholder="City" value="<?php echo set_value('city'); ?>">
                                                <?php echo form_error('city'); ?>
                                            </div>

                                            <div class="form-group">
                                                <label for="state">State</label>
                                                <select class="form-control" name="state">
                                                    <option value="">- Select State-</option>
                                                    <option value="AK" <?php echo set_select('state', 'AK'); ?>>Alaska</option>
                                                    <option value="AL" <?php echo set_select('state', 'AL'); ?>>Alabama</option>
                                                    <option value="AR" <?php echo set_select('state', 'AR'); ?>>Arkansas</option>
                                                    <option value="AZ" <?php echo set_select('state', 'AZ'); ?>>Arizona</option>
                                                    <option value="CA" <?php echo set_select('state', 'CA'); ?>>California</option>
                                                    <option value="CO" <?php echo set_select('state', 'CO'); ?>>Colorado</option>
                                                    <option value="CT" <?php echo set_select('state', 'CT'); ?>>Connecticut</option>
                                                    <option value="DC" <?php echo set_select('state', 'DC'); ?>>District of Columbia</option>
                                                    <option value="DE" <?php echo set_select('state', 'DE'); ?>>Delaware</option>
                                                    <option value="FL" <?php echo set_select('state', 'FL'); ?>>Florida</option>
                                                    <option value="GA" <?php echo set_select('state', 'GA'); ?>>Georgia</option>
                                                    <option value="HI" <?php echo set_select('state', 'HI'); ?>>Hawaii</option>
                                                    <option value="IA" <?php echo set_select('state', 'IA'); ?>>Iowa</option>
                                                    <option value="ID" <?php echo set_select('state', 'ID'); ?>>Idaho</option>
                                                    <option value="IL" <?php echo set_select('state', 'IL'); ?>>Illinois</option>
                                                    <option value="IN" <?php echo set_select('state', 'IN'); ?>>Indiana</option>
                                                    <option value="KS" <?php echo set_select('state', 'KS'); ?>>Kansas</option>
                                                    <option value="KY" <?php echo set_select('state', 'KY'); ?>>Kentucky</option>
                                                    <option value="LA" <?php echo set_select('state', 'LA'); ?>>Louisiana</option>
                                                    <option value="MA" <?php echo set_select('state', 'MA'); ?>>Massachusetts</option>
                                                    <option value="MD" <?php echo set_select('state', 'MD'); ?>>Maryland</option>
                                                    <option value="ME" <?php echo set_select('state', 'ME'); ?>>Maine</option>
                                                    <option value="MI" <?php echo set_select('state', 'MI'); ?>>Michigan</option>
                                                    <option value="MN" <?php echo set_select('state', 'MN'); ?>>Minnesota</option>
                                                    <option value="MO" <?php echo set_select('state', 'MO'); ?>>Missouri</option>
                                                    <option value="MS" <?php echo set_select('state', 'MS'); ?>>Mississippi</option>
                                                    <option value="MT" <?php echo set_select('state', 'MT'); ?>>Montana</option>
                                                    <option value="NC" <?php echo set_select('state', 'NC'); ?>>North Carolina</option>
                                                    <option value="ND" <?php echo set_select('state', 'ND'); ?>>North Dakota</option>
                                                    <option value="NE" <?php echo set_select('state', 'NE'); ?>>Nebraska</option>
                                                    <option value="NH" <?php echo set_select('state', 'NH'); ?>>New Hampshire</option>
                                                    <option value="NJ" <?php echo set_select('state', 'NJ'); ?>>New Jersey</option>
                                                    <option value="NM" <?php echo set_select('state', 'NM'); ?>>New Mexico</option>
                                                    <option value="NV" <?php echo set_select('state', 'NV'); ?>>Nevada</option>
                                                    <option value="NY" <?php echo set_select('state', 'NY'); ?>>New York</option>
                                                    <option value="OH" <?php echo set_select('state', 'OH'); ?>>Ohio</option>
                                                    <option value="OK" <?php echo set_select('state', 'OK'); ?>>Oklahoma</option>
                                                    <option value="OR" <?php echo set_select('state', 'OR'); ?>>Oregon</option>
                                                    <option value="PA" <?php echo set_select('state', 'PA'); ?>>Pennsylvania</option>
                                                    <option value="PR" <?php echo set_select('state', 'PR'); ?>>Puerto Rico</option>
                                                    <option value="RI" <?php echo set_select('state', 'RI'); ?>>Rhode Island</option>
                                                    <option value="SC" <?php echo set_select('state', 'SC'); ?>>South Carolina</option>
                                                    <option value="SD" <?php echo set_select('state', 'SD'); ?>>South Dakota</option>
                                                    <option value="TN" <?php echo set_select('state', 'TN'); ?>>Tennessee</option>
                                                    <option value="TX" <?php echo set_select('state', 'TX'); ?>>Texas</option>
                                                    <option value="UT" <?php echo set_select('state', 'UT'); ?>>Utah</option>
                                                    <option value="VA" <?php echo set_select('state', 'VA'); ?>>Virginia</option>
                                                    <option value="VT" <?php echo set_select('state', 'VT'); ?>>Vermont</option>
                                                    <option value="WA" <?php echo set_select('state', 'WA'); ?>>Washington</option>
                                                    <option value="WI" <?php echo set_select('state', 'WI'); ?>>Wisconsin</option>
                                                    <option value="WV" <?php echo set_select('state', 'WV'); ?>>West Virginia</option>
                                                    <option value="WY" <?php echo set_select('state', 'WY'); ?>>Wyoming</option>
                                                </select>
                                                <?php echo form_error('state'); ?>
                                            </div>

                                            <div class="form-group">
                                                <label for="zip_code">Zip Code</label>
                                                <input type="text" class="form-control" name="zip_code" placeholder="Zip Code" value="<?php echo set_value('zip_code'); ?>">
                                                <?php echo form_error('zip_code'); ?>
                                            </div>


                                        </div> <!-- col-md-4 -->
                                        <div class="col-md-4">
                                            <h3>Card Information</h3>

                                            <div class="form-group">
                                                <label for="cc_type">Credit Card Type</label>
                                                <select class="form-control" name="cc_type">
                                                    <option value="">Select</option>
                                                    <option value="Visa" <?php if (!empty($_POST['cc_type']) && $_POST['cc_type'] == 'Visa') echo 'selected' ?>>Visa</option>
                                                    <option value="MasterCard" <?php if (!empty($_POST['cc_type']) && $_POST['cc_type'] == 'MasterCard') echo 'selected' ?>>MasterCard</option>
                                                    <option value="Discover" <?php if (!empty($_POST['cc_type']) && $_POST['cc_type'] == 'Discover') echo 'selected' ?>>Discover</option>
                                                    <option value="American Express" <?php if (!empty($_POST['cc_type']) && $_POST['cc_type'] == 'American Express') echo 'selected' ?>>American Express</option>
                                                </select>
                                                <?php echo form_error('cc_type'); ?>
                                            </div>

                                            <div class="form-group">
                                                <label for="cc_number">Credit Card Number</label>
                                                <input type="text" class="form-control" name="cc_number"  placeholder="Credit Card number" value="<?php echo set_value('cc_number'); ?>">
                                                <?php echo form_error('cc_number'); ?>
                                            </div>



                                            <div class="form-group">
                                                <label for="card_year">Expiration Month</label>

                                                <select name="cc_exp_month"  class="form-control" >
                                                    <option value="">Month</option>
                                                    <?php for ($i = 1; $i <= 12; $i++) { ?>

                                                        <option value="<?php echo sprintf('%02d', $i); ?>" <?php if (!empty($_POST['cc_exp_month']) && $_POST['cc_exp_month'] == sprintf('%02d', $i)) echo 'selected' ?>><?php echo sprintf('%02d', $i); ?></option>
                                                    <?php } ?>
                                                </select>
                                                <?php echo form_error('cc_exp_month'); ?>

                                            </div>

                                            <div class="form-group">
                                                <label for="card_year">Expiration Year</label>
                                                <select name="cc_exp_year"  class="form-control">
                                                    <option value="">Year</option>
                                                    <?php
                                                    $year = date('Y');
                                                    for ($i = $year; $i <= 2034; $i++) {
                                                        ?>
                                                        <option value="<?php echo $i ?>" <?php if (!empty($_POST['cc_exp_year']) && $_POST['cc_exp_year'] == $i) echo 'selected' ?>><?php echo $i ?></option>
                                                    <?php } ?>
                                                </select>
                                                <?php echo form_error('cc_exp_year'); ?>

                                            </div>

                                            <div class="form-group">
                                                <label for="cc_cvn">CVN</label>
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <input type="text" class="form-control" name="cc_cvn"  placeholder="CVN" value="<?php echo set_value('cc_cvn'); ?>" >
                                                    </div>
                                                </div>
                                                <?php echo form_error('cc_cvn'); ?>
                                            </div>



                                        </div><!-- col-md-4 -->
                                        <div class="col-md-4">
                                            <h3>Purchase Summary</h3>

                                            <div class="text-left">

                                                <?php if ($this->cart->total_items()): ?>



                                                    <?php if ($this->cart->contents()) { ?>
                                                        <table class="table table-bordered0" >



                                                            <tr>

                                                                <th width="60%">Item Description</th>



                                                                <th width="30%" style="text-align:right">Item Price</th>

                                                                <th width="10%" style="text-align:right">Sub-Total</th>



                                                            </tr>



                                                            <?php $i = 1; ?>



                                                            <?php foreach ($this->cart->contents() as $items): ?>



                                                                <?php echo form_hidden($i . '[rowid]', $items['rowid']); ?>



                                                                <tr>

                                                                    <td>

                                                                        <div style="font-size:16px; font-weight:500"><?php echo $items['name']; ?></div>



                                                                        <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>





                                                                            <?php
                                                                            $j = 0;
                                                                            foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): $j++;
                                                                                ?>

                                                                                <?php if ($j > 2): ?>

                                                                                    * <?php echo $option_name; ?>: <?php echo $option_value; ?><br/>

                                                                                <?php endif ?>

                                                                            <?php endforeach; ?>





                                                                        <?php endif; ?>



                                                                    </td>



                                                                    <td style="text-align:right">$<?php echo $this->cart->format_number($items['price']) . ' X ' . $items['qty']; ?></td>

                                                                    <td style="text-align:right"><strong>$<?php echo $this->cart->format_number($items['subtotal']); ?></strong></td>





                                                                </tr>



                                                                <?php $i++; ?>



                                                            <?php endforeach; ?>



                                                            <tr>

                                                                <td > </td>




                                                                <td class="text-right"><strong>Total : </strong></td>

                                                                <td class="text-right"><strong>$<?php echo $this->cart->format_number($this->cart->total()); ?></strong></td>

                                                            </tr>



                                                            <?php if ($locations): ?>

                                                                <tr>

                                                                    <td colspan="2" class="text-right"><strong>Tax : </strong></td>

                                                                    <td class="text-right"><strong>$<?php
                                                                            echo $tax = number_format($this->cart->total() * ($locations->tax_rate / 100), 2);
                                                                            ?></strong></td>



                                                                </tr>



                                                                <tr>

                                                                    <td colspan="2" class="text-right"><strong>Estimated Total : </strong></td>

                                                                    <td class="text-right"><strong>$<?php echo $estimated_total = ($this->cart->total() + $tax); ?></strong></td>



                                                                </tr>

                                                                <?php $this->session->set_userdata('locations_tax', array('location_id' => $locations->id, 'tax_rate' => $tax, 'estimated_total' => $estimated_total)); ?>



                                                            <?php endif ?>



                                                        </table>


                                                    <?php } ?>

                                                </div>

                                                <div class="location-text">

                                                    <h3>Location:</h3>
                                                    <div class="header">

                                                        <span class="store_title"><strong>  <?php echo $locations->name ?></strong></span>
                                                    </div>
                                                    <div class="detail">

                                                        <p> <?php echo $locations->phone_number ?></p>
                                                        <p><?php echo $locations->street_address_1 ?><br> <?php echo $locations->city ?>, <?php echo $locations->state ?> <?php echo $locations->zip_code ?></p>

                                                        <div class="field-items">
                                                            <div class="field-item2"> Type: <?php echo $locations->type ?></div>
                                                        </div>


                                                    </div>
                                                </div>

                                            <?php endif ?>

                                        </div> <!-- col-md-4 -->


                                    </div>
                                    <div class="row">
                                        <div class="col-md-7 col-md-offset-5">
                                            <br>
                                            <button type="submit" class="btn btn-success btn-lg" id="checkout-button">Place My Order</button>
                                            <br><br>
                                        </div>
                                    </div>
                                </form>
                                <script type="text/javascript">
                                    $('#checkout-form').submit(function(){
                                        $('#checkout-button').hide();
                                    });
                                </script>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

</div>

</div>

</div>

</div>