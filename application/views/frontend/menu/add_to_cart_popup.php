<div class="row">
    <div class="col-md-6">
        <div  class="location_pickup" id="location_pickup_<?php echo $locations->id; ?>_<?php echo $locations->id ?>">

           <!-- <input type="hidden" id="get_locationtype_<?php //echo $locations->id?>" name="get_locationtype_<?php //echo $locations->id?>" value="<?php //echo $locations->type ?>">-->
            <?php //echo $locations->type; ?>
            <strong>  <span class="fa fa-home"></span> <?php echo  $locations->name ?></strong><br>
            <?php echo  $locations->street_address_1 ?>,
            <br>
            <?php echo  $locations->city ?>,
            <?php echo  $locations->state ?>,
            <?php echo  $locations->zip_code ?>
            <br>
            <?php if (!empty($locations->phone_number)): ?>
                <span class="fa fa-phone"></span> <?php echo  $locations->phone_number ?>
            <?php endif ?>
        </div>
    </div>
    <div class="col-md-6">
        <div>
            <p>
                <?php if (!empty($locations->opening_time)): ?>
                    <span class="fa fa-clock-o"></span>
                    <span class="location_time"> Opening Time :</span><?php echo $locations->opening_time ?>
                    <br>
                <?php endif ?>
                <?php if (!empty($locations->closing_time)): ?>
                    <span class="fa fa-clock-o"></span>
                    <span class="location_time">Closing Time :</span><?php echo $locations->closing_time ?></p>
                <?php endif ?>
            </div>
        </div>
    </div>
    <hr>
    <?php  if($this->session->userdata('pickup_info')){
            $pickup_info=$this->session->userdata('pickup_info');
        }else{
            $pickup_info=FALSE;
        }  ?>

     <?php if ( $pickup_info==FALSE): ?>


    <div class="row">
        <div class="col-md-6">
            Pickup Date
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <input type="text" class="form-control pickup_date datepicker"  id="pickup_date"  name="pickup_date" value="" placeholder="YYYY-MM-DD" autocomplete="off" readonly="readonly">
                </div>
            </div>
            <br>
        </div>
    </div>
   <!--  <div class="row">
        <div class="col-md-6">
            Pickup Time
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <select name="pickup_hour" id="pickup_hour" class="form-control">
                        <option value="" selected> HH </option>
                        <?php //for ($i=1; $i<=24; $i++){ ?>
                        <option value="<?php //echo sprintf('%02d',$i);?>"><?php //echo sprintf('%02d',$i);?> </option>
                        <?php //} ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <select name="pickup_minute"  id="pickup_minute" class="form-control">
                        <option value="" selected> MM </option>
                        <?php //for ($i=1; $i<=60; $i++){ ?>
                        <option value="<?php //echo sprintf('%02d',$i);?>"><?php //echo sprintf('%02d',$i);?> </option>
                        <?php //} ?>
                    </select>
                </div>
            </div>
        </div>
    </div> -->



<?php endif ?>
 <?php //echo ($locations_products->pos_total_available); ?>

    <script>
        jQuery(document).ready(function($) {

			<?php if($products->check_inventory):?>
			            $('body').find(".pickup_date").datepicker({
			                dateFormat: 'yy-mm-dd',
			                <?php if(!empty($locations_products->pos_total_available)): ?>
			                minDate: 0,
			                <?php else: ?>
			                minDate: 0,
			                <?php endif; ?>
			                maxDate: '<?php echo $products->end_date ?>' ,
			                <?php if($locations->type=='Cooler'):?>
			                beforeShowDay: function(date) {
			                    var day = date.getDay();
			                    return [day == 1 || day == 3 || day == 5, ""];
			                }
			        		<?php endif; ?>
			    		});

			<?php else: ?>

			 $('body').find(".pickup_date").datepicker({
			                dateFormat: 'yy-mm-dd',
			                minDate: 0,
			                maxDate: '<?php echo $products->end_date ?>' ,
			                <?php if($locations->type=='Cooler'):?>
			                beforeShowDay: function(date) {
			                    var day = date.getDay();
			                    return [day == 1 || day == 3 || day == 5, ""];
			                }
			        		<?php endif; ?>
			    		});

			<?php endif; ?>


        });





  /*  if ($('#frm' + frmsid).find('#pickup_hour').val() == '') {
        msg += "Please select pickup hour. \n";
        flag = false;
    }

    if ($('#frm' + frmsid).find('#pickup_minute').val() == '') {
        msg += "Please select pickup minute. \n";
        flag = false;
    }*/

    <?php //else: ?>

    /*var location_idd='<?php echo $this->session->userdata("active_location_id");?>';
    if(location_id.val()!=location_idd){
          if (confirm("You are trying to change the location, this will empty your cart.\n\n Are you sure want to continue.?")) {
            //$('body').find('#frm' + frmsid).submit();
          }else{
            return false;
          }
    }*/

    <?php //endif; ?>



/*if (flag == false) {
    alert(msg);
    return false;
} else {
    //$('body').find('#frm' + frmsid).submit();
}*/



</script>