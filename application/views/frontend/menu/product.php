
<div id="content-wrapper">
    <div class="dynamic-content container " id="main-content">
        <div class="padding-wrapper">
            <div class="row">



                <div class="col-xs-12 col-sm-12 col-md-12">

<br><br>
  <div class="container-detail1 row">
        <div class="col-md-12">
           <!--  <div class="box-grey">
               <div class="row">
                   <div class="col-md-12">
                      <div class="row">
                          <div class="col-md-2">
                           <p>Pick Up Location:</p>
                          </div>
                          <div class="col-md-3">
                         <form  role="form" action="#" method="post" accept-charset="utf-8">
                               <select id="product_detail_location" name="location" class="form-control">
                                 <option value="">Select</option>
                               <?php //if (!empty($locations)): ?>

                                   <?php //foreach ($locations as $row): ?>
                                   <option value="<?php //echo $row->id ?>"><?php //echo $row->name ?></option>
                                   <?php //endforeach ?>
                               <?php //else: ?>
                                    <option value="">NO Location Available</option>
                               <?php //endif ?>
                               </select>
                             </form>
                          </div>
                          <div class="col-md-4">
                          <p>123 main street,
                           Alpharetta, GA 12345</p>
                           <p>
                              <?php //if (!empty($locations)): ?>
                                   <?php// $ad=0; foreach ($locations as $row): $ad++; ?>
                                     <span class="loc-address-<?php //echo $row->id?> location_ad" style="display:<?php //if($ad>1) echo 'none'; else echo 'block';?>">
                                      <?php //echo $row->street_address_1 ?>,
                                      <?php //echo $row->city ?>,
                                      <?php //echo $row->state ?>,
                                      <?php //echo $row->zip_code ?> </span>
                                   <?php //endforeach ?>
                               <?php //endif ?>
                           </p>
                          </div>
                          <div class="col-md-3">
                          <p>770-777-123</p>
                           <p>
                               <?php //if (!empty($locations)): ?>
                                   <?php //$ph=0; foreach ($locations as $row): $ph++; ?>
                                     <span class="loc-address-<?php //echo $row->id?> location_ad" style="display:<?php //if($ph>1) echo 'none'; else echo 'block';?>">
                                      <?php //echo $row->phone_number ?> </span>
                                   <?php// endforeach ?>
                               <?php //endif ?>

                           </p>
                          </div>

                      </div>
                   </div>
               </div>

           </div> -->

        </div> <!-- col-md-3 -->

    </div>
<div class="information-page container-detail2">

    <div class="row">
        <section class="col-md-8">

                <div class="row">
                    <div class="col-md-12">
                        <?php if (!empty($products[0]->products_photos)): ?>
                        <?php if (file_exists($products[0]->products_photos[0]->path)): ?>
                        <img class="img-rounded img-responsive" src="<?php echo base_url($products[0]->products_photos[0]->path); ?>" title="<?php echo $products[0]->products_photos[0]->title; ?>" alt="<?php echo $products[0]->products_photos[0]->alt_text; ?>" >
                        <?php else: ?>
                              <img class="img-rounded img-responsive" src="<?php echo base_url('assets/frontend/img/coming_soon_detail.jpg'); ?>" alt="" >

                        <?php endif ?>
                    <?php else: ?>
                        <img class="img-rounded img-responsive" src="<?php echo base_url('assets/frontend/img/coming_soon_detail.jpg'); ?>" alt="" >
                        <?php endif ?>
                        <br>
                    </div>
                </div>

        </section>
         <section class="col-md-4">
          <div class="">
            <div class="">

                <div class="box">
                    <div class="item-title">
                        <h4> <?php echo $products[0]->name; ?> </h4>
                    </div>

                </div>

                <div class="box">
                    <p><?php echo $products[0]->description; ?></p>

                </div>
                 <div class="box">

                 <?php if(!empty($products[0]->tags_association)): ?>
                <?php foreach ($products[0]->tags_association as $row): ?>
                        <span class="label label-success"><?php echo $row->tag ?></span>&nbsp;
                <?php endforeach ?>
                 <?php endif ?>


                </div>
                <div class="box">

                <?php  if($this->session->userdata('pickup_info')){
                    $pickup_info1=$this->session->userdata('pickup_info');
                }else{
                    $pickup_info1=FALSE;
                } ?>


                    <div class="cart-table">
                        <table class="table0" width="100%" cellpadding="5" cellspacing="5">
                        <?php if (!empty($products[0]->products_portions)): ?>
                        <?php $i=1; foreach ($products[0]->products_portions as $row1): ?>
                        <tr>
                            <td><?php  echo $row1->name ?></td>
                            <td>[<?php echo $row1->weight ?>oz]</td>
                            <td>$<?php echo $row1->price ?></td>
                            <td><span class="cart-link"><i class="fa fa-plus-square"></i> <a class="add_to_cart" onclick="return false" href="#" data-title="<?php echo base_url("cart/add/".$row1->products_id."/".$row1->portions_id)?>"  data-toggle="modal" data-target="#myModal<?php echo $row1->id ?>" data-backdrop="static"> Add to cart</a></span>


                        <div class="modal fade" id="myModal<?php echo $row1->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel"> <?php echo $products[0]->name; ?></h4>
                              </div>
                              <div class="modal-body">
                             <form id="frm<?php echo $row1->id ?>" action="<?php echo base_url() ?>cart/add/<?php echo $products[0]->id;?>" method="post"  accept-charset="utf-8">

                                <table width="100%">
                                <tr>
                                  <td width="30%"><?php  echo $row1->name ?></td>
                                  <td width="30%">[<?php echo $row1->weight ?>oz]</td>
                                  <td width="40%" align="right">$<?php echo $row1->price ?></td>
                                </tr>
                                <tr>
                                  <td colspan="3"><hr>
                                    <input type="hidden" class="portion_id" name="portion_id" value="<?php echo $row1->portions_id;  ?>">

                                  </td>

                                </tr>
                                <?php if ($pickup_info1==FALSE): ?>
                          <tr>
                          <td>Pickup Location</td>

                          <td colspan="2">
                            <select  name="location_id" id="location_<?php echo $row1->id; ?>" class="location_products form-control" onchange="change_location(this.value,'<?php echo $products[0]->id; ?>','<?php echo $row1->id ?>');">
                              <option value="0" selected="selected">Select</option>

                            <?php if (!empty($locations)): ?>
                                <?php foreach ($locations as $row2): ?>
                                <option value="<?php echo $row2['id'] ?>"><?php echo $row2['name'] ?></option>
                                <?php endforeach ?>
                            <?php else: ?>
                                 <option value="">NO Location Available</option>
                            <?php endif ?>
                            </select>

                          <!--   <input type="hidden" id="get_location_id" name="get_location_id" value="0"> -->
                          <!--  <input type="hidden" id="get_location_type" name="get_location_type" value="0"> -->

                          </td>
                          </tr>
                            <?php else: ?>

                              <tr>
                          <td colspan="2">
                          <!-- If you want to change the another location please select here -->



                           <!--  <select  name="location_id" id="location_<?php //echo $row1->id; ?>" class="location_products form-control" onchange="change_location(this.value,'<?php //echo $products[0]->id; ?>','<?php //echo $row1->id ?>');">
                             <option value="0">Select</option>

                           <?php //if (!empty($locations)): ?>
                               <?php //foreach ($locations as $row2): ?>
                               <option value="<?php //echo $row2->id ?>" <?php //if($this->session->userdata('active_location_id')==$row2->id ){ echo 'selected="selected"'; } ?>><?php //echo $row2->name ?></option>
                               <?php //endforeach ?>
                           <?php //else: ?>
                                <option value="">NO Location Available</option>
                           <?php //endif ?>
                           </select> -->

                            <!-- <input type="hidden" id="location_id" name="location_id" value="<?php //echo $this->session->userdata('active_location_id'); ?>">-->

                               <input type="hidden" name="location_id" id="location_<?php echo $row1->id; ?>" class="location_products form-control" value="<?php echo $this->session->userdata('active_location_id'); ?>">
                               	 <input type="hidden" class="form-control pickup_date datepicker"  id="pickup_date"  name="pickup_date"  placeholder="YYYY-MM-DD" autocomplete="off" readonly="readonly" value="<?php echo $pickup_info1['order_pickup_date'] ?>">


                          </td>
                          </tr>


                              <?php endif ?>
                              <tr>
                                <td colspan="3" id="location_data_<?php echo $row1->id ?>">
                                   <!--  <h3><center>Please select location.</center></h3> -->
                                </td>
                              </tr>


                              </table>



                              <?php if ($pickup_info1): ?>

                                <!--<div class="modal-footer" >
                                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                 <button type="button" onclick="return check_add_to_cart2('<?php //echo $row1->portions_id ?>','<?php //echo $products[0]->id?>','<?php //echo $this->session->userdata('active_location_id'); ?>','<?php //echo $row1->id ?>');" date-title=""  class="btn btn-success">Add to cart</button>
                                 </div>-->

                                  <div class="modal-footer" style="display:block;">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>


                                <button type="button" onclick="return check_add_to_cart('<?php echo $row1->id ?>','<?php echo $row1->portions_id ?>','<?php echo $products[0]->id?>','<?php if ($products[0]->check_inventory) echo "0"; else echo "2"; ?>','<?php echo $products[0]->check_inventory?>');" date-title=""  class="btn btn-success">Add to cart</button>


                                <?php else: ?>

                               <div class="modal-footer" style="display:block;">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>


                                <button type="button" onclick="return check_add_to_cart('<?php echo $row1->id ?>','<?php echo $row1->portions_id ?>','<?php echo $products[0]->id?>','<?php if ($products[0]->check_inventory) echo "1"; else echo "2"; ?>','<?php echo $products[0]->check_inventory?>');" date-title=""  class="btn btn-success">Add to cart</button>

                                  </div>
                              <?php endif ?>

                             </form>

                              </div>

                            </div>
                          </div>
                        </div>

                            </td>
                        </tr>
                            <?php $i++; endforeach ?>
                        <?php endif ?>
                    </table>

                    </div>


                </div>

            </div>
             </div> <!-- box -->


        </section>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="box" style="background-color:#FFF">
                <!--<strong>NUTRITIONAL FACTS</strong>-->
                <br>
               <?php if (file_exists($products[0]->nutrition_facts_file_thumb)): ?>
                    <a class="fancybox" href="<?php echo base_url($products[0]->nutrition_facts_file)?>"> <img class="img-rounded img-responsive" src="<?php echo base_url($products[0]->nutrition_facts_file)?>" alt="" style="border:0px"></a>
                  <?php else: ?>
                   <div style="color:#003f22; text-align:center;font-size:18px;"> Nutrition Facts <br>Coming Soon</div>
                <?php endif ?>

            </div>

        </div> <!-- col-md-3 -->
        <div class="col-md-3">
            <div class="box">
            <strong>INGREDIENTS</strong>
            <br>
            <p>
            <?php if (!empty($products[0]->products_ingredients)): ?>
                <?php foreach ($products[0]->products_ingredients as $row): ?>
                  <?php $ingredients[] = $row->name; ?>
                <?php endforeach ?>
                <?php echo implode(', ', $ingredients); ?>
            <?php endif; ?>
             </p>
            </div>

        </div><!-- col-md-3 -->
        <div class="col-md-3">
            <div class="box">
            <strong>PREPARATION INSTRUCTIONS</strong>
            <br>
            <p><?php echo nl2br($products[0]->preparation_instructions); ?></p>
            </div>

        </div><!-- col-md-3 -->
        <div class="col-md-3">
            <div class="box">
            <strong>ADDITIONAL INFORAMTION</strong>
            <p><?php echo nl2br($products[0]->additional_information); ?></p>
            </div>
        </div><!-- col-md-3 -->
    </div>

      <div class="row">
        <div class="col-md-12">
            <div class="box1">
                <div class="row">
                    <div class="col-md-12">
                       <div class="bottom-navigation">

<div class="row">

	<div class="col-xs-12 col-sm-12  col-md-4">

		<div class="previous-btn">
			<?php if (!empty($position_products['previous_product'])): ?>
				<a href="<?php echo site_url('menu/product/'.$position_products['previous_product']['slug'].'/'.$position_products['previous_product']['id']); ?>" class="btn btn-success"><i class="fa fa-arrow-left"></i> Previous Product</a>
			<?php else: ?>
				<a href="#" class="btn btn-success" disabled="disabled"><i class="fa fa-arrow-left"></i> Previous Product</a>
			<?php endif ?>
		</div>

	</div>

	<div class="col-xs-12 col-sm-12 col-md-4">

		<div class="cancel-btn"><a href="<?php echo site_url($cancel_link); ?>" class="btn btn-danger">Cancel</a></div>

	</div>

	<div class="col-xs-12 col-sm-12  col-md-4">

		<div class="next-btn">
			<?php if (!empty($position_products['next_product'])): ?>
				<a href="<?php echo site_url('menu/product/'.$position_products['next_product']['slug'].'/'.$position_products['next_product']['id']); ?>" class="btn btn-success"><i class="fa fa-arrow-right"></i> Next Product</a>
			<?php else: ?>
				<a href="#" class="btn btn-success" disabled="disabled"><i class="fa fa-arrow-right"></i> Next Product</a>
			<?php endif ?>
		</div>

	</div>

</div>






                       </div>
                    </div>
                </div>

            </div>

        </div> <!-- col-md-3 -->

    </div>



</div>


                </div>

            </div>
        </div>
    </div>
</div>