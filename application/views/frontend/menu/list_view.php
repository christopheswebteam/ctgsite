<article class="row">
    <?php if (!empty($products)): ?>
        <?php $p=0;foreach ($products as $row): $p++;?>
        <div class="col-md-6 ">
            <div class="product list-product">
                <div class="row">
                    <div  class=" menu-grid-cell col-md-12">
                        <a href="<?php echo base_url('menu/product/'.$row->slug."/".$row->id) ?>" title=""><h4><?php echo text_limit($row->name,50) ?></h4></a>
                        <p class="detail"><?php echo text_limit($row->description,180) ?></p>
                    </div>
<!--                    <div class="col-md-12 product-description">
                        <div class="row">
                            <div class="col-md-12">
              
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
        <?php if ($p%2==0): ?>
        </article>
        <article class="row">
        <?php endif ?>
    <?php endforeach ?>
<?php endif ?>
</article>