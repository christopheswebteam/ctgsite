<article class="row" id="offset_id<?php echo $offset + $per_page ?>">
    <?php if (!empty($products)): ?>
        <?php
        $p = 0;
        foreach ($products as $row): $p++;
            ?>
            <div class="menu-grid-cell col-md-4 ">
                <div class="product">
                    <div class="product-image" class="overflow: hidden; postion: relative;">
                        <a class="fancybox"  href="<?php echo base_url('menu/product/' . $row->slug . "/" . $row->id) ?>">
                            <div class="scale-box">
                                <?php if (!empty($row->products_photos[0]->thumbnail_path) && file_exists($row->products_photos[0]->thumbnail_path)): ?>
                                    <img src="<?php echo base_url($row->products_photos[0]->thumbnail_path); ?>" alt="" class="scale-image" onload="onImageLoad(event);">

                                <?php else: ?>
                                    <img src="<?php echo base_url('assets/frontend/img/coming_soon_menu.jpg'); ?>" alt="">


                                <?php endif ?>
                            </div>
                        </a>
                    </div>
                    <div class="product-description">
                        <div class="row">
                            <div class="col-md-12 ">
                                <h4><a class="fancybox0"  href="<?php echo base_url('menu/product/' . $row->slug . "/" . $row->id) ?>"><?php echo text_limit($row->name, 30) ?></a></h4>
                                <?php //echo $row->sort    ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php if ($p % 3 == 0): ?>
            </article>
            <article  class="row">
            <?php endif ?>

        <?php endforeach ?>

    <?php endif ?>
</article>