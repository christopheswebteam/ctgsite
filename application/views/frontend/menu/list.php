<div id="content-wrapper">
    <div class="dynamic-content container" id="main-content">
        <div class="padding-wrapper">
            <div class="row">
                <br>
                <div  class="col-xs-12 col-sm-12 col-md-12 white-bg">
                    <section class="row">
                        <div class="col-md-2 ">
                            <div class="row">
                                <!-- <div class="col-xs-2">
                                    <input type="text" class="form-control" id="fliter_search" placeholder="Search">
                                 </div> -->
                                <div class="col-xs-1">
                                    <button id="filter_search" type="button" class="btn btn-info"> <i class="fa fa-search"></i> Filter & Search</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-4 ">
                            <form class="form-horizontal" method="get" role="form">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Sort by </label>
                                    <div class="col-sm-9">
                                        <select name="sort_by" class="form-control" onchange="this.form.submit()">
                                            <option value="" selected>Select</option>
                                            <option value="price-lowest-first" <?php if (!empty($products_search['sort_by']) && $products_search['sort_by'] == 'price-lowest-first') echo 'selected'; ?>>Price: Lowest first</option>
                                            <option value="price-highest-first" <?php if (!empty($products_search['sort_by']) && $products_search['sort_by'] == 'price-highest-first') echo 'selected'; ?>>Price: Highest first</option>
                                            <!--  <option value="colories-lowest-first">Colories: Lowest first</option>
                                            <option value="colories-highest-first">Colories: Highest first</option> -->
                                            <option value="size-smallest-first" <?php if (!empty($products_search['sort_by']) && $products_search['sort_by'] == 'size-smallest-first') echo 'selected'; ?> >Size: Smallest first</option>
                                            <option value="size-largest-first" <?php if (!empty($products_search['sort_by']) && $products_search['sort_by'] == 'size-largest-first') echo 'selected'; ?>>Size: largest first</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-2 ">
                            <div class="menu_view_grid pull-right">
                                <!-- <button class="btn btn-info"><i class="fa fa-align-justify"></i> <span>List View</span></button> -->
                                <button id="grid_btn" class="btn btn-info disabled" ><i class="fa fa-th"></i></button>
                                <button id="list_btn" class="btn btn-info " ><i class="fa fa-list"></i></button>
                            </div>
                        </div>
                    </section>
                </div>
                <?php
                if (isset($products_search['query']) || isset($products_search['price_min']) || isset($products_search['price_min']) || isset($products_search['price_max']) || isset($products_search['item_type'])) {
                    $search_tab_open = TRUE;
                } else {
                    $search_tab_open = FALSE;
                }
                ?>
                <div class="col-xs-12 col-sm-12 col-md-12 ">
                    <aside id="menu_sidebar"  class="col-md-3 filter" style="display:<?php echo ($search_tab_open) ? 'block' : 'none'; ?>;">
                        <form id="filter_search_form" action="" method="get" accept-charset="utf-8" role="form">

                            <div class="pull-right filter-close-button"> <a href="<?php echo site_url('menu/clear_search'); ?>" data-djax-exclude="true"><span class="label label-danger fa fa-times"> Clear All</span></a></div>

                            <div class="search-filter-section">

                                <h4>Search</h4>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <button class="btn btn-success" type="submit"><i class="fa fa-search"></i></button>
                                    </span>
                                    <input type="text" class="form-control" id="query" name="query" value="<?php if (!empty($products_search['query'])) echo trim($products_search['query']); ?>" placeholder="Search" autocomplete="off">
                                </div>
                            </div>
                            <div class="search-filter-section">
                                <h4>Price</h4>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" id="price_min" name="price_min" value="<?php if (!empty($products_search['price_min'])) echo trim($products_search['price_min']); ?>" placeholder="Min" autocomplete="off">
                                        <!-- <span class="" for="price-min">Min</span> -->
                                    </div>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" id="price_max" name="price_max" value="<?php if (!empty($products_search['price_max'])) echo trim($products_search['price_max']); ?>" placeholder="Max" autocomplete="off">
                                       <!--  <span class="" for="price-min">Max</span> -->
                                    </div>
                                </div>
                            </div>
                            <div class="search-filter-section">
                                <h4>Size</h4>
                                <ul class="filter-options" >
                                    <?php if (!empty($portions)): ?>
                                        <?php foreach ($portions as $row): ?>
                                            <li><span> <input type="checkbox" name="size[]" value="<?php echo $row->id ?>" <?php if (!empty($products_search['size']) && in_array($row->id, $products_search['size'])) echo 'checked'; ?>> <?php echo $row->name ?> </span></li>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </ul>
                            </div>
                            <!-- <div class="box1">
                                 <div class="col-md-12 ">
                                     <h4>Item Type</h4>
                                     <ul class="nav" >
                            <?php //if (!empty($product_item_types)):  ?>
                            <?php //foreach ($product_item_types as $row):    ?>
                                             <li><span> <input type="checkbox" name="item_type[]" value="<?php //echo $row->id                         ?>" <?php //if(!empty($products_search['item_type']) && in_array($row->id,$products_search['item_type'])) echo 'checked';                        ?>> <?php //echo $row->name                         ?> </span></li>
                            <?php //endforeach   ?>
                            <?php //endif    ?>
                                     </ul>
                                 </div>
                             </div> -->
                            <div class="search-filter-section">
                                <h4>Product Tags</h4>
                                <ul class="filter-options">
                                    <?php
                                    $categories = array(
                                        '1' => array('title' => 'Feature', 'marked' => false),
                                        '2' => array('title' => 'Dietary', 'marked' => false),
                                        '3' => array('title' => 'Protein', 'marked' => false),
                                        '4' => array('title' => 'Other', 'marked' => false),
                                    );
                                    ?>
                                    <?php if (!empty($product_tags)): ?>
                                        <?php foreach ($product_tags as $row): ?>
                                            <?php if (array_key_exists('' . $row->category, $categories)): ?>
                                                <?php if (!$categories['' . $row->category]['marked']): ?>
                                                    <?php $categories['' . $row->category]['marked'] = true; ?>
                                                    <li class="filter-options-header"> <?php echo $categories['' . $row->category]['title']; ?> </li>                                                    
                                                <?php endif; ?>
                                                <li><span> <input type="checkbox" name="product_tag[]" value="<?php echo $row->id ?>" <?php if (!empty($products_search['product_tag']) && in_array($row->id, $products_search['product_tag'])) echo 'checked'; ?>> <?php echo $row->tag ?> </span></li>
                                            <?php endif; ?>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </ul>
                            </div>
                        </form>
                    </aside> <!-- menu_sidebar -->
                    <section id="menu_panel" class="col-md-<?php echo ($search_tab_open) ? '9' : '12'; ?>">
                        <?php if (!empty($products)): ?>
                            <input type="hidden" id="grid_view_offset" name="grid_view_offset" value="0">
                            <input type="hidden" id="list_view_offset" name="list_view_offset" value="0">
                            <div id="grid_view" >
                                <article class="row">
                                    <?php $p = 0; ?>
                                    <?php foreach ($products as $row): $p++; ?>
                                        <div class="menu-grid-cell  col-md-4 ">
                                            <div class="product">
                                                <div class="product-image">
                                                    <a class="fancybox"  href="<?php echo base_url('menu/product/' . $row->slug . "/" . $row->id) ?>">
                                                        <div class="scale-box">
                                                            <?php if (!empty($row->products_photos[0]->thumbnail_path) && file_exists($row->products_photos[0]->thumbnail_path)): ?>
                                                                <img src="<?php echo base_url($row->products_photos[0]->thumbnail_path); ?>" alt=""  class="scale-image" onload="onImageLoad(event);"/>
                                                            <?php else: ?>
                                                                <img src="<?php echo base_url('assets/frontend/img/coming_soon_menu.jpg'); ?>" alt=""/>
                                                            <?php endif ?>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="product-description">
                                                    <div class="row">
                                                        <div class="col-md-12 ">
                                                            <h4><a class="fancybox" onclick="return false" href="<?php echo site_url('menu/product/'.$row->slug.'/'.$row->id) ?>"><?php echo text_limit($row->name, 30) ?></a></h4>
                                                            <?php //echo $row->sort     ?>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if ($p % 3 == 0): ?>
                                            <?php echo '</article> <article  class="row">'; ?>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                </article>
                            </div>	 <!-- grid_view -->
                            <div id="list_view" style="display:none;">
                                <article class="row">
                                    <?php
                                    $p = 0;
                                    foreach ($products as $row): $p++;
                                        ?>
                                        <div class="menu-grid-cell col-md-6 ">
                                            <div class="product list-product">
                                                <div class="row">
                                                    <div  class="col-md-12">
                                                        <h4><a href="<?php echo site_url('menu/product/'.$row->slug ."/".$row->id); ?>" title=""><?php echo text_limit($row->name, 50) ?></a></h4>
                                                        <p class="detail"><?php echo text_limit($row->description, 180) ?></p>
                                                    </div>
<!--                                                    <div class="col-md-12 product-description">
                                                        <div class="row">
                                                            <div class="col-md-12">

                                                            </div>
                                                        </div>
                                                    </div>-->
                                                </div>
                                            </div>
                                        </div>
                                        <?php if ($p % 2 == 0): ?>
                                            <?php echo '</article> <article  class="row">'; ?>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                </article>
                            </div> <!-- list_view -->
                            <div id="loadmoreajaxloader" style="display:none;" class="white-bg"><center><img src="<?php echo base_url(); ?>assets/frontend/img/ajax-loader.gif" title="Ajax-loader" style="border:1px;"/></center></div>
                        <?php else: ?>
                            <div class="col-xs-12 col-sm-12 col-md-12 white-bg">
                                <?php if ($this->session->userdata('active_location_id')): ?>
                                    <center><h2>NO menu item found belong to that location.</h2></center>
                                <?php else: ?>
                                    <center><h2>NO menu item found.</h2></center>
                                <?php endif ?>
                            </div>
                        <?php endif ?>
                    </section> <!-- menu_panel -->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="mySmallModalLabel">Message</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-success" role="alert"><p>This item has been added to your order.</p></div>
                <div>
                    <center><h3>Would you like to keep shopping or checkout?</h3></center>
                    <br>
                    <br>
                </div>
                <div class="row">
                    <div class="col-md-6"><p><a href="<?php echo base_url('menu') ?>" class="btn btn-success btn-lg ">  CONTINUE SHOPPING</a></p></div>
                    <div class="col-md-6"><p>
                            <a href="<?php echo base_url('cart/checkout') ?>" class="btn btn-success btn-lg pull-right">CHECKOUT</a>
                        </p>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
