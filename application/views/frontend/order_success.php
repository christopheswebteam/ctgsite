<div class="dynamic-content menu-wrapper" id="main-content">

	<div  class="menu padding-wrapper">

		<div class="container animate-in animate-in-fade">

			<div class="row">
				
				<div  class="col-xs-12 col-sm-12 col-md-12">

					<div class="row">

						<div class="col-md-12">

							<div  class="information-page">

								<div class="row">

									<div class="col-md-12">

										<br />
										<br />
										<br />
										<br />

										<center>
											
											<?php if ((!empty($pickup_info['order_pickup_date']) && $pickup_info['order_pickup_date'] == date('Y-m-d')) || ((strtotime(date('h:i A')) > strtotime('12:00 PM')) && ($pickup_info['order_pickup_date'] == date('Y-m-d', strtotime('+1 day'))))): ?>

												<h3>Thank you for your order! Your order has been received. <br> Please check your email to see when your order will be ready for pickup.</h3>

											<?php else: ?>

												<h3>
													Thank you for your order! Your order will be ready for pickup on <?php echo date('m/d/Y', strtotime($pickup_info['order_pickup_date'])); ?> at our <?php echo $location->name; ?> location.<br />
													<br />
													<?php echo trim($location->street_address_1." ".$location->street_address_2); ?><br />
													<?php echo trim($location->city.", ".$location->state." ".$location->zip_code); ?><br />
													<br />
													<?php echo $location->phone_number; ?>
												</h3>

											<?php endif ?>
											
											<br />
											<br />
											
											<p><em>Please be sure to check your spam folder for order updates and confirmation emails.</em></p>
										
										</center>
										
										<br />
										<br />
										<br />
										<br />
										
										<div class="row">
											<div class="col-md-6 col-md-offset-4">
												<p><a href="<?php echo base_url('menu') ?>" class="btn btn-success btn-lg"><< CONTINUE MORE SHOPPING</a></p>
											</div>
										</div>
										
										<br />
										<br />
										<br />
										<br />
										<br />
										<br />
										<br />
										<br />
										
									</div>
								
								</div>
								
							</div>
							
						</div>
						
					</div>
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
</div>