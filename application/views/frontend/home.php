<div class="dynamic-content container" id="main-content">

    <div class="promo-box">
        <p class="title0">Gourmet meals at home.<br />No hassle.</p>
        <p class="subtitle">It's better than having your own personal chef.</p>
    </div>

</div>