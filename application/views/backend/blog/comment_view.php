<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-file"></i>Blog Comment View
        </div>
        <div class="tools">
            <a href="" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body form">

        <div class="form-horizontal">

        <div class="form-body">
            <div class="form-group">
                <label class="col-md-3 control-label"> Author Name </label>
                <div class="col-md-8" style="margin-top:7px;">


                  <?php if (!empty($blog_comment->comment_author)) echo $blog_comment->comment_author;   ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label"> Author Email </label>
                <div class="col-md-8" style="margin-top:7px;">
                  <?php if (!empty($blog_comment->comment_author_email)) echo $blog_comment->comment_author_email;  ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label"> Author Comment </label>
                <div class="col-md-8" style="margin-top:7px;">
                  <?php if (!empty($blog_comment->comment_content)) echo $blog_comment->comment_content;   ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label"> Comment Date </label>
                <div class="col-md-8" style="margin-top:7px;">
                  <?php if (!empty($blog_comment->comment_date))  echo date('Y-m-d', strtotime($blog_comment->comment_date));  ?>
                </div>
            </div>

        </div>
        <div class="form-actions">
            <a href="<?php echo base_url() . 'backend/blog/comments'; ?>" >
              <button class="btn btn-primary" type="button"> Back To Comments</button>
            </a>
        </div>
        </div>
    </div>
</div>