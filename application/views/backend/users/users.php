<div class="row">
    <div class="col-md-12 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-users"></i>Users
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>

                                <th>Email</th>
                                <th>Status</th>
                                <th>Created</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            if (!empty($users)):
                                $i = $offset; foreach ($users as $row): $i++;

                                    ?>

                                    <tr>
                                        <td><?php echo $i ?>.</td>
                                        <td><?php echo $row->first_name.' '.$row->last_name; ?></td>

                                        <td><?php echo $row->email; ?></td>
                                        <td><?php if ($row->status == 0) { ?>
                                                <a class="label label-sm label-warning" href="<?php echo base_url('backend/users/change_status/' . $row->id . '/' . $row->status . '/' . $offset) ?>/users" > Inactive </a>
                                            <?php } if ($row->status == 1) { ?>
                                                <a class="label label-sm label-success"  href="<?php echo base_url('backend/users/change_status/' . $row->id . '/' . $row->status . '/' . $offset) ?>" > Active </a>
                                            <?php } if ($row->status == 2) {

                                                ?>Deactive <?php } if ($row->status == 3) {

                                                ?>
                                                <a class="label label-sm label-danger" href="<?php echo base_url('backend/users/change_status/' . $row->id . '/' . $row->status . '/' . $offset) ?>/users" > Banned </a>
                                            <?php } ?></td>
                                        <td><?php echo $row->created ?></td>
                                        <td>

                                            <a href="<?php echo base_url() . 'backend/users/edit/' . $row->id . '/' . $offset ?>"  class="btn btn-success btn-xs" rel="tooltip" data-placement="left" data-original-title=" Edit ">
                                                <i class="icon-pencil"></i>
                                            </a>
                                            <a href="<?php echo base_url() . 'backend/users/user_delete/' . $row->id . '/' . $offset ?>" class="btn btn-danger btn-xs" rel="tooltip" rel="tooltip" data-placement="bottom" data-original-title="Remove" onclick="return confirm('Are you sure want to delete?');" >
                                                <i class="icon-trash "></i></a>
                                                <!--   <a href="<?php //echo base_url().'backend/user_view/'.$row->id    ?>/<?php //echo $offset     ?>/users" class="btn btn-success btn-xs"><i class="icon-ok"></i></a> -->


                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <th colspan="6"> <center>No users found.</center></th>

                            </tr>

                        <?php endif; ?>
                        </tbody>
                    </table>

                    <div class="text-right">
                        <?php if (!empty($pagination)) echo $pagination; ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

        <!-- END SAMPLE FORM PORTLET-->
    </div>

</div>