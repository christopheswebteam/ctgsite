<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-file"></i>Add Edit
        </div>
        <div class="tools">
            <a href="" class="collapse">
            </a>
        </div>
    </div>

    <div class="portlet-body form">
        <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal')); ?>
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-3 control-label">First Name</label>
                <div class="col-md-8">
                    <input type="text" placeholder="First Name" class="form-control" name="first_name" value="<?php if(!empty($user->first_name)) echo $user->first_name; ?>">
                    <?php echo form_error('first_name'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Last Name</label>
                <div class="col-md-8">
                   
                    <input type="text" placeholder="Last Name" class="form-control" name="last_name" value="<?php if(!empty($user->last_name)) echo $user->last_name; ?>">
                        <?php echo form_error('last_name'); ?>
                   
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Email</label>
                <div class="col-md-8">
                    
                    <input type="text" placeholder="Email" class="form-control" name="email" value="<?php if(!empty($user->email)) echo $user->email; ?>">
                    <?php echo form_error('email'); ?>                        
                    
                </div>
            </div>
          
              
        </div>

        <div class="form-actions">
            <button type="submit" class="btn blue">Submit</button>
            <a href="<?php echo base_url() . 'backend/users'; ?>" ><button class="btn btn-danger" type="button">Cancel</button></a>      </div>
        </form>

    </div>

</div>