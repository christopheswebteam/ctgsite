<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-file"></i>Edit Tag
        </div>
        <div class="tools">
            <a href="" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body">
            <form class="form-horizontal" method="post" action="<?php echo current_url() ?>">

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Tag</label>
                    <div class="col-md-10">
                        <input type="text" placeholder="Tag" class="form-control" name="tag" value="<?php if (!empty($tags->tag)) echo $tags->tag; ?>"><?php echo form_error('tag'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Description</label>
                    <div class="col-md-10">
                        <textarea class="tinymce_editor form-control" cols="100" rows="12" name="description"><?php if (!empty($tags->description)) echo $tags->description; ?></textarea>
                        <?php echo form_error('description'); ?>
                    </div>
                </div>



                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Status</label>
                    <div class="col-md-10">
                        <select name="status" class="form-control">
                            <option value="1" <?php if ($tags->status == 1) echo 'selected="selected"'; ?> >Active</option>
                            <option value="0" <?php if ($tags->status == 0) echo 'selected="selected"'; ?> >Deactive</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Category</label>
                    <div class="col-md-10">
                        <select name="category_id" class="form-control">
                            <option value="">Select Category</option>
                            <option value="1" <?php if(!empty($tags->category)){ if($tags->category=='1') echo 'selected="selected"'; } ?> >Feature</option>
                            <option value="2" <?php if(!empty($tags->category)){ if($tags->category=='2') echo 'selected="selected"'; } ?> >Dietary</option>
                            <option value="3" <?php if(!empty($tags->category)){ if($tags->category=='3') echo 'selected="selected"'; } ?> >Protein</option>
                            <option value="4" <?php if(!empty($tags->category)){ if($tags->category=='4') echo 'selected="selected"'; } ?> >Other</option>
                        </select>
                        <?php echo form_error('category_id'); ?>
                    </div>
                </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn blue">Submit</button>
            <a href="<?php echo base_url() . 'backend/tags/' ?>" ><button class="btn btn-danger" type="button">Cancel</button></a></div>
        </form>
    </div>
</div>