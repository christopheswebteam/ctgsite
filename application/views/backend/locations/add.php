<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">
          <?php echo form_open_multipart(current_url(),array('class' =>'form-horizontal form-row-seperated')); ?>
  <div class="portlet">
    <div class="portlet-title">
        <div class="caption">
            Add Location
        </div>
        <div class="actions btn-set">
          <a href="<?php echo base_url() ?>backend/products/" class="btn default"><i class="fa fa-angle-left"></i> Back</a>
          <button class="btn green"><i class="fa fa-check"></i> Save</button>
        </div>
    </div>
    <div class="row">
        <?php if(form_error('latitude')!=''): ?>
            <div class="alert alert-danger alert-dismissable">
              <button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>
               <?php if(form_error('latitude')!=''){ ?>
               <span><?php echo form_error('latitude'); ?></span>
               <?php } ?>
            </div>
        <?php endif; ?>
    </div>

            <div class="portlet-body">
                <div class="tabbable">
                      <ul class="nav nav-tabs">
                            <li class="active">
                              <a href="#tab_general" data-toggle="tab">
                              Location Information </a>
                            </li>
                            <li>
                              <a href="#tab_meta" data-toggle="tab">
                              Location Map</a>
                            </li>
                            <li>
                              <a href="#tab_images" data-toggle="tab">
                              Location Image </a>
                            </li>
                      </ul>

                    <div class="tab-content no-space">
                        <div class="tab-pane active" id="tab_general">
                            <div class="form-body">


                                <div class="form-group">
                                    <label class="col-sm-2 col-sm-2 control-label">Type</label>
                                    <div class="col-md-10">

                                    <select name="type" class="form-control">
                                    <option value="">Select</option>
                                    <option value="all-locations" <?php if (set_value('name') == 'all-location') echo 'selected="selected"';  ?>>All Locations</option>
                                    <option value="Retail" <?php if (set_value('name') == 'Retail') echo 'selected="selected"';  ?>>Retail</option>
                                    <option value="Cooler" <?php if (set_value('name') == 'Cooler') echo 'selected="selected"';  ?>>Cooler</option>
                                    <option value="delivery-area" <?php if (set_value('name') == 'delivery-area') echo 'selected="selected"';  ?>>Delivery Area</option>

                                    </select>
                                      <?php echo form_error('type'); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 col-sm-2 control-label">Name</label>
                                    <div class="col-md-10">
                                        <input type="text" placeholder="Name" class="form-control" name="name" value="<?php  echo set_value('name'); ?>"><?php echo form_error('name'); ?>
                                    </div>
                                </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">POS ID
                                </label>
                                <div class="col-md-10">
                                  <input type="text" class="form-control" name="pos_locations_id" placeholder="POS ID"  value="<?php echo set_value('pos_locations_id');?>">
                                  <span class="help-block">
                                    <?php echo form_error('pos_locations_id'); ?>
                                  </span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label">Phone Number</label>
                                <div class="col-md-10">
                                    <input type="text" placeholder="Phone Number (999) 999-9999" class="form-control" name="phone_number" value="<?php echo set_value('phone_number'); ?>"><?php echo form_error('phone_number'); ?>
                                </div>
                            </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Opening Time</label>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <input type="text" name="opening_time" id="opening_time" class="form-control timepicker timepicker-no-seconds" readonly="readonly">
                                            <span class="input-group-btn">
                                            <button class="btn default" type="button"><i class="fa fa-clock-o"></i></button>
                                            </span>
                                        </div>
                                        <?php echo form_error('opening_time'); ?>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="control-label col-md-2">Closing Time</label>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <input type="text" name="closing_time" id="closing_time" class="form-control timepicker timepicker-no-seconds"  readonly="readonly">
                                            <span class="input-group-btn">
                                            <button class="btn default" type="button"><i class="fa fa-clock-o"></i></button>
                                            </span>
                                        </div>
                                        <?php echo form_error('closing_time'); ?>
                                    </div>
                                </div>

                         <div class="form-group">
                            <label class="col-md-2 control-label">Tax Rate
                            </label>
                            <div class="col-md-10">
                              <input type="text" class="form-control" name="tax_rate" placeholder="Tax Rate"  value="<?php echo set_value('tax_rate',7);?>">
                              <span class="help-block">
                                <?php echo form_error('tax_rate'); ?>
                              </span>
                            </div>
                          </div>

                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label">Status</label>
                                <div class="col-md-10">
                                    <select name="status" class="form-control">
                                           <option value="1" <?php if (set_value('status') == 1) echo 'selected="selected"'; ?> >Active</option>
                                            <option value="0" <?php if (set_value('status') == 0) echo 'selected="selected"'; ?> >Deactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_meta">
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="col-sm-2 col-sm-2 control-label">Street Address 1</label>
                                    <div class="col-md-10">
                                        <input type="text" placeholder="Street Address 1" id="street_address_1" class="form-control" name="street_address_1" value="<?php  echo set_value('street_address_1'); ?>"><?php echo form_error('street_address_1'); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 col-sm-2 control-label">Street Address 2</label>
                                    <div class="col-md-10">
                                        <input type="text" placeholder="Street Address 2" id="street_address_2" class="form-control" name="street_address_2" value="<?php echo set_value('street_address_2'); ?>">
                                        <?php echo form_error('street_address_2'); ?>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="col-sm-2 col-sm-2 control-label">City</label>
                                    <div class="col-md-10">
                                        <input type="text" placeholder="City" id="city" class="form-control" name="city" value="<?php echo set_value('city'); ?>"><?php echo form_error('city'); ?>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="col-sm-2 col-sm-2 control-label">State</label>
                                    <div class="col-md-10">
                                        <input type="text" placeholder="State" id="state" class="form-control" name="state" value="<?php echo set_value('state'); ?>"><?php echo form_error('state'); ?>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="col-sm-2 col-sm-2 control-label">Zip Code</label>
                                    <div class="col-md-10">
                                        <input type="text" placeholder="Zip Code" class="form-control" name="zip_code" value="<?php echo set_value('zip_code'); ?>"><?php echo form_error('zip_code');  ?>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-2 col-sm-2 control-label">Latitude</label>
                                    <div class="col-md-10">
                                          <input type="text" name="latitude" class="form-control" value="<?php echo set_value('latitude'); ?>" id="latitude"  placeholder="Latitude">
                                          <?php echo form_error('latitude');  ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 col-sm-2 control-label">Logitude</label>
                                    <div class="col-md-10">

                                        <input type="text" placeholder="Longitude" id="longitude" class="form-control" name="longitude" value="<?php echo set_value('longitude'); ?>">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-5  control-label">&nbsp;</label>
                                    <div class="col-md-6">


                                    <a  href="javascript:void(0)" class="btn btn-primary" onclick="test()">Generate Map</a>

                                   <span style="display:none;" id="load_img"> <img src="<?php echo base_url('/assets/backend/admin/layout/img/loading-spinner-grey.gif'); ?>" alt=""></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 col-sm-2 control-label">map</label>
                                    <div class="col-md-10">
                                      <div style="width:800px;height:400px;"  id="map-canvas">
                                        <iframe width="100%" height="385px" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA-yjAv-6LjQ27DZ33J-ShLu_JgbpPstyg&zoom=5&q=RoswellRoad+Sandy+Springs+GA+30328"></iframe>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_images">
                            <!-- <div class="alert alert-success margin-bottom-10">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                              <i class="fa fa-warning fa-lg"></i> Image type and information need to be specified.
                            </div> -->
                            <div id="location_images_uploader_container" class="text-align-reverse margin-bottom-10">
                              <a id="location_images_uploader_pickfiles" href="javascript:;" class="btn yellow">
                              <i class="fa fa-plus"></i> Select Files </a>
                              <a id="location_images_uploader_uploadfiles" href="javascript:;" class="btn green">
                              <i class="fa fa-share"></i> Upload Files </a>
                            </div>
                            <div class="row">
                              <div id="location_images_uploader_filelist" class="col-md-6 col-sm-12">
                              </div>
                            </div>
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr role="row" class="heading">
                                      <th width="10%">Image</th>
                                      <th width="80%">Label</th>
                                      <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="tablebody">
                                    <tr>
                                      <td colspan="3" style="text-align:center;">
                                        No Data Found
                                      </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
      <?php echo form_close(); ?>
    </div>
</div>