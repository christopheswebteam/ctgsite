<div class="row">
    <div class="col-md-12 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-map-marker"></i>Locations <a href="<?php echo base_url() ?>backend/locations/add" class="btn btn-xs yellow">Add New Location <i class="icon-plus"></i> </a>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="16%">Name</th>
                                <th width="10%" class="hidden-phone">Type</th>
                                <th width="12%" class="hidden-phone">Address 1</th>
                                <th width="12%" class="hidden-phone">Address 2</th>
                                <th width="10%" class="hidden-phone">City</th>
                                <th width="5%" class="hidden-phone">State</th>
                                <th width="5%" class="hidden-phone">Zip</th>
                                <th width="10%" class="hidden-phone">Times</th>
                                <th width="5%" class="hidden-phone">Status</th>
                                <th width="10%" class="hidden-phone">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            if (!empty($locations)):
                              $i = $offset; foreach ($locations as $row): $i++;

                                    ?>
                                    <tr class="gradeX">
                                        <td><?php echo $i . "."; ?></td>
                                        <td class=""> <a href="<?php echo base_url() . 'backend/locations/edit/' . $row->id ?>" class="btn btn-small" title="Edit" ><?php echo $row->name; ?></a></td>

                                        <td ><?php echo $row->type; ?></td>
                                        <td ><?php echo $row->street_address_1 ?></td>
                                        <td ><?php echo $row->street_address_2 ?></td>
                                        <td ><?php echo $row->city ?></td>
                                        <td ><?php echo $row->state ?></td>
                                        <td ><?php echo $row->zip_code ?></td>
                                        <td>

                                                <span class="label label-success"> Opening : <?php echo $row->opening_time ?></span>
                                                 <br><br>

                                                <span class="label label-danger"> Closing : <?php echo $row->closing_time ?> </span>

                                        </td>
                                        <td class="to_hide_phone">
                                            <?php if ($row->status): ?>
                                                <span class="label label-success"> Active </span>
                                            <?php else: ?>
                                                <span class="label label-danger"> Deactive </span>
                                            <?php endif; ?>
                                        </td>

                                        <td class="ms">
                                            <a href="<?php echo base_url() . 'backend/locations/edit/' . $row->id ?>" class="btn btn-success btn-xs" title="Edit"  rel="tooltip" data-placement="left" data-original-title=" Edit "><i class="icon-pencil"></i>

                                            </a>

                                            <a href="<?php echo base_url() . 'backend/locations/delete/' . $row->id ?>" class="btn btn-danger btn-xs" rel="tooltip" data-placement="bottom" title="Delete" data-original-title="Remove" onclick="return confirm('Are you sure you want to delete?');" > <i class="icon-trash "></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <th colspan="5"> <center>No Product Tags found.</center></th>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    <div class="text-right">
                        <?php if (!empty($pagination)) echo $pagination; ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- END SAMPLE FORM PORTLET-->
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div>