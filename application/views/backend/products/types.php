<div class="row">
    <div class="col-md-12 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-file"></i>Type's<a href="<?php echo base_url() ?>backend/products/type_add" class="btn btn-xs yellow">Add New Type <i class="icon-plus"></i> </a>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="60%"> Name </th>
                                <th width="10%" class="hidden-phone">Status</th>
                                <th width="10%" class="hidden-phone">Created</th>
                                <th width="10%" class="hidden-phone">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            if (!empty($types)):
                                $i = 0; foreach ($types as $row): $i++;

                                    ?>
                                    <tr class="gradeX">
                                        <td><?php echo $i . "."; ?></td>
                                        <td class=""> <a href="<?php echo base_url() . 'backend/products/type_edit/' . $row->id ?>" class="btn btn-small" title="Edit" ><?php echo $row->type_name; ?></a></td>
                                        <td class="to_hide_phone">
                                            <?php if ($row->status): ?>
                                                <span class="label label-success"> Active </span>
                                            <?php else: ?>
                                                <span class="label label-danger"> Deactive </span>
                                            <?php endif; ?>
                                        </td>
                                        <td ><?php echo date('Y-m-d', strtotime($row->created)); ?></td>
                                        <td class="ms">
                                            <a href="<?php echo base_url() . 'backend/products/type_edit/' . $row->id ?>" class="btn btn-success btn-xs" title="Edit"  rel="tooltip" data-placement="left" data-original-title=" Edit "><i class="icon-pencil"></i>

                                            </a>

                                            <a href="<?php echo base_url() . 'backend/products/type_delete/' . $row->id ?>" class="btn btn-danger btn-xs" rel="tooltip" data-placement="bottom" title="Delete" data-original-title="Remove" onclick="return confirm('Are you sure you want to delete?');" > <i class="icon-trash "></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <th colspan="5"> <center>No Types found.</center></th>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    <div class="text-right">
                        <?php if (!empty($pagination)) echo $pagination; ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- END SAMPLE FORM PORTLET-->
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div>