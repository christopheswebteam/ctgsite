<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-file"></i>Edit Item Type
        </div>
        <div class="tools">
            <a href="" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body">
            <form class="form-horizontal" method="post" action="<?php echo current_url() ?>">

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Item Type</label>
                    <div class="col-md-10">
                        <input type="text" placeholder="Item Type" class="form-control" name="item_type" value="<?php if (!empty($item_types->type_name)) echo $item_types->type_name; ?>"><?php echo form_error('item_type'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Description</label>
                    <div class="col-md-10">
                        <textarea class="tinymce_editor form-control" cols="100" rows="12" name="description"><?php if (!empty($item_types->type_description)) echo $item_types->type_description; ?></textarea>
                        <?php echo form_error('description'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Status</label>
                    <div class="col-md-10">
                        <select name="status" class="form-control">
                            <option value="1" <?php if ($item_types->status == 1) echo 'selected="selected"'; ?> >Active</option>
                            <option value="0" <?php if ($item_types->status = 0) echo 'selected="selected"'; ?> >Deactive</option>
                        </select>
                    </div>
                </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn blue">Submit</button>
            <a href="<?php echo base_url().'backend/products/types'; ?>" ><button class="btn btn-danger" type="button">Cancel</button></a></div>
        </form>
    </div>
</div>