<div class="row">

	<div class="col-md-12">

		<?php echo form_open_multipart(current_url(), array('class' =>'form-horizontal form-row-seperated')); ?>

			<div class="portlet">
			
				<div class="portlet-title">
					<div class="caption">Edit Product</div>
					<div class="actions btn-set">
						<a href="<?php echo site_url('backend/products'); ?>" class="btn default"><i class="fa fa-angle-left"></i> Back</a>
						<button class="btn green"><i class="fa fa-check"></i> Save</button>
					</div>
				</div>
				
				<div class="row">

					<?php if (form_error('nutrition_facts') != ""): ?>
						<div class="alert alert-danger alert-dismissable">
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>
							<span>In Nutrition Facts Tab: <?php echo form_error('nutrition_facts'); ?></span> 
						</div>
					<?php endif; ?> 

					<?php if ((form_error('additional_information') != "") || (form_error('preparation_instructions') != "")): ?>
					
						<div class="alert alert-danger alert-dismissable">
					
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>
							
							<?php if (form_error('additional_information') != ""): ?>
								<span><?php echo form_error('additional_information'); ?></span>
							<?php endif; ?>
							
							<?php if (form_error('preparation_instructions') != ""): ?>
								<span><?php echo form_error('preparation_instructions'); ?></span>
							<?php endif; ?>
							
						</div>
							
					<?php endif; ?>

					<?php if ((form_error('small_price') != "") || (form_error('small_weight') != "")): ?>
					
						<div class="alert alert-danger alert-dismissable">
						
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>      
						
							<?php if (form_error('small_price') != ""): ?>      
								<span><?php echo form_error('small_price'); ?></span>
							<?php endif; ?>
							
							<?php if (form_error('small_weight') != ""): ?>            
								<span><?php echo form_error('small_weight'); ?></span>
							<?php endif; ?>
							
						</div>
						
					<?php endif; ?>

					<?php if ((form_error('medium_price') != "") || (form_error('medium_weight') != "")): ?>
					
						<div class="alert alert-danger alert-dismissable">
						
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>      
						
							<?php if (form_error('medium_price') != ""): ?>      
								<span><?php echo form_error('medium_price'); ?></span>
							<?php endif; ?>
							
							<?php if (form_error('medium_weight') != ""): ?>            
								<span><?php echo form_error('medium_weight'); ?></span>
							<?php endif; ?>
							
						</div>
						
					<?php endif; ?>
					
					<?php if ((form_error('large_price') != "") || (form_error('large_weight') != "")): ?>
					
						<div class="alert alert-danger alert-dismissable">
						
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>      
						
							<?php if (form_error('large_price') != ""): ?>      
								<span><?php echo form_error('large_price'); ?></span>
							<?php endif; ?>
							
							<?php if (form_error('large_weight') != ""): ?>            
								<span><?php echo form_error('large_weight'); ?></span>
							<?php endif; ?>
							
						</div>
						
					<?php endif; ?>
					
					<?php if ((form_error('family_price') != "") || (form_error('family_weight') != "")): ?>
					
						<div class="alert alert-danger alert-dismissable">
						
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>      
						
							<?php if (form_error('family_price') != ""): ?>      
								<span><?php echo form_error('family_price'); ?></span>
							<?php endif; ?>
							
							<?php if (form_error('family_weight') != ""): ?>            
								<span><?php echo form_error('family_weight'); ?></span>
							<?php endif; ?>
							
						</div>
						
					<?php endif; ?>

				</div>
				
				<div class="portlet-body">

					<div class="tabbable">

						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab_general" data-toggle="tab">General</a></li>
							<li><a href="#tab_information" data-toggle="tab">Information</a></li>
							<li><a href="#tab_nutrition_facts" data-toggle="tab">Nutrition Facts</a></li>
							<li><a href="#tab_portions" data-toggle="tab">Portions</a></li>
							<li><a href="#tab_photos" data-toggle="tab">Photos</a></li>
						</ul>

						<div class="tab-content no-space">
	
							<div class="tab-pane active" id="tab_general">
							
								<div class="form-body">

									<div class="form-group">
										<label class="col-md-2 control-label">Status</label>
										<div class="col-md-8">
											<select name="status" class="table-group-action-input form-control input-medium">
												<option value="1">Active</option>
												<option value="0">Inactive</option>
											</select>
											<?php echo form_error('status'); ?>
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label">POS ID</label>
										<div class="col-md-10">
											<input type="text" class="form-control" name="pos_products_id" placeholder="POS ID" value="<?php echo set_value('pos_products_id');?>" />
											<span class="help-block"><?php echo form_error('pos_products_id'); ?></span>
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label">Name</label>
										<div class="col-md-10">
											<input type="text" class="form-control" name="name" placeholder="Name" id="product_name" value="<?php echo set_value('name'); ?>" />
											<span class="help-block"><?php echo form_error('name'); ?></span>
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label">Slug</label>
										<div class="col-md-10">
											<input type="text"  class="form-control" name="slug" placeholder="Slug" id="product_slug" value="<?php echo set_value('slug'); ?>">
											<span class="help-block"><?php echo form_error('slug'); ?></span>
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label">Description</label>
										<div class="col-md-10">
											<textarea class="form-control" name="description"><?php echo set_value('description'); ?></textarea>
											<span class="help-block"><?php echo form_error('description'); ?></span>
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label">Price</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="custom_price" value="<?php echo set_value('custom_price'); ?>" placeholder="Custom Price">
											<span class="help-block"><?php echo form_error('custom_price'); ?></span>
										</div>
									</div> 

									<div class="form-group">
										<label class="col-md-2 control-label">Weight</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="custom_weight" value="<?php echo set_value('custom_weight'); ?>" placeholder="Custom Weight">
											<span class="help-block"><?php echo form_error('custom_weight'); ?></span>
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label">Tags</label>
										<div class="col-md-10">
											<div class="row-fluid">
												<div style="padding-top: 7px;" class="col-md-3">
													<strong style="padding-left: 7px;">Feature</strong>
													<br>   
													<?php if (!empty($feature_tags)): ?>
														<?php foreach ($feature_tags as $tag): ?>
															<input type="checkbox" name="tags[]" class="form-control" value="<?php echo $tag['tag']; ?>"><span><?php echo $tag['tag']; ?></span>
															<br>
														<?php endforeach; ?>
													<?php endif; ?>
												</div>
												<div style="padding-top:7px;" class="col-md-3">
													<strong style="padding-left: 7px;">Dietary</strong>
													<br>
													<?php if (!empty($dietary_tags)): ?>
														<?php foreach ($dietary_tags as $tag): ?>
															<input type="checkbox" name="tags[]" class="form-control" value="<?php echo $tag['tag']; ?>"><span><?php echo $tag['tag']; ?></span>
															<br>
														<?php endforeach; ?>
													<?php endif; ?>
												</div>
												<div style="padding-top:7px;" class="col-md-3">
													<strong style="padding-left: 7px;">Protein</strong>
													<br>
													<?php if (!empty($protein_tags)): ?>
														<?php foreach ($protein_tags as $tag): ?>
															<input type="checkbox" name="tags[]" class="form-control" value="<?php echo $tag['tag']; ?>"><span><?php echo $tag['tag']; ?></span>
															<br>
														<?php endforeach; ?>
													<?php endif; ?>
												</div>
												<div style="padding-top:7px;" class="col-md-3">
													<strong style="padding-left: 7px;">Other</strong>
													<br>
													<?php if (!empty($other_tags)): ?>
														<?php foreach ($other_tags as $tag): ?>
															<input type="checkbox" name="tags[]" class="form-control" value="<?php echo $tag['tag']; ?>"><span><?php echo $tag['tag']; ?></span>
															<br>
														<?php endforeach; ?>
													<?php endif; ?>
												</div>
											</div>
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label">Ingredients</label>
										<div class="col-md-10">
											<input type="hidden" id="ingredients" name="ingredients" class="form-control select2" value="" />
											<span class="help-block"><?php echo form_error('ingredients'); ?></span>
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label">Available Date</label>
										<div class="col-md-10">
											<div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
												<input type="text" class="form-control" placeholder="Start Date" value="<?php echo set_value('start_date'); ?>" name="start_date" />
												<span class="input-group-addon"> to </span>
												<input type="text" class="form-control" placeholder="End Date" value="<?php echo set_value('end_date'); ?>" name="end_date">
											</div>
											<span class="help-block"><?php echo form_error('start_date');?><?php echo form_error('end_date'); ?></span>
										</div>
										
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label">Sort</label>
										<div class="col-md-3">
											<input type="text" class="form-control" name="sort" id="sort" value="<?php echo set_value('sort');?>" />
										</div>
										<span class="help-block"><?php echo form_error('sort'); ?></span>
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label">Check Inventory</label>
										<div class="radio-list">
											<label class="radio-inline">
												<div id="uniform-optionsRadios4" class="radio">                          
													<input id="optionsRadios4" type="radio" value="1" name="check_inventory">                          
												</div>
												Yes
											</label>
											<label class="radio-inline">
												<div id="uniform-optionsRadios5" class="radio">                            
													<input id="optionsRadios5" type="radio" value="0" name="check_inventory">                            
												</div>
												No
											</label>
										</div>
									</div>

								</div>
							
							</div>

							<div class="tab-pane" id="tab_information">
								
								<div class="form-body">           
							
									<div class="form-group">
										<label class="col-md-2 control-label">Additional Information</label>
										<div class="col-md-10">
											<textarea class="form-control maxlength-handler" rows="8" name="additional_information" maxlength="1000"><?php echo set_value('additional_information'); ?></textarea>
											<span class="help-block">Maximum 1000 Characters <?php echo form_error('additional_information'); ?></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-2 control-label">Preparation Instructions</label>
										<div class="col-md-10">
											<textarea class="form-control maxlength-handler" rows="8" name="preparation_instructions" maxlength="1000"><?php echo set_value('preparation_instructions'); ?></textarea>
											<span class="help-block">Maximum 1000 Characters <?php echo form_error('preparation_instructions'); ?></span>
										</div>
									</div>
									
								</div>
								
							</div>

							<div class="tab-pane" id="tab_nutrition_facts">
							
								<div class="form-body">
									
									<div class="form-group">
										<label class="col-md-2 control-label">Nutrition Facts</label>
										<div class="col-md-10">
											<input type="file" name="nutrition_facts" value="" placeholder="" />
										</div>                
									</div>

								</div>

							</div>   

							<div class="tab-pane" id="tab_portions">

								<div class="portlet-body form">                           

									<div action="#" class="form-horizontal">

										<div class="form-body">

											<div class="form-group">
												<label class="col-md-2 control-label">Size</label>
												<div style="padding-top:8px;" class="col-md-8">
													<div class="row-fluid" id="size">
														<?php if (!empty($portions)): ?>
															<?php foreach ($portions as $row): ?>
																<?php if ($row['id'] != 5): ?>
																	<div class="col-md-2">
																		<input type="checkbox" class="group" name="size[]" id="<?php echo $row['name']; ?>" value="<?php echo strtolower($row['name']); ?>"><span><?php echo $row['name']; ?></span>
																	</div>                                 
																<?php endif; ?>
															<?php endforeach; ?>
														<?php endif; ?>
													</div>
													<br>
													<br>
													<?php echo form_error('size[]'); ?>
												</div>
											</div> 
											
											<br>

											<div class="small_group" style="display:none;">

												<div class="form-group">
													<label class="col-md-2 control-label">Small Price</label>
													<div class="col-md-6">
														<input type="text" class="form-control" placeholder="Small Price" name="small_price" value="<?php echo set_value('small_price'); ?>">
														<?php echo form_error('small_price'); ?>
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-2 control-label">Small Weight</label>
													<div class="col-md-6">
														<input type="text" class="form-control" placeholder="Small Weight" name="small_weight" value="<?php echo set_value('small_weight'); ?>">
														<?php echo form_error('small_weight'); ?>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">Small Measure</label>
													<div class="col-md-6">
														<select name="small_weight_measure" class="table-group-action-input form-control input-medium">
															<option value="Ounces">Ounces</option>
														</select>
														<?php echo form_error('small_weight_measure'); ?>
													</div>
												</div>
											
											</div>
											
											<div class="medium_group" style="display:none;">

												<div class="form-group">
													<label class="col-md-2 control-label">Medium Price</label>
													<div class="col-md-6">
														<input type="text" class="form-control" placeholder="Medium Price" name="medium_price" value="<?php echo set_value('medium_price'); ?>">
														<?php echo form_error('medium_price'); ?>
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-2 control-label">Medium Weight</label>
													<div class="col-md-6">
														<input type="text" class="form-control" placeholder="Medium Weight" name="medium_weight" value="<?php echo set_value('medium_weight'); ?>">
														<?php echo form_error('medium_weight'); ?>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">Medium Measure</label>
													<div class="col-md-6">
														<select name="medium_weight_measure" class="table-group-action-input form-control input-medium">
															<option value="Ounces">Ounces</option>
														</select>
														<?php echo form_error('medium_weight_measure'); ?>
													</div>
												</div>
											
											</div>
											
											<div class="large_group" style="display:none;">

												<div class="form-group">
													<label class="col-md-2 control-label">Large Price</label>
													<div class="col-md-6">
														<input type="text" class="form-control" placeholder="Large Price" name="large_price" value="<?php echo set_value('large_price'); ?>">
														<?php echo form_error('large_price'); ?>
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-2 control-label">Large Weight</label>
													<div class="col-md-6">
														<input type="text" class="form-control" placeholder="Large Weight" name="large_weight" value="<?php echo set_value('large_weight'); ?>">
														<?php echo form_error('large_weight'); ?>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">Large Measure</label>
													<div class="col-md-6">
														<select name="large_weight_measure" class="table-group-action-input form-control input-medium">
															<option value="Ounces">Ounces</option>
														</select>
														<?php echo form_error('large_weight_measure'); ?>
													</div>
												</div>
											
											</div>
											
											<div class="family_group" style="display:none;">

												<div class="form-group">
													<label class="col-md-2 control-label">Family Price</label>
													<div class="col-md-6">
														<input type="text" class="form-control" placeholder="Family Price" name="family_price" value="<?php echo set_value('family_price'); ?>">
														<?php echo form_error('family_price'); ?>
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-2 control-label">Family Weight</label>
													<div class="col-md-6">
														<input type="text" class="form-control" placeholder="Family Weight" name="family_weight" value="<?php echo set_value('family_weight'); ?>">
														<?php echo form_error('family_weight'); ?>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">Family Measure</label>
													<div class="col-md-6">
														<select name="family_weight_measure" class="table-group-action-input form-control input-medium">
															<option value="Ounces">Ounces</option>
														</select>
														<?php echo form_error('family_weight_measure'); ?>
													</div>
												</div>
											
											</div>
										
										</div> 

									</div> 

								</div> 

							</div>
							
							<div class="tab-pane" id="tab_photos">
							
								<div id="tab_images_uploader_container" class="text-align-reverse margin-bottom-10">
									<a id="tab_images_uploader_pickfiles" href="javascript:;" class="btn yellow"><i class="fa fa-plus"></i> Select Files</a>
									<a id="tab_images_uploader_uploadfiles" href="javascript:;" class="btn green"><i class="fa fa-share"></i> Upload Files</a>
								</div>
								
								<div class="row">
									<div id="tab_images_uploader_filelist" class="col-md-6 col-sm-12"></div>
								</div>
								
								<table class="table table-bordered table-hover">
								
									<thead>
										<tr role="row" class="heading">
											<th width="10%">Image</th>
											<th width="30%">Title</th>
											<th width="30%">Alt. Text</th>
											<th width="20%">Random Background</th>              
											<th width="10%">Action</th>
										</tr>
									</thead>
									
									<tbody id="tablebody">

										<tr>
											<td colspan="5" style="text-align:center;">No Photos Found</td>
										</tr>

									</tbody>
								
								</table>
							
							</div>

						</div>

					</div>

				</div>

			</div>

		<?php echo form_close(); ?>

	</div>
	
</div>