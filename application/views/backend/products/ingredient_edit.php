<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-file"></i>Edit Ingredient
        </div>
        <div class="tools">
            <a href="" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body">
            <form class="form-horizontal" method="post" action="<?php echo current_url() ?>">
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Name</label>
                    <div class="col-md-10">
                        <input type="text" placeholder="Name" class="form-control" name="name" value="<?php if(!empty($ingredient->name)) echo $ingredient->name; ?>"><?php echo form_error('name'); ?>
                    </div>
                </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn blue">Submit</button>
            <a href="<?php echo base_url() . 'backend/products/ingredients'; ?>" ><button class="btn btn-danger" type="button">Cancel</button></a></div>
        </form>
    </div>
</div>