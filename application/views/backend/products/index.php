<div class="row">

    <div class="col-md-12">
	

        <div class="portlet box blue">
		
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-file"></i>Products <a href="<?php echo base_url('backend/products/add') ?>" class="btn btn-xs yellow">Add New Product <i class="icon-plus"></i> </a>
                </div>
            </div>

            <div class="portlet-body">
			
				<br>
				
				<div class="row">
					<form action="<?php echo current_url() ?>" method="get" accept-charset="utf-8">
						<div class="form-body">
							<div class="form-group">
								<div class="col-md-3">
									<input type="text" value="<?php echo (!empty($_GET['keywords'])) ? $_GET['keywords'] : FALSE; ?>" class="form-control" name="keywords" placeholder="Keywords">
								</div>
								<div class="col-md-3">
									<input type="submit" class="btn btn-primary" name="s" value="Search">
								</div>
							</div>
						</div>
					</form>
				</div>
				
				<br>
				<br>
				
                <div class="table-responsive">
				
                    <table class="table table-bordered table-hover">
					
                        <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th width="30%">Name</th>
                                <th width="30%">Description</th>
                                <th width="10%" class="hidden-phone">Status</th>
                                <th width="10%" class="hidden-phone">Created</th>
                                <th width="10%" class="hidden-phone">Actions</th>
                            </tr>
                        </thead>
						
                        <tbody>
						
                            <?php if (!empty($products)): ?>
								
								<?php foreach ($products as $product): ?>
								
                                    <tr class="gradeX">
									
                                        <td><?php echo $product['id']; ?></td>
                                        <td><a href="<?php echo site_url('backend/products/edit/'.$product['id']); ?>" class="btn btn-small" title="Edit"><?php echo (!empty($product['name'])) ? $product['name'] : FALSE; ?></a></td>
                                        <td><?php echo (!empty($product['description'])) ? $product['description'] : FALSE; ?></td>
                                        <td class="to_hide_phone">
                                            <?php if ($product['status']): ?>
                                                <span class="label label-success">Active</span>
                                            <?php else: ?>
                                                <span class="label label-danger">Inactive</span>
                                            <?php endif; ?>
                                        </td>
                                        <td><?php echo date('m/d/Y', strtotime($product['created'])); ?></td>
                                        <td class="ms">
                                            <a href="<?php echo site_url('backend/products/edit/'.$product['id']); ?>" class="btn btn-success btn-xs" title="Edit" rel="tooltip" data-placement="left" data-original-title="Edit"><i class="icon-pencil"></i></a>
                                            <a href="<?php echo site_url('backend/products/delete/'.$product['id']); ?>" class="btn btn-danger btn-xs" rel="tooltip" data-placement="bottom" title="Delete" data-original-title="Remove" onclick="return confirm('Are you sure you want to delete this product?');"><i class="icon-trash "></i></a>
                                        </td>
										
                                    </tr>
									
                                <?php endforeach; ?>
								
                            <?php else: ?>
							
                                <tr>
                                    <th colspan="6"> <center>No Products found.</center></th>
								</tr>
								
							<?php endif; ?>
							
						</tbody>

					</table>
					
					<div class="text-right">
						<?php echo $pagination; ?>
					</div>

				</div>

			</div>

		</div>
			
	</div>
	
</div>