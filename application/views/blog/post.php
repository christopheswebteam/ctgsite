<div class="dynamic-content container " id="main-content">
    <div id="post" class="blog padding-wrapper">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 white-bg">
                <figure>
                    <img src="<?php echo base_url($blogs->file_path) ?>" alt=""/>
                </figure>
                <header>
                    <h1><?php echo $blogs->blog_title ?></h1>
                    <div class="data-post">
                        <span class="date-post"><?php echo date('d-m-Y',strtotime($blogs->created)) ?></span>
                        <span class="author-post">Admin</span>
                    </div>
                </header>
                <section class="text-post">
                   <p><?php echo ($blogs->blog_content) ?></p>
                </section>

                <section id="comments">
                <hr>
                <hr>
                    <ul class="comments animate_element animate_content">
                        <?php if ($blog_commets): ?>
                            <?php foreach ($blog_commets as $row): ?>

                        <li class="">
                            <a class="photo-user" href="#" title="">
                                <figure>
                                    <img class="" src="http://placehold.it/120x120" alt="" width="70" height="70"/>
                                </figure>
                            </a>
                            <div class="author-comment">
                                <a href="#" title=""><strong><?php echo $row->comment_author ?></strong></a>
                                <div class="date-post"><?php echo date('d-m-Y',strtotime($row->comment_date)) ?></div>
                                <p><?php echo $row->comment_content ?> </p>
                            </div>
                        </li>
                          <?php endforeach ?>
                        <?php endif ?>
                    </ul>
                </section>
                <?php msg_alert_front() ?>
                <section class="form">

                    <?php echo form_open(current_url(), array('role'=>'form'), array('comment_blog_id'=>$blogs->id)); ?>

                        <div class="form-group">
                            <label for="form-name">Name</label>
                            <input type="text" class="form-control"  name="user_name"  placeholder="Name" value="<?php echo set_value('user_name'); ?>">
                            <?php echo form_error('user_name'); ?>
                        </div>
                        <div class="form-group">
                            <label for="form-name">Email</label>
                            <input type="email" class="form-control"  name="user_email" placeholder="Email" value="<?php echo set_value('user_email'); ?>">
                            <?php echo form_error('user_email'); ?>
                        </div>
                        <div class="form-group">
                            <label for="form-message">Message</label>
                            <textarea class="form-control" placeholder="Write a comment" name="comment" id="form-message"><?php echo set_value('comment'); ?></textarea>
                            <?php echo form_error('comment'); ?>
                        </div>


                        <button type="submit" class="btn btn-info">Send</button>

                        <div class="form-error hidden">
                            <div class="alert alert-danger hidden">Fill up name</div>
                            <div class="alert alert-danger hidden">Fill up message</div>
                        </div>
                        <div class="alert alert-success hidden">Thank you for your comment!</div>

                    </form>

                    <!--
                              <form action="?" method="post">
                                <div class="input-wrapper name">
                                  <input class="contact-form" type="text" placeholder="Name" name="form-name" id="form-name">
                                </div>
                                <div class="input-wrapper message">
                                  <textarea class="contact-form" placeholder="Write a comment" name="form-message" id="form-message"></textarea>
                                </div>
                                <div class="input-wrapper submit">
                                  <input type="submit" value="Send" class="buttonform">
                                </div>
                                <div class="form-error hidden">
                                  <div class="alert alert-danger hidden">Fill up name</div>
                                  <div class="alert alert-danger hidden">Fill up message</div>
                                </div>
                                <div class="alert alert-success hidden">Thank you for your comment!</div>
                              </form> -->
                </section>
            </div>
            <div class="col-md-offset-1 col-md-3"></div>
        </div>
    </div>
</div>
