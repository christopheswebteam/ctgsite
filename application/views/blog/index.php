<div class="dynamic-content container " id="main-content">
    <div id="blog" class="blog padding-wrapper">
        <div class="row">
            <div class="col-md-8 white-bg">
            <?php if (!empty($blogs)): ?>
                <?php foreach ($blogs as $row): ?>
                <section class="blog-post">
                    <a href="<?php echo base_url('blog/post/'.$row->blog_slug.'/'.$row->id) ?>" class="img-blog hover-post">
                        <figure>
                            <img src="<?php echo base_url($row->file_path) ?>" alt=""/>
                        </figure>
                    </a>
                    <header>
                        <h1><?php echo text_limit($row->blog_title,55) ?></h1>
                        <div class="data-post">
                            <span class="date-post"><?php echo date('d-m-Y',strtotime($row->created)) ?></span>
                            <span class="author-post">Admin</span>
                        </div>
                    </header>
                    <article class="text-post">
                        <p><?php echo text_limit($row->blog_content,200) ?></p>
                    </article>
                </section>
                  <?php endforeach ?>
              <?php else: ?>
                 <header>
                        <p><center>NO Post Found.</center></p>

                    </header>
            <?php endif ?>

                <div class="text-center">
                    <?php echo $pagination ?>
                </div>

            </div>
                <?php if (!empty($top_post)): ?>
            <div class="col-md-offset-1 col-md-3 white-bg">
                    <?php foreach ($top_post as $row): ?>

                <div class="widget-reservation">
                    <a href="<?php echo base_url('blog/post/'.$row->blog_slug.'/'.$row->id) ?>">
                        <div class="square">
                            <div class="square-bg" style="background-image:url(<?php  echo base_url($row->thumb_image) ?>)"></div>

                            <div class="square-header">
                                <p class="description"><?php echo text_limit($row->blog_title,30) ?></p>
                            </div>
                            <div class="square-post">
                                 <p><?php echo text_limit($row->blog_content,200) ?></p>
                            </div>
                        </div>
                    </a>
                </div>
                 <?php endforeach ?>
            </div>
                <?php endif ?>
        </div>
    </div>
</div>
