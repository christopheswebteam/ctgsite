<div class="dynamic-content menu-wrapper" id="main-content">
    <div  class="menu padding-wrapper">
        <div class="container animate-in animate-in-fade">
            <div class="row">

                <header>
                <div class="col-xs-12 col-sm-12 col-md-3 ">
                  <div class="title">
                    <h2><?php if (!empty($page->post_title)) echo htmlspecialchars_decode($page->post_title); ?></h2>

                  </div>
                </div>
                </header>



                  <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box1">
                                <div class="row">
                                    <div class="col-md-12">

                                     <p><?php if (!empty($page->post_content)) echo htmlspecialchars_decode($page->post_content); ?></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>