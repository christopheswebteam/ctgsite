<div class="dynamic-content menu-wrapper" id="main-content">
    <div  class="menu padding-wrapper">
        <div class="container animate-in animate-in-fade">
            <div class="row">

                <header>
                    <div class="col-xs-12 col-sm-12 col-md-4  col-md-offset-8 ">
                        <div class="title">
                            <h2>Our Containers</h2>

                        </div>
                    </div>
                </header>



                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="information-page">
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="features-image">
                                            <div class="col-md-6">
                                                <img class="img-rounded img-responsive" src="<?php echo FRONTEND_THEME_URL ?>img/containers.jpg" alt="" >
                                            </div>
                                        </div>

                                        <div>
                                            <p><?php echo $this->ipsum->getContent(750, 'plain'); ?> </p>
                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>


                </div>

            </div>
        </div>
    </div>
</div>