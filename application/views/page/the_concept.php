<div class="dynamic-content menu-wrapper" id="main-content">
    <div  class="menu padding-wrapper">
        <div class="container animate-in animate-in-fade">
            <div class="row">

                <header>
                <div class="col-md-4">
                  <div class="title">
                    <h2>The Concept</h2>

                  </div>
                </div>
                </header>

                <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="information-page">
                            <div class="row">
                                <div class="col-md-12">

                               <div class="features-image">
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                 <img class="img-rounded img-responsive" src="<?php echo FRONTEND_THEME_URL ?>img/menus/menu2-big.jpg" alt="" >
                              </div>
                              </div>

                              <div>
                                 <p>Christophe’s To Go is a new concept for people who want to eat well in their own home, but do not have the time or skills to prepare good food.  Christophe’s To Go makes it easy to eat well. </p>
                                 <br>



                                   <ul type="disc">
                                    <li>Choose your meals</li>
                                    <li>Heat the meals in the oven or microwave</li>
                                    <li>Enjoy a delicious, healthful meal</li>
                                   </ul>

                                  <br>
                                  <p>We know it has to be convenient.  So Christophe’s To Go gives you all the options you want</p>
                                  <br>
                                  <ul type="disc">
                                    <li>Retail Stores
                                      <ul type="circle">
                                        <li>Walk In to any of our three retail stores and pickfrom over 50 meals right off the shelf.</li>
                                        <li>Order online in advance and have the food ready for pickup.  You can be in and out in 60 seconds.</li>

                                      </ul>
                                    </li>
                                    <li>Coolers
                                        <ul type="circle">
                                          <li>
                                           Order online the day before our scheduled delivery days and we’ll drop your order in one of our coolers, which are conveniently located throughout the city.  No minimum order and no additional fees.
                                          </li>

                                        </ul>

                                    </li>
                                    <li>Delivery
                                      <ul>
                                        <li>Delivery is available to most of the Metro-Atlanta region.</li>
                                        <li>Your food is specially packed in thermally insulated container with a temperature-tracking sensor.</li>
                                        <li>Food is delivered the same day via courier.</li>
                                        <li>Receive email updates when your package is out for delivery and when it is delivered.</li>
                                      </ul>

                                    </li>

                                  </ul>

                                  <br>
                                  <p>Christophe’s To Go also makes it easy for people with specific dietary needs to eat what they want.</p>
                                  <br>

                                  <ul type="disc">
                                    <li>Paleo | Gluten Free | Vegetarian | Vegan options</li>
                                    <li>Every meal is labeled with nutritional information and ingredients</li>
                                    <li>Consistently portioned meals</li>
                                  </ul>

                                <br>
                                <p>Christophe’s To Go can provide you with the ultimate flexibility.  Do you want chicken, but your spouse wants pork?  No problem.  Kids asking for pasta?  Christophe has it covered.  Now you have the flexibility of serving each family member his or her own meal without the time, expense, and hassle of shopping and cooking.</p>





                              </div>

                                </div>

                            </div>
                        </div>

                    </div>

                </div>


                </div>

            </div>
        </div>
    </div>
</div>