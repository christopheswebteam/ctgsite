<div id="content-wrapper">
    <div class="dynamic-content container " id="main-content">
        <div class="padding-wrapper">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="name">
                        <h2>FAQ</h2>

                    </div>
                    <div class="description">
                        <?php foreach ($faqs as $row): ?>
                            <h4><?php echo $row->question ?></h4>
                            <p><?php echo ($row->answer); ?></p>
                        <?php endforeach ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>