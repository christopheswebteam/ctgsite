<!-- END DASHBOARD STATS -->
<div class="clearfix">
</div>

</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        2014 &copy; <?php echo SITE_NAME ?>
    </div>
    <div class="page-footer-tools">
        <span class="go-top">
            <i class="fa fa-angle-up"></i>
        </span>
    </div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/respond.min.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/excanvas.min.js"></script>
<![endif]-->

<script>
    var BASEURL = "<?php echo base_url(); ?>";
</script>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/jquery-slimscroll/jquery.slimscroll.js" type="text/javascript"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo BACKEND_THEME_URL ?>global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo BACKEND_THEME_URL ?>global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo BACKEND_THEME_URL ?>global/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/plupload/js/plupload.full.min.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo BACKEND_THEME_URL ?>global/plugins/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="<?php echo BACKEND_THEME_URL ?>global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="<?php echo BACKEND_THEME_URL ?>global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript" src="<?php echo BACKEND_THEME_URL ?>global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo BACKEND_THEME_URL ?>global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo BACKEND_THEME_URL ?>admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/scripts/datatable.js"></script>



<script src="<?php echo BACKEND_THEME_URL ?>admin/pages/scripts/tasks.js" type="text/javascript"></script>

<script src="<?php echo BACKEND_THEME_URL ?>dropzone/dropzone.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>global/scripts/products.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>

<script type="text/javascript">




        var uploader = new plupload.Uploader({

            runtimes : 'html5,flash,silverlight,html4',



            browse_button : document.getElementById('location_images_uploader_pickfiles'), // you can pass in id...

            container: document.getElementById('location_images_uploader_container'), // ... or DOM Element itself
            url : BASEURL + "backend/locations/upload/",
            filters : {
                max_file_size : '10mb',
                mime_types: [
                    {title : "Image files", extensions : "jpg,gif,png"},
                    {title : "Zip files", extensions : "zip"}
                ]
            },
            settings:{file_data_name:'userfile'},
			
            // Flash settings

            flash_swf_url : 'assets/plugins/plupload/js/Moxie.swf',
			
            // Silverlight settings

            silverlight_xap_url : 'assets/plugins/plupload/js/Moxie.xap',



            init: {

                PostInit: function() {

                    $('#location_images_uploader_filelist').html("");

                    $('#location_images_uploader_uploadfiles').click(function() {
                        uploader.start();
                        return false;
                    });


                    $('#location_images_uploader_filelist').on('click', '.added-files .remove', function(){

                        uploader.removeFile($(this).parent('.added-files').attr("id"));

                        $(this).parent('.added-files').remove();

                    });

                },



                FilesAdded: function(up, files) {

                    plupload.each(files, function(file) {

                        $('#location_images_uploader_filelist').append('<div class="alert alert-warning added-files" id="uploaded_file_' + file.id + '">' + file.name + '(' + plupload.formatSize(file.size) + ') <span class="status label label-info"></span>&nbsp;</div>');

                    });

                },



                UploadProgress: function(up, file) {

                    $('#uploaded_file_' + file.id + ' > .status').html(file.percent + '%');

                },



                FileUploaded: function(up, file, response) {

                    var response = $.parseJSON(response.response);



                    if (response.result && response.result == 'OK') {

                        var id = response.id; // uploaded file's unique name. Here you can collect uploaded file names and submit an jax request to your server side script to process the uploaded files and update the images tabke
                        get_all_loaction_file();




                        $('#uploaded_file_' + file.id + ' > .status').removeClass("label-info").addClass("label-success").html('<i class="fa fa-check"></i> Done'); // set successfull upload

                    } else {

                        $('#uploaded_file_' + file.id + ' > .status').removeClass("label-info").addClass("label-danger").html('<i class="fa fa-warning"></i> Failed'); // set failed upload

                        Metronic.alert({type: 'danger', message: 'One of uploads failed. Please retry.', closeInSeconds: 10, icon: 'warning'});

                    }

                },



                Error: function(up, err) {

                    Metronic.alert({type: 'danger', message: err.message, closeInSeconds: 10, icon: 'warning'});

                }

            }

        });



        uploader.init();


</script>


<!-- END PAGE LEVEL SCRIPTS -->



<script src="<?php echo BACKEND_THEME_URL ?>tinymce/tinymce.min.js"></script>
<script type="text/javascript">

    tinymce.init({
        //selector: "textarea",

        selector: ".tinymce_editor",
        menubar: false,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor media",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime table contextmenu paste textcolor jbimages",
        ],
        image_advtab: true,
        toolbar: "insertfile undo redo | styleselect | bold italic forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | link image jbimages media | preview code",
		relative_urls: false
    });

</script>

    <script>
      function test(){
        var street = document.getElementById("street_address_1").value;
        if(state==''){
          street = document.getElementById("street_address_2").value;
        }
        var city = document.getElementById("city").value;
        var state = document.getElementById("state").value;
        var address = street+' '+city+' '+state;
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode( { 'address': address},function(results, status){
          var location = results[0].geometry.location;
          var  lat = location.lat();
          var  lng = location.lng();
          change_map(lat,lng);
        });
      }

      function change_map(lat,long){
          document.getElementById("latitude").value=lat;
          document.getElementById("longitude").value=long;
        function initialize(lat,long){
          $('form').find("#load_img").show();
          var centerLatlng = new google.maps.LatLng(lat,long);
          var mapOptions = {
                zoom: 10,
                center: centerLatlng,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DEFAULT
                },
                navigationControl: true,
                navigationControlOptions: {
                    style: google.maps.NavigationControlStyle.DEFAULT
                }
          };
          map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
          marker = new google.maps.Marker({
                draggable: true,
                map: map,
                position: centerLatlng
          });
          google.maps.event.addListener(marker, 'dragend', function () {
           // console.log('show');
          $('form').find("#load_img").show();
                var curLatLng = marker.getPosition();
                var lat= curLatLng.lat();
                var lng= curLatLng.lng();
                document.getElementById("latitude").value=lat;
                document.getElementById("longitude").value=lng;
                if(lat!=''){
                   // console.log('hide');
                  $('form').find("#load_img").hide();
                }
          });
          google.maps.event.trigger(marker, "click");
          $('form').find("#load_img").hide();
        }
        google.maps.event.addDomListener(window, 'load', initialize(lat,long));
      }
      </script>

<script type="text/javascript">
  $(document).ready(function(){

    <?php if (!empty($tags)): ?>
      $("#tags").select2({
        tags:[<?php echo $tags; ?>]
      });
    <?php endif; ?>

    <?php if (!empty($pro_tags)): ?>
      $("#product_tags").select2({
          tags: [<?php echo $pro_tags;?>]
      });
    <?php endif; ?>

    <?php if (!empty($ingredients)): ?>
      $("#ingredients").select2({
          tags: [<?php echo $ingredients; ?>]
      });
    <?php endif; ?>


    $('.timepicker-no-seconds').timepicker({
        autoclose: true,
        minuteStep: 5
    });

    // handle input group button click
    $('.timepicker').parent('.input-group').on('click', '.input-group-btn', function(e){
        e.preventDefault();
        $(this).parent('.input-group').find('.timepicker').timepicker('showWidget');
    });

    <?php if($this->uri->segment(3)!='edit'){ ?>
       $('.small_group').fadeIn();
       $('#Small').attr('checked', 'checked');
       $('#Small').parent().addClass('checked');
    <?php } ?>


  $('#size .group').change(function(event) {
      var val=$(this).val();
      if($(this).is(':checked')){
        $('.'+val+'_group').fadeIn();
      }else{
        $('.'+val+'_group').fadeOut();
        //$('.single_group, .'+val+'_group').prop('checked', false);
      }
  });

  $('#size #single').change(function(event) {
      if($(this).is(':checked')){
        $('.small_group, .large_group, .medium_group, .family_group').fadeOut('slow', function() {
            $('.group').parent().removeClass('checked');
            $('.group').prop('checked', false);
        });
      }else{
        $('#single').parent().removeClass('checked');
        $('.single_group').prop('checked', false);
      }
  });

});

  // $('.group').removeAttr('checked','checked');
</script>

<script type="text/javascript">

function get_all_loaction_file() {
    $.post( BASEURL+'backend/locations/all_file/',{ 'param1':'file' }, function(data) {
       if(data!=''){
            $('#tablebody').html('');
            $('#tablebody').append(data);
       }
       else{
        return false;
       }
    });
}

function remove_location_image(id){
  $.post( BASEURL+'backend/locations/image_remove/',{'image_id':id} , function(data) {
      if(data!=''){
            $('#tablebody').html('');
            $('#tablebody').append(data);
      }
      else{
        return false;
      }
  });
}

</script>
<script>
    jQuery(document).ready(function(){
      Metronic.init(); // init metronic core components
      Layout.init(); // init current layout
      EcommerceProductsEdit.init();
    });
</script>

<script type="text/javascript">

$("#product_name").keyup(function(){
    var str = $(this).val();
    $("#product_slug").val(string_to_slug(str));
});

$("#blog_title").keyup(function(){
    var str = $(this).val();
    $("#blog_slug").val(string_to_slug(str));
});

function string_to_slug(str) {
  str = str.replace(/^\s+|\s+$/g, ''); // trim
  str = str.toLowerCase();

  // remove accents, swap ñ for n, etc
  var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
  var to   = "aaaaeeeeiiiioooouuuunc------";
  for (var i=0, l=from.length ; i<l ; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }

  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-'); // collapse dashes

  return str;
}

</script>


<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>