<?php

$segment = $this->uri->segment(2);
if ($segment == 'dashboard' || $segment == '') $dashboard = 'active'; else $dashboard = '';

if ($segment == 'customers' || $segment == 'add' || $segment == 'edit' ||  $segment == 'view') $customers = 'active'; else $customers = '';

if ($segment == 'orders') $orders = 'active'; else $orders = '';

if ($segment == 'blog' || $segment == 'add' || $segment == 'edit' ||  $segment == 'view') $blog = 'active'; else $blog = '';

if ($segment == 'products' || $segment == 'add' || $segment == 'edit' ||  $segment == 'view' ) $products = 'active'; else $products = '';
if ($segment == 'locations' || $segment == 'add' || $segment == 'edit' ||  $segment == 'view') $locations = 'active'; else $locations = '';

if ($segment == 'tags' || $segment == 'add' || $segment == 'edit' ||  $segment == 'view') $tags = 'active'; else $tags = '';

if ($segment == 'users' || $segment == 'add' || $segment == 'edit' ||  $segment == 'view') $users = 'active'; else $users = '';

?>
<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo SITE_NAME ?> | Admin Dashboard</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="<?php echo BACKEND_THEME_URL ?>global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo BACKEND_THEME_URL ?>global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo BACKEND_THEME_URL ?>global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo BACKEND_THEME_URL ?>global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" type="text/css" href="<?php echo BACKEND_THEME_URL ?>global/plugins/select2/select2.css"/>
        <link rel="stylesheet" href="<?php echo BACKEND_THEME_URL ?>global/plugins/data-tables/DT_bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo BACKEND_THEME_URL ?>global/plugins/bootstrap-datepicker/css/datepicker.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo BACKEND_THEME_URL ?>global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo BACKEND_THEME_URL ?>global/plugins/bootstrap-datetimepicker/css/datetimepicker.css"/>
        <link href="<?php echo BACKEND_THEME_URL ?>global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>

        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        <link href="<?php echo BACKEND_THEME_URL ?>global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo BACKEND_THEME_URL ?>global/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo BACKEND_THEME_URL ?>global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN PAGE STYLES -->
        <link href="<?php echo BACKEND_THEME_URL ?>admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?php echo BACKEND_THEME_URL ?>global/css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo BACKEND_THEME_URL ?>global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo BACKEND_THEME_URL ?>admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo BACKEND_THEME_URL ?>admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?php echo BACKEND_THEME_URL ?>admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
         <link href="<?php echo BACKEND_THEME_URL ?>dropzone/css/dropzone.css" type="text/css" rel="stylesheet" />
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>

    <body class="page-header-fixed">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="<?php echo base_url('backend/dashboard') ?>">
                        <img src="<?php echo base_url() ?>assets/logo-mini.png" alt="logo" class="logo-default"/>
                    </a>
                    <div class="menu-toggler sidebar-toggler hide">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <div class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                </div>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">

                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <li class="dropdown dropdown-user">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <span class="username">
                                    <?php echo superadmin_name() ?> </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo base_url() ?>superadmin/profile">
                                        <i class="fa fa-user"></i> My Profile </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>superadmin/change_password">
                                        <i class="fa fa-lock"></i> Change Password </a>
                                </li>
                                <li class="divider">
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>superadmin/logout">
                                        <i class="fa fa-key"></i> Log Out </a>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                        <!-- END USER LOGIN DROPDOWN -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <ul class="page-sidebar-menu" data-auto-scroll="false" data-auto-speed="200">
                        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                        <li class="sidebar-toggler-wrapper">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <div class="sidebar-toggler">
                            </div>
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                        </li>
                        <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                        <li class="sidebar-search-wrapper hidden-xs">
                            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                            <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                            <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                            <br>
                            <!-- END RESPONSIVE QUICK SEARCH FORM -->
                        </li>
                        <li class="start <?php echo $dashboard ?>">
                            <a href="<?php echo base_url('backend/dashboard') ?>">
                                <i class="fa fa-home"></i>
                                <span class="title">Dashboard</span>
                                <span class="selected"></span>
                            </a>
                        </li>

                        <li class="<?php echo $customers ?>" >
                            <a href="javascript:;">
                                <i class="fa fa-users"></i>
                                <span class="title">Customers</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="<?php echo base_url('backend/customers') ?>">
                                        View Customers</a>
                                <li>
                                    <a href="<?php echo base_url('backend/customers/add') ?>">
                                        Add Customer</a>
                                </li>
                            </ul>
                        </li>

                         <li class="<?php echo $orders ?>" >
                            <a href="javascript:;">
                                <i class="fa fa-file"></i>
                                <span class="title">Orders</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="<?php echo base_url('backend/orders') ?>"> View Orders</a>
                                 </li>
                            </ul>
                        </li>

                         <li class="<?php echo $products ?>" >
                            <a href="javascript:;">
                                <i class="fa fa-file"></i>
                                <span class="title">Products</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="<?php echo base_url('backend/products') ?>">View Products</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('backend/products/add') ?>">Add Product</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('backend/products/ingredients') ?>">View Ingredients</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('backend/products/ingredient_add') ?>">Add Ingredient</a>
                                </li>                                
                            </ul>
                        </li>


                        <li class="<?php echo $locations ?>" >
                            <a href="javascript:;">
                                <i class="fa fa-map-marker"></i>
                                <span class="title">Locations</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="<?php echo base_url('backend/locations') ?>"> View Locations</a>
                                 </li>
                                 <li>
                                    <a href="<?php echo base_url('backend/locations/add') ?>">Add Location</a>
                                 </li>
                            </ul>
                        </li>

                        <li class="<?php echo $blog ?>" >
                            <a href="javascript:;">
                                <i class="fa fa-comments"></i>
                                <span class="title">Blog</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="<?php echo base_url('backend/blog') ?>"> View Posts</a>
                                 </li>
                                 <li>
                                    <a href="<?php echo base_url('backend/blog/add') ?>">Add Post</a>
                                 </li>
                            </ul>
                        </li>

                         <li class="<?php echo $tags ?>" >
                            <a href="javascript:;">
                                <i class="fa fa-file"></i>
                                <span class="title">Tags</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="<?php echo base_url('backend/tags') ?>"> View Tags</a>
                                 </li>
                                 <li>
                                    <a href="<?php echo base_url('backend/tags/add') ?>">Add Tag</a>
                                 </li>
                            </ul>
                        </li>

                         <li class="<?php echo $users ?>" >
                            <a href="javascript:;">
                                <i class="fa fa-users"></i>
                                <span class="title">Users</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="<?php echo base_url('backend/users') ?>">
                                        View Users</a>
                                <li>
                                    <a href="<?php echo base_url('backend/users/add') ?>">
                                        Add User</a>
                                </li>
                            </ul>
                        </li>



                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                    <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Modal title</h4>
                                </div>
                                <div class="modal-body">
                                    Widget settings form goes here
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn blue">Save changes</button>
                                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                    <?php msg_alert(); ?>
