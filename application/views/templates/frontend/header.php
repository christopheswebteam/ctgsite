<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php if (!empty($site_title)) echo $site_title ?> </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="<?php echo FRONTEND_THEME_URL ?>less/custom.css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Covered+By+Your+Grace' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo FRONTEND_THEME_URL ?>less/jquery.maximage.css" type="text/css" media="screen" charset="utf-8" />
        <script src="<?php echo FRONTEND_THEME_URL ?>js/jquery-2.0.3.min.js"></script>
        <script src="<?php echo FRONTEND_THEME_URL ?>js/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
		<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-45958210-1', 'auto');
		ga('send', 'pageview');

		</script>
        <?php $get_nav_menu = get_nav_menu($slug = 'main-menu', $is_location = FALSE); ?>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <div class="container full-bg visible-xs sm-navbar">
            <nav>
                <div class="row">
                    <div class="navbar-inner">
                        <ul class="main-menu nav">
                            <li>
                                <header class="small-logo">
                                    <a href="<?php echo base_url(); ?>" title=""><img src="<?php echo FRONTEND_THEME_URL ?>img/logo.png" alt="Barnelli"/></a>
                                </header>
                            </li>
                            <li class="reorder">
                                <a href="#" title="" class="exclude" data-djax-exclude="true"><i class="fa fa-reorder"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>



        <div id="mobile-nav" class="visible-xs">


            <div class="" id="flyout-container">
                <ul id="menu-mobile" class="nav flyout-menu">

                    <?php if ($get_nav_menu): ?>

                        <?php $m = 0; foreach ($get_nav_menu as $row): $m++; ?>
                            <?php

                            if (!empty($row->child_navigation)) $childchild_navigation = TRUE;
                            else $childchild_navigation = FALSE;

                            ?>

                            <li  <?php if ($childchild_navigation) { echo 'class="menu-item-has-children menu-item"'; } else { echo 'class="menu-item"'; } ?>>

                                <a <?php

                                if ($childchild_navigation) {
                                    echo 'href="#"';
                                } else {

                                    ?> href="<?php echo base_url() ?><?php echo $row->navigation_link ?>" <?php } ?> >
                                        <?php echo ucfirst(strtolower($row->navigation_label)) ?>

                                </a>
                                <?php if ($childchild_navigation) echo '<span class="open-children"><i class="fa fa-angle-down"></i></span>'; ?>
                                <?php if ($childchild_navigation): ?>
                                    <ul class="subnav">
                                        <?php foreach ($row->child_navigation as $child): ?>
                                            <li> <a href="<?php echo base_url() ?><?php echo $child->navigation_link ?>">
                                                    <?php echo ucfirst(strtolower($child->navigation_label)) ?> </a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>

                            </li>
                        <?php endforeach; ?>

                    <?php endif ?>

                </ul>
            </div>

        </div>

        <div class="navbar hidden-xs" style=" position: static;">
            <div class="container0">
                <div class="row0">
                    <nav class="col-md-12 clearfix">
                        <ul class="main-nav white-nav dotted-separator">
                            <li class="menu-image">
                                <div>
                                    <a class="content-link" title="<?php echo SITE_NAME ?>" href="<?php echo base_url(); ?>"><img src="<?php echo FRONTEND_THEME_URL ?>img/logo.png" alt=""></a>
                                </div>
                            </li>


                            <?php if ($get_nav_menu): ?>

                                <?php $m = 0; foreach ($get_nav_menu as $row): $m++; ?>
                                    <?php

                                    if (!empty($row->child_navigation)) $childchild_navigation = TRUE;
                                    else $childchild_navigation = FALSE;

                                    ?>

                                    <li>
                                        <?php if ($childchild_navigation) { echo '<div>'; } ?>

                                        <a <?php

                                        if ($childchild_navigation) {
                                            echo 'class="content-link menu-wrapper menu-item-has-children" href="#"';
                                        } else {

                                            ?> href="<?php echo base_url() ?><?php echo $row->navigation_link ?>" <?php } ?> >
                                                <?php echo ucfirst(strtolower($row->navigation_label)) ?>
                                                <?php if ($childchild_navigation) echo '<i class="icon-angle-down"></i>'; ?>

                                        </a>
                                        <?php if ($childchild_navigation): ?>
                                            <ul class="subnav">
                                                <?php foreach ($row->child_navigation as $child): ?>
                                                    <li> <a href="<?php echo base_url() ?><?php echo $child->navigation_link ?>">
                                                            <?php echo ucfirst(strtolower($child->navigation_label)) ?> </a></li>
                                                <?php endforeach; ?>
                                            </ul>
                                        <?php endif; ?>
                                        <?php if ($childchild_navigation) { echo '</div>'; } ?>
                                    </li>
                                <?php endforeach; ?>

                            <?php endif ?>


                            <li class="">
                                <div class="cart">
                                    <a href="#"><img src="<?php echo FRONTEND_THEME_URL ?>img/cart.png"></a>
                                    <p class="cart-count">0</p>

                                </div>
                            </li>
                        </ul>


                    </nav>
                </div>
            </div>
        </div>


