<div class="" id="flyout-container">
    <ul id="menu-mobile" class="nav flyout-menu">
        <li class="menu-item-has-children menu-item">
            <a title="" href="<?php echo base_url('how_it_works/the_concept'); ?>">How It Works</a>
            <span class="open-children"><i class="fa fa-angle-down"></i></span>
            <ul class="subnav">
                <li><a href="<?php echo base_url('how_it_works/the_concept'); ?>">The Concept</a></li>
                <li><a href="<?php echo base_url('how_it_works/chef_christophe'); ?>">Chef Christophe</a></li>
                <li><a href="<?php echo base_url('how_it_works/the_food'); ?>">The Food</a></li>
                <li><a href="<?php echo base_url('how_it_works/our_containers'); ?>">Our Containers</a></li>
                <li><a href="<?php echo base_url('how_it_works/chef_christophes_team'); ?>">Chef Christophe's Team</a></li>
                <li><a href="<?php echo base_url('how_it_works/catering'); ?>">Catering</a></li>
                <li><a href="<?php echo base_url('how_it_works/the_company'); ?>">The Company</a></li>
                <li><a href="<?php echo base_url('how_it_works/faq'); ?>">FAQ</a></li>
        	</ul>
        </li>
        <li class="menu-item">
            <a title="" href="<?php echo base_url('menu') ?>">Menu</a>
        </li>
        <li class="menu-item">
            <a title="" href="<?php echo base_url('locations') ?>">Locations</a>
        </li>
        <li class="menu-item">
            <a title="" href="<?php echo base_url('blog') ?>">Blog</a>
        </li>
         <li class="menu-item-has-children menu-item">
            <a title="" href="#">Media</a>
            <span class="open-children"><i class="fa fa-angle-down"></i></span>
            <ul class="subnav">
                <li><a href="<?php echo base_url('media/photos'); ?>">Photos</a></li>
                <li><a href="<?php echo base_url('media/videos'); ?>">Videos</a></li>
          </ul>
        </li>

        <li class="menu-item cart">
        <a href="#">
          <span class="glyphicon glyphicon-shopping-cart"></span>
          <span class="badge badge-default"><?php echo $this->cart->total_items();?></span>
        </a>
        </li>

         <!-- <li class="menu-item-has-children menu-item">
            <a title="" href="#" onclick="return false">Login</a>
            <span class="open-children"><i class="fa fa-angle-down"></i></span>
            <ul class="subnav">
               <li>

                   <form method="post" name="contactform" action="<?php //echo base_url() ?>" id="login-form">

                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <label for="user_name">User Name</label>
                          <input type="text" placeholder="User Name" class="form-control" id="name1" name="user_name">


                        <label for="password">Password</label>

                          <input type="text" placeholder="Password" class="form-control" id="password" name="password">
                      </div>
                      <div class="clearfix"></div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        &nbsp;
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="pull-right">
                              <input type="submit" class="btn btn-info small" id="login" value="Login">
                          </div>
                      </div>

                  </form>

               </li>

            </ul>
                 </li> -->


    </ul>
</div>