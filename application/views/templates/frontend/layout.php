<!DOCTYPE html>



<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->

<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->

<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->

<!--[if gt IE 8]><!--> <html class="no-js"><!--<![endif]-->



    <head>

        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">



        <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico" type="image/vnd.microsoft.icon" />



        <title><?php echo (isset($title)) ? $title : "Christophe's To Go"; ?></title>



        <meta name="description" content="" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />



        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />

        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" />



        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/less/custom.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/css/custom.css?<?php echo time(); ?>">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/css/navigation.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/css/jquery-ui.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/css/jquery-ui.structure.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/css/jquery-ui.theme.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/fancybox/jquery.fancybox.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/less/jquery.maximage.css" type="text/css" media="screen" charset="utf-8" />


        <!--[if lte IE 8]>

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/css/ie-8.css" />


        <![endif]-->


        <script src="<?php echo base_url(); ?>assets/frontend/js/jquery-2.0.3.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/frontend/js/modernizr-2.6.2.min.js"></script>



        <script type="text/javascript">

            HOME_URL = '<?php echo site_url(); ?>';

        </script>

        <style type="text/css">

            .modal-body td {

                padding-bottom: 12px;

            }

        </style>



        <script src="<?php echo base_url(); ?>assets/frontend/js/utils/images.pan.js"></script>
        <script src="<?php echo base_url(); ?>assets/frontend/js/ctg/locations.js"></script>

    </head>



    <body class="<?php echo (isset($isHomePage) && $isHomePage === true) ? 'home-page-view' : ''; ?>" style="background-image: url(<?php echo random_background(); ?>); background-attachment: fixed; background-position: left top; background-repeat: no-repeat; background-size: cover; height:100%;">

        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-45958210-1', 'auto');
            ga('send', 'pageview');

        </script>


        <div class="container full-bg visible-xs sm-navbar browser-compatible">

            <nav>

                <div class="row">

                    <div class="navbar-inner">

                        <ul class="main-menu nav">

                            <li>

                                <header class="small-logo">

                                    <a href="<?php echo site_url(); ?>" title="Christophe's To Go">

                                        <img src="<?php echo base_url(); ?>assets/logo-web.png" alt="Christophe's To Go" />

                                    </a>

                                </header>

                            </li>

                            <li class="reorder">

                                <a href="#" title="" class="exclude" data-djax-exclude="true"><i class="fa fa-reorder"></i></a>

                            </li>

                        </ul>

                    </div>

                </div>

            </nav>

        </div>



        <div id="mobile-nav" class="visible-xs browser-compatible">

            <?php $this->load->view('templates/frontend/navigation_mobile'); ?>

        </div>



        <?php $this->load->view('templates/frontend/navigation'); ?>



        <div id="content-wrapper" style="width:100%; position:relative;min-height:100%;" class="browser-compatible">

            <?php $this->load->view($template); ?>

        </div>

        <div class="clear browser-compatible"></div>

        <div class="footer-container browser-compatible">



            <div class="footer-bar">

                <a class="fancybox-privacy-policy" data-fancybox-type="iframe" href="https://www.iubenda.com/privacy-policy/376215">Privacy Policy </a> | <a href="<?php echo base_url('contact_us') ?>">Contact Us</a></div>



            <div class="social-share">

                <ul>

                    <li><a target="_blank" href="https://www.facebook.com/ChristophesToGo"><i class="fa fa-facebook"></i></a></li>

                    <li><a target="_blank" href="https://twitter.com/christophestogo"><i class="fa fa-twitter"></i></a></li>

                    <li><a target="_blank" href="https://plus.google.com/+ChristophesToGoJohnsCreek/about"><i class="fa fa-google-plus"></i></a></li>

                </ul>

            </div>

        </div>

        <div class="browser-incompatible">
            We are sorry but this site is not compatible with your internet browser. Please upgrade to a current browser. We recommend Google Chrome.
        </div>



        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/frontend/js/jquery.djax.min.js"></script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBX8aPjWso8S6V10ZVhx2ZloP2AcHNotO4&amp;sensor=false"></script>

        <script src="<?php echo base_url(); ?>assets/frontend/js/jquery.nicescroll.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/frontend/js/jquery.ba-throttle-debounce.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/frontend/js/transit.js"></script>

        <script src="<?php echo base_url(); ?>assets/frontend/js/jquery.cycle.all.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/frontend/js/jquery.maximage.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/frontend/js/jquery.colorbox-min.js"></script>

        <script src="<?php echo base_url(); ?>assets/frontend/js/owl.carousel.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/frontend/js/jquery-ui.js"></script>




        <script src="<?php echo base_url(); ?>assets/frontend/js/main.js"></script>



        <script src="<?php echo base_url(); ?>assets/frontend/fancybox/jquery.fancybox.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/jquery.endless-scroll.js"></script>





        <!-- Add fancyBox - thumbnail helper (this is optional) -->

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/fancybox/helpers/jquery.fancybox-thumbs.css?v=2.1.5" />

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/fancybox/helpers/jquery.fancybox-thumbs.js?v=2.1.5"></script>

        <!-- Add fancyBox - media helper (this is optional) -->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/fancybox/helpers/jquery.fancybox-media.js?v=1.0.0"></script>

        <script type="text/javascript">


            function check_add_to_cart(frmsid, portionsid, productsid, flag, check_inventory) {



                var msg = "";
                var location_id = $('#frm' + frmsid).find('#location_' + frmsid);
                var pickup_date = $('#frm' + frmsid).find('#pickup_date');
                if (location_id.val() == '0') {
                    alert("Please select a location first.");
                    return false;
                } else {

                    if (pickup_date.val() == '') {
                        alert("Please choose a pickup date.");
                        return false;
                    } else {

                        //if (check_inventory==1) {

                        $.get('<?php echo base_url() ?>menu/check_params/' + productsid + '/' + location_id.val() + '/' + portionsid + '/' + flag, $('#frm' + frmsid).serialize(), function(res) {
                            if (res.status) {
                                //alert('OK '+res.data);
                                $('body').find('#frm' + frmsid).submit();
                            } else {
                                alert(res.data);
                            }

                        });

                        //}else{

                        //$('body').find('#frm' + frmsid).submit();
                        //}
                    }
                }

            }


            $(document).ready(function() {

                setInterval(function() {

                    $.get('<?php echo base_url() ?>cart/total_item', function(data) {

                        $(".badge-default").html(data);

                    });

                }, 2000);

            });

        </script>

        <script>

            _submenu_closeable = true;

            _submenu_holding = false;

            var products = {
                grid: [],
                list: []
            };

            $(document).ready(function() {





                var urlParams;

                (window.onpopstate = function() {

                    var match,
                            pl = /\+/g, // Regex for replacing addition symbol with a space

                            search = /([^&=]+)=?([^&]*)/g,
                            decode = function(s) {

                                return decodeURIComponent(s.replace(pl, " "));

                            },
                            query = window.location.search.substring(1);



                    urlParams = {};

                    while (match = search.exec (query))
                        urlParams[decode(match[1])] = decode(match[2]);

                })();





                function getUrlVars()

                {

                    var vars = [], hash;

                    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

                    for (var i = 0; i < hashes.length; i++)

                    {

                        hash = hashes[i].split('=');

                        vars.push(hash[0]);

                        vars[hash[0]] = hash[1];

                    }

                    return vars;

                }

                function loadProducts(offset, type, per_page, display) {

                    if (display === true) {
                        $('div#lastPostsLoader').html('<img src="<?php echo base_url(); ?>assets/frontend/img/ajax-loader.gif" title="Ajax-loader" style="border:1px;"/>');
                        $('div#loadmoreajaxloader').show();
                    }

                    if (type === 'grid' && products.grid[offset] !== undefined) {
                        if (display === true) {
                            $('#menu_panel').find('#grid_view').append(products.grid[offset]);
                        }
                    } else if (type === 'list' && products.list[offset] !== undefined) {
                        if (display === true) {
                            $('#menu_panel').find('#list_view').append(products.list[offset]);
                        }
                    } else {

                        var params = window.location.search.split('?');
                        $.get("<?php echo base_url('menu/ajax_product'); ?>/" + offset + "/" + per_page, params[1], function(data) {

                            if (data.status) {

                                if (display === true) {
                                    $('#menu_panel').find('#grid_view_offset').val(data.offset);
                                }

                                if (type === 'grid') {
                                    products.grid[offset] = data.products_grid;
                                } else {
                                    products.list[offset] = data.products_list;
                                }

                                if (display === true) {
                                    if (type === 'grid') {
                                        $('#menu_panel').find('#grid_view').append(data.products_grid);
                                    } else {
                                        $('#menu_panel').find('#list_view').append(data.products_list);
                                    }
                                }

                            } else {
                                if (display === true) {
                                    $('div#loadmoreajaxloader').html('<center><h4>No more menus item to show.</h4></center>');
                                }
                            }

                        });
                    }
                }

                var offset = 12;

                for (var i = offset + 3; i <= offset + 15; i += 3) {
                    loadProducts(i, 'grid', 3, false);
                    loadProducts(i, 'list', 2, false);
                }

                $(window).scroll(function() {



                    var wintop = $(window).scrollTop(), docheight = $(document).height(), winheight = $(window).height();

                    var scrolltrigger = 0.95;



                    if ((wintop / (docheight - winheight)) > scrolltrigger) {

                        console.log('scroll bottom');

                        loadProducts(offset, 'grid', 3, true);
                        loadProducts(offset, 'list', 2, true);

                        for (var i = offset + 3; i <= offset + 15; i += 3) {
                            loadProducts(i, 'grid', 3, false);
                            loadProducts(i, 'list', 2, false);
                        }

                        offset = offset + 3;

                    }

                });







                $('.fancybox').fancybox({minWidth: 300, minHeight: 'auto', openEffect: 'elastic', closeEffect: 'elastic', closeSpeed: 'slow', openSpeed: 'slow'});

                $('.fancybox-privacy-policy').fancybox({openEffect: 'elastic', closeEffect: 'elastic', closeSpeed: 'slow', openSpeed: 'slow'});

                $('.fancybox-media').fancybox({
                    openEffect: 'elastic',
                    closeEffect: 'elastic',
                    closeSpeed: 'slow', openSpeed: 'slow',
                    helpers: {
                        media: {}

                    }

                });





                $('a').on({
                    "click": function() {

                        if ($(this).parents('.dropdown.keep-open').length <= 0) {

                            _submenu_closeable = true;

                            $('.single-nav a').removeClass('active');

                            $('.dropdown.keep-open .keep-open-sub a').removeClass('active');

                        }

                    }

                });

                $('.single-nav').on({
                    "click": function() {

                        _submenu_closeable = true;

                        $('.single-nav a').removeClass('active');

                        $('.dropdown.keep-open .keep-open-sub a').removeClass('active');

                        $(this).find('a').addClass('active');

                    }

                });



                $('.dropdown.keep-open .keep-open-sub a').on({
                    "click": function() {

                        _submenu_closeable = false;

                        _submenu_holding = true;

                        $('.single-nav a').removeClass('active');

                        $('.dropdown.keep-open .keep-open-sub a').removeClass('active');

                        $(this).addClass('active');

                    },
                });





                $('.dropdown.keep-open').on({
                    "shown.bs.dropdown": function() {



                    },
                    "click": function() {

                        if (_submenu_holding) {

                            _submenu_holding = false;

                        } else {

                            _submenu_closeable = true;



                        }

                    },
                    "hide.bs.dropdown": function() {

                        return _submenu_closeable;

                    },
                    "hidden.bs.dropdown": function() {

                        if ($('.dropdown.keep-open.open').length == 0) {

                            if ($('.dropdown.keep-open .keep-open-sub a.active').length > 0) {

                                if ($('.dropdown.keep-open .keep-open-sub a.active').parents('.dropdown.keep-open')[0] != $(this)[0]) {

                                    $('.dropdown.keep-open .keep-open-sub a.active').parents('.dropdown.keep-open').addClass('open');

                                    _submenu_closeable = false;

                                }



                            }

                        }

                    }

                });







            });



            function user_login() {

                var flag = true;

                var user_name = jQuery('#user_name');

                var user_password = jQuery('#user_password');

                if (user_name.val() == '') {

                    jQuery('#msg_user_name').html("User Name Required.");

                    flag = false;

                } else {

                    jQuery('#msg_user_name').html('');



                }

                if (user_password.val() == '') {

                    jQuery('#msg_user_password').html("Password Required.");

                    flag = false;

                } else {

                    jQuery('#msg_user_password').html('');

                }

                if (flag == true) {

                    jQuery.post('<?php echo base_url() ?>user/login', {user_name: user_name.val(), password: user_password.val()}, function(data, textStatus, xhr) {

                        if (data == 1) {

                            window.location = "<?php echo base_url() ?>user/dashboard";

                        } else {

                            jQuery('#msg_user_name').html(data);

                        }



                    });

                }

                return false;

            }

        </script>





    </body>



</html>