<div class="navbar hidden-xs navtop top-navigation">
    <div class="yamm hidden-xs">
        <div class="container">


            <div class="header-container">
                <div class="navbar-header logo-container">
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" title="Christophe's To Go" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/logo-web.png" alt="Christophe's To Go" /></a>

      <!--   <a href="#" class="navbar-brand">Christophe's <small>to go</small></a> -->
                </div>
                <div   class="navbar-collapse collapse">
                    <ul id="main-nav1" class="nav navbar-nav">

                        <li class="dropdown yamm-fw keep-open">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="<?php echo site_url('how_it_works/the_concept'); ?>">How It Works<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="yamm-content">
                                        <ul class="nav navbar-nav keep-open-sub">
                                            <li><a class="content-link" href="<?php echo site_url('how_it_works/the_concept'); ?>">The Concept</a></li>
                                            <li><a class="content-link" href="<?php echo site_url('how_it_works/chef_christophe'); ?>">Chef Christophe</a></li>
                                            <li><a class="content-link" href="<?php echo site_url('how_it_works/the_food'); ?>">The Food</a></li>
                                            <li><a class="content-link" href="<?php echo site_url('how_it_works/our_containers'); ?>">Our Containers</a></li>
                                            <li><a class="content-link" href="<?php echo site_url('how_it_works/chef_christophes_team'); ?>">Chef Christophe's Team</a></li>
                                            <li><a class="content-link" href="<?php echo site_url('how_it_works/catering'); ?>">Catering</a></li>
                                            <li><a class="content-link" href="<?php echo site_url('how_it_works/the_company'); ?>">The Company</a></li>
                                            <li><a class="content-link" href="<?php echo site_url('how_it_works/faq'); ?>">FAQ</a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="single-nav"><a class="content-link menu-wrapper" href="<?php echo site_url('menu') ?>">Menu</a></li>
                        <li class="single-nav"><a class="content-link menu-wrapper" href="<?php echo site_url('locations') ?>">Locations</a></li>
                        <li class="single-nav"><a class="content-link" href="<?php echo site_url('blog') ?>">Blog</a></li>

                        <li class="dropdown yamm-fw keep-open">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">Media<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="yamm-content">
                                        <ul class="nav navbar-nav keep-open-sub">
                                            <li><a class="content-link" href="<?php echo site_url('media/photos'); ?>">Photos</a></li>
                                            <li><a class="content-link" href="<?php echo site_url('media/videos'); ?>">Videos</a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </li>

                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li >
                            <a class="content-link shoping-cart-button" href="<?php echo site_url('cart'); ?>">
                                <span class="glyphicon glyphicon-shopping-cart"></span>
                                <span class="badge badge-default"> <?php echo $this->cart->total_items();?> </span>
                            </a>
                        </li>
                        <?php if (user_logged_in()): ?>


                            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" onclick="return false"> <i class="fa fa-user"></i> <?php echo user_name() ?> <b class="caret"></b> </a>
                                <ul class="dropdown-menu user-drop">
                                    <li>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <a href="<?php echo site_url('user/dashboard') ?>" title=""><i class="fa fa-home"></i> Dashboard</a>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <a href="<?php echo site_url('user/logout') ?>" title=""><i class="fa fa-lock"></i> Logout</a>
                                        </div>
                                    </li>

                                </ul>
                            </li>

                        <?php else: ?>



                           <!--  <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" onclick="return false">Login<b class="caret0"></b></a>
                               <ul class="dropdown-menu user-login">
                                   <li>
                                       <form method="post" name="contactform" action="<?php //echo base_url() ?>" id="login-form">
                                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                               <label for="user_name">User Name</label>
                                               <input type="text" placeholder="User Name" class="form-control" id="user_name" name="user_name">
                                               <span id="msg_user_name" class="error" style="color: red;font-size: 12px;"></span><br>


                                               <label for="password">Password</label>

                                               <input type="password" placeholder="Password" class="form-control" id="user_password" name="user_password">
                                               <span id="msg_user_password" class="error" style="color: red;font-size: 12px;"></span>
                                           </div>
                                           <div class="clearfix"></div>
                                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                               &nbsp;
                                           </div>
                                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                               <div class="pull-right">
                                                   <input type="submit" class="btn btn-info small" id="login" value="Login" onclick="return user_login();">
                                               </div>
                                           </div>

                                       </form>
                                   </li>
                               </ul>
                           </li> -->

                        <?php endif; ?>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>

    </div>
    <div class="clear"></div>
</div>
<div class="clear"></div>