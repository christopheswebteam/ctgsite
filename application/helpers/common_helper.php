<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**

 *

 * 	clear cache

 *

 */
if (!function_exists('clear_cache')) {



    function clear_cache()
    {

        $CI = & get_instance();

        $CI->output->set_header('Expires: Wed, 11 Jan 1984 05:00:00 GMT');

        $CI->output->set_header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . 'GMT');

        $CI->output->set_header("Cache-Control: no-cache, no-store, must-revalidate");

        $CI->output->set_header("Pragma: no-cache");
    }

}

/**

 *

 * 	check superadmin logged in

 *

 */
if (!function_exists('superadmin_logged_in')) {



    function superadmin_logged_in()
    {

        $CI = & get_instance();

        $superadmin_info = $CI->session->userdata('superadmin_info');

        if ($superadmin_info['logged_in'] === TRUE && $superadmin_info['user_role'] == 0)
            return TRUE;
        else
            return FALSE;
    }

}

/**

 *

 * 	get superadmin id

 *

 */
if (!function_exists('superadmin_id')) {



    function superadmin_id()
    {

        $CI = & get_instance();

        $superadmin_info = $CI->session->userdata('superadmin_info');

        return $superadmin_info['id'];
    }

}

/**

 *

 * 	superadmin login information

 *

 */
if (!function_exists('superadmin_name')) {



    function superadmin_name()
    {

        $CI = & get_instance();

        $superadmin_info = $CI->session->userdata('superadmin_info');

        if ($superadmin_info['logged_in'] === TRUE)
            return $superadmin_info['name'];
        else
            return FALSE;
    }

}

/* * ***************Admin ***************** */

/**

 *

 * 	user role information

 *

 */
if (!function_exists('user_role')) {



    function user_role()
    {

        $CI = & get_instance();

        $user_info = $CI->session->userdata('user_info');

        if ($user_info['logged_in'] === TRUE)
            return $user_info['user_role'];
        else
            return FALSE;
    }

}

/**

 *

 * 	user login information

 *

 */
if (!function_exists('user_name')) {



    function user_name()
    {

        $CI = & get_instance();

        $user_info = $CI->session->userdata('user_info');

        if ($user_info['logged_in'] === TRUE)
            return $user_info['user_name'];
        else
            return FALSE;
    }

}

/**

 *

 * 	check user logged in

 *

 */
if (!function_exists('user_logged_in')) {



    function user_logged_in()
    {

        $CI = & get_instance();

        $user_info = $CI->session->userdata('user_info');

        if ($user_info['logged_in'] === TRUE)
            return TRUE;
        else
            return FALSE;
    }

}

/**

 *

 * 	get user id

 *

 */
if (!function_exists('user_id')) {



    function user_id()
    {

        $CI = & get_instance();

        $user_info = $CI->session->userdata('user_info');

        return $user_info['id'];
    }

}

/**

 *

 * 	backend pagination

 *

 */
if (!function_exists('backend_pagination')) {



    function backend_pagination()
    {

        $data = array();

        $data['full_tag_open'] = '<ul class="pagination">';

        $data['full_tag_close'] = '</ul>';

        $data['first_tag_open'] = '<li>';

        $data['first_tag_close'] = '</li>';

        $data['num_tag_open'] = '<li>';

        $data['num_tag_close'] = '</li>';

        $data['last_tag_open'] = '<li>';

        $data['last_tag_close'] = '</li>';

        $data['next_tag_open'] = '<li>';

        $data['next_tag_close'] = '</li>';

        $data['prev_tag_open'] = '<li>';

        $data['prev_tag_close'] = '</li>';

        $data['cur_tag_open'] = '<li class="active"><a href="#">';

        $data['cur_tag_close'] = '</a></li>';

        return $data;
    }

}

/**

 *

 * 	frontend pagination

 *

 */
if (!function_exists('frontend_pagination')) {



    function frontend_pagination()
    {

        $data = array();

        $data['full_tag_open'] = '<ul class="pagination">';

        $data['full_tag_close'] = '</ul>';

        $data['first_tag_open'] = '<li>';

        $data['first_tag_close'] = '</li>';

        $data['num_tag_open'] = '<li>';

        $data['num_tag_close'] = '</li>';

        $data['last_tag_open'] = '<li>';

        $data['last_tag_close'] = '</li>';

        $data['next_tag_open'] = '<li>';

        $data['next_tag_close'] = '</li>';

        $data['prev_tag_open'] = '<li>';

        $data['prev_tag_close'] = '</li>';

        $data['cur_tag_open'] = '<li class="active"><a href="#">';

        $data['cur_tag_close'] = '</a></li>';

        return $data;
    }

}

/**

 *

 * 	Menu Information

 *

 */
/* if ( ! function_exists('get_nav_menu')) {

  function get_nav_menu($slug=''){

  $CI =& get_instance();

  $CI->load->model('user_model');

  $menu = $CI->user_model->get_nav_menu($slug);

  if($menu)

  return $menu;

  else

  return FALSE;

  }

  } */

/**

 *

 * 	Menu Location

 *

 */
/* if ( ! function_exists('get_location')) {

  function get_location($id=''){

  $CI =& get_instance();

  $CI->load->model('user_model');

  $location = $CI->user_model->get_location($id);

  if($location)

  return $location;

  else

  return FALSE;

  }

  } */

/**

 *

 * 	Slider Information

 *

 */
/* if ( ! function_exists('get_slider')) {

  function get_slider($slug=''){

  $CI =& get_instance();

  $CI->load->model('user_model');

  $menu = $CI->user_model->get_slider($slug);

  if($menu)

  return $menu;

  else

  return FALSE;

  }

  } */

/**

 *

 * 	form fields Information

 *

 */
if (!function_exists('form_all_fields')) {



    function form_all_fields($slug = '')
    {

        return $all_fields = array(
            '1' => 'Text Field',
            '2' => 'Email',
            '3' => 'URL',
            '4' => 'Telephone number',
            '5' => 'Number (spinbox)',
            '6' => 'Number (slider)',
            '7' => 'Date',
            '8' => 'Text Area',
            '9' => 'Drop-down menu',
            '10' => 'Checkboxes',
            '11' => 'Radio buttons',
            '12' => 'Acceptance',
            '13' => 'Quiz',
            '14' => 'File upload',
            '15' => 'Submit button',
        );
    }

}

/**

 *

 * 	thisis  back end helper

 *

 */
if (!function_exists('msg_alert')) {



    function msg_alert()
    {

        $CI = & get_instance();
        ?>

        <?php if ($CI->session->flashdata('msg_success')): ?>

            <div class="alert alert-success">

                <button type="button" class="close" data-dismiss="alert">&times;</button>

                                                                     <!-- <strong>Success :</strong> <br> --> <?php echo $CI->session->flashdata('msg_success'); ?>

            </div>

        <?php endif; ?>

        <?php if ($CI->session->flashdata('msg_info')): ?>

            <div class="alert alert-info">

                <button type="button" class="close" data-dismiss="alert">&times;</button>

                                                                    <!-- <strong>Info :</strong> <br> --> <?php echo $CI->session->flashdata('msg_info'); ?>

            </div>

        <?php endif; ?>

        <?php if ($CI->session->flashdata('msg_warning')): ?>

            <div class="alert alert-warning">

                <button type="button" class="close" data-dismiss="alert">&times;</button>

                                                                   <!--  <strong>Warning :</strong> <br> --> <?php echo $CI->session->flashdata('msg_warning'); ?>

            </div>

        <?php endif; ?>

        <?php if ($CI->session->flashdata('msg_error')): ?>

            <div class="alert alert-danger">

                <button type="button" class="close" data-dismiss="alert">&times;</button>

                                                                    <!-- <strong>Error :</strong> <br> --> <?php echo $CI->session->flashdata('msg_error'); ?>

            </div>

        <?php endif; ?>

        <?php
    }

}

/**

 *

 * 	thisis  back end helper

 *

 */
if (!function_exists('msg_alert_front')) {



    function msg_alert_front()
    {

        $CI = & get_instance();
        ?>

        <?php if ($CI->session->flashdata('msg_success')): ?>

            <div class="alert alert-success">

                <!--  <button type="button" class="close" data-dismiss="alert">&times;</button> -->

                                                                     <!-- <strong>Success :</strong> <br> --> <?php echo $CI->session->flashdata('msg_success'); ?>

            </div>

        <?php endif; ?>

        <?php if ($CI->session->flashdata('msg_info')): ?>

            <div class="alert alert-info">

                <!-- <button type="button" class="close" data-dismiss="alert">&times;</button> -->

                                                                    <!-- <strong>Info :</strong> <br> --> <?php echo $CI->session->flashdata('msg_info'); ?>

            </div>

        <?php endif; ?>

        <?php if ($CI->session->flashdata('msg_warning')): ?>

            <div class="alert alert-warning">

                <!-- <button type="button" class="close" data-dismiss="alert">&times;</button> -->

                                                                   <!--  <strong>Warning :</strong> <br> --> <?php echo $CI->session->flashdata('msg_warning'); ?>

            </div>

        <?php endif; ?>

        <?php if ($CI->session->flashdata('msg_error')): ?>

            <div class="alert alert-danger">

                <!-- <button type="button" class="close" data-dismiss="alert">&times;</button> -->

                                                                    <!-- <strong>Error :</strong> <br> --> <?php echo $CI->session->flashdata('msg_error'); ?>

            </div>

        <?php endif; ?>

        <?php
    }

}

if (!function_exists('upload_file')) {

    function upload_file($param = null)
    {
        $CI = & get_instance();

        $config['upload_path'] = './assets/uploads/';
        $config['allowed_types'] = 'gif|jpg|png|xls|xlsx|csv|jpeg|pdf|doc|docx';
        $config['max_size'] = 1024 * 90;
        $config['image_resize'] = FALSE;
        $config['resize_width'] = 126;
        $config['resize_height'] = 126;

        if ($param) {

            $config = $param + $config;
        
		}

        $CI->load->library('upload', $config);

        if (!empty($config['file_name'])) {
		
            $file_status = $CI->upload->do_upload($config['file_name']);
			
        } else {
		
            $file_status = $CI->upload->do_upload();
			
		}

        if (!$file_status) {

            return array('STATUS' => FALSE, 'FILE_ERROR' => $CI->upload->display_errors());
			
        } else {

            $upload_data = $CI->upload->data();

            $upload_file = explode('.', $upload_data['file_name']);

            if ($config['image_resize'] && in_array(strtolower($upload_file[1]), array('gif', 'jpeg', 'jpg', 'png', 'bmp', 'jpe'))) {

                $params = array(
                    'source_image' => $config['source_image'].$upload_data['file_name'],
                    'new_image' => $config['new_image'].$upload_data['file_name'],
                    'create_thumb' => FALSE,
                    'maintain_ratio' => FALSE,
                    'width' => $config['resize_width'],
                    'height' => $config['resize_height'],
                );

                image_resize($params);
            }

            return array('STATUS' => TRUE, 'UPLOAD_DATA' => $upload_data);
			
        }
		
    }

}

if (!function_exists('image_resize')) {

    function image_resize($param = null)
    {
        $CI = & get_instance();

        $config['image_library'] = 'gd2';
        $config['source_image'] = './assets/uploads/';
        $config['new_image'] = './assets/uploads/';
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['width'] = 150;
        $config['height'] = 150;

        if ($param) {

            $config = $param + $config;
			
        }

        $CI->load->library('image_lib', $config);

        if (!$CI->image_lib->resize()) {

            die($CI->image_lib->display_errors());
			
        } else {

            return array('STATUS' => TRUE, 'MESSAGE' => 'Image resized.');
			
        }
		
    }

}

/**

 *

 * 	image delete

 *

 */
if (!function_exists('file_delete')) {



    function file_delete($param = null)
    {

        $config['file_path'] = './assets/uploads/';

        $config['file_thumb_path'] = './assets/uploads/';

        if ($param) {

            $config = $param + $config;
        }

        //print_r($config); die;

        if (file_exists($config['file_path'])) {

            unlink($config['file_path']);
        }

        if (file_exists($config['file_thumb_path'])) {

            unlink($config['file_thumb_path']);
        }
    }

}

/**

 *

 * 	Menu Information

 *

 */
if (!function_exists('get_nav_menu')) {



    function get_nav_menu($slug = '', $is_location = FALSE)
    {

        $CI = & get_instance();

        //$CI->load->model('user_model');

        if ($menu = $CI->common_model->get_nav_menu($slug, $is_location))
            return $menu;
        else
            return FALSE;
    }

}

/**

 *

 * 	Get YouTube video ID from URL

 *

 */
if (!function_exists('get_youtube_id_from_url')) {



    function get_youtube_thumbnail($youtube_url = '', $alt = TRUE, $number = 1)
    {

        $youtubeId = preg_replace('/^[^v]+v.(.{11}).*/', '$1', $youtube_url);

        if ($alt)
            $alt = 'alt="AA' . $youtubeId . '"';
        else
            $alt = '';

        return'<img style="border-radius: 0px !important; transition: none 0s ease 0s;" class="img-rounded img-responsive" src="http://img.youtube.com/vi/' . $youtubeId . '/mqdefault.jpg" ' . $alt . '>';
    }

}



/**

 *

 * Get Location Default Image

 *

 *
 * */
if (!function_exists('get_location_images')) {

    function get_location_images($location_id = '')
    {
        $CI = & get_instance();

        $CI->db->where('locations_id', $location_id);

        $CI->db->where('random_background', 1);

        $CI->db->order_by('id', 'desc');

        $query = $CI->db->get('locations_photos');

        if ($query->num_rows() > 0)
            return $query->row();
        else
            return FALSE;
    }

}





//for option

if (!function_exists('get_option_value')) {



    function get_option_value($key = FALSE)
    {

        $CI = & get_instance();

        if ($option = $CI->getoption->get_option_value($key))
            return $option;
        else
            return FALSE;
    }

}

if (!function_exists('file_download')) {



    function file_download($title = FALSE, $data = FALSE)
    {

        $data = str_replace('./', '', $data);

        $CI = & get_instance();

        $CI->load->helper('download');

        if (!empty($title) && !empty($data)):

            $title = url_title($title, '-', TRUE);

            if ($file = file_get_contents($data)) {

                $extend = end(explode('.', $data));

                $file_name = $title . '.' . $extend;

                force_download($file_name, $file);
            } else {

                return FALSE;
            }

        endif;
    }

}

if (!function_exists('get_post')) {



    function get_post($slug = '', $is_slug = FALSE)
    {

        $CI = & get_instance();

        if (!empty($slug))
            return $CI->common_model->get_post($slug, $is_slug);
        else
            return FALSE;
    }

}

if (!function_exists('user_infos')) {



    function user_infos()
    {

        $CI = & get_instance();

        return $CI->common_model->get_row('users', array('id' => user_id()));
    }

}

if (!function_exists('gettwitterfeeds')) {



    function gettwitterfeeds()
    {

        $CI = & get_instance();

        //include APPPATH.'libraries/twitter/api.php';
        //$CI->api = new Api();

        $twitter_feed = $CI->api->get_user_timeline(array('screen_name' => 'test2mailer', 'count' => 1));

        if (!empty($twitter_feed)) {

            echo "@" . $twitter_feed[0]['text'];

            echo "<br>";

            $timespan = explode(',', timespan(strtotime($twitter_feed[0]['created_at']), time()), 2);

            echo "<span>Posted " . $timespan[0] . " ago </span>";
        }
    }

}

if (!function_exists('get_fb_feed')) {



    function get_fb_feed()
    {

        $CI = & get_instance();

        $fb_post = $CI->fb_api->get_fb_post_timeline();

        if (!empty($fb_post)) {

            //print_r($fb_post);

            echo $fb_post['data'][0]['message'];

            echo "<br>";

            $timespan1 = explode(',', timespan(strtotime($fb_post['data'][0]['created_time']), time()), 2);

            echo "<span>Posted " . $timespan1[0] . " ago </span>";
        }
    }

}

/**

 *

 * 	thumbnail image

 *

 */
if (!function_exists('create_thumbnail')) {



    function create_thumbnail($config_img = '')
    {

        $CI = & get_instance();

        $config_image['image_library'] = 'gd2';

        $config_image['source_image'] = $config_img['source_path'] . $config_img['file_name'];

        //$config_image['create_thumb'] = TRUE;

        $config_image['new_image'] = $config_img['destination_path'] . $config_img['file_name'];

        $config_image['maintain_ratio'] = TRUE;

        $config_image['height'] = $config_img['height'];

        $config_image['width'] = $config_img['width'];

        list($width, $height, $type, $attr) = getimagesize($config_img['source_path'] . $config_img['file_name']);

        if ($width < $height) {

            $cal = $width / $height;

            $config_image['width'] = $config_img['width'] * $cal;
        }

        if ($height < $width) {

            $cal = $height / $width;

            $config_image['height'] = $config_img['height'] * $cal;
        }

        $CI->load->library('image_lib', $config_image);

        if (!$CI->image_lib->resize())
            return array('status' => FALSE, 'error_msg' => $CI->image_lib->display_errors());
        else
            return array('status' => TRUE, 'file_name' => $config_img['file_name']);
    }

}

/**

 *

 * 	get_social_url

 *

 */
if (!function_exists('get_option_url')) {



    function get_option_url($option_name)
    {

        $CI = & get_instance();

        if ($query = $CI->common_model->get_row('options', array('option_name' => $option_name)))
            return $query->option_value;
        else
            return false;
    }

}

if (!function_exists('get_seo_meta_tags')) {



    function get_seo_meta_tags($params = '')
    {

        $CI = & get_instance();

        if (!empty($params)):

            $CI->db->like('page_name', $params);

            $seometatags_query = $CI->db->get('meta_tags');

            if ($seometatags_query->num_rows() > 0)
                return $seometatags_query->row();
            else
                return FALSE;

        else:

            return FALSE;

        endif;
    }

}

if (!function_exists('random_background')) {



    function random_background($params = '')
    {
        $CI = & get_instance();
        $CI->load->model('products_model');
        $CI->db->where('random_background', 1);
        $CI->db->order_by('id', 'random');
        $CI->db->limit(1);
        $queryf = $CI->db->get('products_photos');
        if ($queryf->num_rows() > 0) {
            return base_url($queryf->row()->path);
        } else {
            return FALSE;
        }


        // $sliders = array('slider1.jpg', 'slider2.jpg', 'slider3.jpg', 'slider4.jpg', 'slider5.jpg');
        // $random_slider = array_rand($sliders, 1);
        // return  base_url('assets/frontend/img/'.$sliders[$random_slider]);
    }

}



if (!function_exists('text_limit')) {

    function text_limit($string = FALSE, $limit = 100)
    {

        if (empty($string))
            return "";

        elseif (strlen($string) > $limit)
            return substr(strip_tags($string), 0, $limit) . "...";
        else
            return $string;
    }

}





if (!function_exists('get_locations_nav')) {

    function get_locations_nav()
    {

        $CI = & get_instance();

        return $CI->common_model->get_result('locations', array('status' => 1), array('id', 'name', 'location_slug'), array('id', 'asc'));
    }

}



if (!function_exists('get_menu_products_photoss')) {

    function get_menu_products_photoss($product_id = 0, $multi = FALSE)
    {

        $CI = & get_instance();

        $CI->load->model('products_model');

        return $CI->products_model->get_menu_products_photoss($product_id, $multi);
    }

}



if (!function_exists('check_exist_tag')) {

    function check_exist_tag($tag = '')
    {

        $CI = & get_instance();

        $CI->load->model('products_model');

        $string = url_title($tag, '-', TRUE);

        $CI->db->where('slug', $string);

        $CI->db->from('tags');

        $query = $CI->db->get();

        if ($query->num_rows() > 0) {

            return $query->row()->id;
        } else {

            $tag_data = array('tag' => ucfirst($tag), 'slug' => url_title($tag, '-', TRUE), 'status' => 1);

            return $CI->products_model->insert('tags', $tag_data);
        }
    }

}



if (!function_exists('check_exist_ingredient')) {

    function check_exist_ingredient($ingredient = '')
    {

        $CI = & get_instance();

        $CI->load->model('products_model');

        $string = url_title($ingredient, '-', TRUE);

        $CI->db->where('slug', $string);

        $CI->db->from('ingredients');

        $query = $CI->db->get();

        if ($query->num_rows() > 0) {

            return $query->row()->id;
        } else {

            $ingred = array('name' => ucfirst($ingredient), 'slug' => url_title($ingredient, '-', TRUE));

            return $CI->products_model->insert('ingredients', $ingred);
        }
    }

}



if (!function_exists('check_size_id')) {

    function check_size_id($size = '')
    {

        $CI = & get_instance();

        $CI->load->model('products_model');

        $CI->db->where('name', $size);

        $CI->db->from('portions');

        $query = $CI->db->get();

        if ($query->num_rows() > 0) {

            return $query->row()->id;
        } else {

            return FALSE;
        }
    }

}



if (!function_exists('get_ingredient_name')) {

    function get_ingredient_name($id = '')
    {

        $CI = & get_instance();

        $CI->load->model('products_model');

        $CI->db->where('id', $id);

        $CI->db->from('ingredients');

        $query = $CI->db->get();

        if ($query->num_rows() > 0) {

            return $query->row()->name;
        } else {

            return FALSE;
        }
    }

}



if (!function_exists('get_tag_name')) {

    function get_tag_name($id = '')
    {

        $CI = & get_instance();

        $CI->load->model('products_model');

        $CI->db->where('id', $id);

        $CI->db->from('tags');

        $query = $CI->db->get();

        if ($query->num_rows() > 0) {

            return $query->row()->tag;
        } else {

            return FALSE;
        }
    }

}



