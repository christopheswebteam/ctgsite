<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Uri_routes_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_route_by_uri($uri)
    {
        $this->db->select();
		$this->db->from('uri_routes');
		$this->db->where('uri', $uri);
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
		
			return $query->row_array();
			
		} else {
		
			return FALSE;
			
		}
    }
	
}
