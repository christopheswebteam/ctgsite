<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Content_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($table_name = '', $data = '')
    {
        $query = $this->db->insert($table_name, $data);
        if ($query) return $this->db->insert_id();
        else return FALSE;
    }

    public function get_result($table_name = '', $id_array = '', $columns = array(), $order_by = array())
    {
        if (!empty($columns)):
            $all_columns = implode(",", $columns);
            $this->db->select($all_columns);
        endif;
        if (!empty($order_by)):
            $this->db->order_by($order_by[0], $order_by[1]);
        endif;

        if (!empty($id_array)):
            foreach ($id_array as $key => $value) {
                $this->db->where($key, $value);
            }
        endif;
        $query = $this->db->get($table_name);
        if ($query->num_rows() > 0) return $query->result();
        else return FALSE;
    }

    public function get_row($table_name = '', $id_array = '', $columns = array())
    {
        if (!empty($columns)):
            $all_columns = implode(",", $columns);
            $this->db->select($all_columns);
        endif;
        if (!empty($id_array)):
            foreach ($id_array as $key => $value) {
                $this->db->where($key, $value);
            }
        endif;
        $query = $this->db->get($table_name);
        if ($query->num_rows() > 0) return $query->row();
        else return FALSE;
    }

    public function update($table_name = '', $data = '', $id_array = '')
    {
        if (!empty($id_array)):
            foreach ($id_array as $key => $value) {
                $this->db->where($key, $value);
            }
        endif;
        return $this->db->update($table_name, $data);
    }

    public function delete($table_name = '', $id_array = '')
    {
        return $this->db->delete($table_name, $id_array);
    }

    public function blogs($offset = '', $per_page = '')
    {
        if ($offset >= 0 && $per_page > 0) {
            $this->db->order_by('id', 'desc');
            $this->db->limit($per_page, $offset);
            $query = $this->db->get('blogs');
            if ($query->num_rows() > 0) return $query->result();
            else return FALSE;
        }else {
            $query = $this->db->get('blogs');
            return $query->num_rows();
        }
    }

    public function checkimg($tablename = '', $id = '')
    {
        $query = $this->db->get_where($tablename, array('id' => $id));
        return $query->row();
    }

    public function pages($offset = '', $per_page = '', $post_type = '')
    {
        $this->db->where('post_type', $post_type);
        $this->db->from('posts');
        if ($offset >= 0 && $per_page > 0) {
            $this->db->limit($per_page, $offset);
            $this->db->order_by('id', 'desc');
            $query = $this->db->get();
            if ($query->num_rows() > 0) return $query->result();
            else return FALSE;
        }else {
            return $this->db->count_all_results();
        }
    }

    public function posts($offset = '', $per_page = '')
    {
        $this->db->from('posts');
        $this->db->where('post_type', 'post');
        if ($offset >= 0 && $per_page > 0) {
            $this->db->limit($per_page, $offset);
            $this->db->order_by('id', 'desc');
            $query = $this->db->get();
            if ($query->num_rows() > 0) return $query->result();
            else return FALSE;
        }else {

            return $this->db->count_all_results();
        }
    }

    public function faqs($offset = '', $per_page = '')
    {
        $this->db->from('faqs');
        if ($offset >= 0 && $per_page > 0) {
            $this->db->limit($per_page, $offset);
            $this->db->order_by('id', 'desc');
            $query = $this->db->get();
            if ($query->num_rows() > 0) return $query->result();
            else return FALSE;
        }else {

            return $this->db->count_all_results();
        }
    }


    public function blog_comments($offset = '', $per_page = '')
    {
        $this->db->from('blog_comments');
        if ($offset >= 0 && $per_page > 0) {
            $this->db->limit($per_page, $offset);
            $this->db->order_by('id', 'desc');
            $query = $this->db->get();
            if ($query->num_rows() > 0) return $query->result();
            else return FALSE;
        }else {

            return $this->db->count_all_results();
        }
    }


    public function get_blog_tag($blog_id='')
    {
        $this->db->where('type', 'blog');
        $this->db->where('key', $blog_id);
        $this->db->from('tags_association');
        $query = $this->db->get();
        if ($query->num_rows() > 0) return $query->result();
        else return FALSE;
    }
}
