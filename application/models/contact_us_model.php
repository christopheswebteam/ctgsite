<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contact_us_model extends CI_Model{
   
     public function insert($data = ''){
        $query = $this->db->insert('contact_us', $data);
        if ($query) 
        	return $this->db->insert_id();
        else 
        	return FALSE;
    }

}
