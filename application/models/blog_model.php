<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Blog_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_post_by_slug($slug){
        $this->db->select();
        $this->db->from('blogs');
        $this->db->where('slug', $slug);
        $this->db->limit(1);
        $result = $this->db->get();

        if($result && $result->num_rows() > 0){
            return $result->row_array();
        }

        return false;
    }

	public function get_posts($limit = 5, $offset = 0, $active_only = FALSE)
	{
		$this->db->select('blogs.*');
		$this->db->select('users.first_name as author_name_first');
		$this->db->select('users.last_name as author_name_last');
		$this->db->select('GROUP_CONCAT(tags.tag) as tags');

		$this->db->from('blogs');

		if ($active_only) {

			$this->db->where('blogs.status', '1');

		}

		$this->db->join('users', 'blogs.author = users.id', 'left');
		$this->db->join('tags_association', 'blogs.id = tags_association.key AND tags_association.type = "Post"', 'left');
		$this->db->join('tags', 'tags_association.tags_id = tags.id', 'left');

		$this->db->group_by('blogs.id');

		$this->db->limit($limit, $offset);

		$this->db->order_by('blogs.created', 'desc');

		$query = $this->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->result_array();

		} else {

			$result = FALSE;

		}

		return $result;
	}

	public function count_posts($active_only = FALSE)
	{
		$this->db->select('id');
		$this->db->from('blogs');

		if ($active_only) {

			$this->db->where('status', '1');

		}

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function get_post_by_id($id, $active_only = FALSE)
	{
		$this->db->select('blogs.*');
		$this->db->select('users.first_name as author_name_first');
		$this->db->select('users.last_name as author_name_last');
		$this->db->select('GROUP_CONCAT(tags.tag) as tags');

		$this->db->from('blogs');

		$this->db->where('blogs.id', $id);

		if ($active_only) {

			$this->db->where('blogs.status', '1');

		}

		$this->db->join('users', 'blogs.author = users.id', 'left');
		$this->db->join('tags_association', 'blogs.id = tags_association.key AND tags_association.type = "Post"', 'left');
		$this->db->join('tags', 'tags_association.tags_id = tags.id', 'left');

		$this->db->group_by('blogs.id');

		$query = $this->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->row_array();

		} else {

			$result = FALSE;

		}

		return $result;
	}

	public function add_post($db_data)
	{
		$db_data['created'] = date('Y-m-d h:i:s');

		$this->db->insert('blogs', $db_data);

		if ($this->db->affected_rows() > 0) {

			return $this->db->insert_id();

		} else {

			return FALSE;

		}
	}

	public function edit_post_by_id($id, $db_data)
	{
		$db_data['updated'] = date('Y-m-d h:i:s');

		$this->db->where('id', $id);
		$this->db->update('blogs', $db_data);

		if ($this->db->affected_rows() > 0) {

			return TRUE;

		} else {

			return FALSE;

		}
	}

	public function delete_post_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('blogs');

		if ($this->db->affected_rows() > 0) {

			return TRUE;

		} else {

			return FALSE;

		}
	}
}