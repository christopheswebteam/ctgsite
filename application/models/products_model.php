<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Products_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_products($limit = 5, $offset = 0)
    {
        $this->db->select('products.id');
        $this->db->select('products.name');
        $this->db->select('products.description');
        $this->db->select('products.status');
        $this->db->select('products.created');
        $this->db->select('products.id');
        $this->db->select('GROUP_CONCAT(tags.tag) as tags');

        $this->db->from('products');

        if ($this->input->get('keywords')) {

            $this->db->like('products.name', $this->input->get('keywords'));
        }

        $this->db->join('tags_association', 'products.id = tags_association.key AND tags_association.type = "Product"', 'left');
        $this->db->join('tags', 'tags_association.tags_id = tags.id', 'left');

        $this->db->group_by('products.id');

        $this->db->limit($limit, $offset);

        $this->db->order_by('products.created', 'desc');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            $result = $query->result_array();
        } else {

            $result = FALSE;
        }

        return $result;
    }

    public function count_products()
    {
        $this->db->select('id');
        $this->db->from('products');

        if ($this->input->get('keywords')) {

            $this->db->like('products.name', $this->input->get('keywords'));
        }

        $query = $this->db->get();

        return $query->num_rows();
    }

    public function get_product_by_id($id, $active_only = FALSE)
    {
        $this->db->select('products.*');
        $this->db->select('GROUP_CONCAT(tags.tag) as tags');
        $this->db->select('GROUP_CONCAT(ingredients.name) as ingredients');

        $this->db->from('products');

        $this->db->where('products.id', $id);

        if ($active_only) {

            $this->db->where('products.status', '1');
        }

        $this->db->join('tags_association', 'products.id = tags_association.key AND tags_association.type = "Product"', 'left');
        $this->db->join('tags', 'tags_association.tags_id = tags.id', 'left');

        $this->db->join('products_ingredients', 'products.id = products_ingredients.products_id', 'left');
        $this->db->join('ingredients', 'products_ingredients.ingredients_id = ingredients.id', 'left');

        $this->db->group_by('products.id');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            $result = $query->row_array();
        } else {

            $result = FALSE;
        }

        return $result;
    }

    public function get_product_portions_by_id($id, $custom_only = FALSE)
    {
        $this->db->select();
        $this->db->from('products_portions');
        $this->db->where('products_id', $id);

        if ($custom_only) {

            $this->db->where('is_custom', 1);
        }

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            $result = $query->result_array();
        } else {

            $result = FALSE;
        }

        return $result;
    }

    public function get_product_ingredients_by_id($id)
    {
        $this->db->select();
        $this->db->from('products_ingredients');
        $this->db->where('products_id', $id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            $result = $query->result_array();
        } else {

            $result = FALSE;
        }

        return $result;
    }

    public function get_product_photos_by_id($id)
    {
        $this->db->select();
        $this->db->from('products_photos');
        $this->db->where('products_id', $id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            $result = $query->result_array();
        } else {

            $result = FALSE;
        }

        return $result;
    }

    public function get_product_tags_by_id($id)
    {
        $this->db->select();
        $this->db->from('tags_association');
        $this->db->where('key', $id);
        $this->db->where('type', 'Product');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            $result = $query->result_array();
        } else {

            $result = FALSE;
        }

        return $result;
    }

    public function insert_product($db_data)
    {
        $db_data['created'] = date('Y-m-d h:i:s');

        $this->db->insert('products', $db_data);

        if ($this->db->affected_rows() > 0) {

            return $this->db->insert_id();
        } else {

            return FALSE;
        }
    }

    public function edit_product_by_id($id, $db_data)
    {
        $db_data['modified'] = date('Y-m-d h:i:s');

        $this->db->where('id', $id);
        $this->db->update('products', $db_data);

        if ($this->db->affected_rows() > 0) {

            return TRUE;
        } else {

            return FALSE;
        }
    }

    public function get_portions()
    {
        $this->db->select();
        $this->db->from('portions');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            $result = $query->result_array();
        } else {

            $result = FALSE;
        }

        return $result;
    }

    public function get_tags_by_category($category)
    {
        $this->db->select();
        $this->db->from('tags');
        $this->db->where('category', $category);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            $result = $query->result_array();
        } else {

            $result = FALSE;
        }

        return $result;
    }

    public function upsert_ingredients_and_associate($products_id, $ingredients)
    {
        $this->load->helper('url');

        // Step 1: Clear all previous ingredients for product.

        $this->db->where('products_id', $products_id);
        $this->db->delete('products_ingredients');

        // Step 2: Upsert ingredients in array.

        foreach ($ingredients as $ingredient) {

            $this->db->select('id');
            $this->db->from('ingredients');
            $this->db->where('name', $ingredient);

            $query = $this->db->get();

            if ($query->num_rows() > 0) {

                $result = $query->row_array();

                $ingredients_id = $result['id'];
            } else {

                $ingredients_db_data['name'] = $ingredient;
                $ingredients_db_data['slug'] = strtolower(url_title($ingredient, '-', TRUE));

                $this->db->insert('ingredients', $ingredients_db_data);

                $ingredients_id = $this->db->insert_id();
            }

            // Step 3: Add ingredient association.

            $products_ingredients_db_data['products_id'] = $products_id;
            $products_ingredients_db_data['ingredients_id'] = $ingredients_id;

            $this->db->insert('products_ingredients', $products_ingredients_db_data);
        }
    }

    public function update_product_photo_by_id($id, $db_data)
    {
        $this->db->where('id', $id);
        $this->db->update('products_photos', $db_data);

        if ($this->db->affected_rows() > 0) {

            return TRUE;
        } else {

            return FALSE;
        }
    }

    public function delete_product_photo_by_id($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('products_photos');

        if ($this->db->affected_rows() > 0) {

            return TRUE;
        } else {

            return FALSE;
        }
    }

    public function delete_products_portions_by_products_id($products_id)
    {
        $this->db->where('products_id', $products_id);
        $this->db->delete('products_portions');

        if ($this->db->affected_rows() > 0) {

            return TRUE;
        } else {

            return FALSE;
        }
    }

    public function get_portions_id_by_name($name)
    {
        $this->db->select('id');
        $this->db->from('portions');
        $this->db->where('name', ucfirst(strtolower($name)));

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            $result = $query->row_array();

            return $result['id'];
        } else {

            return FALSE;
        }
    }

    public function insert_products_portions($db_data)
    {
        $this->db->insert('products_portions', $db_data);

        if ($this->db->affected_rows() > 0) {

            return $this->db->insert_id();
        } else {

            return FALSE;
        }
    }

    /**
     * Return all active locations, ordered by location title.
     */
    public function get_all_active_locations()
    {
        $this->db->select();
        $this->db->from('locations');
        $this->db->where('status', 1);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            $result = $query->result_array();
        } else {

            $result = FALSE;
        }

        return $result;
    }

    /**
     *
     * @param int $product_id
     */
    public function get_product_related_locations($product_id)
    {
        //To Be Implmented
    }

    ////////////////////////////////////////////////////////////////////

    public function insert($table_name = '', $data = '')
    {
        $query = $this->db->insert($table_name, $data);
        if ($query)
            return $this->db->insert_id();
        else
            return FALSE;
    }

    public function get_result($table_name = '', $id_array = '', $columns = array(), $order_by = array())
    {
        if (!empty($columns)):
            $all_columns = implode(",", $columns);
            $this->db->select($all_columns);
        endif;
        if (!empty($order_by)):
            $this->db->order_by($order_by[0], $order_by[1]);
        endif;

        if (!empty($id_array)):
            foreach ($id_array as $key => $value) {
                $this->db->where($key, $value);
            }
        endif;
        $query = $this->db->get($table_name);
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    public function get_row($table_name = '', $id_array = '', $columns = array())
    {
        if (!empty($columns)):
            $all_columns = implode(",", $columns);
            $this->db->select($all_columns);
        endif;
        if (!empty($id_array)):
            foreach ($id_array as $key => $value) {
                $this->db->where($key, $value);
            }
        endif;
        $query = $this->db->get($table_name);
        if ($query->num_rows() > 0)
            return $query->row();
        else
            return FALSE;
    }

    public function update($table_name = '', $data = '', $id_array = '')
    {
        if (!empty($id_array)):
            foreach ($id_array as $key => $value) {
                $this->db->where($key, $value);
            }
        endif;
        return $this->db->update($table_name, $data);
    }

    public function delete($table_name = '', $id_array = '')
    {
        // foreach ($id_array as $key => $value){
        // 		$this->db->where($key, $value);
        // }
        return $this->db->delete($table_name, $id_array);
    }

    public function products($offset = '', $per_page = '', $type = "")
    {

        if (!empty($_GET['pname'])) {
            $this->db->like('name', trim($this->input->get('pname')));
        }
        if (!empty($type)) {
            $this->db->like('products_types_id', $type);
        }
        $this->db->order_by('id', 'desc');
        $this->db->from('products');
        if ($offset >= 0 && $per_page > 0) {
            $this->db->limit($per_page, $offset);
            $query = $this->db->get();
            if ($query->num_rows() > 0)
                return $query->result();
            else
                return FALSE;
        }else {
            return $this->db->count_all_results();
        }
    }

    public function types($offset = '', $per_page = '')
    {
        $this->db->from('products_types');
        if ($offset >= 0 && $per_page > 0) {
            $this->db->limit($per_page, $offset);
            $this->db->order_by('id', 'desc');
            $query = $this->db->get();
            if ($query->num_rows() > 0)
                return $query->result();
            else
                return FALSE;
        }else {

            return $this->db->count_all_results();
        }
    }

    public function get_product_tag($product_id = '')
    {
        $this->db->where('type', 'product');
        $this->db->where('key', $product_id);
        $this->db->from('tags_association');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    public function ingredients($offset = '', $per_page = '')
    {
        $this->db->from('ingredients');
        if ($offset >= 0 && $per_page > 0) {
            $this->db->limit($per_page, $offset);
            $this->db->order_by('id', 'desc');
            $query = $this->db->get();
            if ($query->num_rows() > 0)
                return $query->result();
            else
                return FALSE;
        }else {
            return $this->db->count_all_results();
        }
    }

    public function get_filter_products_portions($size = '')
    {

        if (is_array($size)) {
            $size = " WHERE `portions_id` IN ('" . implode("','", $size) . "')";
        } else {
            $size = '';
        }
        /* $this->db->select('products_id');
          $this->db->where_in('portions_id',$size);
          $this->db->group_by('products_id');
          $this->db->order_by('products_id','asc'); */
        //$query = $this->db->get('products_portions0');
        $query12 = $this->db->query("SELECT `products_id` FROM (`products_portions`)  $size  GROUP BY `products_id` ORDER BY `products_id` ASC");
        if ($query12->num_rows() > 0) {
            $sizes = array();
            foreach ($query12->result() as $row)
                $sizes[] = $row->products_id;
            return $sizes;
        } else
            return FALSE;
    }

    public function get_filter_products_sizes($price_min, $price_max)
    {
        /* $this->db->select('products_id');
          //if($price_min) $price_min=$price_min-1;
          if($price_max) $price_max=$price_max+1;
          $where="price >='$price_min' AND price <='$price_max'";
          $this->db->where($where);
          $this->db->group_by('products_id');
          $this->db->order_by('products_id','asc');
          $query = $this->db->get('products_portions0'); */


        //if($price_max) $price_max=$price_max+1;

        $where13 = " WHERE price >='$price_min' AND price <='$price_max'";


        $query13 = $this->db->query("SELECT `products_id` FROM (`products_portions`) $where13 GROUP BY `products_id` ORDER BY `products_id` asc;");
        if ($query13->num_rows() > 0) {
            $prices = array();
            foreach ($query13->result() as $row)
                $prices[] = $row->products_id;
            return $prices;
        } else
            return FALSE;
    }

    public function get_filter_tag($tags = '')
    {
        /* $this->db->select('key');
          $this->db->where_in('tags_id',$tags);
          $this->db->where('type','Product');
          $this->db->group_by('key');
          $this->db->order_by('key','asc');
          $query = $this->db->get('tags_association'); */

        if (is_array($tags)) {
            $tags14 = " WHERE `tags_id` IN ('" . implode("','", $tags) . "') AND";
        } else {
            $tags14 = '';
        }

        $query14 = $this->db->query("SELECT `key` FROM (`tags_association`) $tags14 `type` = 'Product' GROUP BY `key` ORDER BY `key` asc");
        if ($query14->num_rows() > 0) {
            $tags1 = array();
            foreach ($query14->result() as $row)
                $tags1[] = $row->key;
            return $tags1;
        } else
            return FALSE;
    }

    public function get_filter_products_location($location = '')
    {
        $this->db->select('products_id');
        $this->db->where('locations_id', $location);
        $this->db->group_by('products_id');
        $this->db->order_by('products_id', 'asc');
        $query = $this->db->get('locations_products');
        if ($query->num_rows() > 0) {
            $locations = array();
            foreach ($query->result() as $row)
                $locations[] = $row->products_id;
            return $locations;
        } else
            return FALSE;
    }

    public function get_sort_porduct_price($order = 'desc')
    {
        $sql = "SELECT * FROM `products_portions` order by price $order;";
        $query0 = $this->db->query($sql);
        $porduct_price = array();
        if ($query0->num_rows() > 0) {
            foreach ($query0->result() as $row)
                $porduct_price[] = $row->products_id;
            return $porduct_price;
        } else {
            return array(0);
        }
    }

    public function search_products($id = 0, $offset, $per_page)
    {
        $menu_db = $this->db;

        if (!empty($_GET)) {
            $this->session->set_userdata('products_search', $_GET);
        }

        $products_search = $this->session->userdata('products_search');

        if (!empty($products_search['size'])) {
            $menu_db->where_in('p.id', $this->get_filter_products_portions($products_search['size'], 'SIZE'));
        }

        if (!empty($products_search['price_min']) && !empty($products_search['price_max'])) {
            $menu_db->where_in('p.id', $this->get_filter_products_sizes($products_search['price_min'], $products_search['price_max']));
        }

        if (!empty($products_search['product_tag'])) {
            $menu_db->where_in('p.id', $this->get_filter_tag($products_search['product_tag']));
        }

        if ($id) {

            $menu_db->where('p.id', $id);
        }

        $where = "(p.start_date <= DATE(NOW()) AND  p.end_date >= DATE(NOW()))";
        $menu_db->where($where);


        $menu_db->where('p.status', 1);
        if (!empty($products_search['query'])) {
            $menu_db->like('p.name', trim($products_search['query']));
        }




        $menu_db->select('p.*, MIN(pp.price) AS minprice');
        $menu_db->from('products AS p');
        $menu_db->join('products_portions AS pp', 'pp.products_id = p.id', 'left');
        $menu_db->group_by('p.id');



        if ($offset >= 0 && $per_page > 0) {


            if (!empty($products_search['sort_by'])) {
                if ($products_search['sort_by'] == 'price-lowest-first') {
                    //$price_highest_first=$this->get_sort_porduct_price('asc');
                    $menu_db->order_by('minprice', 'asc');
                }
                if ($products_search['sort_by'] == 'price-highest-first') {
                    //$price_highest_first=$this->get_sort_porduct_price('desc');
                    $menu_db->order_by('minprice', 'desc');
                }
                if ($products_search['sort_by'] == 'size-smallest-first') {
                    $menu_db->order_by('sort', 'asc');
                }
                if ($products_search['sort_by'] == 'size-largest-first') {
                    $menu_db->order_by('sort', 'desc');
                }
            } else {
                $menu_db->order_by('p.sort', 'desc');
            }


            $menu_db->limit($per_page, $offset);

            $query = $menu_db->get();

            if ($query->num_rows() > 0) {

                $results = $query->result_array();

                $products = array();

                foreach ($results as $key => $product) {

                    $products[] = (object) array(
                            'id' => $product['id'],
                            'name' => $product['name'],
                            'slug' => $product['slug'],
                            'description' => $product['description'],
                            'product_name' => $product['name'],
                            'product_slug' => $product['slug'],
                            'start_date' => $product['start_date'],
                            'end_date' => $product['end_date'],
                            'sort' => $product['sort'],
                            'check_inventory' => $product['check_inventory'],
                            'preparation_instructions' => $product['preparation_instructions'],
                            'additional_information' => $product['additional_information'],
                            'nutrition_facts_file' => $product['nutrition_facts_file'],
                            'nutrition_facts_file_thumb' => $product['nutrition_facts_file_thumb'],
                            'products_photos' => $this->get_result('products_photos', array('products_id' => $product['id'])),
                            'products_portions' => $this->get_prproducts_portions($product['id']),
                            'tags_association' => $this->get_product_tags($product['id']),
                            'products_ingredients' => $this->get_products_ingredients($product['id']),
                            'previous_product_id' => (isset($results[$key - 1])) ? $results[$key - 1]['id'] : FALSE,
                            'previous_product_uri' => (isset($results[$key - 1])) ? "menu/product/" . $results[$key - 1]['slug'] . "/" . $results[$key - 1]['id'] : FALSE,
                            'next_product_id' => (isset($results[$key + 1])) ? $results[$key + 1]['id'] : FALSE,
                            'next_product_uri' => (isset($results[$key + 1])) ? "menu/product/" . $results[$key + 1]['slug'] . "/" . $results[$key + 1]['id'] : FALSE
                    );
                }

                return $products;
            } else {

                return FALSE;
            }
        } else {

            return $menu_db->count_all_results();
        }
    }

    public function get_prproducts_portions($products_id = 0)
    {
        $this->db->select('pp.*,p.name');
        $this->db->where('pp.products_id', $products_id);
        $this->db->from('products_portions as pp');
        $this->db->join('portions as p', 'p.id=pp.portions_id');
        //$this->db->group_by('ta.tags_id');
        $this->db->order_by('pp.portions_id', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    public function get_product_item_types()
    {
        $this->db->where('status', 1);
        $this->db->from('products_types');
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    public function get_product_tags($key = 0)
    {
        if ($key)
            $this->db->where('ta.key', $key);

        $this->db->select('t.id,t.tag,t.slug,t.category');
        $this->db->where('ta.type', 'Product');
        $this->db->from('tags_association as ta');
        $this->db->join('tags as t', 't.id=ta.tags_id');
        $this->db->group_by('ta.tags_id');
        $this->db->order_by('t.category', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    public function get_all_tags($value = '')
    {
        $this->db->from('tags');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    public function get_location_type($location_id = '')
    {
        $this->db->where('id', $location_id);
        $this->db->from('locations');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->row()->type;
        else
            return FALSE;
    }

    public function get_products_ingredients($product_id = 0)
    {
        $this->db->where('products_id', $product_id);
        $this->db->select('i.*');
        $this->db->from('products_ingredients as pi');
        $this->db->join('ingredients as i', 'i.id=pi.ingredients_id');
        //$this->db->order_by('i.name','ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    public function get_menu_products_photoss($product_id, $multi)
    {
        $this->db->where('products_id', $product_id);
        $this->db->from('products_photos');
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return ($multi) ? $query->result() : $query->row();
        else
            return FALSE;
    }

    public function get_locations_products($products_id = 0)
    {
        $this->db->select('l.*');
        $this->db->where('l.status', 1);
        $this->db->where('lp.products_id', $products_id);
        $this->db->from('locations_products as lp');
        $this->db->join('locations as l', 'l.id=lp.locations_id');
        $this->db->order_by('l.name', 'ASC');
        $this->db->group_by('l.id');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    public function get_position_products($id)
    {
        $result['previous_product'] = FALSE;
        $result['next_product'] = FALSE;

        $this->db->select('id, slug');
        $this->db->from('products');
        $this->db->where("status = 1 AND start_date <= DATE(NOW()) AND end_date >= DATE(NOW())");

        if ($this->session->userdata('products_search')) {

            $products_search = $this->session->userdata('products_search');

            if (!empty($products_search['size'])) {

                $this->db->where_in('id', $this->get_filter_products_portions($products_search['size'], 'SIZE'));
            }

            if ((!empty($products_search['price_min'])) && (!empty($products_search['price_max']))) {

                $this->db->where_in('id', $this->get_filter_products_sizes($products_search['price_min'], $products_search['price_max']));
            }

            if (!empty($products_search['product_tag'])) {

                $this->db->where_in('id', $this->get_filter_tag($products_search['product_tag']));
            }

            if (!empty($products_search['query'])) {

                $this->db->like('name', trim($products_search['query']));
            }
        }

        $this->db->order_by('sort', 'desc');

        $query = $this->db->get();

        if ($query->num_rows()) {

            $products = $query->result_array();

            foreach ($products as $key => $product) {

                if ($product['id'] == $id) {

                    if (isset($products[$key - 1])) {

                        $result['previous_product'] = $products[$key - 1];
                    }

                    if (isset($products[$key + 1])) {

                        $result['next_product'] = $products[$key + 1];
                    }
                }
            }
        }

        return $result;
    }

    public function get_cart_portion($product_id, $portion_id)
    {
        $this->db->select('pp.*,p.name');
        $this->db->where('pp.products_id', $product_id);
        $this->db->where('pp.portions_id', $portion_id);
        $this->db->from('products_portions as pp');
        $this->db->join('portions as p', 'p.id=pp.portions_id');
        //$this->db->group_by('ta.tags_id');
        $this->db->order_by('pp.portions_id', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->row();
        else
            return FALSE;
    }

}
