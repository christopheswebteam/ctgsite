<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Orders_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

	public function get_orders()
	{
		$this->db->select('orders.id, orders.total, orders.date_added, customers.name_first, customers.name_last');
		$this->db->from('orders');
		$this->db->join('customers', 'orders.customers_id = customers.id', 'left');
		$this->db->order_by('date_added', 'desc');
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
		
			$result = $query->result_array();
			
		} else {
		
			$result = FALSE;
			
		}
		
		return $result;
	}
	
	/*
	public function get_order_by_id($orders_id)
	{
		$this->load->model('v1/customers_model');
		$this->load->model('v1/locations_model');

		$this->db->select();
		$this->db->from('orders');
		$this->db->where('id', $orders_id);

		$query = $this->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->row_array();

			$result['status_current'] = $this->get_current_order_status($orders_id);
			$result['location'] = $this->locations_model->get_location_by_id($result['locations_id']);
			$result['customer'] = $this->customers_model->get_customer_by_id($result['customers_id']);
			$result['products'] = $this->get_orders_products($orders_id);

			return $result;

		} else {

			return FALSE;

		}

	
	*/
}
