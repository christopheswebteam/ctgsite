<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Common_model extends CI_Model
{
	
	public function get_all_ingredients()
	{
		$this->db->select('id, name');
		$this->db->from('ingredients');
		$this->db->order_by('name', 'asc');
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
		
			$result = $query->result_array();
			
		} else {
		
			$result = FALSE;
			
		}
		
		return $result;
	}
	
	public function get_all_tags()
	{
		$this->db->select('id, tag');
		$this->db->from('tags');
		$this->db->order_by('tag', 'asc');
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
		
			$result = $query->result_array();
			
		} else {
		
			$result = FALSE;
			
		}
		
		return $result;
	}
	
	public function upsert_tags_and_associate($asset_type, $asset_id, $tags)
	{
		$this->load->helper('url');
		
		// Step 1: Clear all previous tags for asset.
		
		$this->db->where('type', $asset_type);
		$this->db->where('key', $asset_id);
		$this->db->delete('tags_association');
		
		// Step 2: Upsert tags in array.
		
		foreach ($tags as $tag) {
		
			$this->db->select('id');
			$this->db->from('tags');
			$this->db->where('tag', $tag);
			
			$query = $this->db->get();
			
			if ($query->num_rows() > 0) {
				
				$result = $query->row_array();
				
				$tags_id = $result['id'];
			
			} else {
			
				$tags_db_data['tag'] = $tag;
				$tags_db_data['slug'] = strtolower(url_title($tag, '-', TRUE));
				$tags_db_data['created'] = date('Y-m-d h:i:s');
				
				$this->db->insert('tags', $tags_db_data);
				
				$tags_id = $this->db->insert_id();
			
			}
			
			// Step 3: Add tag association.
			
			$tags_association_db_data['type'] = $asset_type;
			$tags_association_db_data['tags_id'] = $tags_id;
			$tags_association_db_data['key'] = $asset_id;
			
			$this->db->insert('tags_association', $tags_association_db_data);
		
		}
	}
	
	public function users_dropdown()
	{
		$dropdown = array();
		
		$dropdown[0] = "- Choose User -";
		
		$this->db->select('id, first_name, last_name');
		$this->db->from('users');
		$this->db->order_by('first_name');
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
		
			$users = $query->result_array();
			
			foreach ($users as $user) {
			
				$dropdown[$user['id']] = trim($user['first_name']." ".$user['last_name']);
			
			}

		}
		
		return $dropdown;
	
	}

    public function insert($table_name = '', $data = '')
    {
        $query = $this->db->insert($table_name, $data);
        if ($query) return $this->db->insert_id();
        else return FALSE;
    }

    public function get_result($table_name = '', $id_array = '', $columns = array(), $order_by = array())
    {
        if (!empty($columns)):
            $all_columns = implode(",", $columns);
            $this->db->select($all_columns);
        endif;
        if (!empty($order_by)):
            $this->db->order_by($order_by[0], $order_by[1]);
        endif;
        if (!empty($id_array)):
            foreach ($id_array as $key => $value) {
                $this->db->where($key, $value);
            }
        endif;
        $query = $this->db->get($table_name);
        if ($query->num_rows() > 0) return $query->result();
        else return FALSE;
    }

    public function get_row($table_name = '', $id_array = '', $columns = array(), $order_by = array())
    {

        if (!empty($columns)):
            $all_columns = implode(",", $columns);
            $this->db->select($all_columns);
        endif;
        if (!empty($id_array)):
            foreach ($id_array as $key => $value) {
                $this->db->where($key, $value);
            }
        endif;
        if (!empty($order_by)):
            $this->db->order_by($order_by[0], $order_by[1]);
        endif;
        $query = $this->db->get($table_name);
        if ($query->num_rows() > 0) return $query->row();
        else return FALSE;
    }

    public function update($table_name = '', $data = '', $id_array = '')
    {
        if (!empty($id_array)):
            foreach ($id_array as $key => $value) {
                $this->db->where($key, $value);
            }
        endif;
        return $this->db->update($table_name, $data);
    }

    public function delete($table_name = '', $id_array = '')
    {
        return $this->db->delete($table_name, $id_array);
    }

    public function password_check($data = '', $user_id = '')
    {
        $query = $this->db->get_where('users', $data, array('id' => $user_id));
        if ($query->num_rows() > 0) return TRUE;
        else {
            //$this->form_validation->set_message('password_check', 'The %s field can not match');
            return FALSE;
        }
    }

    public function get_nav_menu($slug = '', $is_location = FALSE)
    {
        $this->db->select('navigations.id,navigations.navigation_label,navigations.navigation_link');
        $this->db->order_by('navigations.navigation_order', 'asc');
        $this->db->group_by('navigations.navigation_order');
        $this->db->from('navigations');

        $this->db->join('navigation_categories', 'navigation_categories.id=navigations.navigation_category_id', 'right');
        $this->db->where('navigations.navigation_parent_id', 0);
        if ($is_location) $this->db->where('navigations.navigation_location', $slug);
        else $this->db->where('navigation_categories.navigation_category_slug', $slug);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $main_menu = array();
            foreach ($query->result() as $row):
                $main_menu[] = (object) array(
                        'id' => $row->id,
                        'navigation_label' => $row->navigation_label,
                        'navigation_link' => $row->navigation_link,
                        'child_navigation' => $this->get_result('navigations', array('navigation_parent_id' => $row->id), array('id', 'navigation_label', 'navigation_link'), array('navigation_order', 'asc')),
                );
            endforeach;
            return $main_menu;
        } else return FALSE;
    }

    public function page($slug = '')
    {
        if (!empty($slug)) {
            $query = $this->db->get_where('posts', array('post_slug' => $slug, 'post_type' => 'page', 'status' => 1));
            if ($query->num_rows() > 0) return $query->row();
            else return FALSE;
        }else {
            return FALSE;
        }
    }

    public function get_post($slug, $is_slug)
    {
        if ($is_slug) $where = array('id' => $slug, 'post_type' => 'post');
        else $where = array('post_slug' => $slug, 'post_type' => 'post');
        return $this->get_row('posts', $where, array('post_title', 'post_content'));
    }

    public function news_categories($offset = '', $per_page = '')
    {


        if ($offset >= 0 && $per_page > 0) {
            $this->db->order_by('id', 'desc');
            $this->db->limit($per_page, $offset);
            $query = $this->db->get('news_categories');
            if ($query->num_rows() > 0) return $query->result();
            else return FALSE;
        }else {
            $query = $this->db->get('news_categories');
            return $query->num_rows();
        }
    }

    public function blogs($offset = '', $per_page = '') {

        if (!empty($_GET['cat'])) {
            $this->db->where('category', $_GET['cat']);
        }
        if ($offset >= 0 && $per_page > 0) {
            $this->db->order_by('id', 'desc');
            $this->db->where('blog_status', 1);
            $this->db->limit($per_page, $offset);
            $query = $this->db->get('blogs');
            if ($query->num_rows() > 0)
                return $query->result();
            else
                return FALSE;
        }else {
            $query = $this->db->get('blogs');
            return $query->num_rows();
        }
    }

    public function top_post($offset=5){
        $this->db->from('blogs');
        $this->db->order_by('id', 'desc');
        $this->db->limit($offset, 0);
        $this->db->where('blog_status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

}
