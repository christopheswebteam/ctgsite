<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        clear_cache();
        $this->load->model('user_model');
        // $this->output->enable_profiler(TRUE);
    }

    public function index()
    {
        redirect('');
    }

    private function check_login()
    {
        if (user_logged_in() === FALSE) redirect('');
    }

    public function login(){

        if($_POST){

            $data  = array('user_name'=>$this->input->post('user_name'),'password' => sha1($this->input->post('password')));

            if($this->user_model->login($data,FALSE)){

                echo "1";

            }else{

                echo "Invalid User Name or Password.";

            }

        }else{

            echo "NAN";

        }


    }

    function club_name_check($str = '')
    {
        $str = url_title($str);
        if ($this->common_model->get_row('club', array('slug' => $str))):
            $this->form_validation->set_message('restro_name_check', 'The Club name already exists.');
            return FALSE;
        else:
            return TRUE;
        endif;
    }

    public function registration_activation()
    {
        $data['title_top'] = 'Activation link';
        $data['template'] = 'user/registration_activation';
        $this->load->view('templates/frontend_template', $data);
    }

    public function username_check($str = '')
    {

        if ($this->common_model->get_row('users', array('user_name' => $str))):
            $this->form_validation->set_message('username_check', 'The %s address already exists.');
            return FALSE;
        else:
            return TRUE;
        endif;
    }

    public function check_unique_email($new_email, $old_email)
    {
        if ($new_email == $old_email) {
            return TRUE;
        } else {
            $resp = $this->user_model->get_row('users', array('email' => $new_email));
            if ($resp) {
                $this->form_validation->set_message('check_unique_email', 'Email alredy exist.');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    function email_check($str = '')
    {

        if ($this->common_model->get_row('users', array('email' => $str))):
            $this->form_validation->set_message('email_check', 'The %s address already exists.');
            return FALSE;
        else:
            return TRUE;
        endif;
    }

    public function activation($key = '')
    {
        if ($key == "" || $key == NULL) {
            redirect('user');
        }

        if ($user = $this->user_model->get_row('users', array('user_role >' => 0, 'status' => 0, 'secret_key' => trim($key)))) {
            $user_data = array('status' => 1, 'secret_key' => '');
            $this->user_model->update('users', $user_data, array('id' => $user->id));

            $data = array('email' => $user->email, 'password' => $user->password);
            if ($user->user_role == 1) $type = 'admin';
            else $type = 'user';
            if ($this->user_model->login($data, $type)) {
                if ($type == 'admin') redirect('admin/profile');
                else redirect('user/profile');
            }
        }else {
            redirect('user');
        }
    }

    public function forgot_password()
    {
        $data['title_top'] = 'Forgot password';
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {

            if ($user = $this->user_model->get_row('users', array('email' => trim($_POST['email'])))) {
                $new_password_key = trim(md5($user->email));
                $user->id;
                $user->email;
                $this->user_model->update('users', array('new_password_key' => $new_password_key, 'new_password_requested' => date('Y-m-d H:i:s')), array('id' => $user->id));
                $this->load->library('chapter247_email');
                $email_template = $this->chapter247_email->get_email_template(2);
                $param = array(
                    'template' => array(
                        'temp' => $email_template->template_body,
                        'var_name' => array(
                            'name' => $user->first_name . ' ' . $user->last_name,
                            'activation_key' => base_url() . 'user/reset_password/' . $new_password_key
                        ),
                    ),
                    'email' => array(
                        'to' => $user->email,
                        'from' => NO_REPLY_EMAIL,
                        'from_name' => NO_REPLY_EMAIL_FROM_NAME,
                        'subject' => $email_template->template_subject,
                    )
                );

                $status = $this->chapter247_email->send_mail($param);

                $msg_success = '<h3>Instructions have been sent.</h3><p>Password reset instructions have been sent to <strong>' . $user->email . '</strong>. Don’t forget to check your spam and junk mail filters if they don’t turn up.</p>
';    //'Please check your email,Your reset password link send to your account.'
                $this->session->set_flashdata('msg_success', $msg_success);
            } else {
                $this->session->set_flashdata('msg_error', 'Your Email address not found in our Services.');
            }
            redirect('user/forgot_password');
        }

        $data['template'] = 'user/forgot_password';
        $this->load->view('templates/frontend_template', $data);
    }

    public function reset_password($activation_key = '')
    {
        $data['title_top'] = 'Reset password';
        $this->form_validation->set_rules('password', 'New Password', 'required|matches[confpassword]');
        $this->form_validation->set_rules('confpassword', 'Confirm Password', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == TRUE) {

            $user = $this->user_model->get_row('users', array('new_password_key' => trim($activation_key)));

            if ($user == FALSE) {
                $this->session->set_flashdata('msg_error', 'Your activation key expired.');
                redirect('user/reset_password');
            }

            $user_data = array(
                'password' => sha1(trim($this->input->post('password'))),
                'new_password_key' => '');

            if ($this->user_model->update('users', $user_data, array('id' => $user->id))) {
                $this->session->set_flashdata('msg_success', 'Your Password has reset successfully now you can Login.');
                redirect('user/login');
            }
        }
        $data['template'] = 'user/reset_password';
        $this->load->view('templates/frontend_template', $data);
    }

    public function logout()
    {
        $this->check_login(); //check  login authentication
        $this->session->sess_destroy();
        redirect('','refresh');
    }

    public function profile_photo_check()
    {
        $config['upload_path'] = 'assets/uploads/profile';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('profile_photo')) {
            $this->form_validation->set_message('profile_photo_check', $this->upload->display_errors());
            return FALSE;
        } else {
            $profile_photo = $this->upload->data();
            $param = array('height' => 100,
                'width' => 100,
                'source_image' => 'assets/uploads/profile/' . $profile_photo['file_name'],
                'new_image' => 'assets/uploads/profile/');
            image_resize($param);
            $this->session->set_userdata('profile_photo', $profile_photo['file_name']);
            return true;
        }
    }

    public function signup(){

        $this->form_validation->set_rules('designation', 'Designation', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');
        $this->form_validation->set_rules('password', 'Password', 'required|matches[confpassword]');
        $this->form_validation->set_rules('confpassword', 'Confirm Password', 'required');
        $this->form_validation->set_rules('phone', 'Phone', 'required');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {


            $user_data = array(
                'email' => $this->input->post('email'),
                'password' => sha1($this->input->post('password')),
                'created' => date('Y-m-d H:i:s'),
                'last_ip' => $this->input->ip_address(),
                'user_role' => '1',
                'created' => date('Y-m-d H:i:s')
            );
            if ($user_id = $this->common_model->insert('users', $user_data)):
                redirect('user/registration_host');
            else:
                $this->session->set_flashdata('msg_error', 'failed, Please try again.');
                redirect('user/registration_host');
            endif;
        }

        $data['title'] = "User Signup";
        $data['template'] = 'user/signup';

        $this->load->view('templates/frontend/layout', $data);
    }

    public function dashboard(){
        $this->check_login(); //check login authentication

        $data['title'] = "User Dashboard";
        $data['template'] = "frontend/user/dashboard";
        $this->load->view('templates/frontend/layout', $data);
    }

    public function change_password()
    {
        $this->check_login(); //check login authentication
        $this->form_validation->set_rules('oldpassword', 'Old Password', 'required|callback_password_check');
        $this->form_validation->set_rules('newpassword', 'New Password', 'required|matches[confpassword]');
        $this->form_validation->set_rules('confpassword', 'Confirm Password', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $data['title_top'] = 'Change password';
        if ($this->form_validation->run() == TRUE) {
            $user_data = array('password' => sha1($this->input->post('newpassword')));
            if ($this->user_model->update('users', $user_data, array('id' => user_id()))) {
                $this->session->set_flashdata('msg_success', 'Your password updated successfully.');
                redirect('user/change_password');
            } else {
                $this->session->set_flashdata('msg_error', 'Update failed, Please try again.');
                redirect('user/change_password');
            }
        }
        $data['template'] = 'user/change_password';
        $this->load->view('templates/frontend_template', $data);
    }

    public function password_check($oldpassword)
    {
        if ($this->user_model->password_check(array('password' => sha1($oldpassword)), user_id())) {
            return TRUE;
        } else {
            $this->form_validation->set_message('password_check', 'The %s does not match.');
            return FALSE;
        }
    }

}
