<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $slug = strtolower($this->uri->segment(1));
        switch ($slug) {

            case 'contact':

                $this->form_validation->set_rules('firstname', 'First Name', 'required');
                $this->form_validation->set_rules('lastname', 'Last Name', 'required');
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
                //$this->form_validation->set_rules('mobile','Mobile Number', 'required');
                $this->form_validation->set_rules('message', 'Message', 'required');
                $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                if ($this->form_validation->run() == TRUE) {
                    $user_data = array(
                        'firstname' => $this->input->post('firstname'),
                        'lastname' => $this->input->post('lastname'),
                        'email' => $this->input->post('email'),
                        //'mobile'	=>	$this->input->post('mobile'),
                        'created' => date('Y-m-d'),
                        'message' => $this->input->post('message'),
                        'user_ip' => $this->input->ip_address(),
                    );
                    $this->load->model('user_model');



                    if ($user_id = $this->user_model->insert('contact_us', $user_data)) {
                        $this->session->set_flashdata('msg_success', 'Message sent successfully.');

                        $this->load->library('chapter247_email');
                        $email_template = $this->chapter247_email->get_email_template(10);

                        $param = array(
                            'template' => array(
                                'temp' => $email_template->template_body,
                                'var_name' => array(
                                    'name' => $this->input->post('firstname') . ' ' . $this->input->post('lastname'),
                                    'msg' => $this->input->post('message'),
                                ),
                            ),
                            'email' => array(
                                'to' => 'jagdish@chapter247.com',
                                'from' => NO_REPLY_EMAIL,
                                'from_name' => NO_REPLY_EMAIL_FROM_NAME,
                                'subject' => $email_template->template_subject,
                            )
                        );
                        $status = $this->chapter247_email->send_mail($param);

                        redirect(current_url());
                    } else {
                        $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                        redirect(current_url());
                    }
                }

                $data['title'] = ucfirst(str_replace('-', ' ', $slug));
                $data['template'] = "page/contact";
                break;


            case 'faq':

                $data['site_title'] = "FAQ | " . SITE_NAME;
                $data['faqs'] = $this->common_model->get_result('faqs', array('status' => 1));
                $data['template'] = 'page/faq';

                break;

            case 'login':

                $data['site_title'] = "Login | " . SITE_NAME;
                $data['template'] = 'user/login';
                break;

             case 'the-concept':
             case 'concept':

                $data['title'] = "The Concept | " . SITE_NAME;
                $data['template'] = 'page/the_concept';
            break;

             case 'about':
             case 'about-the-chef':
                $data['title'] = "The Concept | " . SITE_NAME;
                $data['template'] = 'page/about';
            break;

             case 'food':
             case 'the-food':
                $data['title'] = "THE FOOD | " . SITE_NAME;
                $data['template'] = 'page/the-food';
            break;

             case 'containers':
             case 'our-containers':
                $data['title'] = "OUR CONTAINERS | " . SITE_NAME;
                $data['template'] = 'page/our-containers';
            break;

            case 'comapny':
             case 'the-comapny':
                $data['title'] = "The Company | " . SITE_NAME;
                $data['template'] = 'page/the-comapny';
            break;




            /* case 'about':
              case 'about-us':

              $data['title']=ucfirst(str_replace('-', ' ',$slug ));
              $data['page']=$this->common_model->page($slug);
              $data['template']='page/about';
              break; */

            default:

                if ($data['page'] = $this->common_model->page($slug)) {
                    $data['site_title'] = ucfirst(str_replace('-', ' ', $slug)) . " | " . SITE_NAME;
                    $data['template'] = 'page/page';
                } else {
                    $data['site_title'] = "404 | " . SITE_NAME;
                    $data['template'] = 'page/page_404';
                }
                break;
        }


        //$data['random_slider']=random_background();
        // $this->load->view('home/index',$data);
        $this->load->view('templates/frontend/layout', $data);
    }

}
