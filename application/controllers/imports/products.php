<?php

/**
 * @property Csvreader $csvreader
 * @property CI_DB_active_record $db
 */
class Products extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function csv()
    {
        $this->load->library('csvreader');
        $this->load->helper('url');

        $csv = $this->csvreader->parse_file(__DIR__ . '/data/UpdatedProducts_Table.csv');

        foreach ($csv as $line) {
            $productId = $this->_parseProduct($line);
            if ($productId) {
                $this->_parseIngredients($productId, $line);
                $this->_parsePrices($productId, $line);
            }
        }

        echo "<br/>Product Import Finished.";
    }

    private function _parseProduct($line)
    {
        $product = array();
        $product['product_name'] = $line['Item_Name'];
        $product['product_slug'] = strtolower(url_title($line['Item_Name']));
        $product['product_description'] = $line['Description'];
        $product['product_sku'] = $line['SKU'];
        $product['pos_products_id'] = $line['SKU'];
        $product['instructions'] = $line['Prep Instructions'];
        $product['additional_info'] = $line['Additional Information'];
        $product['created'] = date('Y-m-d H:i:s');
        $product['status'] = 1;
        if (trim($line['Start Date'])) {
            $product['start_date'] = date('Y-m-d H:i:s', strtotime($line['Start Date']));
        }
        if (trim($line['End Date'])) {
            $product['end_date'] = date('Y-m-d H:i:s', strtotime($line['End Date']));
        }

        if (!$this->_productExists($product['product_slug'])) {
            $this->db->insert('products', $product);
            if ($this->db->affected_rows() > 0) {
                return $this->db->insert_id();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function _productExists($slug)
    {
        $this->db->select('count(*) as total');
        $this->db->from('products');
        $this->db->where('product_slug', $slug);
        $result = $this->db->get();
        if ($result && $result->num_rows() > 0) {
            $row = $result->row();
            return $row->total > 0;
        } else {
            return false;
        }
    }

    private function _parseIngredients($productId, $line)
    {
        $ingredients = $line['Ingredients'];

        /*
         * First Clean Ingredients
         */
        $ingredients = preg_replace('/\(.*?\)/', ' ', $ingredients);
        $ingredients = preg_replace('/\\s+/', ' ', $ingredients);
        $ingredients = trim($ingredients);


        $productIngredients = array();

        /*
         * Break down ingredients and fetch/add to database.
         */
        $tokens = explode(',', $ingredients);
        foreach ($tokens as $token) {
            $ingredient = ucwords(trim($token));
            if ($ingredient != '') {
                $ingredientId = $this->_fetchOrAddIngredient($ingredient);
                if ($ingredientId) {
                    $productIngredients[] = array(
                        'products_id' => $productId,
                        'ingredients_id' => $ingredientId,
                    );
                }
            }
        }

        if (count($productIngredients) > 0) {
            $this->db->insert_batch('products_ingredients', $productIngredients);
        }
    }

    private function _fetchOrAddIngredient($ingredient)
    {

        $ingredient = str_replace('.', ' ', $ingredient);
        $ingredient = str_replace('And', ' ', $ingredient);
        $ingredient = str_replace('Or', ' ', $ingredient);
        $ingredient = preg_replace('/[^\s\w\-\.]/', '', $ingredient);
        $ingredient = preg_replace('/\s+/', ' ', $ingredient);
        $ingredient = trim($ingredient);

        $this->db->select('*');
        $this->db->from('ingredients');
        $this->db->where('name', $ingredient);
        $this->db->limit(1);
        $result = $this->db->get();
        if ($result && $result->num_rows() > 0) {
            $row = $result->row();
            return $row->id;
        } else {
            $insertIngredient = array(
                'name' => $ingredient,
                'slug' => strtolower(url_title($ingredient)),
            );
            $this->db->insert('ingredients', $insertIngredient);
            if ($this->db->affected_rows() > 0) {
                return $this->db->insert_id();
            } else {
                return false;
            }
        }
    }

    private function _parsePrices($productId, $line)
    {
        $productPrices = array();

        $portions = array(
            'Small' => 1,
            'Medium' => 2,
            'Large' => 3,
            'Family' => 4,
        );

        foreach ($portions as $key => $value) {
            if (array_key_exists($key . ' Weight', $line) && array_key_exists($key . ' Price', $line)) {
                if (trim($line[$key . ' Weight']) || trim($line[$key . ' Price'])) {
                    $productPrices[] = array(
                        'products_id' => $productId,
                        'portions_id' => $value,
                        'weight' => trim(preg_replace('[0-9\.]', '', $line[$key . ' Weight'])),
                        'price' => trim(preg_replace('[0-9\.]', '', $line[$key . ' Price'])),
                    );
                }
            }
        }

        if (count($productPrices) > 0) {
            $this->db->insert_batch('products_portions', $productPrices);
        }
    }

}
