<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Superadmin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        clear_cache();
        $this->load->model('superadmin_model');

    }

    public function index()
    {
        $this->login();
    }

    private function _check_login()
    {
        if (superadmin_logged_in() === FALSE) redirect('superadmin/login');
    }

    public function login()
    {
        if (superadmin_logged_in() === TRUE) redirect('backend/dashboard');

        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            $this->load->model('user_model');
            $data = array('email' => $this->input->post('email'), 'password' => sha1($this->input->post('password')));
            if ($this->user_model->login($data, 'superadmin')) {
                redirect('backend/dashboard');
            } else {
                redirect('superadmin/login');
            }
        }
        $this->load->view('superadmin/login');
    }

    public function logout(){
        $this->_check_login(); //check  login authentication
        $this->session->sess_destroy();
        redirect('superadmin/login');
    }


    public function profile()
    {
        $this->_check_login(); //check login authentication

        $this->form_validation->set_rules('name', 'Name', 'required');

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == TRUE) {
            $user_data = array(
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
            );

            if ($this->superadmin_model->update('users', $user_data, array('id' => superadmin_id()))) {
                $this->session->set_flashdata('msg_success', 'Profile updated successfully.');
                redirect('superadmin/profile');
            } else {
                $this->session->set_flashdata('msg_error', 'Update failed, Please try again.');
                redirect('superadmin/profile');
            }
        } else {
            $data['user'] = $this->superadmin_model->get_row('users', array('id' => superadmin_id()));

            $data['template'] = 'superadmin/profile';
            $this->load->view('templates/backend/layout', $data);
        }
    }

    public function change_password()
    {
        $this->_check_login(); //check login authentication
        $this->form_validation->set_rules('oldpassword', 'Old Password', 'required|callback_password_check');
        $this->form_validation->set_rules('newpassword', 'New Password', 'required|matches[confpassword]');
        $this->form_validation->set_rules('confpassword', 'Confirm Password', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            $user_data = array('password' => sha1($this->input->post('newpassword')));

            if ($this->superadmin_model->update('users', $user_data, array('id' => superadmin_id()))) {
                $this->session->set_flashdata('msg_success', 'Password updated successfully.');
                redirect('superadmin/change_password');
            } else {
                $this->session->set_flashdata('msg_error', 'Update failed, Please try again.');
                redirect('superadmin/change_password');
            }
        }
        $data['template'] = 'superadmin/change_password';
        $this->load->view('templates/backend/layout', $data);
    }

    public function password_check($oldpassword)
    {
        $this->load->model('user_model');
        if ($this->user_model->password_check(array('password' => sha1($oldpassword)), superadmin_id())) {
            return TRUE;
        } else {
            $this->form_validation->set_message('password_check', 'The %s does not match.');
            return FALSE;
        }
    }

}
