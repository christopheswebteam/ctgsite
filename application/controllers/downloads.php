<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Downloads extends CI_Controller
{

    public function catering_menu()
	{
		$this->load->helper('download');
	   
		$data = file_get_contents(realpath('assets/files/catering_menu.pdf'));
		$name = "catering_menu.pdf";

		force_download($name, $data);
    }

}