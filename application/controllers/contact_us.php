<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contact_us extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){


        $data['title'] = "Contact Us";
        $data['template'] = "frontend/contact_us";

        $this->load->view('templates/frontend/layout', $data);
    }

    public function securimage_show(){
    	require_once APPPATH.'/libraries/securimage/securimage_show.php';
    }

    public function send(){

		if (isset($_POST['name'])) {

				require_once APPPATH.'/libraries/securimage/securimage.php';

				$securimage = new Securimage();

				if ($securimage->check($_POST['captcha']) == false) {
					$validation[] = array('message' => 'Wrong captcha code', 'id' => 'form-captcha');
				}

				header('Content-Type: application/json');

				if (empty($validation)) {

					 $user_data = array(
		                'name' => $this->input->post('name'),
		                'email' => $this->input->post('email'),
		                'message' => $this->input->post('message'),
		                'ip_address' => $this->input->ip_address(),
		                'created' => date('Y-m-d H:i:s')
	            	);

					$this->load->model('contact_us_model');
	           		$this->contact_us_model->insert($user_data);

           		 	$this->load->library('chapter247_email');
					$email_template=$this->chapter247_email->get_email_template(1);

					$param=array(
					'template' 	=>	array(
									'temp'	=> 	$email_template->template_body,
									'var_name' 	=>  $user_data,
					),
					'email'	=> 	array(
							'to' 		=>	 $this->input->post('email'),
							'from' 		=>	 NO_REPLY_EMAIL,
							'from_name' =>	 NO_REPLY_EMAIL_FROM_NAME,
							'subject'	=>	 $email_template->template_subject,
						)
					);
					$status=$this->chapter247_email->send_mail($param);
					if ($status['EMAIL_STATUS']) {
						echo json_encode(array('success'=>(bool)true, 'message'=>''));
					} else {
						echo json_encode(array('success'=>(bool)false, 'type'=>'system', 'data'=>array('message'=>'Sending error, please try again later')));
					}
				} else {
					echo json_encode(array('success'=>(bool)false, 'type'=>'validation', 'data'=>$validation));
				}

			die();
		}
    }

}