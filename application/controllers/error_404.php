<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Error_404 extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['title'] = "404 Error Page";
        $data['template'] = "frontend/error_404";

        $this->load->view('templates/frontend/layout', $data);
    }

}