<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class How_it_works extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function the_concept()
    {
        $data['title'] = "The Concept";
        $data['template'] = "frontend/how_it_works/the_concept";

        $this->load->view('templates/frontend/layout', $data);
    }

    public function chef_christophe()
    {
        $data['title'] = "Chef Christophe";
        $data['template'] = "frontend/how_it_works/chef_christophe";

        $this->load->view('templates/frontend/layout', $data);
    }

    public function the_food()
    {
        $data['title'] = "The Food";
        $data['template'] = "frontend/how_it_works/the_food";

        $this->load->view('templates/frontend/layout', $data);
    }

    public function our_containers()
    {
        $data['title'] = "Our Containers";
        $data['template'] = "frontend/how_it_works/our_containers";

        $this->load->view('templates/frontend/layout', $data);
    }

    public function chef_christophes_team()
    {
        $data['title'] = "Chef Christophe's Team";
        $data['template'] = "frontend/how_it_works/chef_christophes_team";

        $this->load->view('templates/frontend/layout', $data);
    }

    public function catering()
    {
        $data['title'] = "Catering";
        $data['template'] = "frontend/how_it_works/catering";

        $this->load->view('templates/frontend/layout', $data);
    }

    public function the_company()
    {
        $data['title'] = "The Company";
        $data['template'] = "frontend/how_it_works/the_company";

        $this->load->view('templates/frontend/layout', $data);
    }

    public function faq()
    {
        $data['title'] = "Frequently Asked Questions";
        $data['template'] = "frontend/how_it_works/faq";

        $this->load->view('templates/frontend/layout', $data);
    }

}