<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dynamic_routing extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->model('uri_routes_model');

        $uri = $this->uri->uri_string();

        $route = $this->uri_routes_model->get_route_by_uri($uri);

        if ($route) {

            if ($route['type'] == "Redirect") {

                redirect($route['redirect_destination']);
            } elseif ($route['type'] == "View") {

                $data['template'] = $route['view_file_path'];

                $this->load->view('templates/frontend/layout', $data);
            }
        } else {

            $this->load->model('blog_model');
            $blog_post = $this->blog_model->get_post_by_slug($uri);
            if ($blog_post) {
                redirect('blog/post/' . $blog_post['slug'] . '/' . $blog_post['id']);
            } else {

                show_404();
            }
        }
    }

}
