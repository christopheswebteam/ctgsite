<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sandbox extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function time()
    {
        echo "Current Timezone: " . date_default_timezone_get();
        echo "<br/>Current Timestamp: " . date('Y-m-d H:i:s');
        echo "<br/>Current GMT Timestamp: " . gmdate('Y-m-d H:i:s');
    }
	
	public function resend_order_to_pos($order_id)
	{
		$this->load->library('ctg_api');
		
		$this->ctg_api->debug = TRUE;
		
		$doit = $this->ctg_api->post('orders/send_order_to_pos', array('order_id' => $order_id));
		
		exit($doit);
	}
	
	public function melvin_moon()
	{
		$string = '{"order_error":"","locations":{"id":"1","type":"Retail","status":"1","name":"Johns Creek","location_slug":"johns-creek","street_address_1":"9775 Medlock Bridge Road","street_address_2":"Suite P","city":"Johns Creek","state":"GA","zip_code":"30097","latitude":"34.016121","longitude":"-84.190445","opening_time":"10:00 AM","closing_time":"8:00 PM","phone_number":"(678) 686-0599","pos_locations_id":"0","tax_rate":"7"},"customer_id":"19","name_first":"melvin","name_last":"moon","email_address":"mmoon1163@gmail.com","phone_number":"4047974255","street_address_1":"2910 Buford unit 1311","street_address_2":"","city":"Buford","state":"GA","zip_code":"30518","cc_name_first":"melvin","cc_name_last":"moon","cc_email_address":"mmoon1163@gmail.com","cc_phone_number":"4047974255","cc_street_address_1":"2910 Buford unit 1311","cc_street_address_2":"","cc_city":"Buford","cc_state":"GA","cc_zip_code":"30518","cc_type":"Visa","cc_number":"4430473034566795","cc_exp_month":"07","cc_exp_year":"2017","cc_cvn":"302","order_id":"215","order_subtotal":"90.85","order_tax":"6.36","order_total":"97.21","order_locations_id":"1","order_pos_locations_id":"0","order_pickup_date":"2014-08-06","products":[{"quantity":"2","products_id":"119","pos_products_id":"1734","portions_id":"3","pos_portions_id":"7","pos_sku":"17347","unit_price":"5.49","total_price":"10.98"},{"quantity":"2","products_id":"162","pos_products_id":"1734","portions_id":"3","pos_portions_id":"7","pos_sku":"17347","unit_price":"7.49","total_price":"14.98"},{"quantity":"2","products_id":"100","pos_products_id":"1734","portions_id":"3","pos_portions_id":"7","pos_sku":"17347","unit_price":"5.49","total_price":"10.98"},{"quantity":"2","products_id":"108","pos_products_id":"1734","portions_id":"3","pos_portions_id":"7","pos_sku":"17347","unit_price":"8.49","total_price":"16.98"},{"quantity":"2","products_id":"113","pos_products_id":"1734","portions_id":"2","pos_portions_id":"7","pos_sku":"17347","unit_price":"5.99","total_price":"11.98"},{"quantity":"5","products_id":"161","pos_products_id":"1734","portions_id":"3","pos_portions_id":"7","pos_sku":"17347","unit_price":"4.99","total_price":"24.95"}],"CTG-API-KEY":"841c87f2b73813be3e01a05be65b4f94f5a27161"}';
		
		$data = (array)json_decode($string);
		
		echo "<pre>";
		print_r($data);
		echo "</pre>";
	}

    public function order_test()
    {
        $this->load->library('ctg_api');
		
        // Customer Information
		
        $order_data['customer_id'] = "19";
		$order_data['name_first'] = "Melvin";
        $order_data['name_last'] = "Moon";
        $order_data['email_address'] = "mmoon1163@gmail.com";
        $order_data['phone_number'] = "4047974255";
        $order_data['street_address_1'] = "2910 Buford unit 1311";
        $order_data['street_address_2'] = "";
        $order_data['city'] = "Buford";
        $order_data['state'] = "GA";
        $order_data['zip_code'] = "30518";
		
        // Order Information
		
		$order_data['order_id'] = "215";
        $order_data['order_subtotal'] = "90.85";
        $order_data['order_tax'] = "6.36";
        $order_data['order_total'] = "97.21";
        $order_data['order_pickup_date'] = "2014-08-06";
        $order_data['order_pickup_time'] = "";
        $order_data['order_locations_id'] = "1";
		
        // Credit Card Information
		
        $order_data['cc_name_first'] = "melvin";
        $order_data['cc_name_last'] = "moon";
        $order_data['cc_street_address_1'] = "2910 Buford unit 1311";
        $order_data['cc_street_address_2'] = "";
        $order_data['cc_city'] = "Buford";
        $order_data['cc_state'] = "GA";
        $order_data['cc_zip_code'] = "30518";
        $order_data['cc_type'] = "Visa"; // Visa, MasterCard, Discover, American Express
        $order_data['cc_number'] = "4430473034566795";
        $order_data['cc_cvn'] = "302";
        $order_data['cc_exp_month'] = "07";
        $order_data['cc_exp_year'] = "2017";
		
        // Products Information

        $order_data['products'][0]['quantity'] = "2";
        $order_data['products'][0]['products_id'] = "119";
        $order_data['products'][0]['portions_id'] = "3";
        $order_data['products'][0]['unit_price'] = "5.49";
		
		$order_data['products'][1]['quantity'] = "2";
        $order_data['products'][1]['products_id'] = "162";
        $order_data['products'][1]['portions_id'] = "3";
        $order_data['products'][1]['unit_price'] = "7.49";
		
		$order_data['products'][2]['quantity'] = "2";
        $order_data['products'][2]['products_id'] = "100";
        $order_data['products'][2]['portions_id'] = "3";
        $order_data['products'][2]['unit_price'] = "5.49";
		
		$order_data['products'][3]['quantity'] = "2";
        $order_data['products'][3]['products_id'] = "108";
        $order_data['products'][3]['portions_id'] = "3";
        $order_data['products'][3]['unit_price'] = "8.49";
		
		$order_data['products'][4]['quantity'] = "2";
        $order_data['products'][4]['products_id'] = "113";
        $order_data['products'][4]['portions_id'] = "2";
        $order_data['products'][4]['unit_price'] = "5.99";
		
		$order_data['products'][5]['quantity'] = "5";
        $order_data['products'][5]['products_id'] = "161";
        $order_data['products'][5]['portions_id'] = "3";
        $order_data['products'][5]['unit_price'] = "4.99";
		
        $this->ctg_api->debug = TRUE;
		
        $order = $this->ctg_api->post('orders/submit_order_to_pos', $order_data);
		
        exit($order);
		
        echo "<pre>";
        print_r($order);
        echo "</pre>";
    }

}
