<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menu extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('products_model');
    }

    public function index($offset = 0)
    {
        $data['title'] = "Menu";

        $per_page = 12;

        if (isset($_POST['product_location'])) {

            $data['location_id'] = $_POST['product_location'];

            $this->session->set_userdata('active_location_id', $_POST['product_location']);
        }

        $data['portions'] = $this->products_model->get_result('portions');
        $data['locations'] = $this->products_model->get_result('locations', array('status' => 1));
        $data['product_item_types'] = $this->products_model->get_product_item_types();
        $data['product_tags'] = $this->products_model->get_product_tags();
        $data['products'] = $this->products_model->search_products('', $offset, $per_page);
        $data['products_search'] = $this->session->userdata('products_search');
        $data['template'] = "frontend/menu/list";

        //$data['url_query_string'] = ((is_array($_GET)) && (count($_GET) > 0)) ? http_build_query($_GET) : FALSE;

        $this->load->view('templates/frontend/layout', $data);
    }

    public function clear_search()
    {
        $this->session->unset_userdata('products_search');

        redirect('menu');
    }

    public function product($slug = 0, $id = 0)
    {
        if ($products = $this->products_model->search_products($id, 0, 1)) {

            $data['title'] = $products[0]->name;
            $data['products'] = $products;
            $data['locations'] = $this->products_model->get_all_active_locations();
            $data['position_products'] = $this->products_model->get_position_products($id);

            if ($this->session->userdata('products_search')) {

                $data['cancel_link'] = "menu?" . http_build_query($this->session->userdata('products_search'));
            } else {

                $data['cancel_link'] = "menu";
            }

            $data['template'] = "frontend/menu/product";
        } else {

            $data['template'] = "page/page_404";
        }

        $this->load->view('templates/frontend/layout', $data);
    }

    public function ajax_product($offset = 0, $per_page = 3)
    {
        // $per_page=2;
        $products = $this->products_model->search_products('', $offset, $per_page);
        $config['base_url'] = base_url('menu/ajax_product/');
        $config['total_rows'] = $this->products_model->search_products('', 0, 0);
        $config['per_page'] = $per_page;

        $this->pagination->initialize($config);

        $data['pagination'] = $this->pagination->create_links();
        $data['total_rows'] = $config['total_rows'];
        $data['offset'] = $offset;
        $data['per_page'] = $per_page;
        // $data['products']=$products;
        $data['products_grid'] = $this->load->view('frontend/menu/grid_view', array('products' => $products, 'offset' => $offset, 'per_page' => $per_page), TRUE);
        $data['products_list'] = $this->load->view('frontend/menu/list_view', array('products' => $products, 'offset' => $offset, 'per_page' => $per_page), TRUE);

        if ($products) {
            $data['status'] = TRUE;
        } else {
            $data['status'] = FALSE;
        }

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function check_product_location_exist($location_id = 0)
    {
        if (!empty($location_id)) {
            if ($this->session->userdata('active_location_id')) {
                if ($this->session->userdata('active_location_id') == $location_id) {
                    die('EXIST');
                } else {
                    die('CHANGED');
                }
            } else {
                die('NEW');
            }
        } else {
            die('EMPTY');
        }
    }

    public function check_product_location()
    {
        if (isset($_GET)) {
            $arrayName = $this->input->get('location_id');
            $this->session->set_userdata('active_location_id', $arrayName);
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function manage_location()
    {
        if ($_POST) {
            if (!empty($_POST['location_id'])) {
                $location_id = $_POST['location_id'];
                $location_type = $this->products_model->get_location_type($location_id);
                echo $location_type;
            }
        }
    }

    public function get_location_attrib($location_id = 0, $product_id = 0)
    {

        $data = array();
        if (empty($location_id)):
            $response = array('status' => 0, 'data' => 0);

        else:

            if ($locations_products = $this->products_model->get_row('locations_products', array('locations_id' => $location_id, 'products_id' => $product_id))):

                if ($locations = $this->products_model->get_row('locations', array('id' => $location_id, 'status' => 1))) {

                    //$this->session->set_userdata('active_location_id',$location_id);
                    $data['locations_products'] = $locations_products;
                    $data['locations'] = $locations;
                    $data['products'] = $this->products_model->get_row('products', array('id' => $product_id, 'status' => 1));

                    $result = $data1 = $this->load->view('frontend/menu/add_to_cart_popup', $data, TRUE);


                    $response = array('status' => 1, 'data' => $result);
                } else {
                    $response = array('status' => 0, 'data' => 0);
                }


            else:
                //$string= "<h4 style='color:red'><center>Oops Unfortunately<br> The selected item is not currently available for this location.</center></h4>";

                $data['locations_products'] = FALSE;
                $data['locations'] = $this->products_model->get_row('locations', array('id' => $location_id, 'status' => 1));
                $data['products'] = $this->products_model->get_row('products', array('id' => $product_id, 'status' => 1));

                $result = $data1 = $this->load->view('frontend/menu/add_to_cart_popup', $data, TRUE);


                $response = array('status' => 1, 'data' => $result);

            endif;


        endif;

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function check_outdated_porduct_on_location($portion_id = 0, $product_id = 0, $location_id = 0)
    {

        if (!empty($portion_id) && !empty($product_id) && !empty($location_id)):


            $pickup_info = $this->session->userdata('pickup_info');
            // echo $pickup_info['order_pickup_date'];
            // if($pickup_info['order_pickup_date']==date('Y-m-d')){

            $lp = $this->products_model->get_row('locations_products', array('locations_id' => $location_id, 'products_id' => $product_id, 'portions_id' => $portion_id, 'pos_total_available_date LIKE' => $pickup_info['order_pickup_date'] . " %"));
            if ($lp) {
                if ($lp->pos_total_available) {
                    $response = array('status' => 1, 'data' => $lp->pos_total_available);
                } else {
                    $response = array('status' => 2, 'data' => "availabe " . $lp->pos_total_available);
                }
            } else {
                $response = array('status' => 2, 'data' => 'Not Found.');
            }

        /*   }else{
          $response=array('status'=>1,'data'=> "user selected future date.");
          } */


        else:
            $response = array('status' => 0, 'data' => "Can't empty $portion_id , $product_id , $location_id");
        endif;


        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function check_params($products_id = 0, $locations_id = 0, $portions_id = '', $first_time = 10)
    {
        $response = array();

        if (!empty($products_id) && !empty($locations_id) && !empty($portions_id)):


            $locations = $this->products_model->get_row('locations', array('id' => $locations_id));
            //$locations->type;	$locations->opening_time; $locations->closing_time;
            $tomorrow = strtotime('+1 day', strtotime(date('Y-m-d')));


            if ($first_time == 0 || $first_time == 1 || $first_time == 2):


                if (!empty($_GET['pickup_date']) && $_GET['pickup_date'] == date('Y-m-d')) {

                    $befor_twnty_time = date("h:i A", strtotime('+20 minutes', strtotime(date('Y-m-d h:i:s A'))));

                    if (strtotime($befor_twnty_time) >= strtotime($locations->closing_time)) {
                        $address = $locations->name . ", " . $locations->street_address_1 . ", " . $locations->city . ", " . $locations->state . ", " . $locations->zip_code . "\n Phone number " . $locations->phone_number;




                        $response = array('status' => 0, 'data' => "Sorry. The store is either closing soon or is already closed and we cannot take same-day orders this close to closing time. Please call the store if you want to pick up meals today.\n Store Details: \n" . $address);
                        $first_time = 3;
                    } else {

                        if ($locations->type == 'Cooler') {
                            //12:01pm -> 11:59pm
                            if (strtotime(date('h:i A')) >= strtotime('12:01 PM') && strtotime(date('h:i A')) <= strtotime('11:59 PM')) {
                                $response = array('status' => 0, 'data' => "Sorry Too late for next day delivery.\n Please select next available delivery date. or select Retail store.");
                                $first_time = 3;
                            } else {
                                $response = array('status' => 1, 'data' => 'cutoff time for "Cooler location".');
                            }
                        }

                        if ($locations->type == 'Retail') {
                            //12:01pm -> 11:59pm
                            if (strtotime(date('h:i A')) >= strtotime('12:01 PM') && strtotime(date('h:i A')) <= strtotime('11:59 PM')) {
                                //$response=array('status'=>0,'data'=>"Sorry Too late for next day delivery.\n Please select next date.");
                                //$first_time=3;
                                $response = array('status' => 1, 'data' => 'cutoff time for "Retail location".');
                            } else {
                                $response = array('status' => 1, 'data' => 'cutoff time for "Retail location".');
                            }
                        }
                    }
                } else {
                    $response = array('status' => 1, 'data' => 'cutoff time ok.');
                    /* $befor_sevenday_time= date("Y-m-d", strtotime('+7 days',strtotime(date('Y-m-d'))));

                      if(!empty($_GET['pickup_date']) && strtotime($_GET['pickup_date']) > $befor_sevenday_time){
                      $response=array('status'=>0,'data'=>'Sorry. Orders can be placed within days of ordering date.');
                      }else{
                      $response=array('status'=>1,'data'=>'cutoff time ok.');
                      } */
                }

            endif;


            if ($first_time == 1):

                if (!empty($_GET['pickup_date']) && strtotime($_GET['pickup_date']) == $tomorrow):
                    //1405076220 ## 1405105200
                    if (strtotime(date('h:i A')) <= strtotime('12:00 PM')) {
                        //This is available for all locations (both Retail and Cooler)
                        $response = array('status' => 1, 'data' => 'This is available for all locations (both Retail and Cooler).');
                    } else {
                        if ($locations->type == 'Cooler') {
                            //choose a Retail location or change their pickup date
                            $response = array('status' => 0, 'data' => 'The cutoff time to order this product for pickup at this location tomorrow was 12:00PM EST. Please choose another location or change your pickup date.');
                        } else {
                            //continune
                            $response = array('status' => 1, 'data' => 'Retail location available.');
                        }
                    }
                else:
                    //$response=array('status'=>1,'data'=>'available for today.');
                    $locations_productss = $this->products_model->get_row('locations_products', array('locations_id' => $locations_id, 'products_id' => $products_id, 'portions_id' => $portions_id));

                    if ($locations_productss) {
                        if ($locations_productss->pos_total_available) {
                            $response = array('status' => 1, 'data' => 'Available');
                        } else {
                            $response = array('status' => 0, 'data' => 'This Item Is Not Available For Pick Up Today.');
                        }
                    } else {
                        $befor_sevenday_time = date("Y-m-d", strtotime('+7 days', strtotime(date('Y-m-d'))));

                        if (!empty($_GET['pickup_date']) && strtotime($_GET['pickup_date']) > strtotime($befor_sevenday_time)) {
                            $response = array('status' => 0, 'data' => 'Sorry. Orders can be placed within 7 days of ordering date.');
                        } else {
                            $response = array('status' => 1, 'data' => 'cutoff time ok.11');
                        }
                    }


                endif; elseif ($first_time == 0):

                //second time

                $pickup_info = $this->session->userdata('pickup_info');
                $lp = $this->products_model->get_row('locations_products', array('locations_id' => $locations_id, 'products_id' => $products_id, 'portions_id' => $portions_id));
                if ($lp) {
                    if ($lp->pos_total_available) {
                        $response = array('status' => 1, 'data' => $lp->pos_total_available);
                    } else {
                        $response = array('status' => 0, 'data' => "This item is currently not available for pickup today at this location. kindly to choose another product.");
                    }
                } else {
                    if (strtotime($_GET['pickup_date']) >= strtotime(date('Y-m-d'))) {
                        $response = array('status' => 1, 'data' => 'For futures day available.');
                    } else {
                        $response = array('status' => 0, 'data' => 'This produt is currently not available for pickup today at the location you specified. Please choose another product.');
                    }
                }

            endif;

        else:
            $response = array('status' => 0, 'data' => "Can't empty $portions_id , $products_id , $locations_id");
        endif;

        header('Content-Type: application/json');
        echo json_encode($response);
    }

}
