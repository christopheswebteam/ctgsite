<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['title'] = "Christophe's To Go";
        $data['template'] = "frontend/home";
        $data['isHomePage'] = true;

        $this->load->view('templates/frontend/layout', $data);
    }

}