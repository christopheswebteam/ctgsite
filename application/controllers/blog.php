<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller
{

    public function index($page_number = 0)
	{
       $this->page(0);
    }

    public function page($page_number = 0)
	{
		$this->load->model('blog_model');
		$this->load->helper('text');
		
		$per_page = 5;

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['base_url'] = base_url().'blog/page/';
        $config['total_rows'] = $this->blog_model->count_posts(TRUE);
        $config['per_page'] = $per_page;

        $this->pagination->initialize($config);

        $data['pagination'] = $this->pagination->create_links();
		$data['page_number'] = $page_number;
        $data['posts'] = $this->blog_model->get_posts($per_page, $page_number, TRUE);

        $data['title'] = "Christophe's To Go Blog";
        $data['template'] = "frontend/blog/list";

        $this->load->view('templates/frontend/layout', $data);
    }

    public function post($slug = "whatever", $post_id = 0)
    {
		$this->load->model('blog_model');
		
        if (empty($post_id)) {
            redirect('blog');
        }

        $post = $this->blog_model->get_post_by_id($post_id, TRUE);

        if ($post) {
		
			$data['post'] = $post;
		
		}
		
        $data['title'] = $post['title'];
        $data['template'] = "frontend/blog/post";

        $this->load->view('templates/frontend/layout', $data);

    }

}