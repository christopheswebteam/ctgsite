<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('cart');
        $this->load->model('products_model');
        //$this->output->enable_profiler(TRUE);
    }

    public function index(){
      //$this->session->unset_userdata('pickup_info');
       if($this->session->userdata('active_location_id')){
         $data['locations']= $this->products_model->get_row('locations',array('id'=>$this->session->userdata('active_location_id')));
       }else{
        $data['locations']=FALSE;
       }
      $data['template'] = "frontend/cart";
      $this->load->view('templates/frontend/layout', $data);
    }

    public function add($product_id=0,$portion_id=0,$location_id=0){

    	if(!empty($product_id) && $this->input->post()){


        $portion_id= $this->input->post('portion_id');
        $location_id= $this->input->post('location_id');

        if($this->session->userdata('active_location_id')){
            if($this->session->userdata('active_location_id')!=$location_id){
                $this->cart->destroy();
                $this->session->set_userdata('active_location_id',$location_id);
            }
         }else{
          $this->session->set_userdata('active_location_id',$location_id);
         }


      if(isset($_POST['pickup_date']) && !empty($_POST['pickup_date'])){
          $pickup_info=array(
            'order_pickup_date'=>$this->input->post('pickup_date'),
           // 'order_pickup_time'=>$this->input->post('pickup_hour').":".$this->input->post('pickup_minute'),
            );
          $this->session->set_userdata('pickup_info',$pickup_info);
        }


      //  $this->session->set_userdata('active_location_id',$location_id);

    		if($products=$this->products_model->get_row('products',array('id'=>$product_id,'status'=>1))):

      		$portions=$this->products_model->get_cart_portion($product_id,$portion_id);

          $name = preg_replace('/[^A-Za-z0-9\-\ ]/', '', $products->name);

    			$data = array(
            'id'      => 'ctg_'.$products->id,
            'qty'     => 1,
            'price'   => $portions->price,
            'name'    => $name,
            'options' => array(
            'Portion_id'=>$portion_id,
            'Location_id'=>$location_id,
            'Portion' => $portions->name,
            'Weight' => $portions->weight.' '.$portions->weight_measure,

              )
    			);
  			$this->cart->insert($data);
  			endif;

    	}

		  redirect('cart');
    }

    public function update($id=''){
      if($this->input->post()){
        $data = array();
        foreach($_POST as $row){
          $data[] =array(
                'rowid' => $row['rowid'],
                'qty'   =>  $row['qty']
              );
        }
        $this->cart->update($data);
      }
			redirect('cart');
    }

    public function delete($rowid='0'){

      $data=array(
               'rowid'  =>  $rowid,
                'qty'   => 0
              );
      $this->cart->update($data);
      if($this->cart->total()==0){
         $this->session->unset_userdata('pickup_info');
         $this->session->unset_userdata('active_location_id');
      }

    	redirect('cart');

    }

    public function total_item(){
      echo $this->cart->total_items();
    }

    public function checkout()
	{
		$this->load->library('ctg_api');
		
        $data['order_error']='';

        if($this->session->userdata('active_location_id')){
         $data['locations']= $this->products_model->get_row('locations',array('id'=>$this->session->userdata('active_location_id')));
       }else{
        $data['locations']=FALSE;
       }
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('email_address', 'E-Mail Address', 'required|valid_email');
        $this->form_validation->set_rules('phone_number', 'Phone Number', 'required');
        $this->form_validation->set_rules('street_address_1', 'Street Address 1', 'required');
        $this->form_validation->set_rules('street_address_2', 'Street Address 1', '');
        $this->form_validation->set_rules('city', 'City', 'required');
        $this->form_validation->set_rules('state', 'State', 'required');
        $this->form_validation->set_rules('zip_code', 'Zip Code', 'required');

        $this->form_validation->set_rules('cc_type', 'Credit Card Type', 'required');
        $this->form_validation->set_rules('cc_number', 'Credit Card Number', 'required|numeric');
        $this->form_validation->set_rules('cc_exp_month', 'Expiration Month', 'required');
        $this->form_validation->set_rules('cc_exp_year', 'Expiration Year', 'required');
        $this->form_validation->set_rules('cc_cvn', 'CVN', 'required|numeric');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {

          // Customer Info
		  
		  $data['customer_id'] = $this->session->userdata('customer_id');
          $data['name_first']=$this->input->post('first_name');
          $data['name_last']=$this->input->post('last_name');
          $data['email_address']=$this->input->post('email_address');
          $data['phone_number']=$this->input->post('phone_number');
          $data['street_address_1']=$this->input->post('street_address_1');
          $data['street_address_2']=$this->input->post('street_address_2');
          $data['city']=$this->input->post('city');
          $data['state']=$this->input->post('state');
          $data['zip_code']=$this->input->post('zip_code');

          // CC Info
          $data['cc_name_first']=$this->input->post('first_name');
          $data['cc_name_last']=$this->input->post('last_name');
          $data['cc_email_address']=$this->input->post('email_address');
          $data['cc_phone_number']=$this->input->post('phone_number');
          $data['cc_street_address_1']=$this->input->post('street_address_1');
          $data['cc_street_address_2']=$this->input->post('street_address_2');
          $data['cc_city']=$this->input->post('city');
          $data['cc_state']=$this->input->post('state');
          $data['cc_zip_code']=$this->input->post('zip_code');

          $data['cc_type']=$this->input->post('cc_type');  // Visa, MasterCard, Discover, American Express
          $data['cc_number']=$this->input->post('cc_number');
          $data['cc_exp_month']=$this->input->post('cc_exp_month');
          $data['cc_exp_year']=$this->input->post('cc_exp_year');
          $data['cc_cvn']=$this->input->post('cc_cvn');

          // Order Info
		  
		  $data['order_id'] = $this->session->userdata('order_id');

          if ($this->session->userdata('locations_tax')):
              $locations_tax=$this->session->userdata('locations_tax');
              $data['order_subtotal']=$this->cart->total();
              $data['order_tax']=$locations_tax['tax_rate'];
              $data['order_total'] =($this->cart->total()+$locations_tax['tax_rate']);
              $data['order_locations_id'] = $locations_tax['location_id'];
          else:
              $data['order_subtotal']=$this->cart->total();
              $data['order_tax']=0;
              $data['order_total'] =$this->cart->total();
              $data['order_locations_id']=0;
          endif;

          $pickup_info=$this->session->userdata('pickup_info');

           $data['order_pos_locations_id']=0;
           $data['order_pickup_date']= date('Y-m-d',strtotime($pickup_info['order_pickup_date']));
          // $data['order_pickup_time']= $pickup_info['order_pickup_time'];

           //product info
          foreach ($this->cart->contents() as $items):
           $product_options= $this->cart->product_options($items['rowid']);
            $data['products'][]=array(
              'quantity' => $items['qty'],
              'products_id' => substr($items['id'],4),
              'pos_products_id' => 1734,
              'portions_id' => $product_options['Portion_id'],
              'pos_portions_id' => 7,
              'pos_sku' => 17347,
              'unit_price' => $this->cart->format_number($items['price']),
              'total_price' => $this->cart->format_number($items['subtotal'])
              );
          endforeach;
		  
			//$this->ctg_api->debug = TRUE;

			$response = $this->ctg_api->post('orders/submit_order_to_pos', $data);
			
			$customer_id = (isset($response['data']['customer_id'])) ? $response['data']['customer_id'] : FALSE;
			$order_id = (isset($response['data']['order_id'])) ? $response['data']['order_id'] : FALSE;
			
			$this->session->set_userdata('customer_id', $customer_id);
			$this->session->set_userdata('order_id', $order_id);

			//exit($response);
			
          if ($response['status']) {
           
            $this->cart->destroy();

            $this->session->set_flashdata('msg_success', $response['message']);
			
			$this->session->unset_userdata('customer_id');
			$this->session->unset_userdata('order_id');
			
            redirect('cart/order_success');
			
          } else {
		  
            $this->session->set_flashdata('msg_error', $response['error'][0]);
			
            $data['order_error'] = $response['error'][0];

          }
      }

      $data['template'] = "frontend/checkout";
	  
      $this->load->view('templates/frontend/layout', $data);
	  
    }

    public function order_success()
	{
		if (($this->session->userdata('pickup_info')) && ($this->session->userdata('active_location_id'))) {
			
			$data['pickup_info'] = $this->session->userdata('pickup_info');
			$data['location'] = $this->products_model->get_row('locations', array('id' => $this->session->userdata('active_location_id')));
			$data['template'] = "frontend/order_success";
			
			$this->session->unset_userdata('pickup_info');
			$this->session->unset_userdata('active_location_id');
		  
			$this->load->view('templates/frontend/layout', $data);
			
		} else {
		
			redirect('menu');
		
		}
    }

}