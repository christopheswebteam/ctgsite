<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Media extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
         $this->load->model('media_model');

    }

    public function index(){
    	$this->photos();
    }

    public function photos(){
        $data['location_slug'] = 'Photos';

        $data['template'] = "frontend/media/photos";
        $this->load->view('templates/frontend/layout', $data);
    }

     public function videos(){
        $data['location_slug'] = 'Videos';

        $data['template'] = "frontend/media/videos";
        $this->load->view('templates/frontend/layout', $data);
    }

}