<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Locations extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
         $this->load->model('locations_model');

    }

    public function index($location_slug = FALSE)
    {
    	$data['location_slug'] = $location_slug;


        $data['front_locations'] = $this->locations_model->front_locations();
        $data['retail']   = $this->locations_model->tab_locations('Retail');
        $data['cooler']   = $this->locations_model->tab_locations('Cooler');
        $data['delivery'] = $this->locations_model->tab_locations('delivery-area');

        $data['template'] = "frontend/locations";
        $this->load->view('templates/frontend/layout', $data);
    }

}