<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        clear_cache();
        $this->load->model('user_model');
    }

    private function _check_login(){
        if(superadmin_logged_in()===FALSE)
            redirect('superadmin/login');
    }

    public function index($offset=0)
    {
        $this->_check_login(); //check login authentication
        $per_page = 10;
        $data['offset'] = $offset;
        $data['users'] = $this->user_model->users($offset, $per_page);
        $config = backend_pagination();
        $config['base_url'] = base_url().'backend/users/';
        $config['total_rows'] = $this->user_model->users(0, 0);
        $config['per_page'] = $per_page;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();               
        $data['template']='backend/users/users';
        $this->load->view('templates/backend/layout', $data);
    }

    public function add()
    {
        $this->_check_login(); //check login authentication
        $this->form_validation->set_rules('first_name','First Name','required');
        $this->form_validation->set_rules('last_name','Last Name','required');
        $this->form_validation->set_rules('email','Email','required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password','Password','required|matches[confpassword]');
        $this->form_validation->set_rules('confpassword','Confirm Password','required');
        if($this->form_validation->run() == TRUE){
            $user_data = array(
                'user_role' => 0,
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'email' => $this->input->post('email'),
                'password' => sha1(trim($this->input->post('password'))),
                'last_ip' => $this->input->ip_address(),
                'created' => date('Y-m-d h:i:s')
            );
            if ($this->user_model->insert('users', $user_data)){
                $this->send_mail($this->input->post('first_name'),$this->input->post('last_name'),$this->input->post('email'),$this->input->post('password'));
                $this->session->set_flashdata('msg_success', 'New User added successfully.');
                redirect('backend/users/');
            } else {
                $this->session->set_flashdata('msg_error', 'New User add failed, Please try again.');
                redirect('backend/users/');
            }
        }

        $data['template'] = 'backend/users/user_add';
        $this->load->view('templates/backend/layout', $data);
    }

    public function send_mail($first_name='',$last_name='',$email='',$password='')
    {
        $this->_check_login();
        echo $first_name;
        echo $last_name;
        echo $email;
        echo $password;


        $config = Array(                      
                       'mailtype' => 'html',
                       'charset' => 'iso-8859-1',
                       'wordwrap' => TRUE
                    );

        $message = '<!DOCTYPE html>
                    <html lang="en">
                    <body>
                    <div>
                        <h1>HELLO Mr/Miss'.$first_name.' '.$last_name.'</h1>
                        <div id="body">
                        <p>welcome to christophestogo.com</p>
                            <p>Your account has been activated and  click to below link or copy paste the url in browser <br>
                                below  are the   credentials    URL '.base_url().'superadmin/login <br> you can   </p><br>
                            <p>Email Id:'.$email.'</p><br>
                            <p>Password:'.$password.'</p><br><br>

                            <p>Thanks</p>
                            <p>Owner of Christophestogo</p>

                        </div>
                    </div>
                    </body>
                    </html>';
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from(NO_REPLY_EMAIL); // change it to yours
        $this->email->to($email);// change it to yours
        $this->email->subject('Registered your account');
        $this->email->message($message);
        if($this->email->send())
        {          
          return TRUE;
        }else{
         echo   show_error($this->email->print_debugger()); die();
        }
    }

    public function edit($user_id = '',$offset = "",$page = 'users')
    {
        $this->_check_login(); //check login authentication
        if (empty($user_id)) redirect('backend/users/');

        $this->form_validation->set_rules('first_name', 'Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == TRUE) {
            $user_data = array(
                'user_role' => $this->input->post('user_role'),
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),      
                'email' => $this->input->post('email'),
            );
            if ($this->user_model->update('users', $user_data, array('id' => $user_id))) {
                $this->session->set_flashdata('msg_success', 'User updated successfully.');
                redirect('backend/users/' . $page . '/' . $offset);
            } else {
                $this->session->set_flashdata('msg_error', 'Update failed, Please try again.');
                redirect('backend/users/' . $page . '/' . $offset);
            }
        }

        $data['user'] = $this->user_model->get_row('users', array('id' => $user_id));

        $data['template'] = 'backend/users/user_edit';
        $this->load->view('templates/backend/layout', $data);
    }

    public function change_status($id="", $status = "", $offset = "", $page = 'users')
    {
        $this->_check_login(); //check login authentication

        if ($status == 0) $userstatus = 1;
        if ($status == 1) $userstatus = 3;
        if ($status == 3) $userstatus = 1;

        $data = array('status' => $userstatus);
        if ($this->user_model->update('users', $data, array('id' => $id))) if ($page == 'clubs') {
                $this->user_model->update('club', $data, array('user_id' => $id));
            }
        $this->session->set_flashdata('msg_success', 'Status changed successfully.');
        redirect('backend/users/' . $page . '/' . $offset);
    }

    public function user_delete($user_id = '', $offset = "", $page = 'users')
    {
        $this->_check_login(); //check login authentication
        if (empty($user_id)) redirect('backend/users/');
        if ($this->user_model->delete('users', array('id' => $user_id))) {
            $this->session->set_flashdata('msg_success', 'User deleted successfully.');
            redirect('backend/users/'.$page.'/'.$offset);
        } else {
            $this->session->set_flashdata('msg_error', 'User delete failed, Please try again.');
            redirect('backend/users/');
        }
    }

}
