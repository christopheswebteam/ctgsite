<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('orders_model');
    }

    public function index()
    {
        $data['orders'] = $this->orders_model->get_orders();
		
		$data['template'] = "backend/orders/index";
		
        $this->load->view('templates/backend/layout', $data);
    }

}
