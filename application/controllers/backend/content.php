<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Content extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('content_model');
    }

    public function index()
    {
         $this->pages();
    }

     private function _check_login(){
		if(superadmin_logged_in()===FALSE)
			redirect('superadmin/login');
	}

	public function pages($offset = 0)
    {
        $this->_check_login(); //check login authentication

        $per_page = 10;
        $data['offset'] = $offset;
        $data['pages'] = $this->content_model->pages($offset, $per_page, 'page');
        $config = backend_pagination();
        $config['base_url'] = base_url() . 'backend/content/pages/';
        $config['total_rows'] = $this->content_model->pages(0, 0, 'page');
        $config['per_page'] = $per_page;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['template'] = 'superadmin/pages';
        $this->load->view('templates/superadmin_template', $data);
    }

    public function page_add()
    {
        $this->_check_login(); //check login authentiatio
        $this->form_validation->set_rules('post_title', 'Page Title', 'required');
        $this->form_validation->set_rules('post_content', 'Page Content', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            $page_data = array(
                'post_title' => $this->input->post('post_title'),
                'post_slug' => url_title($this->input->post('post_title'), '-', TRUE),
                'post_content' => $this->input->post('post_content'),
                'post_type' => 'page',
                'status' => $this->input->post('status'),
                'post_created' => date('Y-m-d h:i:s'));
            if ($this->content_model->insert('posts', $page_data)) {
                $this->session->set_flashdata('msg_success', 'Page added successfully.');
                redirect('backend/content/pages');
            } else {
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/content/pages');
            }
        }

        $data['template'] = 'superadmin/page_add';
        $this->load->view('templates/superadmin_template', $data);
    }

    public function page_edit($page_id = '', $offset = '')
    {
        $this->_check_login(); //check login authentication

        if (empty($page_id)) redirect('superadmin/pages');

        $this->form_validation->set_rules('post_title', 'Page Title', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            $page_data = array(
                'post_title' => $this->input->post('post_title'),
                'post_slug' => url_title($this->input->post('post_title'), '-', TRUE),
                'post_content' => $this->input->post('post_content'),
                'post_type' => 'page',
                'status' => $this->input->post('status'),
                'post_updated' => date('Y-m-d h:i:s'));
            if($this->content_model->update('posts', $page_data, array('id' => $page_id))) {
                $this->session->set_flashdata('msg_success', 'Page updated successfully.');
                redirect('backend/content/pages/'.$offset);
            }else{
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/content/pages/'.$offset);
            }
        }

        $data['page'] = $this->content_model->get_row('posts', array('id' => $page_id, 'post_type' => 'page'));
        $data['template'] = 'superadmin/page_edit';
        $this->load->view('templates/superadmin_template', $data);
    }

    public function page_delete($page_id = '', $offset = '')
    {
        $this->_check_login(); //check login authentication

        if (empty($page_id)) redirect('backend/content/pages');

        if ($this->content_model->delete('posts', array('id' => $page_id, 'post_type' => 'page'))) {
            $this->session->set_flashdata('msg_success', 'Page deleted successfully.');
            redirect('backend/content/pages/' . $offset);
        } else {
            $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
            redirect('backend/content/pages/' . $offset);
        }
    }

    public function posts($offset = 0)
    {
        $this->_check_login(); //check login authentication
        $per_page = 10;
        $data['offset'] = $offset;
        $data['pages'] = $this->content_model->pages($offset, $per_page, 'post');
        $config = backend_pagination();
        $config['base_url'] = base_url().'backend/content/posts/';
        $config['total_rows'] = $this->content_model->pages(0, 0, 'post');
        $config['per_page'] = $per_page;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['template'] = 'superadmin/posts';
        $this->load->view('templates/superadmin_template', $data);
    }

    public function post_add()
    {
        $this->_check_login(); //check login authentication
        $this->form_validation->set_rules('post_title', 'Post Title', 'required');
        $this->form_validation->set_rules('post_content', 'Post Content', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            $page_data = array(
                        'post_title' => $this->input->post('post_title'),
                        'post_slug' => url_title($this->input->post('post_title'),'-',TRUE),
                        'post_category' => $this->input->post('post_category'),
                        'post_content' => $this->input->post('post_content'),
                        'post_type' => 'post',
                        'status' => $this->input->post('status'),
                        'post_created' => date('Y-m-d h:i:s')
                    );
            if ($this->content_model->insert('posts', $page_data)) {
                $this->session->set_flashdata('msg_success', 'Post added successfully.');
                redirect('backend/content/posts');
            } else {
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/content/posts');
            }
        }
        $data['post'] = $this->content_model->get_result('posts', array('post_type' => 'post'));
        $data['template'] = 'superadmin/post_add';
        $this->load->view('templates/superadmin_template', $data);
    }

    public function post_edit($page_id = '', $offset = '')
    {
        $this->_check_login(); //check login authentication

        if (empty($page_id)) redirect('backend/content/posts');

        $this->form_validation->set_rules('post_title', 'Post Title', 'required');
        $this->form_validation->set_rules('post_content', 'Post Content', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            $page_data = array(
                'post_title' => $this->input->post('post_title'),
                'post_slug' => url_title($this->input->post('post_title'), '-', TRUE),
                'post_content' => $this->input->post('post_content'),
                'post_category' => $this->input->post('post_category'),
                'post_type' => 'post',
                'status' => $this->input->post('status'),
                'post_updated' => date('Y-m-d h:i:s'));
            if ($this->content_model->update('posts', $page_data, array('id' => $page_id))) {
                $this->session->set_flashdata('msg_success', 'Post updated successfully.');
                redirect('backend/content/posts/' . $offset);
            } else {
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/content/posts/' . $offset);
            }
        }

        $data['page'] = $this->content_model->get_row('posts', array('id' => $page_id, 'post_type' => 'post'));
        $data['template'] = 'superadmin/post_edit';
        $this->load->view('templates/superadmin_template', $data);
    }

    public function post_delete($page_id = '', $offset = '')
    {
        $this->_check_login(); //check login authentication

        if (empty($page_id)) redirect('superadmin/posts');

        if ($this->content_model->delete('posts', array('id' => $page_id, 'post_type' => 'post'))) {
            $this->session->set_flashdata('msg_success', 'Post deleted successfully.');
            redirect('backend/content/posts/' . $offset);
        } else {
            $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
            redirect('backend/content/posts/' . $offset);
        }
    }

    // -------------------------- NEWS  -------------------------------------------------------//

    public function blogs($offset = 0)
    {
        $this->_check_login(); //check login authentication
        $per_page = 20;
        $data['offset'] = $offset;
        $data['blogs'] = $this->content_model->blogs($offset, $per_page);
        $config = backend_pagination();
        $config['base_url'] = base_url() . 'backend/content/blogs/';
        $config['total_rows'] = $this->content_model->blogs(0, 0);
        $config['per_page'] = $per_page;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['template'] = 'superadmin/blogs';
        $this->load->view('templates/superadmin_template', $data);
    }

    public function blog_add()
    {
        $this->_check_login(); //check login authentication
        $this->form_validation->set_rules('blog_title', 'Title', 'required');
        $this->form_validation->set_rules('blog_content', 'Content', 'required');
        $this->form_validation->set_rules('tags[]','blog tag','required');
        //if(!empty($_FILES['blog_features_image']['name'])):
        $this->form_validation->set_rules('blog_features_image', '', 'callback_blog_features_image_check');
        //endif;

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            if ($this->session->userdata('blog_features_image')):
                $blog_features_image = $this->session->userdata('blog_features_image');
                $blog_data['file_path'] = $blog_features_image['image'];
                $blog_data['thumb_image'] = $blog_features_image['thumb_image'];
            endif;
            $blog_data['blog_title'] = $this->input->Post('blog_title');
            $blog_data['blog_slug'] = url_title($this->input->Post('blog_title'),'-',TRUE);
            $blog_data['blog_content'] = $this->input->Post('blog_content');
            $blog_data['blog_status'] = $this->input->Post('blog_status');
            $blog_data['created'] = date('Y-m-d h:i:s');
            //$blog_data['tags']	= 	$this->input->Post('tag');
            if ($blog_id=$this->content_model->insert('blogs', $blog_data)){           
                if(!empty($_POST['tags'])){
                    $count = count($_POST['tags']);
                    for($i=0; $i < $count; $i++){
                        $tag_data = array(
                                        'type'  =>'blog',
                                        'tags_id'=>$_POST['tags'][$i],
                                        'key'=>$blog_id
                                    ); 
                        $this->content_model->insert('tags_association', $tag_data);
                    }
                }
                $this->session->set_flashdata('msg_success', 'Blog Created successfully.');
                if ($this->session->userdata('blog_features_image')):
                    $this->session->unset_userdata('blog_features_image');
                endif;
                redirect('backend/content/blogs');
            }else {
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/content/blogs');
            }
        }
        $data['tags'] = $this->content_model->get_result('tags');
        $data['template'] = 'superadmin/blog_add';
        $this->load->view('templates/superadmin_template', $data);
    }

    public function blog_edit($page_id = '', $offset = '')
    {
        $this->_check_login(); //check login authentication
        if (empty($page_id)) redirect('superadmin/blogs/'.$offset);
        $data['blog'] = $this->content_model->get_row('blogs', array('id' => $page_id));
        $data['tags_association'] = $this->content_model->get_blog_tag($page_id);
        $this->form_validation->set_rules('blog_title', 'Title', 'required');
        $this->form_validation->set_rules('blog_content', 'Content', 'required');
        $this->form_validation->set_rules('tags[]','blog tag','required');
        if (!empty($_FILES['blog_features_image']['name'])):
            $this->form_validation->set_rules('blog_features_image', '', 'callback_blog_features_image_check');
        endif;
        $this->form_validation->set_error_delimiters('<div class="error">','</div>');
        if ($this->form_validation->run() == TRUE) {

            if ($this->session->userdata('blog_features_image')):
                $blog_features_image = $this->session->userdata('blog_features_image');
                $blog_data['file_path'] = $blog_features_image['image'];
                $blog_data['thumb_image'] = $blog_features_image['thumb_image'];
            endif;
            $blog_data['blog_title'] = $this->input->Post('blog_title');
            $blog_data['blog_slug'] = url_title($this->input->Post('blog_title'),'-',TRUE);
            $blog_data['blog_content'] = $this->input->Post('blog_content');
            $blog_data['blog_status'] = $this->input->Post('blog_status');
            $blog_data['updated'] = date('Y-m-d h:i:s');
            //$blog_data['tags']	= 	$this->input->Post('tag');
            if ($this->content_model->update('blogs',$blog_data,array('id'=>$page_id))){
                if ($this->session->userdata('blog_features_image')):
                    $this->session->unset_userdata('blog_features_image');
                    @unlink($data['blog']->image);
                    @unlink($data['blog']->thumb_image);
                endif;

                if(!empty($_POST['tags'])){
                    $this->content_model->delete('tags_association',array('key'=>$page_id,'type'=>'blog'));
                    $count = count($_POST['tags']);
                    for($i=0; $i < $count; $i++){
                        $tag_data = array(
                                        'type'  =>'blog',
                                        'tags_id'=>$_POST['tags'][$i],
                                        'key'=>$page_id
                                    ); 
                        $this->content_model->insert('tags_association', $tag_data);
                    }
                }

                $this->session->set_flashdata('msg_success', 'Blog updated successfully.');
                redirect('backend/content/blogs/' . $offset);
            }else {
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/content/blogs/' . $offset);
            }
        }
        $data['tags'] = $this->content_model->get_result('tags');
        $data['template'] = 'superadmin/blog_edit';
        $this->load->view('templates/superadmin_template', $data);
    }

    public function blog_delete($page_id = '', $offset = '')
    {
        $this->_check_login(); //check login authentication

        if (empty($page_id)) redirect('superadmin/blogs/' . $offset);

        $data['blog'] = $this->content_model->checkimg('blogs', $page_id);

        @unlink($data['blog']->image);
        @unlink($data['blog']->thumb_image);

        if ($this->content_model->delete('blogs', array('id' => $page_id))) {
            $this->content_model->delete('tags_association',array('key'=>$page_id,'type'=>'blog'));
            $this->content_model->delete('blog_comments', array('comment_blog_id' => $page_id));

            $this->session->set_flashdata('msg_success', 'Blog deleted successfully.');
            redirect('backend/content/blogs/' . $offset);
        } else {
            $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
            redirect('backend/content/blogs/' . $offset);
        }
    }

    public function blog_features_image_check($str)
    {
        if ($this->session->userdata('blog_features_image')) {
            return TRUE;
        } else {
            $param = array(
                'file_name' => 'blog_features_image',
                'upload_path' => './assets/uploads/blog/',
                'allowed_types' => 'gif|jpg|png|jpeg',
                'image_resize' => TRUE,
                'source_image' => './assets/uploads/blog/',
                'new_image' => './assets/uploads/blog/thumb/',
                'encrypt_name' => TRUE,);

            $upload_file = upload_file($param, array('thumb_width' => 120, 'thumb_height' => 70));
            if ($upload_file['STATUS']) {
                $this->session->set_userdata('blog_features_image', array('image' => $param['upload_path'] . $upload_file['UPLOAD_DATA']['file_name'], 'thumb_image' => $param['new_image'] . $upload_file['UPLOAD_DATA']['file_name']));
                return TRUE;
            } else {
                $this->form_validation->set_message('blog_features_image_check', $upload_file['FILE_ERROR']);
                return FALSE;
            }
        }
    }

    public function changeuserstatus_t($id = "", $status = "", $offset = "", $table_name = "")
    {
        $this->_check_login(); //check login authentication
        if ($status == 0) $userstatus = 1;
        if ($status == 1) $userstatus = 0;
        $data = array('status' => $userstatus);
        if ($this->content_model->update($table_name,$data,array('id'=>$id))) $this->session->set_flashdata('msg_success','Status Updated successfully.');
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function faqs($offset = 0)
    {
        $this->_check_login(); //check login authentication

        $per_page = 10;
        $data['faqs'] = $this->content_model->faqs($offset, $per_page);
        $data['offset'] = $offset;
        $config = backend_pagination();
        $config['base_url'] = base_url() . 'backend/content/faqs/';
        $config['total_rows'] = $this->content_model->faqs(0, 0);
        $config['per_page'] = $per_page;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['template'] = 'superadmin/faqs';
        $this->load->view('templates/superadmin_template', $data);
    }

    public function faq_add()
    {
        $this->_check_login(); //check login authentication
        $this->form_validation->set_rules('question', 'Question', 'required');
        $this->form_validation->set_rules('answer', 'Answer', 'required');
        $this->form_validation->set_rules('tag_id', 'Tag', 'required');        
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            $category_data = array(
                'question' => $this->input->post('question'),
                'answer' => $this->input->post('answer'),
                'status' => $this->input->post('status'),
                'tag_id'=>$this->input->post('tag_id'),
                'created' => date('Y-m-d h:i:s')
                );

            if($this->content_model->insert('faqs', $category_data)) {

                $this->session->set_flashdata('msg_success', 'Faq added successfully.');
                redirect('backend/content/faqs');
            } else {
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/content/faq_add');
            }
        }        
        $data['tags'] = $this->content_model->get_result('tags');
        $data['template'] = 'superadmin/faq_add';
        $this->load->view('templates/superadmin_template', $data);
    }

    public function faq_edit($id = '')
    {
        $this->_check_login(); //check login authentication
        if (empty($id)) redirect(base_url() . 'backend/content/faqs');
        $this->form_validation->set_rules('question', 'Question', 'required');
        $this->form_validation->set_rules('answer', 'Answer', 'required');
        $this->form_validation->set_rules('tag_id','Tag', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            $category_data = array(
                'question' => $this->input->post('question'),
                'answer' => $this->input->post('answer'),
                'status' => $this->input->post('status'),
                'tag_id'=>$this->input->post('tag_id')
            );
            if ($this->content_model->update('faqs', $category_data, array('id' => $id))) {
                $this->session->set_flashdata('msg_success', 'Faq updated successfully.');
                redirect('backend/content/faqs');
            } else {
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/content/faq_edit');
            }
        }
        $data['faq'] = $this->content_model->get_row('faqs', array('id' => $id));
        $data['tags'] = $this->content_model->get_result('tags');
        $data['template'] = 'superadmin/faq_edit';
        $this->load->view('templates/superadmin_template', $data);
    }

    public function faq_delete($id = '')
    {
        $this->_check_login(); //check login authentication
        if (empty($id)) redirect(base_url() . 'backend/content/faqs');
        if ($this->content_model->delete('faqs', array('id' => $id))) {
            $this->session->set_flashdata('msg_success', 'Faq deleted successfully.');
            redirect('backend/content/faqs');
        } else {
            $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
            redirect('backend/content/faqs');
        }
    }

    public function blog_comments($offset = 0)
    {
        $this->_check_login(); //check login authentication
        $per_page = 20;
        $data['offset'] = $offset;
        $data['blog_comments'] = $this->content_model->blog_comments($offset, $per_page);
        $config = backend_pagination();
        $config['base_url'] = base_url().'backend/content/blog_comments/';
        $config['total_rows'] = $this->content_model->blog_comments(0, 0);
        $config['per_page'] = $per_page;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['template'] = 'superadmin/blog_comments';
        $this->load->view('templates/superadmin_template', $data);
    }

    public function blog_comment_view($com_id='')
    {
        if(empty($com_id)) redirect(base_url() . 'backend/content/blog_comments');
        $data['blog_comment'] = $this->content_model->get_row('blog_comments',array('id'=>$com_id));

        $data['template'] = 'superadmin/blog_comment_view';
        $this->load->view('templates/superadmin_template', $data);
    }

    public function blog_comment_delete($com_id = '')
    {
        $this->_check_login(); //check login authentication
        if (empty($com_id)) redirect(base_url() . 'backend/content/blog_comments');
        if ($this->content_model->delete('blog_comments',array('id'=>$com_id))) {
            $this->session->set_flashdata('msg_success', 'blog Comment deleted successfully.');
            redirect('backend/content/blog_comments');
        } else {
            $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
            redirect('backend/content/blog_comments');
        }
    }

    public function blog_comment_status($com_id="",$status="",$offset="")
    {
        $this->_check_login(); //check login authentication
        if(empty($com_id)) redirect('backend/content/blog_comments');
        if($status==0){
            $com_status=1;
        }
        if($status==1){
            $com_status=2;
        }
        if($status==2){
                 $com_status=1;
        }
        $data = array('comment_approved'=>$com_status);
        if($this->content_model->update('blog_comments',$data,array('id'=>$com_id)))
        {
            $this->session->set_flashdata('msg_success','Blog Comment updated successfully.');
            redirect('backend/content/blog_comments/'.$offset);
        }

    }


}
