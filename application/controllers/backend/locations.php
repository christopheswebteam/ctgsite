<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Locations extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('locations_model');
    }

    private function _check_login()
    {
        if (superadmin_logged_in() === FALSE)
            redirect('superadmin/login');
    }

    public function index($offset = 0)
    {
        $this->_check_login(); //check login authentication
        $per_page = 10;
        $data['offset'] = $offset;
        $data['locations'] = $this->locations_model->locations($offset, $per_page);
        $config = backend_pagination();
        $config['base_url'] = base_url() . 'backend/locations/index/';
        $config['total_rows'] = $this->locations_model->locations(0, 0);
        $config['per_page'] = $per_page;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['template'] = "backend/locations/index";
        $this->load->view('templates/backend/layout', $data);
    }

    function check_latitude($latitude)
    {
        if ($latitude == '') {
            $this->form_validation->set_message('check_latitude', 'You must Generate Map First');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function add()
    {
        $this->_check_login(); //check login authentication
        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('street_address_1', 'Street Address 1', 'required');
        $this->form_validation->set_rules('street_address_2', 'Street Address 2', '');
        $this->form_validation->set_rules('city', 'City', 'required');
        $this->form_validation->set_rules('state', 'State', 'required');
        $this->form_validation->set_rules('zip_code', 'Zip code', 'required');
        $this->form_validation->set_rules('latitude', 'Latitude', 'trim|callback_check_latitude');
        $this->form_validation->set_rules('longitude', 'Longitude', 'required');
        $this->form_validation->set_rules('pos_locations_id', 'POS ID', 'required|numeric');
        $this->form_validation->set_rules('phone_number', 'Phone number', 'required');
        $this->form_validation->set_rules('opening_time', 'Opening time', 'required');
        $this->form_validation->set_rules('closing_time', 'Closing time', 'required');
        $this->form_validation->set_rules('tax_rate', 'Tax rate', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_message('latitude', 'You must Generate Map');
        if ($this->form_validation->run() == TRUE) {
            $images = $this->session->userdata('location_image');
            if (empty($images)) {
                $this->session->set_flashdata('msg_error', 'Please upload Product image');
                redirect('backend/locations/add');
            }
            $location_data = array(
                'type' => $this->input->post('type'),
                'name' => $this->input->post('name'),
                'location_slug' => url_title($this->input->post('name'), '-', TRUE),
                'street_address_1' => $this->input->post('street_address_1'),
                'street_address_2' => $this->input->post('street_address_2'),
                'city' => $this->input->post('city'),
                'state' => strtoupper($this->input->post('state')),
                'zip_code' => $this->input->post('zip_code'),
                'pos_locations_id' => $this->input->post('pos_locations_id'),
                'phone_number' => $this->input->post('phone_number'),
                'latitude' => $this->input->post('latitude'),
                'longitude' => $this->input->post('longitude'),
                'opening_time' => $this->input->post('opening_time'),
                'closing_time' => $this->input->post('closing_time'),
                'tax_rate' => $this->input->post('tax_rate'),
                'status' => $this->input->post('status')
            );
            if ($location_id = $this->locations_model->insert('locations', $location_data)) {
                $arrayName = array();
                if ($this->session->userdata('location_image')) {
                    $images = $this->session->userdata('location_image');
                    $count = count($images);
                    for ($i = 0; $i < $count; $i++) {

                        $img = explode('/', $images[$i]['image']);
                        if (!empty($_POST['image_' . $i])) {
                            $image_name = $_POST['image_' . $i];
                        } else {
                            $image_name = str_replace('.jpg', '', $img[4]);
                        }
                        if ($_POST['is_feature_' . $i] == 1) {
                            $is_feature = $_POST['is_feature_' . $i];
                        } else {

                            $is_feature = 0;
                        }
                        $arrayName = array(
                            'locations_id' => $location_id,
                            'path' => $images[$i]['image'],
                            'label' => $image_name,
                            'random_background' => $is_feature,
                            'thumbnail_path' => $images[$i]['thumb_image'],
                            'status' => 1
                        );
                        $this->locations_model->insert('locations_photos', $arrayName);
                    }
                    $this->session->unset_userdata('location_image');
                }
                $this->session->set_flashdata('msg_success', 'Location added successfully.');
                redirect('backend/locations');
            } else {
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/locations');
            }
        }
        if ($this->session->userdata('location_image')) {
            $image_url = 'assets/uploads/locations/';
            $images = $this->session->userdata('location_image');
            $count = count($images);
            for ($i = 0; $i < $count; $i++) {
                $new_file = $images[$i]['image'];
                $thumb_file = $images[$i]['thumb_image'];
                @unlink($new_file);
                @unlink($thumb_file);
            }
            $this->session->unset_userdata('location_image');
            $this->session->set_flashdata('msg_error', 'Please Reupload image files.');
        }
        $data['template'] = "backend/locations/add";
        $this->load->view('templates/backend/layout', $data);
    }

    function upload()
    {
        ini_set('memory_limit', '256M');
        if (!empty($_FILES)) {

            $arrayName = array();
            if (!empty($_FILES['file']['name'])) {
                $param = array(
                    'image_library' => 'gd2',
                    'file_name' => 'file',
                    'upload_path' => './assets/uploads/locations/',
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    'image_resize' => TRUE,
                    'source_image' => './assets/uploads/locations/',
                    'new_image' => './assets/uploads/locations/thumbnail/',
                    'resize_width' => 350,
                    'resize_height' => 300,
                    'encrypt_name' => TRUE
                );
                $upload_file = upload_file($param);
                if ($upload_file['STATUS']) {
                    $uplaod_file = array(
                        'image' => $param['upload_path'] . $upload_file['UPLOAD_DATA']['file_name'],
                        'thumb_image' => $param['new_image'] . $upload_file['UPLOAD_DATA']['file_name']
                    );
                    if ($this->session->userdata('location_image')) {
                        $images = $this->session->userdata('location_image');
                        $count = count($images);
                        for ($i = 0; $i < $count; $i++) {
                            $j = $count - 1;
                            $arrayName[$i] = array(
                                'image' => $images[$i]['image'],
                                'thumb_image' => $images[$i]['thumb_image']
                            );
                            if ($i == $j) {
                                $h = $i + 1;
                                $arrayName[$h] = $uplaod_file;
                            }
                        }
                        $this->session->set_userdata('location_image', $arrayName);
                    } else {
                        $arrayName[0] = $uplaod_file;
                        $this->session->set_userdata('location_image', $arrayName);
                    }
                    die('{"jsonrpc" : "2.0", "result" : "OK", "id" : "1"}');
                } else {
                    die('{"jsonrpc" : "2.0", "result" : "' . $upload_file['FILE_ERROR'] . '", "id" : "1"}');
                }
            }
        }
    }

    public function remove_location_image()
    {
        $arrayName = array();
        $image_url = 'assets/uploads/locations/';
        if ($_POST) {
            if ($_POST['file'] != '') {
                if ($this->session->userdata('location_image')) {
                    $images = $this->session->userdata('location_image');
                    $count = count($images);
                    for ($i = 0; $i < $count; $i++) {
                        if ($images[$i]['image'] != $_POST['file']) {
                            $arrayName[$i] = array(
                                'image' => $images[$i]['image_name'],
                                'thumb_image' => $images[$i]['thumb_image']
                            );
                        }
                    }
                    $this->session->set_userdata('location_image', $arrayName);
                }
                $new_file = $image_url . $_POST['file'];
                $thumb_file = $image_url . 'thumbnail/' . $_POST['file'];
                @unlink($new_file);
                @unlink($thumb_file);
                echo'success';
            }
        }
    }

    public function image_remove()
    {
        $resop = '';
        if ($_POST) {
            $id = $_POST['image_id'];
            if ($this->session->userdata('location_image')) {
                $arrayName = '';
                $images = $this->session->userdata('location_image');
                $count = count($images);
                $this->session->unset_userdata('location_image');
                for ($i = 0; $i < $count; $i++) {

                    if ($id == $i) {
                        $image_url1 = 'assets/uploads/products/';

                        $new_file = $images[$i]['image'];
                        $thumb_file = $images[$i]['thumb_image'];
                        @unlink($new_file);
                        @unlink($thumb_file);
                        $id = 'flag';
                    } else {
                        if ($id == 'flag') {
                            $j = $i - 1;
                            $arrayName[$j] = array(
                                'image' => $images[$i]['image'],
                                'thumb_image' => $images[$i]['thumb_image']
                            );
                        } else {
                            $arrayName[$i] = array(
                                'image' => $images[$i]['image'],
                                'thumb_image' => $images[$i]['thumb_image']
                            );
                        }
                    }
                }
                if (!empty($arrayName)) {
                    $this->session->set_userdata('location_image', $arrayName);
                }
            }

            $location_image = $this->session->userdata('location_image');

            if (!empty($location_image)) {
                $images = $this->session->userdata('location_image');
                $count = count($images);
                for ($i = 0; $i < $count; $i++) {
                    $resop.= '<tr>';
                    $resop.='<td>';
                    $resop.='<a href="' . base_url($images[$i]['thumb_image']) . '" class="fancybox-button" data-rel="fancybox-button"><img class="img-responsive" src="' . base_url($images[$i]['thumb_image']) . '" alt=""></a>';
                    $resop.='</td>';
                    $resop.='<td>';
                    $img = explode('/', $images[$i]['image']);
                    $image_name = str_replace('.jpg', '', $img[4]);
                    $resop.='<div " ><input class="form-control type="text" name="image_' . $i . '" value="' . $image_name . '" placeholder="">';
                    $resop.='</div>';
                    $resop.='</td>';
                    $resop.='<td> <div style="margin-left:20px;"  class="radio-list">';
                    $resop.='<label class="radio-inline">';
                    $resop.='<input type="radio" name="is_feature_' . $i . '" id="is_feature" value="1" > YES </label>';
                    $resop.='<label class="radio-inline">';
                    $resop.='<input type="radio" name="is_feature_' . $i . '" id="is_feature" value="0" checked> NO</label>';
                    $resop.='</div>';
                    $resop.='</td>';
                    $resop.='<td>';
                    $resop.='<a href="javascript:void(0)" onclick="remove_location_image(' . $i . ');"  class="btn default btn-sm">';
                    $resop.='<i class="fa fa-times"></i> Remove </a>';
                    $resop.='</td>';
                    $resop.='</tr>';
                }
                echo $resop;
            } else {
                echo $resop = '<tr><td style="text-align:center;" colspan="3">NO data Found</td></tr>';
            }
        }
    }

    public function all_file()
    {
        $resop = '';
        if ($_POST) {
            $images = $this->session->userdata('location_image');
            echo $count = count($images);
            for ($i = 0; $i < $count; $i++) {
                $resop.= '<tr>';
                $resop.='<td>';
                $resop.='<a href="' . base_url($images[$i]['thumb_image']) . '" class="fancybox-button" data-rel="fancybox-button"><img class="img-responsive" src="' . base_url($images[$i]['thumb_image']) . '" alt=""></a>';
                $resop.='</td>';
                $resop.='<td>';
                $img = explode('/', $images[$i]['image']);
                $image_name = str_replace('.jpg', '', $img[4]);
                $resop.='<div " ><input class="form-control type="text" name="image_' . $i . '" value="' . $image_name . '" placeholder="">';
                $resop.='</div>';
                $resop.='</td>';
                $resop.='<td> <div style="margin-left:20px;"  class="radio-list">';
                $resop.='<label class="radio-inline">';
                $resop.='<input type="radio" name="is_feature_' . $i . '" id="is_feature" value="1" > YES </label>';
                $resop.='<label class="radio-inline">';
                $resop.='<input type="radio" name="is_feature_' . $i . '" id="is_feature" value="0" checked> NO</label>';
                $resop.='</div>';
                $resop.='</td>';
                $resop.='<td>';
                $resop.='<a href="javascript:void(0)" onclick="remove_location_image(' . $i . ');"  class="btn default btn-sm">';
                $resop.='<i class="fa fa-times"></i> Remove </a>';
                $resop.='</td>';
                $resop.='</tr>';
            }
            echo $resop;
        }
    }

    public function edit($location_id = '')
    {
        $this->_check_login(); //check login authentication
        if (empty($location_id))
            redirect('backend/locations');
        $data['locations_photos'] = $this->locations_model->get_result('locations_photos', array('locations_id' => $location_id));

        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('street_address_1', 'Street Address 1', 'required');
        $this->form_validation->set_rules('street_address_2', 'Street Address 2', '');
        $this->form_validation->set_rules('city', 'City', 'required');
        $this->form_validation->set_rules('state', 'State', 'required');
        $this->form_validation->set_rules('zip_code', 'Zip code', 'required');
        $this->form_validation->set_rules('latitude', 'Latitude', 'trim|callback_check_latitude');
        $this->form_validation->set_rules('longitude', 'Longitude', 'required');
        $this->form_validation->set_rules('pos_locations_id', 'POS ID', 'required|numeric');
        $this->form_validation->set_rules('phone_number', 'Phone number', 'required');
        $this->form_validation->set_rules('opening_time', 'Opening time', 'required');
        $this->form_validation->set_rules('closing_time', 'Closing time', 'required');
        $this->form_validation->set_rules('tax_rate', 'Tax rate', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {

            $location_data = array(
                'type' => $this->input->post('type'),
                'name' => $this->input->post('name'),
                'location_slug' => url_title($this->input->post('name'), '-', TRUE),
                'street_address_1' => $this->input->post('street_address_1'),
                'street_address_2' => $this->input->post('street_address_2'),
                'city' => $this->input->post('city'),
                'state' => strtoupper($this->input->post('state')),
                'zip_code' => $this->input->post('zip_code'),
                'phone_number' => $this->input->post('phone_number'),
                'pos_locations_id' => $this->input->post('pos_locations_id'),
                'latitude' => $this->input->post('latitude'),
                'longitude' => $this->input->post('longitude'),
                'opening_time' => $this->input->post('opening_time'),
                'closing_time' => $this->input->post('closing_time'),
                'tax_rate' => $this->input->post('tax_rate'),
                'status' => $this->input->post('status')
            );
            if ($this->locations_model->update('locations', $location_data, array('id' => $location_id))) {


                $arrayName = array();
                if ($this->session->userdata('location_image')) {
                    if (!empty($data['locations_photos'])):
                        foreach ($data['locations_photos'] as $value) {
                            $new_file = $value->path;
                            $thumb_file = $value->thumbnail_path;
                            @unlink($new_file);
                            @unlink($thumb_file);
                        }
                    endif;
                    $this->locations_model->delete('locations_photos', array('locations_id' => $location_id));
                    $images = $this->session->userdata('location_image');
                    $count = count($images);
                    for ($i = 0; $i < $count; $i++) {

                        $img = explode('/', $images[$i]['image']);
                        if (!empty($_POST['image_' . $i])) {
                            $image_name = $_POST['image_' . $i];
                        } else {
                            $image_name = str_replace('.jpg', '', $img[4]);
                        }
                        if ($_POST['is_feature_' . $i] == 1) {
                            $is_feature = $_POST['is_feature_' . $i];
                        } else {
                            $is_feature = 0;
                        }

                        $arrayName = array(
                            'locations_id' => $location_id,
                            'path' => $images[$i]['image'],
                            'label' => $image_name,
                            'random_background' => $is_feature,
                            'thumbnail_path' => $images[$i]['thumb_image'],
                            'status' => 1
                        );
                        $this->locations_model->insert('locations_photos', $arrayName);
                    }
                    $this->session->unset_userdata('location_image');
                } else {
                    if (!empty($data['locations_photos'])) {
                        $i = 1;
                        foreach ($data['locations_photos'] as $row) {
                            if (!empty($_POST['photo_' . $row->id . '_' . $i])) {
                                $array = array('label' => $_POST['photo_' . $row->id . '_' . $i], 'random_background' => $_POST['is_feature_' . $row->id . '_' . $i]);
                                $this->locations_model->update('locations_photos', $array, array('id' => $row->id));
                            }

                            $i++;
                        }
                    }
                }
                $this->session->set_flashdata('msg_success', 'Location updated successfully.');
                redirect('backend/locations');
            } else {
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/locations');
            }
        }
        $data['locations'] = $this->locations_model->get_row('locations', array('id' => $location_id));
        if ($this->session->userdata('location_image')) {
            $image_url = 'assets/uploads/locations/';
            $images = $this->session->userdata('location_image');
            $count = count($images);
            for ($i = 0; $i < $count; $i++) {
                $new_file = $images[$i]['image'];
                $thumb_file = $images[$i]['thumb_image'];
                @unlink($new_file);
                @unlink($thumb_file);
            }
            $this->session->unset_userdata('location_image');
            $this->session->set_flashdata('msg_error', 'Please Reupload image files.');
        }
        $data['template'] = "backend/locations/edit";
        $this->load->view('templates/backend/layout', $data);
    }

    public function delete($location_id = '')
    {
        $this->_check_login(); //check login authentication

        if (empty($location_id))
            redirect('backend/locations');

        if ($locations_photos = $this->locations_model->get_result('locations_photos', array('locations_id' => $location_id))) {

            if (!empty($locations_photos)):
                foreach ($locations_photos as $value) {
                    $new_file = $value->path;
                    $thumb_file = $value->thumbnail_path;
                    @unlink($new_file);
                    @unlink($thumb_file);
                }
            endif;
            $this->locations_model->delete('locations_photos', array('locations_id' => $location_id));
        }

        if ($this->locations_model->delete('locations', array('id' => $location_id))) {
            $this->session->set_flashdata('msg_success', 'Location deleted successfully.');
            redirect('backend/locations');
        } else {
            $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
            redirect('backend/locations');
        }
    }

    private function get_lat_long_from_address($address = '')
    {
        $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);

        if ($response) {
            $json_a = json_decode($response, TRUE); //json decoder
            $lat_long = array();
            $lat_long['latitude'] = $json_a['results'][0]['geometry']['location']['lat']; // get lat for json
            $lat_long['longitude'] = $json_a['results'][0]['geometry']['location']['lng']; // get ing for json

            return $lat_long;
        } else {
            return FALSE;
        }
    }

}
