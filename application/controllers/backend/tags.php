<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tags extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('tags_model');
    }


    private function _check_login(){
		if(superadmin_logged_in()===FALSE)
			redirect('superadmin/login');
	}

    public function index($offset = 0)
    {
        $this->_check_login(); //check login authentication

        $per_page = 10;
        $data['offset'] = $offset;
        $data['tags'] = $this->tags_model->tags($offset, $per_page);
        $config = backend_pagination();
        $config['base_url'] = base_url() . 'backend/tags/index/';
        $config['total_rows'] = $this->tags_model->tags(0, 0);
        $config['per_page'] = $per_page;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['template'] = "backend/tags/index";
        $this->load->view('templates/backend/layout', $data);

    }

    public function add()
    {
        $this->_check_login(); //check login authentication
        $this->form_validation->set_rules('tag', 'Tag', 'required');
        $this->form_validation->set_rules('status', 'Status', 'trim');
        $this->form_validation->set_rules('category_id', 'Category', 'trim');
        $this->form_validation->set_rules('description', 'Description', 'trim');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            $tag_data = array(
                'tag' => $this->input->post('tag'),
                'slug' => url_title($this->input->post('tag'), '-', TRUE),
                'description' => $this->input->post('description'),
                'category' => $this->input->post('category_id'),
                'status' => $this->input->post('status'),
                'created' => date('Y-m-d h:i:s')
            );
            if ($this->tags_model->insert('tags', $tag_data)) {
                $this->session->set_flashdata('msg_success', 'Product Tag added successfully.');
                redirect('backend/tags');
            } else {
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/tags');
            }
        }
        $data['template'] = "backend/tags/add";
        $this->load->view('templates/backend/layout', $data);
    }

    public function edit($tag_id = '')
    {
        $this->_check_login(); //check login authentication
        if (empty($tag_id)) redirect('backend/tags');
        $this->form_validation->set_rules('tag', 'Tag', 'required');
        $this->form_validation->set_rules('status', 'Status', 'trim');
        $this->form_validation->set_rules('category_id', 'Category', 'trim');
        $this->form_validation->set_rules('description', 'Description', 'trim');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE) {
            $type_data = array(
                'tag' => $this->input->post('tag'),
                'slug' => url_title($this->input->post('tag'), '-', TRUE),
                'description' => $this->input->post('description'),
                'category' => $this->input->post('category_id'),
                'status' => $this->input->post('status'),
                'modified' => date('Y-m-d h:i:s')
            );
            if ($this->tags_model->update('tags', $type_data, array('id' => $tag_id))) {
                $this->session->set_flashdata('msg_success', 'Item Type updated successfully.');
                redirect('backend/tags');
            }else{
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
                redirect('backend/tags');
            }
        }
        $data['tags'] = $this->tags_model->get_row('tags', array('id' => $tag_id));

         $data['template'] = "backend/tags/edit";
        $this->load->view('templates/backend/layout', $data);
    }

    public function delete($tag_id = '')
    {
        $this->_check_login(); //check login authentication
        if (empty($tag_id)) redirect('backend/tags');
        if ($this->tags_model->delete('tags', array('id' => $tag_id))) {
            $this->session->set_flashdata('msg_success', 'Item Type deleted successfully.');
            redirect('backend/tags');
        } else {
            $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
            redirect('backend/tags');
        }
    }
}