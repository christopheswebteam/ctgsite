<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
		
        $this->load->model('blog_model');
    }

    public function index()
    {
        $this->posts();
    }

    private function _check_login()
	{
		if (superadmin_logged_in()===FALSE) {
			redirect('superadmin/login');
		}
	}

    public function posts($offset = 0)
    {
        $this->_check_login(); //check login authentication
		
        $per_page = 10;
		
		$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['base_url'] = base_url().'backend/blog/posts/';
        $config['total_rows'] = $this->blog_model->count_posts();
        $config['per_page'] = $per_page;
        $config['uri_segment'] = 4;
		
        $this->pagination->initialize($config);
		
        $data['pagination'] = $this->pagination->create_links();
		$data['offset'] = $offset;
        $data['posts'] = $this->blog_model->get_posts($per_page, $offset);
        $data['template'] = "backend/blog/index";
		
        $this->load->view('templates/backend/layout', $data);
    }

    public function add()
    {
        $this->_check_login();
		
        $this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('slug', 'Slug', 'trim|required|strtolower');
        $this->form_validation->set_rules('content', 'Content', 'trim|required');
        $this->form_validation->set_rules('blog_featured_image', '', 'callback_blog_featured_image_check');
		$this->form_validation->set_rules('author', 'Author', 'trim|required|is_natural_no_zero');
		
		$this->form_validation->set_message('is_natural_no_zero', 'The %s field is required.');
		
        if ($this->form_validation->run() == TRUE) {

            if ($this->session->userdata('blog_featured_image')) {
			
                $blog_featured_image = $this->session->userdata('blog_featured_image');
				
                $blog_data['image'] = $blog_featured_image['image'];
                $blog_data['thumb_image'] = $blog_featured_image['thumb_image'];
				
            }
			
			$blog_data['status'] = $this->input->post('status');
            $blog_data['title'] = $this->input->post('title');
            $blog_data['slug'] = strtolower(url_title($this->input->post('slug'), '-', TRUE));
            $blog_data['content'] = $this->input->post('content');
			$blog_data['author'] = $this->input->post('author');

            if ($id = $this->blog_model->add_post($blog_data)) {
			
				if (!empty($_POST['tags'])) {
					
					$input_tags = explode(',', $_POST['tags']);
                    
                    $this->common_model->upsert_tags_and_associate('Post', $id, $input_tags);
	
                }
			
                $this->session->set_flashdata('msg_success', 'Blog post created successfully.');
				
                if ($this->session->userdata('blog_featured_image')) {
				
                    $this->session->unset_userdata('blog_featured_image');
					
                }
				
                redirect('backend/blog');
				
            } else {
			
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
				
                redirect('backend/blog');
				
            }
			
        }

        $tags = "";
		
        if ($all_tags = $this->common_model->get_all_tags()) {
		
            $tags_array = array();
			
            foreach ($all_tags as $tag) {
			
                $tags_array[] = $tag['tag'];
				
            }
			
            $tags = '"' . implode('","', $tags_array) . '"';
        }
		
        $data['tags'] = $tags;
		$data['users_dropdown'] = $this->common_model->users_dropdown();
        $data['template'] = 'backend/blog/add';
		
        $this->load->view('templates/backend/layout', $data);
    }

    public function edit($id = 0, $offset = 0)
    {
        $this->_check_login(); //check login authentication
		
        if (empty($id)) {

			redirect('backend/blog/'.$offset);

		}

        $data['post'] = $this->blog_model->get_post_by_id($id);

        $this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('slug', 'Slug', 'trim|required|strtolower');
        $this->form_validation->set_rules('content', 'Content', 'trim|required');
		$this->form_validation->set_rules('author', 'Author', 'trim|required|is_natural_no_zero');
		
		$this->form_validation->set_message('is_natural_no_zero', 'The %s field is required.');

        if (!empty($_FILES['blog_featured_image']['name'])) {
		
            $this->form_validation->set_rules('blog_featured_image', 'Featured Image', 'callback_blog_featured_image_check');
			
        }
		
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
        if ($this->form_validation->run() == TRUE) {

            if ($this->session->userdata('blog_featured_image')) {
			
                $blog_featured_image = $this->session->userdata('blog_featured_image');
				
                $blog_data['image'] = $blog_featured_image['image'];
                $blog_data['thumb_image'] = $blog_featured_image['thumb_image'];
				
            }
			
            $blog_data['status'] = $this->input->post('status');
            $blog_data['title'] = $this->input->post('title');
            $blog_data['slug'] = strtolower(url_title($this->input->post('slug'), '-', TRUE));
            $blog_data['content'] = $this->input->post('content');
			$blog_data['author'] = $this->input->post('author');
			
            if ($this->blog_model->edit_post_by_id($id, $blog_data)) {

				if (!empty($_POST['tags'])) {
					
					$tags = explode(',', $_POST['tags']);
                    
                    $this->common_model->upsert_tags_and_associate('Post', $id, $tags);
	
                }

                if ($this->session->userdata('blog_featured_image')) {
				
                    $this->session->unset_userdata('blog_featured_image');
					
                    @unlink($data['post']['image']);
                    @unlink($data['post']['thumb_image']);
					
                }

                $this->session->set_flashdata('msg_success', 'Blog post updated successfully.');
				
                redirect('backend/blog/posts/'.$offset);
				
            } else {
			
                $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
				
                redirect('backend/blog/edit/'.$id.'/'.$offset);
				
            }
			
        }
		
		
		$tags = "";
		
        if ($all_tags = $this->common_model->get_all_tags()) {
		
            $tags_array = array();
			
            foreach ($all_tags as $tag) {
			
                $tags_array[] = $tag['tag'];
				
            }
			
            $tags = '"' . implode('","', $tags_array) . '"';
        }
		
        $data['tags'] = $tags;
		$data['users_dropdown'] = $this->common_model->users_dropdown();
        $data['template'] = "backend/blog/edit";
		
        $this->load->view('templates/backend/layout', $data);
    }

    public function delete($id = 0, $offset = 0)
    {
        $this->_check_login();

        if (empty($id)){
		
			redirect('backend/blog/'.$offset);
			
		}

        $data['post'] = $this->blog_model->get_post_by_id($id);

        @unlink($data['post']['image']);
        @unlink($data['post']['thumb_image']);

        if ($this->blog_model->delete_post_by_id($id)) {

            $this->session->set_flashdata('msg_success', 'Blog post deleted successfully.');
			
            redirect('backend/blog/posts/'.$offset);
			
        } else {
		
            $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
			
            redirect('backend/blog/posts/'.$offset);
			
        }
    }

    public function blog_featured_image_check($str)
    {
        if ($this->session->userdata('blog_featured_image')) {
		
            return TRUE;
			
        } else {
		
            $params = array(
                'file_name' => 'blog_featured_image',
                'upload_path' => './assets/uploads/blog/',
                'allowed_types' => 'gif|jpg|png|jpeg',
                'image_resize' => TRUE,
                'source_image' => './assets/uploads/blog/',
                'new_image' => './assets/uploads/blog/thumb/',
                'resize_width' => 220,
                'resize_height' => 200,
                'encrypt_name' => TRUE);

            $upload_file = upload_file($params);
			
            if ($upload_file['STATUS']) {
			
                $this->session->set_userdata('blog_featured_image', array('image' => $params['upload_path'].$upload_file['UPLOAD_DATA']['file_name'], 'thumb_image' => $params['new_image'].$upload_file['UPLOAD_DATA']['file_name']));
                return TRUE;
				
            } else {
			
                $this->form_validation->set_message('blog_featured_image_check', $upload_file['FILE_ERROR']);
                return FALSE;
				
            }
			
        }
		
    }

}