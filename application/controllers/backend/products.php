<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('products_model');
    }

    private function _check_login()
	{
		if (superadmin_logged_in()===FALSE) {
			redirect('superadmin/login');
		}
	}

    public function index($offset = 0)
    {
        $this->_check_login(); //check login authentication

        $per_page = 10;

		$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['base_url'] = base_url().'backend/products/index/';
        $config['total_rows'] = $this->products_model->count_products();
        $config['per_page'] = $per_page;
        $config['uri_segment'] = 4;

        $this->pagination->initialize($config);

        $data['pagination'] = $this->pagination->create_links();
		$data['offset'] = $offset;
        $data['products'] = $this->products_model->get_products($per_page, $offset);
        $data['template'] = "backend/products/index";

        $this->load->view('templates/backend/layout', $data);
    }

    public function add()
    {
        $this->_check_login();

		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('pos_products_id', 'POS ID', 'required|numeric');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('slug', 'Slug', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_rules('tags[]', 'Product Tags', 'required');
		$this->form_validation->set_rules('ingredients', 'Ingredients', 'required');
		$this->form_validation->set_rules('start_date', 'Start Date', 'required');
        $this->form_validation->set_rules('end_date', 'End Date', 'required');
        $this->form_validation->set_rules('sort', 'Default Sort Location ', 'required|numeric');
        $this->form_validation->set_rules('check_inventory', 'Inventory', 'required');
        $this->form_validation->set_rules('additional_information', 'Additional Information', 'required');
        $this->form_validation->set_rules('preparation_instructions', 'Preparation Instructions', 'required');

		if (!empty($_FILES['nutrition_facts']['name'])) {

			$this->form_validation->set_rules('nutrition_facts', 'Nutrition Facts', 'callback_nutrition_facts_check');

		}

        if (!empty($_POST['size'])){

            $this->form_validation->set_rules('size[]', 'Product Size', 'required');

        } else {

            $this->form_validation->set_rules('custom_price', 'Price', 'required');
            $this->form_validation->set_rules('custom_weight', 'weight', 'required');

        }

        if (!empty($_POST['size'])) {

            if (in_array('small', $_POST['size'], TRUE)) {

                $this->form_validation->set_rules('small_price', 'Small Price', 'required');
                $this->form_validation->set_rules('small_weight', 'Small Weight', 'required');
				$this->form_validation->set_rules('small_weight_measure', 'Small Measure', 'required');

            }

            if (in_array('medium', $_POST['size'], TRUE)) {

                $this->form_validation->set_rules('medium_price', 'Medium Price', 'required');
                $this->form_validation->set_rules('medium_weight', 'Medium Weight', 'required');
				$this->form_validation->set_rules('medium_weight_measure', 'Medium Measure', 'required');

            }

            if (in_array('large', $_POST['size'], TRUE)) {

                $this->form_validation->set_rules('large_price', 'Large Price', 'required');
                $this->form_validation->set_rules('large_weight', 'Large Weight', 'required');
				$this->form_validation->set_rules('large_weight_measure', 'Large Measure', 'required');

            }

            if (in_array('family', $_POST['size'], TRUE)) {

                $this->form_validation->set_rules('family_price', 'Family Price', 'required');
                $this->form_validation->set_rules('family_weight', 'Family Weight', 'required');
				$this->form_validation->set_rules('family_weight_measure', 'Family Measure', 'required');

            }

        }

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == TRUE) {

            $product_data = array(
				'status' => $this->input->post('status'),
				'pos_products_id' => $this->input->post('pos_products_id'),
                'name' => $this->input->post('name'),
                'slug' => strtolower(url_title($this->input->post('slug'))),
                'description' => $this->input->post('description'),
                'start_date' => date('Y-m-d', strtotime($this->input->post('start_date'))),
                'end_date' => date('Y-m-d', strtotime($this->input->post('end_date'))),
				'sort' => $this->input->post('sort'),
                'check_inventory' => $this->input->post('check_inventory'),
                'additional_information' => $this->input->post('additional_information'),
                'preparation_instructions' => $this->input->post('preparation_instructions'),
            );

            if ($this->session->userdata('nutrition_facts')) {

                $nutrition_facts = $this->session->userdata('nutrition_facts');

                $product_data['nutrition_facts_file'] = $nutrition_facts['image'];
                $product_data['nutrition_facts_file_thumb'] = $nutrition_facts['thumb_image'];

            }

            if ($id = $this->products_model->insert_product($product_data)) {

                if (!empty($_POST['custom_price'])) {

                    $this->products_model->delete_products_portions_by_products_id($id);

                    $products_portions_data = array(
                        'portions_id' => 5,
                        'products_id' => $id,
                        'price' => $this->input->post('custom_price'),
                        'weight' => $this->input->post('custom_weight'),
                        'is_custom' => 1
                    );

                    $this->products_model->insert_products_portions($products_portions_data);

                }

                if (!empty($_POST['size'])) {

					// Clear out all old portions so we can simply insert the new ones.
                    $this->products_model->delete_products_portions_by_products_id($id);

                    $count = count($_POST['size']);

                    for ($i = 0; $i < $count; $i++) {

                        $portions_id = $this->products_model->get_portions_id_by_name($_POST['size'][$i]);

						if ($portions_id) {

							$products_portions_data = array(
								'products_id' => $id,
								'portions_id' => $portions_id,
								'price' => $this->input->post($_POST['size'][$i].'_price'),
								'weight' => $this->input->post($_POST['size'][$i].'_weight'),
								'weight_measure' => $this->input->post($_POST['size'][$i].'_weight_measure')
							);

							$this->products_model->insert_products_portions($products_portions_data);

						}

                    }

                }

                if (!empty($_POST['tags'])) {

					$input_tags = $_POST['tags'];

                    $this->common_model->upsert_tags_and_associate('Product', $id, $input_tags);

                }

                if (!empty($_POST['ingredients'])) {

					$input_ingredients = explode(',', $_POST['ingredients']);

					$this->products_model->upsert_ingredients_and_associate($id, $input_ingredients);

                }

                if ($this->session->userdata('products_photos')) {

                    $products_photos = $this->session->userdata('products_photos');

                    $count = count($products_photos);

                    for ($i = 0; $i < $count; $i++) {

                        $photo_path_array = explode('/', $products_photos[$i]['path']);

                        if (!empty($_POST['title_'.$i])) {

                            $title = $_POST['title_'.$i];

                        } else {

                            $title = str_replace('.jpg', '', $photo_path_array[4]);

                        }

						if (!empty($_POST['alt_text_'.$i])) {

                            $alt_text = $_POST['alt_text_'.$i];

                        } else {

                            $alt_text = FALSE;

                        }

                        if (isset($_POST['random_background_'.$i])) {

                            $random_background = $_POST['random_background_'.$i];

                        } else {

							$random_background = 0;

						}

                        $products_photos_data = array(
                            'products_id' => $id,
                            'title' => $title,
							'alt_text' => $alt_text,
                            'random_background' => $random_background,
                            'path' => $products_photos[$i]['path'],
                            'thumbnail_path' => $products_photos[$i]['thumbnail_path']
                        );

                        $this->products_model->insert('products_photos', $products_photos_data);

                    }

                    $this->session->unset_userdata('products_photos');

                } else {

                    if (!empty($data['product_photos'])) {

                        $i = 1;

                        foreach ($data['product_photos'] as $photo) {

							$products_photos_db_data = array(
								'title' => $_POST['title_'.$photo['id'].'_'.$i],
								'alt_text' => $_POST['alt_text_'.$photo['id'].'_'.$i],
								'random_background' => $_POST['random_background_'.$photo['id'].'_'.$i]
							);

							$this->products_model->update_product_photo_by_id($photo['id'], $products_photos_db_data);

                            $i++;

                        }

                    }

                }

                $this->session->set_flashdata('msg_success', 'Product Updated successfully.');

                $this->session->unset_userdata('nutrition_facts');

                redirect('backend/products');
            }

        }

		// Delete all product photos stored in the session.
		// We assume that either the form submission failed or the form hasn't been processed yet.
		// Either way we want a clean session to work with for the next request.
        if ($this->session->userdata('products_photos')) {

            $products_photos = $this->session->userdata('products_photos');

            $count = count($products_photos);

            for ($i = 0; $i < $count; $i++) {

                $path = $products_photos[$i]['path'];
                $thumbnail_path = $products_photos[$i]['thumbnail_path'];

                @unlink($path);
                @unlink($thumbnail_path);

            }

            $this->session->unset_userdata('products_photos');

            $this->session->set_flashdata('msg_error', 'Please Reupload image files.');

        }

		$ingredients = "";

        if ($all_ingredients = $this->common_model->get_all_ingredients()) {

            $ingredients_array = array();

            foreach ($all_ingredients as $ingredient) {

                $ingredients_array[] = $ingredient['name'];

            }

            $ingredients = '"' . implode('","', $ingredients_array) . '"';
        }

        $data['ingredients'] = $ingredients;

        $data['portions'] = $this->products_model->get_portions();

        $data['feature_tags'] = $this->products_model->get_tags_by_category(1);
        $data['dietary_tags'] = $this->products_model->get_tags_by_category(2);
        $data['protein_tags'] = $this->products_model->get_tags_by_category(3);
        $data['other_tags'] = $this->products_model->get_tags_by_category(4);

        $data['template'] = "backend/products/product_add";

        $this->load->view('templates/backend/layout', $data);
    }

    public function edit($id = FALSE)
    {
        $this->_check_login();

        if (empty($id)) {
			redirect('backend/products');
		}

        $data['product'] = $this->products_model->get_product_by_id($id);
        $data['product_portions'] = $this->products_model->get_product_portions_by_id($id);
		$data['product_custom_portions'] = $this->products_model->get_product_portions_by_id($id, TRUE);
		$data['product_ingredients'] = $this->products_model->get_product_ingredients_by_id($id);
		$data['product_photos'] = $this->products_model->get_product_photos_by_id($id);
		$data['product_tags'] = $this->products_model->get_product_tags_by_id($id);

		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('pos_products_id', 'POS ID', 'required|numeric');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('slug', 'Slug', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_rules('tags[]', 'Product Tags', 'required');
		$this->form_validation->set_rules('ingredients', 'Ingredients', 'required');
		$this->form_validation->set_rules('start_date', 'Start Date', 'required');
        $this->form_validation->set_rules('end_date', 'End Date', 'required');
        $this->form_validation->set_rules('sort', 'Default Sort Location ', 'required|numeric');
        $this->form_validation->set_rules('check_inventory', 'Inventory', 'required');
        $this->form_validation->set_rules('additional_information', 'Additional Information', 'required');
        $this->form_validation->set_rules('preparation_instructions', 'Preparation Instructions', 'required');

		if (!empty($_FILES['nutrition_facts']['name'])) {

			$this->form_validation->set_rules('nutrition_facts', 'Nutrition Facts', 'callback_nutrition_facts_check');

		}

        if (!empty($_POST['size'])){

            $this->form_validation->set_rules('size[]', 'Product Size', 'required');

        } else {

            $this->form_validation->set_rules('custom_price', 'Price', 'required');
            $this->form_validation->set_rules('custom_weight', 'weight', 'required');

        }

        if (!empty($_POST['size'])) {

            if (in_array('small', $_POST['size'], TRUE)) {

                $this->form_validation->set_rules('small_price', 'Small Price', 'required');
                $this->form_validation->set_rules('small_weight', 'Small Weight', 'required');
				$this->form_validation->set_rules('small_weight_measure', 'Small Measure', 'required');

            }

            if (in_array('medium', $_POST['size'], TRUE)) {

                $this->form_validation->set_rules('medium_price', 'Medium Price', 'required');
                $this->form_validation->set_rules('medium_weight', 'Medium Weight', 'required');
				$this->form_validation->set_rules('medium_weight_measure', 'Medium Measure', 'required');

            }

            if (in_array('large', $_POST['size'], TRUE)) {

                $this->form_validation->set_rules('large_price', 'Large Price', 'required');
                $this->form_validation->set_rules('large_weight', 'Large Weight', 'required');
				$this->form_validation->set_rules('large_weight_measure', 'Large Measure', 'required');

            }

            if (in_array('family', $_POST['size'], TRUE)) {

                $this->form_validation->set_rules('family_price', 'Family Price', 'required');
                $this->form_validation->set_rules('family_weight', 'Family Weight', 'required');
				$this->form_validation->set_rules('family_weight_measure', 'Family Measure', 'required');

            }

        }

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == TRUE) {

            $product_data = array(
				'status' => $this->input->post('status'),
				'pos_products_id' => $this->input->post('pos_products_id'),
                'name' => $this->input->post('name'),
                'slug' => strtolower(url_title($this->input->post('slug'))),
                'description' => $this->input->post('description'),
                'start_date' => date('Y-m-d', strtotime($this->input->post('start_date'))),
                'end_date' => date('Y-m-d', strtotime($this->input->post('end_date'))),
				'sort' => $this->input->post('sort'),
                'check_inventory' => $this->input->post('check_inventory'),
                'additional_information' => $this->input->post('additional_information'),
                'preparation_instructions' => $this->input->post('preparation_instructions'),
            );

            if ($this->session->userdata('nutrition_facts')) {

                $nutrition_facts = $this->session->userdata('nutrition_facts');

                $product_data['nutrition_facts_file'] = $nutrition_facts['image'];
                $product_data['nutrition_facts_file_thumb'] = $nutrition_facts['thumb_image'];

                if (!empty($data['product']['nutrition_facts_file'])) {

                    $old_file = $data['product']['nutrition_facts_file'];
                    $old_file_thumb = $data['product']['nutrition_facts_file_thumb'];

                    @unlink($old_file);
                    @unlink($old_file_thumb);

                }

            }

            if ($this->products_model->edit_product_by_id($id, $product_data)) {

                if (!empty($_POST['custom_price'])) {

                    $this->products_model->delete_products_portions_by_products_id($id);

                    $products_portions_data = array(
                        'portions_id' => 5,
                        'products_id' => $id,
                        'price' => $this->input->post('custom_price'),
                        'weight' => $this->input->post('custom_weight'),
                        'is_custom' => 1
                    );

                    $this->products_model->insert_products_portions($products_portions_data);

                }

                if (!empty($_POST['size'])) {

					// Clear out all old portions so we can simply insert the new ones.
                    $this->products_model->delete_products_portions_by_products_id($id);

                    $count = count($_POST['size']);

                    for ($i = 0; $i < $count; $i++) {

                        $portions_id = $this->products_model->get_portions_id_by_name($_POST['size'][$i]);

						if ($portions_id) {

							$products_portions_data = array(
								'products_id' => $id,
								'portions_id' => $portions_id,
								'price' => $this->input->post($_POST['size'][$i].'_price'),
								'weight' => $this->input->post($_POST['size'][$i].'_weight'),
								'weight_measure' => $this->input->post($_POST['size'][$i].'_weight_measure')
							);

							$this->products_model->insert_products_portions($products_portions_data);

						}

                    }

                }

                if (!empty($_POST['tags'])) {

					$input_tags = $_POST['tags'];

                    $this->common_model->upsert_tags_and_associate('Product', $id, $input_tags);

                }

                if (!empty($_POST['ingredients'])) {

					$input_ingredients = explode(',', $_POST['ingredients']);

					$this->products_model->upsert_ingredients_and_associate($id, $input_ingredients);

                }

                if ($this->session->userdata('products_photos')) {

                    $products_photos = $this->session->userdata('products_photos');

                    $count = count($products_photos);

                    for ($i = 0; $i < $count; $i++) {

                        $photo_path_array = explode('/', $products_photos[$i]['path']);

                        if (!empty($_POST['title_'.$i])) {

                            $title = $_POST['title_'.$i];

                        } else {

                            $title = str_replace('.jpg', '', $photo_path_array[4]);

                        }

						if (!empty($_POST['alt_text_'.$i])) {

                            $alt_text = $_POST['alt_text_'.$i];

                        } else {

                            $alt_text = FALSE;

                        }

                        if (isset($_POST['random_background_'.$i])) {

                            $random_background = $_POST['random_background_'.$i];

                        } else {

							$random_background = 0;

						}

                        $products_photos_data = array(
                            'products_id' => $id,
                            'title' => $title,
							'alt_text' => $alt_text,
                            'random_background' => $random_background,
                            'path' => $products_photos[$i]['path'],
                            'thumbnail_path' => $products_photos[$i]['thumbnail_path']
                        );

                        $this->products_model->insert('products_photos', $products_photos_data);

                    }

                    $this->session->unset_userdata('products_photos');

                } else {

                    if (!empty($data['product_photos'])) {

                        $i = 1;

                        foreach ($data['product_photos'] as $photo) {

							$products_photos_db_data = array(
								'title' => $_POST['title_'.$photo['id'].'_'.$i],
								'alt_text' => $_POST['alt_text_'.$photo['id'].'_'.$i],
								'random_background' => $_POST['random_background_'.$photo['id'].'_'.$i]
							);

							$this->products_model->update_product_photo_by_id($photo['id'], $products_photos_db_data);

                            $i++;

                        }

                    }

                }

                $this->session->set_flashdata('msg_success', 'Product Updated successfully.');

                $this->session->unset_userdata('nutrition_facts');

                redirect('backend/products');
            }

        }

		// Delete all product photos stored in the session.
		// We assume that either the form submission failed or the form hasn't been processed yet.
		// Either way we want a clean session to work with for the next request.
        if ($this->session->userdata('products_photos')) {

            $products_photos = $this->session->userdata('products_photos');

            $count = count($products_photos);

            for ($i = 0; $i < $count; $i++) {

                $path = $products_photos[$i]['path'];
                $thumbnail_path = $products_photos[$i]['thumbnail_path'];

                @unlink($path);
                @unlink($thumbnail_path);

            }

            $this->session->unset_userdata('products_photos');

            $this->session->set_flashdata('msg_error', 'Please Reupload image files.');

        }

		$ingredients = "";

        if ($all_ingredients = $this->common_model->get_all_ingredients()) {

            $ingredients_array = array();

            foreach ($all_ingredients as $ingredient) {

                $ingredients_array[] = $ingredient['name'];

            }

            $ingredients = '"' . implode('","', $ingredients_array) . '"';
        }

        $data['ingredients'] = $ingredients;

        $data['portions'] = $this->products_model->get_portions();

        $data['feature_tags'] = $this->products_model->get_tags_by_category(1);
        $data['dietary_tags'] = $this->products_model->get_tags_by_category(2);
        $data['protein_tags'] = $this->products_model->get_tags_by_category(3);
        $data['other_tags'] = $this->products_model->get_tags_by_category(4);

        $data['template'] = "backend/products/product_edit";

        $this->load->view('templates/backend/layout', $data);
    }

    public function nutrition_facts_check($str)
    {
        if ($this->session->userdata('nutrition_facts')) {

            return TRUE;

        } else {

            $param = array(
                'file_name' => 'nutrition_facts',
                'upload_path' => './assets/uploads/products/',
                'allowed_types' => 'gif|jpg|png|jpeg',
                'image_resize' => TRUE,
                'source_image' => './assets/uploads/products/',
                'resize_width' => 350,
                'resize_height' => 300,
                'new_image' => './assets/uploads/products/thumbnail/',
                'encrypt_name' => TRUE
			);

            $upload_file = upload_file($param);

            if ($upload_file['STATUS']) {

                $this->session->set_userdata('nutrition_facts', array('image' => $param['upload_path'].$upload_file['UPLOAD_DATA']['file_name'], 'thumb_image' => $param['new_image'].$upload_file['UPLOAD_DATA']['file_name']));
				return TRUE;

            } else {

                $this->form_validation->set_message('nutrition_facts_check', $upload_file['FILE_ERROR']);
                return FALSE;

            }

        }
    }

    function ajax_photo_upload()
    {
        ini_set('memory_limit', '256M');
        if (!empty($_FILES)) {

            $products_photos = array();

            if (!empty($_FILES['file']['name'])) {

                $param = array(
                    'file_name' => 'file',
                    'upload_path' => './assets/uploads/products/',
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    'image_resize' => TRUE,
                    'source_image' => './assets/uploads/products/',
                    'new_image' => './assets/uploads/products/thumbnail/',
                    'resize_width' => 350,
                    'resize_height' => 300,
                    'encrypt_name' => TRUE
                );

                $upload_file = upload_file($param);

                if ($upload_file['STATUS']) {

                    if ($this->session->userdata('products_photos')) {

                        $previous_products_photos = $this->session->userdata('products_photos');

                        $count = count($previous_products_photos);

                        for ($i = 0; $i < $count; $i++) {

                            $j = $count - 1;

                            $products_photos[$i] = array(
                                'path' => $previous_products_photos[$i]['path'],
                                'thumbnail_path' => $previous_products_photos[$i]['thumbnail_path']
                            );

                            if ($i == $j) {

                                $h = $i + 1;

                                $products_photos[$h] = array(
									'path' => $param['upload_path'].$upload_file['UPLOAD_DATA']['file_name'],
									'thumbnail_path' => $param['new_image'].$upload_file['UPLOAD_DATA']['file_name']
								);

                            }

                        }

                        $this->session->set_userdata('products_photos', $products_photos);

                    } else {

                        $products_photos[0] = array(
							'path' => $param['upload_path'].$upload_file['UPLOAD_DATA']['file_name'],
							'thumbnail_path' => $param['new_image'].$upload_file['UPLOAD_DATA']['file_name']
						);

                        $this->session->set_userdata('products_photos', $products_photos);

                    }

                    die('{"jsonrpc" : "2.0", "result" : "OK", "id" : "1"}');

                } else {

                    die('{"jsonrpc" : "2.0", "result" : "'.$upload_file['FILE_ERROR'].'", "id" : "1"}');

                }

            }

        }

    }

	/*
    public function remove_image()
    {
        $arrayName = array();
        $image_url = 'assets/uploads/products/';
        if ($_POST) {
            if ($_POST['file'] != '') {
                if ($this->session->userdata('products_photos')) {
                    $images = $this->session->userdata('products_photos');
                    $count = count($images);
                    for ($i = 0; $i < $count; $i++) {
                        if ($images[$i]['image'] != $_POST['file']) {
                            $arrayName[$i] = array(
                                'image' => $images[$i]['image_name'],
                                'thumb_image' => $images[$i]['thumb_image']
                            );
                        }
                    }
                    $this->session->set_userdata('products_photos', $arrayName);
                }
                $new_file = $image_url . $_POST['file'];
                $thumb_file = $image_url . 'thumbnail/' . $_POST['file'];
                @unlink($new_file);
                @unlink($thumb_file);
                echo'success';
            }
        }
    }
	*/

    public function ajax_new_product_photo_line()
    {
        $row = "";

        if ($_POST) {

            $products_photos = $this->session->userdata('products_photos');

            echo $count = count($products_photos);

            for ($i = 0; $i < $count; $i++) {

				$path = explode('/', $products_photos[$i]['path']);
                $title = str_replace('.jpg', '', $path[4]);

                $row .= "<tr>";
                $row .= "<td>";
                $row .= "<a href=\"".base_url($products_photos[$i]['thumbnail_path'])."\" class=\"fancybox-button\" data-rel=\"fancybox-button\"><img class=\"img-responsive\" src=\"".base_url($products_photos[$i]['thumbnail_path'])."\" alt=\"\"></a>";
                $row .= "</td>";
                $row .= "<td>";
                $row .= "<div><input class=\"form-control\" type=\"text\" name=\"title_".$i."\" value=\"".$title."\" /></div>";
                $row .= "</td>";
				$row .= "<td>";
                $row .= "<div><input class=\"form-control\" type=\"text\" name=\"alt_text_".$i."\" value=\"\" /></div>";
                $row .= "</td>";
                $row .= "<td>";
				$row .= "<div class=\"radio-list\">";
                $row .= "<label class=\"radio-inline\">";
                $row .= "<input type=\"radio\" name=\"random_background_".$i."\" id=\"random_background\" value=\"1\" />Yes";
				$row .= "</label>";
                $row .= "<label class=\"radio-inline\">";
                $row .= "<input type=\"radio\" name=\"random_background_".$i."\" id=\"random_background\" value=\"0\" checked />No";
				$row .= "</label>";
                $row .= "</div>";
                $row .= "</td>";
                $row .= "<td>";
                $row .= "</td>";
                $row .= "</tr>";

            }

            echo $row;

        }

    }

    public function delete($products_id = '')
    {
        $this->_check_login(); //check login authentication
        if (empty($products_id)) redirect(base_url() . 'backend/products');
        if ($products = $this->products_model->get_row('products', array('id' => $products_id))) {
            $products_photos = $this->products_model->get_result('products_photos', array('products_id' => $products_id));
            if (!empty($products->nutritional_info_file)) {
                $new_file_nutri = $products->nutritional_info_file;
                $thumb_file_nutri = $products->nutritional_info_file_thumb;
                @unlink($new_file_nutri);
                @unlink($thumb_file_nutri);
            }
            if (!empty($products_photos)):
                foreach ($products_photos as $value) {
                    $new_file = $value->path;
                    $thumb_file = $value->thumbnail_path;
                    @unlink($new_file);
                    @unlink($thumb_file);
                }
            endif;

            $this->products_model->delete('products_ingredients', array('products_id' => $products_id));
            $this->products_model->delete('products_portions', array('products_id' => $products_id));
            $this->products_model->delete('tags_association', array('key' => $products_id, 'type' => 'product'));
            $this->products_model->delete('products_photos', array('products_id' => $products_id));
            $this->products_model->delete('products', array('id' => $products_id));
        }
        $this->session->set_flashdata('msg_success', 'Product deleted successfully.');
        redirect(base_url() . 'backend/products');
    }

    public function ajax_remove_product_photo()
    {
        $row = "";

        if ($_POST) {

            $products_id = (int)$_POST['products_id'];
			$products_photos_id = (int)$_POST['products_photos_id'];

			$previous_products_photos = $this->products_model->get_product_photos_by_id($products_id);

            if ($previous_products_photos) {

                $products_photos = array();

                $count = count($previous_products_photos);

                for ($i = 0; $i < $count; $i++) {

                    if ($products_photos_id == $previous_products_photos[$i]['id']) {

						$this->products_model->delete_product_photo_by_id($products_photos_id);

                        $path = $previous_products_photos[$i]['path'];
                        $thumbnail_path = $previous_products_photos[$i]['thumbnail_path'];

                        @unlink($path);
                        @unlink($thumbnail_path);

                    } else {

						$products_photos[] = array(
							'path' => $previous_products_photos[$i]['path'],
							'thumbnail_path' => $previous_products_photos[$i]['thumbnail_path']
						);

                    }

                }

            }

            if (!empty($products_photos)) {

                $count = count($products_photos);

                for ($i = 0; $i < $count; $i++) {

					$path = explode('/', $products_photos[$i]['path']);
					$title = str_replace('.jpg', '', $path[4]);

                    $row .= "<tr>";
					$row .= "<td>";
					$row .= "<a href=\"".base_url($products_photos[$i]['thumbnail_path'])."\" class=\"fancybox-button\" data-rel=\"fancybox-button\"><img class=\"img-responsive\" src=\"".base_url($products_photos[$i]['thumbnail_path'])."\" alt=\"\"></a>";
					$row .= "</td>";
					$row .= "<td>";
					$row .= "<div><input class=\"form-control\" type=\"text\" name=\"title_".$i."\" value=\"".$title."\" /></div>";
					$row .= "</td>";
					$row .= "<td>";
					$row .= "<div><input class=\"form-control\" type=\"text\" name=\"alt_text_".$i."\" value=\"\" /></div>";
					$row .= "</td>";
					$row .= "<td>";
					$row .= "<div class=\"radio-list\">";
					$row .= "<label class=\"radio-inline\">";
					$row .= "<input type=\"radio\" name=\"random_background_".$i."\" id=\"random_background\" value=\"1\" />Yes";
					$row .= "</label>";
					$row .= "<label class=\"radio-inline\">";
					$row .= "<input type=\"radio\" name=\"random_background_".$i."\" id=\"random_background\" value=\"0\" checked />No";
					$row .= "</label>";
					$row .= "</div>";
					$row .= "</td>";
					$row .= "<td>";
					$row .= "<a href=\"javascript:void(0)\" onclick=\"remove_product_photo(".$i.");\" class=\"btn default btn-sm\">";
					$row .= "<i class=\"fa fa-times\"></i> Remove";
					$row .= "</a>";
					$row .= "</td>";
					$row .= "</tr>";

                }

                echo $row;

            } else {

                echo $row = "<tr><td colspan=\"5\">No Photos Found</td></tr>";

            }
        }
    }

    /* Manage Items */

    public function ingredients($offset = 0)
    {
        $this->_check_login(); //check login authentication
        $per_page = 25;
        $data['offset'] = $offset;
        $data['ingredients'] = $this->products_model->ingredients($offset, $per_page);
        $config = backend_pagination();
        $config['base_url'] = base_url() . 'backend/products/ingredients/';
        $config['total_rows'] = $this->products_model->ingredients(0, 0);
        $config['per_page'] = $per_page;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['template'] = 'backend/products/ingredients';
        $this->load->view('templates/backend/layout', $data);
    }

    public function ingredient_add($value = '')
    {
        $this->_check_login(); //check login authentication
        $this->form_validation->set_rules('name', 'Ingredient Name', 'required');
        if ($this->form_validation->run() == TRUE) {
            $ingred_data = array(
                'name' => $this->input->post('name'),
                'slug' => url_title($this->input->post('name'), '-', TRUE)
            );
            if ($this->products_model->insert('ingredients', $ingred_data)) {
                $this->session->set_flashdata('msg_success', 'Ingredient added successfully.');
                redirect('backend/products/ingredients');
            }
        }
        $data['template'] = 'backend/products/ingredient_add';
        $this->load->view('templates/backend/layout', $data);
    }

    public function ingredient_edit($ingred_id = '')
    {
        $this->_check_login(); //check login authentication
        if (empty($ingred_id)) redirect(base_url() . 'backend/products/ingredients');
        $data['ingredient'] = $this->products_model->get_row('ingredients', array('id' => $ingred_id));
        $this->form_validation->set_rules('name', 'Ingredient Name', 'required');
        if ($this->form_validation->run() == TRUE) {
            $ingred_data = array(
                'name' => $this->input->post('name'),
                'slug' => url_title($this->input->post('name'), '-', TRUE)
            );
            if ($this->products_model->update('ingredients', $ingred_data, array('id' => $ingred_id))) {
                $this->session->set_flashdata('msg_success', 'Ingredient added successfully.');
                redirect('backend/products/ingredients');
            }
        }
        $data['template'] = 'backend/products/ingredient_edit';
        $this->load->view('templates/backend/layout', $data);
    }

    public function ingredient_delete($ingred_id = '')
    {
        $this->_check_login(); //check login authentication
        if (empty($ingred_id)) redirect('backend/products/ingredients');
        if ($this->products_model->delete('ingredients', array('id' => $ingred_id))) {
            $this->session->set_flashdata('msg_success', 'Ingredient deleted successfully.');
            redirect('backend/products/ingredients');
        } else {
            $this->session->set_flashdata('msg_error', 'Failed, Please try again.');
            redirect('backend/products/ingredients');
        }
    }

}
